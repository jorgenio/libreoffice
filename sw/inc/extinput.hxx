/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _EXTINPUT_HXX
#define _EXTINPUT_HXX

#include <pam.hxx>
#include <i18npool/lang.h>
#include <vector>

class CommandExtTextInputData;

class SwExtTextInput : public SwPaM
{
    std::vector<sal_uInt16> aAttrs;
    String sOverwriteText;
    sal_Bool bInsText : 1;
    sal_Bool bIsOverwriteCursor : 1;
    LanguageType eInputLanguage;
public:
    SwExtTextInput( const SwPaM& rPam, Ring* pRing = 0 );
    virtual ~SwExtTextInput();

    void SetInputData( const CommandExtTextInputData& rData );
    const std::vector<sal_uInt16>& GetAttrs() const { return aAttrs; }
    void SetInsText( sal_Bool bFlag )       { bInsText = bFlag; }
    sal_Bool IsOverwriteCursor() const      { return bIsOverwriteCursor; }
    void SetOverwriteCursor( sal_Bool bFlag );
    void SetLanguage(LanguageType eSet) { eInputLanguage = eSet;}
};

#endif  //_EXTINPUT_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
