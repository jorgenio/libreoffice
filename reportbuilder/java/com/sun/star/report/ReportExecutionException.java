/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package com.sun.star.report;

public class ReportExecutionException extends Exception
{

    /**
     * Constructs a new exception with <code>null</code> as its detail message. The cause is
     * not initialized, and may subsequently be initialized by a call to {@link
     * #initCause}.
     */
    public ReportExecutionException()
    {
    }

    /**
     * Constructs a new exception with the specified cause and a detail message of
     * <tt>(cause==null ? null : cause.toString())</tt> (which typically contains the class
     * and detail message of <tt>cause</tt>). This constructor is useful for exceptions that
     * are little more than wrappers for other throwables (for example, {@link
     * java.security.PrivilegedActionException}).
     *
     * @param cause the cause (which is saved for later retrieval by the {@link #getCause()}
     *              method).  (A <tt>null</tt> value is permitted, and indicates that the
     *              cause is nonexistent or unknown.)
     * @since 1.4
     */
    public ReportExecutionException(Throwable cause)
    {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message.  The cause is not
     * initialized, and may subsequently be initialized by a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for later retrieval by
     *                the {@link #getMessage()} method.
     */
    public ReportExecutionException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.  <p>Note that
     * the detail message associated with <code>cause</code> is <i>not</i> automatically
     * incorporated in this exception's detail message.
     *
     * @param message the detail message (which is saved for later retrieval by the {@link
     *                #getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the {@link
     *                #getCause()} method).  (A <tt>null</tt> value is permitted, and
     *                indicates that the cause is nonexistent or unknown.)
     * @since 1.4
     */
    public ReportExecutionException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
