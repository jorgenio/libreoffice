/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package com.sun.star.report;

/**
 *
 * @author Ocke Janssen
 */
public class OfficeToken
{

    public static final String GRAPHIC = "graphic";
    public static final String GRAPHICS = "Graphics";
    public static final String GRAPHIC_PROPERTIES = "graphic-properties";
    public static final String PARAGRAPH = "paragraph";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String FRAME = "frame";
    public static final String STYLE_NAME = "style-name";
    public static final String BACKGROUND_COLOR = "background-color";
    public static final String COVERED_TABLE_CELL = "covered-table-cell";
    public static final String TABLE = "table";
    public static final String TABLE_COLUMN = "table-column";
    public static final String TABLE_COLUMNS = "table-columns";
    public static final String TABLE_HEADER_COLUMNS = "table-header-columns";
    public static final String TABLE_HEADER_ROWS = "table-header-rows";
    public static final String TABLE_ROWS = "table-rows";
    public static final String TABLE_ROW = "table-row";
    public static final String TABLE_CELL = "table-cell";
    public static final String P = "p";
    public static final String OBJECT_OLE = "object-ole";
    public static final String IMAGE = "image";
    public static final String IMAGE_DATA = "image-data";
    public static final String PRESERVE_IRI = "preserve-IRI";
    public static final String SCALE = "scale";
    public static final String NAME = "name";
    public static final String SHAPES = "shapes";
    public static final String ISOTROPIC = "isotropic";
    public static final String ANISOTROPIC = "anisotropic";
    public static final String NONE = "none";
}
