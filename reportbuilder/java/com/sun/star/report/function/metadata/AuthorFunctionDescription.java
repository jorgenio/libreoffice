/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package com.sun.star.report.function.metadata;

import org.pentaho.reporting.libraries.formula.function.AbstractFunctionDescription;
import org.pentaho.reporting.libraries.formula.function.FunctionCategory;
import org.pentaho.reporting.libraries.formula.typing.Type;
import org.pentaho.reporting.libraries.formula.typing.coretypes.TextType;

/**
 *
 * @author Ocke Janssen
 */
public class AuthorFunctionDescription extends AbstractFunctionDescription
{

    public AuthorFunctionDescription()
    {
        super("AUTHOR", "com.sun.star.report.function.metadata.Author-Function");
    }

    public FunctionCategory getCategory()
    {
        return MetaDataFunctionCategory.CATEGORY;
    }

    public int getParameterCount()
    {
        return 0;
    }

    public Type getParameterType(final int position)
    {
        return null;
    }

    public Type getValueType()
    {
        return TextType.TYPE;
    }

    public boolean isParameterMandatory(final int position)
    {
        return false;
    }
}
