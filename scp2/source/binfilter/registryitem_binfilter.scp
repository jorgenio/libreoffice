/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "macros.inc"

RegistryItem gid_Regitem_Sdw_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdw";
    Name = "Content Type";
    Value = "application/vnd.stardivision.writer";
End

RegistryItem gid_Regitem_Sdw_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.writer";
    Name = "Extension";
    Value = ".sdw";
End

RegistryItem gid_Regitem_Sdw_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-starwriter";
    Name = "Extension";
    Value = ".sdw";
End

RegistryItem gid_Regitem_Sgl_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sgl";
    Name = "Content Type";
    Value = "application/vnd.stardivision.writer-global";
End

RegistryItem gid_Regitem_Sgl_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.writer-global";
    Name = "Extension";
    Value = ".sgl";
End

RegistryItem gid_Regitem__Sdw
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdw";
    Value = "soffice.StarWriterDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sdw
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sdw";
    Value = "soffice.StarWriterDocument.5";
    Styles = ();
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sgl
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sgl";
    Value = "soffice.StarWriterGlobalDocument.5";
    Styles = ();
End

RegistryItem gid_Regitem__Sgl
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sgl";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "soffice.StarWriterGlobalDocument.5";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterDocument.5";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(SO52_TEXT_OOO)
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\DefaultIcon";
    Value = "<progpath>\program\soffice.bin,1";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell";
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell\new";
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell\new\command";
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell\open\command";
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell\print\command";
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarWriterDocument.5\shell\printto\command";
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(SO51_MASTERDOC_OOO)
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\DefaultIcon";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "<progpath>\program\soffice.bin,9";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell\new";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell\new\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell\open\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell\print\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starwriterglobaldocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarWriterGlobalDocument.5\shell\printto\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

// calc

RegistryItem gid_Regitem_Sdc_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdc";
    Name = "Content Type";
    Value = "application/vnd.stardivision.calc";
End

RegistryItem gid_Regitem_Sdc_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.calc";
    Name = "Extension";
    Value = ".sdc";
End

RegistryItem gid_Regitem_Sdc_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-starcalc";
    Name = "Extension";
    Value = ".sdc";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5";
    REG_VALUE_LNG(SO51_SPREADSHEET_OOO)
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\DefaultIcon";
    Value = "<progpath>\program\soffice.bin,3";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell";
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell\new";
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell\new\command";
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell\open\command";
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell\print\command";
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starcalcdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarCalcDocument.5\shell\printto\command";
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem__Sdc
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdc";
    Value = "soffice.StarCalcDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sdc
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sdc";
    Value = "soffice.StarCalcDocument.5";
    Styles = ();
End

// impress
RegistryItem gid_Regitem_Sdd_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdd";
    Name = "Content Type";
    Value = "application/vnd.stardivision.impress";
End

RegistryItem gid_Regitem_Sdd_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.impress";
    Name = "Extension";
    Value = ".sdd";
End

RegistryItem gid_Regitem_Sdd_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-starimpress";
    Name = "Extension";
    Value = ".sdd";
End

RegistryItem gid_Regitem_Sdp_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdp";
    Name = "Content Type";
    Value = "application/vnd.stardivision.impress-packed";
End

RegistryItem gid_Regitem_Sdp_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.impress-packed";
    Name = "Extension";
    Value = ".sdp";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5";
    REG_VALUE_LNG(SO51_PRESENT_OOO)
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\DefaultIcon";
    Value = "<progpath>\program\soffice.bin,7";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell";
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell\new";
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell\new\command";
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell\open\command";
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell\print\command";
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starimpressdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarImpressDocument.5\shell\printto\command";
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem__Sdd
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdd";
    Value = "soffice.StarImpressDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sdd
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sdd";
    Value = "soffice.StarImpressDocument.5";
    Styles = ();
End

RegistryItem gid_Regitem__Sdp
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sdp";
    Value = "soffice.StarStorageDocument.5";
End

// .sdp no longer supported
// RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sdp
//     ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
//     Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
//     ModuleID = gid_Module_Optional_Binfilter;
//     Name = ".sdp";
//     Value = "soffice.StarStorageDocument.5";
//     Styles = ();
// End

// draw

RegistryItem gid_Regitem_Sda_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sda";
    Name = "Content Type";
    Value = "application/vnd.stardivision.draw";
End

RegistryItem gid_Regitem_Sda_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.draw";
    Name = "Extension";
    Value = ".sda";
End

RegistryItem gid_Regitem_Sda_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-stardraw";
    Name = "Extension";
    Value = ".sda";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5";
    REG_VALUE_LNG(SO51_DRAWING_OOO)
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\DefaultIcon";
    Value = "<progpath>\program\soffice.bin,5";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell";
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell\new";
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell\new\command";
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell\open\command";
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell\print\command";
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Stardrawdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarDrawDocument.5\shell\printto\command";
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem__Sda
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sda";
    Value = "soffice.StarDrawDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sda
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sda";
    Value = "soffice.StarDrawDocument.5";
    Styles = ();
End

// chart
RegistryItem gid_Regitem_Sds_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".sds";
    Name = "Content Type";
    Value = "application/vnd.stardivision.chart";
End

RegistryItem gid_Regitem_Sds_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.chart";
    Name = "Extension";
    Value = ".sds";
End

RegistryItem gid_Regitem_Sds_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-starchart";
    Name = "Extension";
    Value = ".sds";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(SO52_CHART_OOO)
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\DefaultIcon";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "<progpath>\program\soffice.bin,0";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell\new";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell\new\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell\open\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell\print\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument.5\shell\printto\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starchartdocument_Curver
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarChartDocument\CurVer";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "soffice.StarChartDocument.5";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(SO50_TEMPLATE_OOO)
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\DefaultIcon";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "<progpath>\program\soffice.bin,10";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "new";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell\new";
    ModuleID = gid_Module_Optional_Binfilter;
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell\new\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell\open\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell\print\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starofficetemplate_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = "soffice.StarOfficeTemplate.5\shell\printto\command";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem__Sds
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sds";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "soffice.StarChartDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_sds
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".sds";
    Value = "soffice.StarChartDocument.5";
    Styles = ();
End

RegistryItem gid_Regitem__Vor
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".vor";
    ModuleID = gid_Module_Optional_Binfilter;
    Value = "soffice.StarOfficeTemplate.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_vor
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".vor";
    Value = "soffice.StarOfficeTemplate.5";
    Styles = ();
End

// StarMath
RegistryItem gid_Regitem_Smf_Contenttype
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".smf";
    Name = "Content Type";
    Value = "application/vnd.stardivision.math";
End

RegistryItem gid_Regitem_Smf_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/vnd.stardivision.math";
    Name = "Extension";
    Value = ".smf";
End

RegistryItem gid_Regitem_Smf_So4_Mime_Database
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "MIME\Database\Content Type\application/x-starmath";
    Name = "Extension";
    Value = ".smf";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5";
    REG_VALUE_LNG(SO51_FORMULA_OOO)
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Defaulticon
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\DefaultIcon";
    Value = "<progpath>\program\soffice.bin,12";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell";
    Value = "open";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell_New
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell\new";
    REG_VALUE_LNG(NEW)
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell_New_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell\new\command";
    Value = "\"<progpath>\program\soffice.exe\" -n \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell_Open_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell\open\command";
    Value = "\"<progpath>\program\soffice.exe\" -o \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell_Print_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell\print\command";
    Value = "\"<progpath>\program\soffice.exe\" -p \"%1\"";
End

RegistryItem gid_Regitem_Soffice_Starmathdocument_5_Shell_Printto_Command
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = "soffice.StarMathDocument.5\shell\printto\command";
    Value = "\"<progpath>\program\soffice.exe\" -pt \"%2\" \"%1\"";
End

RegistryItem gid_Regitem__Smf
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    ModuleID = gid_Module_Optional_Binfilter;
    Subkey = ".smf";
    Value = "soffice.StarMathDocument.5";
End

RegistryItem gid_Regitem_Software_Manufacturer_Productname_Productversion_Capabilities_FileAssociations_smf
    ParentID = PREDEFINED_HKEY_LOCAL_MACHINE;
    Subkey = "Software\%MANUFACTURER\%PRODUCTNAME%PRODUCTADDON\%PRODUCTVERSION\Capabilities\FileAssociations";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = ".smf";
    Value = "soffice.StarMathDocument.5";
    Styles = ();
End


// .SDW
RegistryItem gid_Regitem_OpenOffice_SDW_OpenWith_Writer
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sdw\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarWriterDocument.5";
    Value = " ";
End

// .VOR
RegistryItem gid_Regitem_OpenOffice_VOR_OpenWith_Writer
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".vor\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarWriterDocument.5";
    Value = " ";
End

// .SGL
RegistryItem gid_Regitem_OpenOffice_SGL_OpenWith_Writer
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sgl\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarWriterGlobalDocument.5";
    Value = " ";
End

// .SDC
RegistryItem gid_Regitem_OpenOffice_SDC_OpenWith_Calc
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sdc\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarCalcDocument.5";
    Value = " ";
End

// .VOR
RegistryItem gid_Regitem_OpenOffice_VOR_OpenWith_Calc
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".vor\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarCalcDocument.5";
    Value = " ";
End

// .SDD
RegistryItem gid_Regitem_OpenOffice_SDD_OpenWith_Impress
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sdd\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarImpressDocument.5";
    Value = " ";
End

// .SDP
RegistryItem gid_Regitem_OpenOffice_SDP_OpenWith_Impress
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sdp\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarImpressDocument.5";
    Value = " ";
End

// .SDA
RegistryItem gid_Regitem_OpenOffice_SDA_OpenWith_Impress
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sda\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarImpressDocument.5";
    Value = " ";
End

// .VOR
RegistryItem gid_Regitem_OpenOffice_VOR_OpenWith_Impress
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".vor\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarImpressDocument.5";
    Value = " ";
End

// .SDA
RegistryItem gid_Regitem_OpenOffice_SDA_OpenWith_Draw
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sda\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarDrawDocument.5";
    Value = " ";
End

// .SDD
RegistryItem gid_Regitem_OpenOffice_SDD_OpenWith_Draw
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".sdd\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarDrawDocument.5";
    Value = " ";
End

// .VOR
RegistryItem gid_Regitem_OpenOffice_VOR_OpenWith_Draw
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".vor\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarDrawDocument.5";
    Value = " ";
End

// .SMF
RegistryItem gid_Regitem_OpenOffice_SMF_OpenWith_Math
    ParentID = PREDEFINED_HKEY_CLASSES_ROOT;
    Subkey = ".smf\OpenWithProgIDs";
    ModuleID = gid_Module_Optional_Binfilter;
    Name = "soffice.StarMathDocument.5";
    Value = " ";
End


