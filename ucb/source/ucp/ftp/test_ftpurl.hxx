/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
extern int test_ftpurl(void);
extern int test_ftpproperties(void);
extern int test_ftpopen(void);
extern int test_ftplist(void);
extern int test_ftpparent(void);

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
