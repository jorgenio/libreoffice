#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2000, 2010 Oracle and/or its affiliates.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

.IF "$(OOO_SUBSEQUENT_TESTS)" == ""
nothing .PHONY:
.ELSE

PRJ = ../../..
PRJNAME = sc
TARGET = qa_complex_tdoc

.IF "$(OOO_JUNIT_JAR)" != ""
PACKAGE = complex/tdoc

JAVATESTFILES = \
   CheckContentProvider.java \
   CheckTransientDocumentsContent.java \
   CheckTransientDocumentsContentProvider.java \
   CheckTransientDocumentsDocumentContent.java

JAVAFILES = $(JAVATESTFILES) \
    TestDocument.java \
    _XChild.java \
    _XCommandInfoChangeNotifier.java \
    _XCommandProcessor.java \
    _XComponent.java \
    _XContent.java \
    _XPropertiesChangeNotifier.java \
    _XPropertyContainer.java \
    _XPropertySetInfoChangeNotifier.java \
    _XServiceInfo.java \
    _XTypeProvider.java

JARFILES = OOoRunner.jar ridl.jar test.jar unoil.jar
EXTRAJARFILES = $(OOO_JUNIT_JAR)

# Sample how to debug
# JAVAIFLAGS=-Xdebug  -Xrunjdwp:transport=dt_socket,server=y,address=9003,suspend=y

.END

.INCLUDE: settings.mk
.INCLUDE: target.mk
.INCLUDE: installationtest.mk

ALLTAR : javatest

.END

