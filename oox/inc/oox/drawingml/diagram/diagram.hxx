/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef OOX_DRAWINGML_DIAGRAM_HXX
#define OOX_DRAWINGML_DIAGRAM_HXX

#include <rtl/ustring.hxx>
#include "oox/drawingml/shape.hxx"
#include "oox/core/xmlfilterbase.hxx"

#include <com/sun/star/xml/dom/XDocument.hpp>

namespace oox { namespace drawingml {

/** load diagram data, and put resulting graphic into shape

    This method loads the diagram data fragments from the given paths,
    generate and layout the shapes, and push it as children into the
    referenced shape.
 */
void loadDiagram( ShapePtr& pShape,
                  core::XmlFilterBase& rFilter,
                  const ::rtl::OUString& rDataModelPath,
                  const ::rtl::OUString& rLayoutPath,
                  const ::rtl::OUString& rQStylePath,
                  const ::rtl::OUString& rColorStylePath );

void loadDiagram( const ShapePtr& pShape,
                  core::XmlFilterBase& rFilter,
                  const ::com::sun::star::uno::Reference<
                     ::com::sun::star::xml::dom::XDocument>& rXDataModelDom,
                  const ::com::sun::star::uno::Reference<
                     ::com::sun::star::xml::dom::XDocument>& rXLayoutDom,
                  const ::com::sun::star::uno::Reference<
                     ::com::sun::star::xml::dom::XDocument>& rXQStyleDom,
                  const ::com::sun::star::uno::Reference<
                     ::com::sun::star::xml::dom::XDocument>& rXColorStyleDom );
} }

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
