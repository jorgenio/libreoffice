#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2000, 2010 Oracle and/or its affiliates.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..

PRJNAME=shell
TARGET=xmlparser
ENABLE_EXCEPTIONS=TRUE
VISIBILITY_HIDDEN = TRUE


# --- Settings -----------------------------------------------------

.INCLUDE :  settings.mk

.IF "$(GUI)" == "WNT"
CFLAGS+=-DISOLATION_AWARE_ENABLED -DWIN32_LEAN_AND_MEAN -DXML_UNICODE -D_NTSDK -DUNICODE -D_UNICODE
.IF "$(COM)" == "MSC"
CFLAGS+=-wd4710 -wd4711 -wd4514 -wd4619 -wd4217 -wd4820
.ENDIF
CDEFS+=-U_WIN32_IE -D_WIN32_IE=0x501 -U_WIN32_WINNT -D_WIN32_WINNT=0x0501

.IF "$(BUILD_X64)"!=""
CFLAGS_X64+=-DISOLATION_AWARE_ENABLED -DWIN32_LEAN_AND_MEAN -DXML_UNICODE -D_NTSDK -DUNICODE -D_UNICODE
.IF "$(COM)" == "MSC"
CFLAGS_X64+=-wd4710 -wd4711 -wd4514 -wd4619 -wd4217 -wd4820
.ENDIF
CDEFS_X64+=-U_WIN32_IE -D_WIN32_IE=0x501 -U_WIN32_WINNT -D_WIN32_WINNT=0x0501
.ENDIF # "$(BUILD_X64)"!=""
.ENDIF

# --- Files --------------------------------------------------------

.IF "$(SYSTEM_EXPAT)" == "YES"
CFLAGS+=-DSYSTEM_EXPAT
.ENDIF

SLOFILES=$(SLO)$/xml_parser.obj

.IF "$(BUILD_X64)"!=""
SLOFILES_X64=$(SLO_X64)$/xml_parser.obj
.ENDIF # "$(BUILD_X64)"!=""

.INCLUDE :  set_wntx64.mk
.INCLUDE :  target.mk
.IF "$(OS)" == "WNT" 
INCLUDE!:=$(subst,/stl, $(INCLUDE))
.ENDIF 
.INCLUDE :  tg_wntx64.mk

