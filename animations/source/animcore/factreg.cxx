/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <osl/diagnose.h>
#include <cppuhelper/factory.hxx>
#include <cppuhelper/implementationentry.hxx>

#include <com/sun/star/registry/XRegistryKey.hpp>

using namespace ::rtl;
using namespace ::cppu;
using namespace ::com::sun::star::uno;
using namespace ::com::sun::star::lang;
using namespace ::com::sun::star::registry;

#include "factreg.hxx"

namespace animcore
{
    rtl_StandardModuleCount g_moduleCount = MODULE_COUNT_INIT;
}

using namespace animcore;

#define IMPLEMENTATION_ENTRY(N)\
{\
    createInstance_##N, getImplementationName_##N ,\
    getSupportedServiceNames_##N, createSingleComponentFactory ,\
    &g_moduleCount.modCnt , 0\
}\

static struct ImplementationEntry g_entries[] =
{
    IMPLEMENTATION_ENTRY( PAR ),
    IMPLEMENTATION_ENTRY( SEQ ),
    IMPLEMENTATION_ENTRY( ITERATE ),
    IMPLEMENTATION_ENTRY( ANIMATE ),
    IMPLEMENTATION_ENTRY( SET ),
    IMPLEMENTATION_ENTRY( ANIMATECOLOR ),
    IMPLEMENTATION_ENTRY( ANIMATEMOTION ),
    IMPLEMENTATION_ENTRY( ANIMATETRANSFORM ),
    IMPLEMENTATION_ENTRY( TRANSITIONFILTER ),
    IMPLEMENTATION_ENTRY( AUDIO ),
    IMPLEMENTATION_ENTRY( COMMAND ),
    IMPLEMENTATION_ENTRY( TargetPropertiesCreator ),
    { 0, 0, 0, 0, 0, 0 }
};

extern "C"
{

SAL_DLLPUBLIC_EXPORT void * SAL_CALL animcore_component_getFactory(
    const sal_Char * pImplName, void * pServiceManager, void * pRegistryKey )
{
    return component_getFactoryHelper( pImplName, pServiceManager, pRegistryKey , g_entries );
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
