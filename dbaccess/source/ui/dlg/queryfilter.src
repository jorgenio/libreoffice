/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef DBAUI_QUERYFILTER_HRC
#include "queryfilter.hrc"
#endif
#include "dbaccess_helpid.hrc"

ModalDialog DLG_FILTERCRIT
{
    OutputSize = TRUE ;
    Hide = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( 303 , 76 ) ;
    /* These strings are equal to the toolbox bubble helptext from MID_SBA_QRY_FILTERCRIT in toolbox.hrc */
    Text [ en-US ] ="Standard Filter";

    Moveable = TRUE ;
    Closeable = TRUE ;
    HelpId = HID_DLG_FILTERCRIT ;
    ListBox LB_WHEREFIELD1
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHEREFIELD1";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 60 , 25 ) ;
        Size = MAP_APPFONT ( 60 , 90 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
    };
    ListBox LB_WHERECOMP1
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHERECOMP1";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 124 , 25 ) ;
        Size = MAP_APPFONT ( 49 , 72 ) ;
        TabStop = TRUE ;
        DDExtraWidth = TRUE ;
        DropDown = TRUE ;
        CurPos = 0 ;
    };
    Edit ET_WHEREVALUE1
    {
        HelpID = "dbaccess:Edit:DLG_FILTERCRIT:ET_WHEREVALUE1";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 177 , 25 ) ;
        Size = MAP_APPFONT ( 60 , 12 ) ;
        TabStop = TRUE ;
    };
    ListBox LB_WHERECOND2
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHERECOND2";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 15 , 41 ) ;
        Size = MAP_APPFONT ( 41 , 36 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        CurPos = 0 ;
        StringList [ en-US ] =
        {
            < "AND" ; Default ; > ;
            < "OR" ; Default ; > ;
        };
    };
    ListBox LB_WHEREFIELD2
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHEREFIELD2";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 60 , 41 ) ;
        Size = MAP_APPFONT ( 60 , 90 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
    };
    ListBox LB_WHERECOMP2
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHERECOMP2";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 124 , 41 ) ;
        Size = MAP_APPFONT ( 49 , 72 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
        CurPos = 0 ;
    };
    Edit ET_WHEREVALUE2
    {
        HelpID = "dbaccess:Edit:DLG_FILTERCRIT:ET_WHEREVALUE2";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 177 , 41 ) ;
        Size = MAP_APPFONT ( 60 , 12 ) ;
        TabStop = TRUE ;
    };
    ListBox LB_WHERECOND3
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHERECOND3";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 15 , 57 ) ;
        Size = MAP_APPFONT ( 41 , 36 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        CurPos = 0 ;
        StringList [ en-US ] =
        {
            < "AND" ; Default ; > ;
            < "OR" ; Default ; > ;
        };
    };
    ListBox LB_WHEREFIELD3
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHEREFIELD3";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 60 , 57 ) ;
        Size = MAP_APPFONT ( 60 , 90 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
    };
    ListBox LB_WHERECOMP3
    {
        HelpID = "dbaccess:ListBox:DLG_FILTERCRIT:LB_WHERECOMP3";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 124 , 57 ) ;
        Size = MAP_APPFONT ( 49 , 72 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
        CurPos = 0 ;
    };
    Edit ET_WHEREVALUE3
    {
        HelpID = "dbaccess:Edit:DLG_FILTERCRIT:ET_WHEREVALUE3";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 177 , 57 ) ;
        Size = MAP_APPFONT ( 60 , 12 ) ;
        TabStop = TRUE ;
    };
    FixedText FT_WHEREFIELD
    {
        Pos = MAP_APPFONT ( 60 , 14 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Center = TRUE ;
        Text [ en-US ] = "Field name" ;
    };
    FixedText FT_WHERECOMP
    {
        Pos = MAP_APPFONT ( 124 , 14 ) ;
        Size = MAP_APPFONT ( 50 , 8 ) ;
        Center = TRUE ;
        Text [ en-US ] = "Condition" ;
    };
    FixedText FT_WHEREVALUE
    {
        Pos = MAP_APPFONT ( 177 , 14 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Center = TRUE ;
        Text [ en-US ] = "Value" ;
    };
    FixedText FT_WHEREOPER
    {
        Pos = MAP_APPFONT ( 15 , 14 ) ;
        Size = MAP_APPFONT ( 40 , 8 ) ;
        Center = TRUE ;
        Text [ en-US ] = "Operator" ;
    };
    FixedLine FL_FIELDS
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 236 , 8 ) ;
        Text [ en-US ] = "Criteria" ;
    };
    OKButton BT_OK
    {
        Pos = MAP_APPFONT ( 249 , 6 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BT_CANCEL
    {
        Pos = MAP_APPFONT ( 249 , 23 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    HelpButton BT_HELP
    {
        Pos = MAP_APPFONT ( 249 , 43 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    String STR_NOENTRY
    {
        Text [ en-US ] = "- none -" ;
    };
    String STR_COMPARE_OPERATORS
    {
        Text [ en-US ] = "=;<>;<;<=;>;>=;like;not like;null;not null";
    };
};
