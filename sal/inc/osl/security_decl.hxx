/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _OSL_SECURITY_DECL_HXX_
#define _OSL_SECURITY_DECL_HXX_

#include <rtl/ustring.hxx>
#   include <osl/security.h>

namespace osl
{

/** capsulate security informations for one user.
    A object of this class is used to execute a process with the rights an
    security options of a scecified user.
    @see Process::executeProcess
*/
class Security
{
protected:
    oslSecurity m_handle;

public:
    /// constructor
    inline Security();
    /// destructor
    inline ~Security();
    /** get the security information for one user.
        The underlying operating system is asked for this information.
        @param[in] strName denotes the name of the user
        @param[in] strPasswd denotes the password of this user
        @return True, if the specified user is known by the underlying operating system,
        otherwise False
    */
    inline sal_Bool SAL_CALL logonUser(const rtl::OUString& strName,
                                       const rtl::OUString& strPasswd);
    /** get the security information for one user.
        This method will try to login the user at the denoted file server.
        If a network resource named \\server\username exists and this resource
        could be connected by this user, the methos will return true and getHomeDir
        will return \\server\username.
        @param[in] strName denotes the name of the user
        @param[in] strPasswd denotes the password of this user
        @return True, if the specified user is known by file server and the
        could be connected, otherwise False
    */
    inline sal_Bool SAL_CALL logonUser(const rtl::OUString & strName,
                                       const rtl::OUString & strPasswd,
                                       const rtl::OUString & strFileServer);

    /** get the ident of the logged in user.
        @param[out] strIdent is the OUString which returns the name
        @return True, if any user is successfuly logged in, otherwise False
    */
    inline sal_Bool  SAL_CALL getUserIdent( rtl::OUString& strIdent) const;

    /** get the name of the logged in user.
        @param[out] strName is the OUString which returns the name
        @return True, if any user is successfuly logged in, otherwise False
    */
    inline sal_Bool SAL_CALL getUserName( rtl::OUString& strName) const;

    /** get the home directory of the logged in user.
        @param[out] strDirectory is the OUString which returns the directory name
         @return True, if any user is successfuly logged in, otherwise False
    */
    inline sal_Bool SAL_CALL getHomeDir( rtl::OUString& strDirectory) const;

    /** get the directory for configuration data of the logged in user.
        @param[out] strDirectory is the OUString which returns the directory name
        @return True, if any user is successfuly logged in, otherwise False
    */
    inline sal_Bool SAL_CALL getConfigDir( rtl::OUString & strDirectory) const;

    /** Query if the user who is logged inhas administrator rigths.
        @return True, if the user has administrator rights, otherwise false.
    */
    inline sal_Bool SAL_CALL isAdministrator() const;

    /** Returns the underlying oslSecurity handle
     */
    inline oslSecurity getHandle() const;

};

}

#endif  // _OSL_SECURITY_HXX_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
