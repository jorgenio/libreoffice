/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* Not unicode */
#undef _UNICODE

/* Support MBCS and SBCS */

#ifndef _MBCS
#define _MBCS
#endif

#include "sntprintf.c"

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
