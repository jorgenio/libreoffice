/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#ifndef _SV_SALFONTUTILS_HXX
#define _SV_SALFONTUTILS_HXX

#include "vcl/outfont.hxx"

static const char *kFontWeightThin1 = "Thin";
static const char *kFontWeightThin2 = "thin";

static const char *kFontWeightLight1 = "Light";
static const char *kFontWeightLight2 = "light";

static const char *kFontWeightBold1 = "Bold";
static const char *kFontWeightBold2 = "bold";

static const char *kFontWeightUltra1 = "Ultra";
static const char *kFontWeightUltra2 = "ultra";

static const char *kFontWeightSemi1 = "Semi";
static const char *kFontWeightSemi2 = "semi";

static const char *kFontWeightNormal1 = "Normal";
static const char *kFontWeightNormal2 = "normal";

static const char *kFontWeightMedium1 = "Medium";
static const char *kFontWeightMedium2 = "medium";

static const char *kFontWeightBlack1 = "Black";
static const char *kFontWeightBlack2 = "black";

static const char *kFontWeightRoman1 = "Roman";
static const char *kFontWeightRoman2 = "roman";

static const char *kFontWeightRegular1 = "Regular";
static const char *kFontWeightRegular2 = "regular";


#endif  // _SV_SALFONTUTILS_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
