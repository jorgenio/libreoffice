/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MfcControl.rc
//

#define IDS_MFCCONTROL               1
#define IDS_MFCCONTROL_PPG           2

#define IDS_MFCCONTROL_PPG_CAPTION   200

#define IDD_PROPPAGE_MFCCONTROL      200


#define IDB_MFCCONTROL               1


#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           101
#define _APS_NEXT_COMMAND_VALUE         32768

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
