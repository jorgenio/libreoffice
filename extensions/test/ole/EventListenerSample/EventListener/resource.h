/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EventListener.rc
//
#define IDS_PROJNAME                    100
#define IDR_EVTLISTENER                 101

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
