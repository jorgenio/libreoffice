/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by so_activex.rc
//
#define IDS_PROJNAME                    100
#define IDB_SOACTIVEX                 101
#define IDR_SOACTIVEX                 102
#define IDB_SOCOMWINDOWPEER                 103
#define IDR_SOCOMWINDOWPEER                 104
#define IDB_SODISPATCHINTERCEPTOR                 105
#define IDR_SODISPATCHINTERCEPTOR                 106
#define IDB_SODOCUMENTEVENTLISTENER                 107
#define IDR_SODOCUMENTEVENTLISTENER                 108


// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
