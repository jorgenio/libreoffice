/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
// stdafx1.cpp : source file that includes just the standard includes
//  stdafx1.pch will be the pre-compiled header
//  stdafx1.obj will contain the pre-compiled type information

#ifdef __MINGW32__
#define _INIT_ATL_COMMON_VARS
#endif
#include "stdafx2.h"

#ifdef _ATL_STATIC_REGISTRY
#include <statreg.h>
#include <statreg.cpp>
#endif

#if defined(_MSC_VER) && (_MSC_VER >= 1300)
#undef _DEBUG
#endif
#include <atlimpl.cpp>

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
