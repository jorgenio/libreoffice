/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _EXTENSIONS_PROPCTRLR_FONTITEMIDS_HXX_
#define _EXTENSIONS_PROPCTRLR_FONTITEMIDS_HXX_

#define CFID_FONT               1
#define CFID_HEIGHT             2
#define CFID_WEIGHT             3
#define CFID_POSTURE            4
#define CFID_LANGUAGE           5
#define CFID_UNDERLINE          6
#define CFID_STRIKEOUT          7
#define CFID_WORDLINEMODE       8
#define CFID_CHARCOLOR          9
#define CFID_RELIEF             10
#define CFID_EMPHASIS           11

#define CFID_CJK_FONT           12
#define CFID_CJK_HEIGHT         13
#define CFID_CJK_WEIGHT         14
#define CFID_CJK_POSTURE        15
#define CFID_CJK_LANGUAGE       16
#define CFID_CASEMAP            17
#define CFID_CONTOUR            18
#define CFID_SHADOWED           19

#define CFID_FONTLIST           20

#define CFID_FIRST_ITEM_ID  CFID_FONT
#define CFID_LAST_ITEM_ID   CFID_FONTLIST

//........................................................................
namespace pcr
{
//........................................................................
//........................................................................
}   // namespace pcr
//........................................................................

#endif // _EXTENSIONS_PROPCTRLR_FONTITEMIDS_HXX_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
