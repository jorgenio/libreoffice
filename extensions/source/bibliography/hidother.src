/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "bibliography.hrc"

hidspecial HID_BIB_IDENTIFIER_POS                   { HelpID = HID_BIB_IDENTIFIER_POS          ; };
hidspecial HID_BIB_AUTHORITYTYPE_POS                { HelpID = HID_BIB_AUTHORITYTYPE_POS       ; };
hidspecial HID_BIB_AUTHOR_POS                       { HelpID = HID_BIB_AUTHOR_POS              ; };
hidspecial HID_BIB_TITLE_POS                        { HelpID = HID_BIB_TITLE_POS               ; };
hidspecial HID_BIB_YEAR_POS                         { HelpID = HID_BIB_YEAR_POS                ; };
hidspecial HID_BIB_ISBN_POS                         { HelpID = HID_BIB_ISBN_POS                ; };
hidspecial HID_BIB_BOOKTITLE_POS                    { HelpID = HID_BIB_BOOKTITLE_POS           ; };
hidspecial HID_BIB_CHAPTER_POS                      { HelpID = HID_BIB_CHAPTER_POS             ; };
hidspecial HID_BIB_EDITION_POS                      { HelpID = HID_BIB_EDITION_POS             ; };
hidspecial HID_BIB_EDITOR_POS                       { HelpID = HID_BIB_EDITOR_POS              ; };
hidspecial HID_BIB_HOWPUBLISHED_POS                 { HelpID = HID_BIB_HOWPUBLISHED_POS        ; };
hidspecial HID_BIB_INSTITUTION_POS                  { HelpID = HID_BIB_INSTITUTION_POS         ; };
hidspecial HID_BIB_JOURNAL_POS                      { HelpID = HID_BIB_JOURNAL_POS             ; };
hidspecial HID_BIB_MONTH_POS                        { HelpID = HID_BIB_MONTH_POS               ; };
hidspecial HID_BIB_NOTE_POS                         { HelpID = HID_BIB_NOTE_POS                ; };
hidspecial HID_BIB_ANNOTE_POS                       { HelpID = HID_BIB_ANNOTE_POS              ; };
hidspecial HID_BIB_NUMBER_POS                       { HelpID = HID_BIB_NUMBER_POS              ; };
hidspecial HID_BIB_ORGANIZATIONS_POS                { HelpID = HID_BIB_ORGANIZATIONS_POS       ; };
hidspecial HID_BIB_PAGES_POS                        { HelpID = HID_BIB_PAGES_POS               ; };
hidspecial HID_BIB_PUBLISHER_POS                    { HelpID = HID_BIB_PUBLISHER_POS           ; };
hidspecial HID_BIB_ADDRESS_POS                      { HelpID = HID_BIB_ADDRESS_POS             ; };
hidspecial HID_BIB_SCHOOL_POS                       { HelpID = HID_BIB_SCHOOL_POS              ; };
hidspecial HID_BIB_SERIES_POS                       { HelpID = HID_BIB_SERIES_POS              ; };
hidspecial HID_BIB_REPORTTYPE_POS                   { HelpID = HID_BIB_REPORTTYPE_POS          ; };
hidspecial HID_BIB_VOLUME_POS                       { HelpID = HID_BIB_VOLUME_POS              ; };
hidspecial HID_BIB_URL_POS                          { HelpID = HID_BIB_URL_POS                 ; };
hidspecial HID_BIB_CUSTOM1_POS                      { HelpID = HID_BIB_CUSTOM1_POS             ; };
hidspecial HID_BIB_CUSTOM2_POS                      { HelpID = HID_BIB_CUSTOM2_POS             ; };
hidspecial HID_BIB_CUSTOM3_POS                      { HelpID = HID_BIB_CUSTOM3_POS             ; };
hidspecial HID_BIB_CUSTOM4_POS                      { HelpID = HID_BIB_CUSTOM4_POS             ; };
hidspecial HID_BIB_CUSTOM5_POS                      { HelpID = HID_BIB_CUSTOM5_POS             ; };
hidspecial HID_BIB_DB_GRIDCTRL                      { HelpID = HID_BIB_DB_GRIDCTRL             ; };
