/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#define ENGLISH_US              1
#define ENGLISH_US_ISO          "en-US"
#define ENGLISH_US_PROPERTY     "en_US"
#define ENGLISH_US_INDEX            1
#define GERMAN_DE               4
#define GERMAN_DE_ISO           "x-german"
#define GERMAN_DE_INDEX             3
#define GERMAN_ISO2                     "de-DE"

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
