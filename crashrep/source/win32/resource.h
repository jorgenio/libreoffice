/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by soreport.rc
//
#define IDD_DIALOG_FRAME                101
#define IDS_WELCOME_CAPTION             102
#define IDS_WELCOME_HEADER              103
#define IDS_WELCOME_BODY1               104
#define IDS_PRIVACY_URL                 105
#define IDS_NEXT_BUTTON                 106
#define IDD_WELCOME_PAGE                107
#define IDS_CANCEL_BUTTON               107
#define IDD_REPORT_PAGE                 108
#define IDS_REPORT_CAPTION              108
#define IDS_REPORT_HEADER               109
#define IDS_DESCRIPTION                 110
#define IDS_REPORT_INTRO                110
#define IDS_ENTER_TITLE                 111
#define IDS_ALLOW_CONTACT               112
#define IDS_ENTER_DESCRIPTION           113
#define IDS_BACK_BUTTON                 114
#define IDS_SEND_BUTTON                 115
#define IDS_DONOT_SEND_BUTTON           116
#define IDS_SHOW_REPORT_BUTTON          117
#define IDS_SAVE_REPORT_BUTTON          118
#define IDD_PREVIEW_FRAME               119
#define IDS_ERROR_MSG_SIMPLE_MAPI       120
#define IDS_ERROR_MSG_DISK_FULL         121
#define IDS_OK_BUTTON                   122
#define IDS_OPTIONS_BUTTON              123
#define IDS_LABEL_EMAIL                 124
#define IDD_OPTIONS_FRAME               125
#define IDS_OPTIONS_CAPTION             126
#define IDS_PROXY_SETTINGS_HEADER       127
#define IDS_PROXY_SYSTEM                128
#define IDS_PROXY_DIRECT                129
#define IDS_PROXY_MANUAL                130
#define IDS_LABEL_PROXYSERVER           131
#define IDS_LABEL_PROXYPORT             132
#define IDS_PROXY_DESCRIPTION           133
#define IDS_ERROR_MSG_PROXY             134
#define IDS_ERROR_MSG_NOCONNECT         135
#define IDS_SENDING_REPORT_HEADER       136
#define IDS_SENDING_REPORT_STATUS       137
#define IDS_SENDING_REPORT_STATUS_FINISHED 138
#define IDD_SENDING_STATUS              139
#define IDS_WELCOME_BODY2               140
#define IDS_WELCOME_BODY3               141
#define IDS_ERROR_MSG_NOEMAILADDRESS    142
#define IDS_MSG_CMDLINE_USAGE           143
#define IDS_MSG_PARAM_PROCESSID         144
#define IDS_MSG_PARAM_PROCESSID_DESCRIPTION 145
#define IDS_MSG_PARAM_HELP_DESCRIPTION 146
#define IDC_RICHEDIT_HEADER             1005
#define IDC_RICHEDIT21                  1006
#define IDC_ALLOW_CONTACT               1007
#define IDC_EDIT_TITLE                  1008
#define IDC_EDIT_DESCRIPTION            1009
#define IDC_SHOW_REPORT                 1010
#define IDC_SAVE_REPORT                 1011
#define IDNEXT                          1012
#define IDBACK                          1014
#define IDC_REPORT_INTRO                1015
#define IDC_ENTER_TITLE                 1016
#define IDFINISH                        1016
#define IDC_ENTER_DESCRIPTION           1017
#define IDC_EDIT3                       1018
#define IDC_HEADER                      1019
#define IDC_STATICBK                    1020
#define IDC_EDIT_PREVIEW                1021
#define IDC_EDIT_EMAIL                  1022
#define IDC_OPTIONS                     1023
#define IDC_PROXY_SETTINGS              1024
#define IDC_RADIO_SYSTEM                1025
#define IDC_RADIO_DIRECT                1026
#define IDC_RADIO_MANUAL                1027
#define IDC_LABEL_PROXYSERVER           1028
#define IDC_LABEL_PROXYPORT             1029
#define IDC_EDIT_PROXYSERVER            1030
#define IDC_EDIT_PROXYPORT              1031
#define IDC_LABEL_EMAIL                 1032
#define IDC_PROXY_DESCRIPTION           1033
#define IDC_SENDING_REPORT_STATUS       1034
// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
