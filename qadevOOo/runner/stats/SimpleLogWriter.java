/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package stats;

import share.LogWriter;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SimpleLogWriter extends PrintWriter implements LogWriter {

    boolean m_bLogging = false;
    share.DescEntry entry = null;
    share.Watcher ow = null;

    public SimpleLogWriter() {
        super(System.out);
        Calendar cal = new GregorianCalendar();
        DecimalFormat dfmt = new DecimalFormat("00");
        super.println("LOG> Log started " +
            dfmt.format(cal.get(Calendar.DAY_OF_MONTH)) + "." +
            dfmt.format(cal.get(Calendar.MONTH)) + "." +
            dfmt.format(cal.get(Calendar.YEAR)) + " - " +
            dfmt.format(cal.get(Calendar.HOUR_OF_DAY)) + ":" +
            dfmt.format(cal.get(Calendar.MINUTE)) + ":" +
            dfmt.format(cal.get(Calendar.SECOND)));
        super.flush();
    }

    public boolean initialize(share.DescEntry _entry, boolean _bLogging) {
        m_bLogging = _bLogging;
        entry = _entry;

        return true;
    }

    public void println(String msg) {
        if ((ow == null) && (entry != null))
        {
            this.ow = (share.Watcher) entry.UserDefinedParams.get("Watcher");
            if (this.ow != null)
            {
                this.ow.ping();
            }
        }
        else
        {
            if (ow != null)
            {
                this.ow.ping();
            }
            else
            {
                // special case: ow == null && entry == null
                System.out.println(msg);
            }
        }

        if (m_bLogging) {
            super.println("LOG> " + msg);
            super.flush();
        }
    // else
    // {
    //     super.println(" ++ " + msg);
    //     super.flush();
    // }
    }

    public boolean summary(share.DescEntry entry) {
        return true;
    }

    public Object getWatcher() {
        return this.ow;
    }

    public void setWatcher(Object watcher)
    {
        if (watcher != null)
        {
            entry.UserDefinedParams.put("Watcher", (share.Watcher) watcher);
        }
    }
}
