/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.drawing;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.drawing.TextProperties</code>
* service properties :
* <ul>
*  <li><code> IsNumbering</code></li>
*  <li><code> NumberingRules</code></li>
*  <li><code> TextAutoGrowHeight</code></li>
*  <li><code> TextAutoGrowWidth</code></li>
*  <li><code> TextContourFrame</code></li>
*  <li><code> TextFitToSize</code></li>
*  <li><code> HorizontalAdjust</code></li>
*  <li><code> VerticalAdjust</code></li>
*  <li><code> TextLeftDistance</code></li>
*  <li><code> TextRightDistance</code></li>
*  <li><code> TextUpperDistance</code></li>
*  <li><code> TextLowerDistance</code></li>
*  <li><code> TextMaximumFrameHeight</code></li>
*  <li><code> TextMaximumFrameWidth</code></li>
*  <li><code> TextMinimumFrameHeight</code></li>
*  <li><code> TextMinimumFrameWidth</code></li>
*  <li><code> TextAnimationAmount</code></li>
*  <li><code> TextAnimationCount</code></li>
*  <li><code> TextAnimationDelay</code></li>
*  <li><code> TextAnimationDirection</code></li>
*  <li><code> TextAnimationKind</code></li>
*  <li><code> TextAnimationStartInside</code></li>
*  <li><code> TextAnimationStopInside</code></li>
*  <li><code> TextWritingMode</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.drawing.TextProperties
*/
public class _TextProperties extends MultiPropertyTest {

}

