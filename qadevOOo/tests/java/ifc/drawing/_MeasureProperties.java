/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.drawing;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.drawing.MeasureProperties</code>
* service properties :
* <ul>
*  <li><code> MeasureBelowReferenceEdge</code></li>
*  <li><code> MeasureHelpLine1Length</code></li>
*  <li><code> MeasureHelpLine2Length</code></li>
*  <li><code> MeasureHelpLineDistance</code></li>
*  <li><code> MeasureHelpLineOverhang</code></li>
*  <li><code> MeasureKind</code></li>
*  <li><code> MeasureLineDistance</code></li>
*  <li><code> MeasureOverhang</code></li>
*  <li><code> MeasureShowUnit</code></li>
*  <li><code> MeasureTextAutoAngle</code></li>
*  <li><code> MeasureTextAutoAngleView</code></li>
*  <li><code> MeasureTextFixedAngle</code></li>
*  <li><code> MeasureTextHorizontalPosition</code></li>
*  <li><code> MeasureTextVerticalPosition</code></li>
*  <li><code> MeasureTextIsFixedAngle</code></li>
*  <li><code> MeasureTextRotate90</code></li>
*  <li><code> MeasureTextUpsideDown</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.drawing.MeasureProperties
*/
public class _MeasureProperties extends MultiPropertyTest {

}

