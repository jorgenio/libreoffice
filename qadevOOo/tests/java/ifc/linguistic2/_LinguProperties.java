/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.linguistic2;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.linguistic2.LinguProperties</code>
* service properties:
* <ul>
*   <li><code>'IsGermanPreReform'</code></li>
*   <li><code>'IsUseDictionaryList'</code></li>
*   <li><code>'IsIgnoreControlCharacters'</code></li>
*   <li><code>'IsSpellUpperCase'</code></li>
*   <li><code>'IsSpellWithDigits'</code></li>
*   <li><code>'IsSpellCapitalization'</code></li>
*   <li><code>'HyphMinLeading'</code></li>
*   <li><code>'HyphMinTrailing'</code></li>
*   <li><code>'HyphMinWordLength'</code></li>
*   <li><code>'DefaultLocale'</code></li>
*   <li><code>'IsHyphAuto'</code></li>
*   <li><code>'IsHyphSpecial'</code></li>
*   <li><code>'IsSpellAuto'</code></li>
*   <li><code>'IsSpellHide'</code></li>
*   <li><code>'IsSpellInAllLanguages'</code></li>
*   <li><code>'IsSpellSpecial'</code></li>
*   <li><code>'IsWrapReverse'</code></li>
*   <li><code>'DefaultLocale_CJK'</code></li>
*   <li><code>'DefaultLocale_CTL'</code></li>
* </ul>
* @see com.sun.star.linguistic2.LinguProperties
*/
public class _LinguProperties extends MultiPropertyTest {
}  // finish class _LinguProperties


