/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.formula;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.formula.FormulaProperties</code>
* service properties :
* <ul>
*  <li><code> Alignment</code></li>
*  <li><code> BaseFontHeight</code></li>
*  <li><code> CustomFontNameFixed</code></li>
*  <li><code> FontFixedIsItalic</code></li>
*  <li><code> FontFixedIsBold</code></li>
*  <li><code> CustomFontNameSans</code></li>
*  <li><code> FontSansIsItalic</code></li>
*  <li><code> FontSansIsBold</code></li>
*  <li><code> CustomFontNameSerif</code></li>
*  <li><code> FontSerifIsItalic</code></li>
*  <li><code> FontSerifIsBold</code></li>
*  <li><code> FontNameFunctions</code></li>
*  <li><code> FontFunctionsIsItalic</code></li>
*  <li><code> FontFunctionsIsBold</code></li>
*  <li><code> FontNameNumbers</code></li>
*  <li><code> FontNumbersIsItalic</code></li>
*  <li><code> FontNumbersIsBold</code></li>
*  <li><code> FontNameText</code></li>
*  <li><code> FontTextIsItalic</code></li>
*  <li><code> FontTextIsBold</code></li>
*  <li><code> FontNameVariables</code></li>
*  <li><code> FontVariablesIsItalic</code></li>
*  <li><code> FontVariablesIsBold</code></li>
*  <li><code> Formula</code></li>
*  <li><code> IsScaleAllBrackets</code></li>
*  <li><code> IsTextMode</code></li>
*  <li><code> RelativeFontHeightFunctions</code></li>
*  <li><code> RelativeFontHeightIndices</code></li>
*  <li><code> RelativeFontHeightLimits</code></li>
*  <li><code> RelativeFontHeightOperators</code></li>
*  <li><code> RelativeFontHeightText</code></li>
*  <li><code> RelativeBracketDistance</code></li>
*  <li><code> RelativeBracketExcessSize</code></li>
*  <li><code> RelativeFractionBarExcessLength</code></li>
*  <li><code> RelativeFractionBarLineWeight</code></li>
*  <li><code> RelativeFractionDenominatorDepth</code></li>
*  <li><code> RelativeFractionNumeratorHeight</code></li>
*  <li><code> RelativeIndexSubscript</code></li>
*  <li><code> RelativeIndexSuperscript</code></li>
*  <li><code> RelativeLineSpacing</code></li>
*  <li><code> RelativeLowerLimitDistance</code></li>
*  <li><code> RelativeMatrixColumnSpacing</code></li>
*  <li><code> RelativeMatrixLineSpacing</code></li>
*  <li><code> RelativeOperatorExcessSize</code></li>
*  <li><code> RelativeOperatorSpacing</code></li>
*  <li><code> RelativeRootSpacing</code></li>
*  <li><code> RelativeScaleBracketExcessSize</code></li>
*  <li><code> RelativeSpacing</code></li>
*  <li><code> RelativeSymbolMinimumHeight</code></li>
*  <li><code> RelativeSymbolPrimaryHeight</code></li>
*  <li><code> RelativeUpperLimitDistance</code></li>
*  <li><code> TopMargin</code></li>
*  <li><code> BottomMargin</code></li>
*  <li><code> LeftMargin</code></li>
*  <li><code> RightMargin</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.formula.FormulaProperties
*/
public class _FormulaProperties extends MultiPropertyTest {


}  // finish class _FormulaProperties

