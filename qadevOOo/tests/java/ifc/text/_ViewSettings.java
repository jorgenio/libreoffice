/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.text;

import lib.MultiPropertyTest;

/**
 * Testing <code>com.sun.star.text.ViewSettings</code>
 * service properties :
 * <ul>
 *  <li><code> ShowAnnotations</code></li>
 *  <li><code> ShowBreaks</code></li>
 *  <li><code> ShowDrawings</code></li>
 *  <li><code> ShowFieldCommands</code></li>
 *  <li><code> ShowFootnoteBackground</code></li>
 *  <li><code> ShowGraphics</code></li>
 *  <li><code> ShowHiddenParagraphs</code></li>
 *  <li><code> ShowHiddenText</code></li>
 *  <li><code> ShowHoriRuler</code></li>
 *  <li><code> ShowHoriScrollBar</code></li>
 *  <li><code> ShowIndexMarkBackground</code></li>
 *  <li><code> ShowParaBreaks</code></li>
 *  <li><code> ShowProtectedSpaces</code></li>
 *  <li><code> ShowSoftHyphens</code></li>
 *  <li><code> ShowSpaces</code></li>
 *  <li><code> ShowTableBoundaries</code></li>
 *  <li><code> ShowTables</code></li>
 *  <li><code> ShowTabstops</code></li>
 *  <li><code> ShowTextBoundaries</code></li>
 *  <li><code> ShowTextFieldBackground</code></li>
 *  <li><code> ShowVertRuler</code></li>
 *  <li><code> ShowVertScrollBar</code></li>
 *  <li><code> SmoothScrolling</code></li>
 *  <li><code> IsVertRulerRightAligned</code></li>
 *  <li><code> ShowOnlineLayout</code></li>
 *  <li><code> ZoomType</code></li>
 *  <li><code> ZoomValue</code></li>
 * </ul> <p>
 * Properties testing is automated by <code>lib.MultiPropertyTest</code>.
 * @see com.sun.star.text.ViewSettings
 */
public class _ViewSettings extends MultiPropertyTest {

} // finish class _ViewSettings

