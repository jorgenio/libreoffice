/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.text;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.text.PrintSettings</code>
* service properties :
* <ul>
*  <li><code> PrintGraphics</code></li>
*  <li><code> PrintTables</code></li>
*  <li><code> PrintDrawings</code></li>
*  <li><code> PrintLeftPages</code></li>
*  <li><code> PrintRightPages</code></li>
*  <li><code> PrintControls</code></li>
*  <li><code> PrintReversed</code></li>
*  <li><code> PrintPaperFromSetup</code></li>
*  <li><code> PrintFaxName</code></li>
*  <li><code> PrintAnnotationMode</code></li>
*  <li><code> PrintProspect</code></li>
*  <li><code> PrintPageBackground</code></li>
*  <li><code> PrintBlackFonts</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.text.PrintSettings
*/
public class _PrintSettings extends MultiPropertyTest {

}

