/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.sheet;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.sheet.SheetFilterDescriptor</code>
* service properties :
* <ul>
*  <li><code> IsCaseSensitive</code></li>
*  <li><code> SkipDuplicates</code></li>
*  <li><code> UseRegularExpressions</code></li>
*  <li><code> SaveOutputPosition</code></li>
*  <li><code> Orientation</code></li>
*  <li><code> ContainsHeader</code></li>
*  <li><code> CopyOutputData</code></li>
*  <li><code> OutputPosition</code></li>
*  <li><code> MaxFieldCount</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.sheet.SheetFilterDescriptor
*/
public class _SheetFilterDescriptor extends MultiPropertyTest {
}  // finish class _SheetFilterDescriptor


