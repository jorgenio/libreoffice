/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

package ifc.sheet;

import lib.MultiPropertyTest;

/**
* Testing <code>com.sun.star.sheet.GlobalSheetSettings</code>
* service properties :
* <ul>
*  <li><code> MoveSelection</code></li>
*  <li><code> MoveDirection</code></li>
*  <li><code> EnterEdit</code></li>
*  <li><code> ExtendFormat</code></li>
*  <li><code> RangeFinder</code></li>
*  <li><code> ExpandReferences</code></li>
*  <li><code> MarkHeader</code></li>
*  <li><code> UseTabCol</code></li>
*  <li><code> Metric</code></li>
*  <li><code> Scale</code></li>
*  <li><code> DoAutoComplete</code></li>
*  <li><code> StatusBarFunction</code></li>
*  <li><code> UserLists</code></li>
* </ul> <p>
* Properties testing is automated by <code>lib.MultiPropertyTest</code>.
* @see com.sun.star.sheet.GlobalSheetSettings
*/
public class _GlobalSheetSettings extends MultiPropertyTest {
}


