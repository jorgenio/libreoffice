/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by XMergeSync.rc
//
#define IDS_NOJAVA                      1
#define IDS_BADCLASSPATH                2

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
