<!--

  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
  
  Copyright 2000, 2010 Oracle and/or its affiliates.
 
  OpenOffice.org - a multi-platform office productivity suite
 
  This file is part of OpenOffice.org.
 
  OpenOffice.org is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License version 3
  only, as published by the Free Software Foundation.
 
  OpenOffice.org is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License version 3 for more details
  (a copy is included in the LICENSE file that accompanied this code).
 
  You should have received a copy of the GNU Lesser General Public License
  version 3 along with OpenOffice.org.  If not, see
  <http://www.openoffice.org/license.html>
  for a copy of the LGPLv3 License.

-->

<!ENTITY nFO "http://www.w3.org/1999/XSL/Format">
<!ENTITY nXLink "http://www.w3.org/1999/xlink">
<!ENTITY nSVG "http://www.w3.org/2000/svg">

<!-- StarOffice namespace names and prefixes -->

<!ENTITY nOpenOffice "http://openoffice.org/2000">
<!ENTITY nOpenOffice2001 "http://openoffice.org/2001">

<!ENTITY nOffice "&nOpenOffice;/office">
<!ENTITY nStyle "&nOpenOffice;/style">
<!ENTITY nText "&nOpenOffice;/text">
<!ENTITY nTable "&nOpenOffice;/table">
<!ENTITY nMeta "&nOpenOffice;/meta">
<!ENTITY nScript "&nOpenOffice;/script">
<!ENTITY nDraw "&nOpenOffice;/drawing">
<!ENTITY nChart "&nOpenOffice;/chart">
<!ENTITY nNumber "&nOpenOffice;/datastyle">
<!ENTITY nConfig "&nOpenOffice2001;/config">


<!-- dublin core namespace name and prefic -->
<!ENTITY nDC "http://purl.org/dc/elements/1.1/">
