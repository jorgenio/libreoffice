/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __ooo_vba_word_XGlobals_idl__
#define __ooo_vba_word_XGlobals_idl__

#include <com/sun/star/uno/XInterface.idl>

module ooo {  module vba {  module word {
interface XDocument;
interface XWindow;
interface XSystem;
interface XOptions;
interface XSelection;
interface XGlobals : com::sun::star::uno::XInterface
{
    [attribute, readonly] XDocument ActiveDocument;
    [attribute, readonly] XWindow ActiveWindow;
    [attribute, readonly] string Name;
    [attribute, readonly] ooo::vba::word::XSystem System;
    [attribute, readonly] ooo::vba::word::XOptions Options;
    [attribute, readonly] ooo::vba::word::XSelection Selection;
    any CommandBars( [in] any Index );
    any Documents( [in] any Index );
    any Addins( [in] any Index );
    any Dialogs( [in] any Index );
    any ListGalleries( [in] any aIndex );
    float CentimetersToPoints([in] float Centimeters );
};

}; }; };

#endif


