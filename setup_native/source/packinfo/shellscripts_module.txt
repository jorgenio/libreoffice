%format deb

%postinstall << END
if [ "$$1" = "configure" ] ; then
  # update desktop database
  if [ -x /usr/bin/update-desktop-database ]; then
    update-desktop-database -q /usr/share/applications
  fi
  # update debian style menus
  if [ -x /usr/bin/update-menus ]; then
    update-menus
  fi
fi
exit 0
END

%postremove << END
if [ "$$1" != "purge" ]
then
  if [ -x /usr/bin/update-desktop-database ]
  then
    update-desktop-database -q /usr/share/applications
  fi
  if [ -x /usr/bin/update-menus ]
  then
    update-menus
  fi
fi
exit 0
END

%format all

