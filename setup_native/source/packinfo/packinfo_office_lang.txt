# Format:

# Start
# scpModule
# shellscript file (optional)
# solarispackagename
# solarisrequires (optional)
# solarisprovides (optional)
# packagename
# linuxreplaces (optional)
# requires (optional)
# provides (optional)
# copyright
# vendor
# description
# destpath
# End

Start
module = "gid_Module_Langpack_Basis"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core01"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-core01"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core01"
End

Start
module = "gid_Module_Langpack_Fonts"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-fonts"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-fonts"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-fonts"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Language fonts module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core01"
End

Start
module = "gid_Module_Langpack_Resource"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-res"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-res"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-res"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Language resource module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core01"
End

Start
module = "gid_Module_Langpack_Writer"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-writer"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-writer"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-writer"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Writer language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-writer"
End

Start
module = "gid_Module_Langpack_Calc"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-calc"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-calc"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-calc"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Calc language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-calc"
End

Start
module = "gid_Module_Langpack_Impress"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-impress"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-impress"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-impress"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Impress language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-impress"
End

Start
module = "gid_Module_Langpack_Draw"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-draw"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-draw"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-draw"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Draw language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-draw"
End

Start
module = "gid_Module_Langpack_Math"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-math"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-math"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-math"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Math language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-math"
End

Start
module = "gid_Module_Langpack_Base"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-base"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-base"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-base"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Base language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-base"
End

Start
module = "gid_Module_Langpack_Onlineupdate"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-onlineupd"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-onlineupd"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-onlineupd"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2009 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "Online update language module for LibreOffice %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
pkg_list_entry = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-onlineupdate"
End

Start
module = "gid_Module_Optional_Extensions_Sun_T_Pack"
script = "shellscripts_extensions.txt"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-extension-sun-templates-pack-%LANGUAGESTRING"
solarisrequires =  "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core01 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core02 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core03 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core04 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core05 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core06 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION"), %BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-core07 (Name="Core module for %PRODUCTNAME %PRODUCTVERSION")"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-extension-sun-templates-pack-%LANGUAGESTRING"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-core01,%BASISPACKAGEPREFIX%PRODUCTVERSION-core02,%BASISPACKAGEPREFIX%PRODUCTVERSION-core03,%BASISPACKAGEPREFIX%PRODUCTVERSION-core04,%BASISPACKAGEPREFIX%PRODUCTVERSION-core05,%BASISPACKAGEPREFIX%PRODUCTVERSION-core06,%BASISPACKAGEPREFIX%PRODUCTVERSION-core07"
linuxpatchrequires = ""
copyright = "1999-2008 by OpenOffice.org"
solariscopyright = "solariscopyrightfile"
vendor = "The Document Foundation"
description = "SUN Template Pack (%LANGUAGESTRING) %PRODUCTNAME %PRODUCTVERSION"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
End

# Language depended package definitions for OxygenOffice Professional

# 1/4 Templates

Start
module = "gid_Module_Optional_Accessories_Tem"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-templates"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-extra-templates"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-templates"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2008 by %PRODUCTNAME"
solariscopyright = "solariscopyrightfile"
vendor = "%PRODUCTNAME"
description = "Extra templates for %PRODUCTNAME %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
End

# 2/4 Samples

Start
module = "gid_Module_Optional_Accessories_Sam"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-samples"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-extra-samples"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-samples"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2008 by %PRODUCTNAME"
solariscopyright = "solariscopyrightfile"
vendor = "%PRODUCTNAME"
description = "Extra samples for %PRODUCTNAME %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
End

# 3/4 Documentations

Start
module = "gid_Module_Optional_Accessories_Doc"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-documentations"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-extra-documentations"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-documentations"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2008 by %PRODUCTNAME"
solariscopyright = "solariscopyrightfile"
vendor = "%PRODUCTNAME"
description = "Extra documentations for %PRODUCTNAME %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
End

# 4/4 Advertisement

Start
module = "gid_Module_Optional_Accessories_Adv"
solarispackagename = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-advertisement"
solarisrequires = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING"
packagename = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING-extra-advertisement"
provides = "%BASISPACKAGEPREFIX%WITHOUTDOTPRODUCTVERSION-%LANGUAGESTRING-extra-advertisement"
requires = "%BASISPACKAGEPREFIX%PRODUCTVERSION-%LANGUAGESTRING"
copyright = "1999-2008 by %PRODUCTNAME"
solariscopyright = "solariscopyrightfile"
vendor = "%PRODUCTNAME"
description = "Extra advertisement for %PRODUCTNAME %PRODUCTVERSION, language %LANGUAGESTRING"
destpath = "/opt"
packageversion = "%ABOUTBOXPRODUCTVERSION"
End

# END OF Language depended package definitions
