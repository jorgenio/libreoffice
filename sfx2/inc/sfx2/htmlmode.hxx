/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SFX_HTMLMODE_HXX_
#define _SFX_HTMLMODE_HXX_


#define HTMLMODE_ON                 0x0001
#define HTMLMODE_PARA_DISTANCE      0x0004
#define HTMLMODE_FRM_COLUMNS        0x0010
#define HTMLMODE_SOME_STYLES        0x0020 /* mind. MS IE */
#define HTMLMODE_FULL_STYLES        0x0040 /* == SW */
#define HTMLMODE_PARA_BLOCK         0x0100
#define HTMLMODE_SOME_ABS_POS       0x2000
#define HTMLMODE_RESERVED1          0x4000
#define HTMLMODE_RESERVED0          0x8000


#endif



















/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
