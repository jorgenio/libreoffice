/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 // include ---------------------------------------------------------------
#include <sfx2/sfx.hrc>
#include "dialog.hrc"
#include "templdlg.hrc"
#include "helpid.hrc"

ImageList DLG_STYLE_DESIGNER
{
    Prefix = "sc";
    MaskColor = Color { Red = 0xFFFF ; Green = 0x0000 ; Blue = 0xFFFF ; };
    IdList =
    {
        SID_STYLE_WATERCAN ;
        SID_STYLE_NEW_BY_EXAMPLE ;
        SID_STYLE_UPDATE_BY_EXAMPLE ;
    };
    IdCount = { 3 ; };
};

 // RID_STYLECATALOG ------------------------------------------------------
ModalDialog RID_STYLECATALOG
{
    HelpId = CMD_SID_STYLE_CATALOG ;
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( 181 , 134 ) ;
    Text [ en-US ] = "Style Catalog" ;
    Moveable = TRUE ;
    OKButton BT_OK
    {
        DefButton = TRUE ;
        Pos = MAP_APPFONT ( 123 , 6 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    CancelButton BT_CANCEL
    {
        Pos = MAP_APPFONT ( 123 , 23 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    PushButton BT_ORG
    {
        HelpID = "sfx2:PushButton:RID_STYLECATALOG:BT_ORG";
        Pos = MAP_APPFONT ( 123 , 97 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Organizer..." ;
        TabStop = TRUE ;
    };
    PushButton BT_DEL
    {
        HelpID = "sfx2:PushButton:RID_STYLECATALOG:BT_DEL";
        Pos = MAP_APPFONT ( 123 , 78 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Delete..." ;
        TabStop = TRUE ;
    };
    PushButton BT_EDIT
    {
        HelpID = "sfx2:PushButton:RID_STYLECATALOG:BT_EDIT";
        Pos = MAP_APPFONT ( 123 , 60 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Modify..." ;
        TabStop = TRUE ;
    };
    PushButton BT_NEW
    {
        HelpID = "sfx2:PushButton:RID_STYLECATALOG:BT_NEW";
        Pos = MAP_APPFONT ( 123 , 43 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~New..." ;
        TabStop = TRUE ;
    };
    HelpButton BT_HELP
    {
        Pos = MAP_APPFONT ( 123 , 114 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    ListBox BT_TOOL
    {
        HelpID = "sfx2:ListBox:RID_STYLECATALOG:BT_TOOL";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 6 ) ;
        Size = MAP_APPFONT ( 110 , 55 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
    };
    Control BT_VLIST
    {
        HelpId = HID_TEMPLATE_FMT ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 23 ) ;
        Size = MAP_APPFONT ( 110 , 89 ) ;
        TabStop = TRUE ;
    };
    ListBox BT_FLIST
    {
        HelpID = "sfx2:ListBox:RID_STYLECATALOG:BT_FLIST";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 114 ) ;
        Size = MAP_APPFONT ( 110 , 77 ) ;
        TabStop = TRUE ;
        Hide = TRUE ;
        DropDown = TRUE ;
    };
};

 // Strings ---------------------------------------------------------------
String STR_STYLE_ELEMTLIST
{
    Text [ en-US ] = "Style List" ;
};
String STR_STYLE_FILTER_HIERARCHICAL
{
    Text [ en-US ] = "Hierarchical" ;
};

 // DLG_STYLE_DESIGNER ----------------------------------------------------
DockingWindow DLG_STYLE_DESIGNER
{
    Text [ en-US ] = "Styles and Formatting" ;

    HelpId = CMD_SID_STYLE_DESIGNER ;
    OutputSize = TRUE ;
    Hide = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( 116 , 215 ) ;
    Pos = MAP_APPFONT ( 0 , 0 ) ;
    Closeable = TRUE ;
    Zoomable = TRUE ;
    Sizeable = TRUE ;
    Dockable = TRUE ;
    EnableResizing = TRUE ;
    Border = TRUE ;
    Moveable = TRUE ;
    ToolBox TB_ACTION
    {
        Pos = MAP_APPFONT ( 3 , 3 ) ;
        Size = MAP_APPFONT ( 57 , 42 ) ;
        LineCount = 1 ;
        ItemImageList = DLG_STYLE_DESIGNER ;
        HelpId = HID_TEMPLDLG_TOOLBOX_RIGHT ;
        ItemList =
        {
            ToolBoxItem
            {
                HelpId = HID_TEMPLDLG_WATERCAN ;
                Identifier = SID_STYLE_WATERCAN ;
                Text [ en-US ] = "Fill Format Mode" ;
            };
            ToolBoxItem
            {
                HelpId = HID_TEMPLDLG_NEWBYEXAMPLE ;
                Identifier = SID_STYLE_NEW_BY_EXAMPLE ;
                Text [ en-US ] = "New Style from Selection" ;
            };
            ToolBoxItem
            {
                HelpId = HID_TEMPLDLG_UPDATEBYEXAMPLE ;
                Identifier = SID_STYLE_UPDATE_BY_EXAMPLE ;
                Text [ en-US ] = "Update Style" ;
            };
        };
    };
};


















