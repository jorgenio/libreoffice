/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <childlist.hxx>

#include <libxml/tree.h>

#include <node.hxx>
#include <document.hxx>


namespace DOM
{
    CChildList::CChildList(::rtl::Reference<CNode> const& pBase,
                ::osl::Mutex & rMutex)
        : m_pNode(pBase)
        , m_rMutex(rMutex)
    {
    }

    /**
    The number of nodes in the list.
    */
    sal_Int32 SAL_CALL CChildList::getLength() throw (RuntimeException)
    {
        ::osl::MutexGuard const g(m_rMutex);

        sal_Int32 length = 0;
        if (m_pNode != NULL)
        {
            xmlNodePtr cur = m_pNode->GetNodePtr();
            if (0 != cur) {
                cur = cur->children;
            }
            while (cur != NULL)
            {
                length++;
                cur = cur->next;
            }
        }
        return length;

    }
    /**
    Returns the indexth item in the collection.
    */
    Reference< XNode > SAL_CALL CChildList::item(sal_Int32 index)
        throw (RuntimeException)
    {
        ::osl::MutexGuard const g(m_rMutex);

        if (m_pNode != NULL)
        {
            xmlNodePtr cur = m_pNode->GetNodePtr();
            if (0 != cur) {
                cur = cur->children;
            }
            while (cur != NULL)
            {
                if (index-- == 0) {
                    return Reference< XNode >(
                            m_pNode->GetOwnerDocument().GetCNode(cur).get());
                }
                cur = cur->next;
            }
        }
        return 0;
    }
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
