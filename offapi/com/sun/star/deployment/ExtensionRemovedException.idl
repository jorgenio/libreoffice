/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_EXTENSIONREMOVEDEXCEPTION_IDL
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_EXTENSIONREMOVEDEXCEPTION_IDL

#include "com/sun/star/uno/Exception.idl"

module com { module sun { module star { module deployment {

interface XPackage;

/**
   indicates that a function call with the given arguments is not supported
   because the extension was removed. <member>XPackage::isRemoved</member> will
   return true on that object.

   @since OOo 3.3
*/
exception ExtensionRemovedException: com::sun::star::uno::Exception {

};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
