/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_XGridFieldDataSupplier_idl__
#define __com_sun_star_form_XGridFieldDataSupplier_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/reflection/XIdlClass.idl>


//=============================================================================

 module com {  module sun {  module star {  module form {

//=============================================================================

/** provides access to the data of a GridControl

    <p>You can retrieve the data type information and the data in a row.</p>

    <p>This interface allows to retrieve data even for rows which are not current, which is quite useful,
    as normally, you can't affect the current row in a grid control without moving the cursor of the underlying
    <type scope="com::sun::star::form::component">DataForm</type>.</p>

    @see XGrid
    @see com::sun::star::form::control::GridControl
    @see com::sun::star::form::component::GridControl

    @deprecated
 */
published interface XGridFieldDataSupplier: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** checks whether or not the content of the grid's columns can be retrieved in the requested format.

        <p>Not every cell content can be retrieved in every representation. For example, in a text column,
        you usually won't be able to retrieve the content as double.<br/>
        To check if the type you need is supported by the columns, use this method.</p>

        @return
            A sequence of boolean flags. Each flag corresponds to the respective column, and <TRUE/> indicates
            that the column content can be retrieved in the requested format, <FALSE/> denies this.

        @see XGridColumnFactory
        @see DataAwareControlModel
        @see XGridFieldDataSupplier::queryFieldData
    */
    sequence<boolean> queryFieldDataType( [in] type xType );

    //-------------------------------------------------------------------------
    /** retrieves the actual column data for the given row

        <p>If a column does not support the requested type, <NULL/> is returned at the respective
        position.</p>

        @see XGridFieldDataSupplier::queryFieldDataType
    */
    sequence<any> queryFieldData( [in] long nRow,
             [in] type xType );
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
