/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_XInsertListener_idl__
#define __com_sun_star_form_XInsertListener_idl__

#include <com/sun/star/lang/XEventListener.idl>

#include <com/sun/star/lang/EventObject.idl>


//=============================================================================

 module com {  module sun {  module star {  module form {

//=============================================================================

/** allows to receive notifications about insertions into a database form.

    <p>Please do <em><b>not</b></em> use anymore, this interface is deprecated, and
    superseded by functionality from the <type scope="com::sun::star::form::component">DataForm</type>
    service, as well as the <type scope="com::sun::star::sdbc">XRowSetListener</type> and
    <type scope="com::sun::star::sdb">XRowSetApproveListener</type> interfaces.</p>

    @deprecated
 */
published interface XInsertListener: com::sun::star::lang::XEventListener
{
    //-------------------------------------------------------------------------

    /** is invoked when a database form starts inserting a record.
     */
    void inserting( [in] com::sun::star::lang::EventObject aEvent );

    //-------------------------------------------------------------------------

    /** is invoked after a database form has inserted a record to a data source.
     */
    void inserted( [in] com::sun::star::lang::EventObject aEvent );

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
