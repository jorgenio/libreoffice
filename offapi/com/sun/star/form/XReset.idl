/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_form_XReset_idl__
#define __com_sun_star_form_XReset_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/form/XResetListener.idl>


//=============================================================================

 module com {  module sun {  module star {  module form {

//=============================================================================

/** provides functionality to reset components to some default values.

    <p>The semantics of <em>default value</em> depends on the providing service.</p>
 */
published interface XReset: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** resets a component to some default value.
     */
    [oneway] void reset();

    //-------------------------------------------------------------------------

    /** adds the specified listener to receive events related to resetting the
        component.

        @param aListener
            the listener to add.
     */
    [oneway] void addResetListener( [in] com::sun::star::form::XResetListener aListener );

    //-------------------------------------------------------------------------

    /** removes the specified listener

        @param aListener
            the listener to remove
     */
    [oneway] void removeResetListener( [in] com::sun::star::form::XResetListener aListener );

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
