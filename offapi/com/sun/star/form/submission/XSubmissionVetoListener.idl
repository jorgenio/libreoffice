/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_submission_XSubmissionVetoListener_idl__
#define __com_sun_star_form_submission_XSubmissionVetoListener_idl__

#include <com/sun/star/lang/XEventListener.idl>
#include <com/sun/star/util/VetoException.idl>

//=============================================================================

module com {  module sun {  module star {  module form { module submission {

//=============================================================================

/** is implement by components which want to observe (and probably veto) the
    submission of data.

    @see XSubmission
*/
interface XSubmissionVetoListener : com::sun::star::lang::XEventListener
{
    /** is invoked when a component, at which the listener has been registered,
        is about to submit its data.

        @param event
            The submission event. The <member scope="com::sun::star::lang">EventObject::Source</member>
            member of the structure describes the component which is about to submit its data.

        @throws com::sun::star::util::VetoException
            when the submission is vetoed. <member scope="com::sun::star::uno">Exception::Message</member>
            should contain a justification for the veto then.
     */
    void    submitting( [in] com::sun::star::lang::EventObject event )
                raises ( com::sun::star::util::VetoException );

};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
