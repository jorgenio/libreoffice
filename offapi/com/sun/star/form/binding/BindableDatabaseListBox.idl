/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_binding_BindableDatabaseListBox_idl__
#define __com_sun_star_form_binding_BindableDatabaseListBox_idl__

#include <com/sun/star/form/component/DatabaseListBox.idl>
#include <com/sun/star/form/binding/BindableDataAwareControlModel.idl>

//=============================================================================

module com {  module sun {  module star {  module form {  module binding {

//=============================================================================

/** This service specifies a list box model which is data-aware and thus can be bound to a
    database field, and additionally supports binding to arbitrary external values.

    <p>There are four possible ways how a <type>BindableDatabaseListBox</type> exchanges
    values with an external binding, the following lists explains all of them. If a new binding
    is set at a <type>BindableDatabaseListBox</type>, the types from the following list are
    tried in descending order: The first type supported by the binding is used for data exchange.
    <ol><li><em>sequences of integers</em>: When used, the integers in the sequence will represent
            the indexes of the selected entries of the list box.</li>
        <li><em>integer values</em>: When used, the value will represent the index of the selected entry.
            If no entry is selected, -1 will be transfered. If more than one entry is selected, <NULL/>
            will be transfered.</li>
        <li><em>sequences of strings</em>: When used, the strings in the sequence present the texts
            of the selected entries of the list box.</li>
        <li><em>string values</em>: When used. the value will represent the text of the selected entry.
            If no entry is selected, an empty string will be transfered. If more than one entry is selected,
            <NULL/> will be transfered.</li>
    </ol></p>

    @see com::sun::star::form::binding::XValueBinding::supportsType
 */
service BindableDatabaseListBox
{
    service com::sun::star::form::component::DatabaseListBox;

    /** specifies the interaction between an internal binding to a database column,
        and an external value binding.</p>
    */
    service com::sun::star::form::binding::BindableDataAwareControlModel;

};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
