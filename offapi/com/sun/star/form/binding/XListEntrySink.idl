/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_binding_XListEntrySink_idl__
#define __com_sun_star_form_binding_XListEntrySink_idl__

#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com {  module sun {  module star {  module form { module binding {

interface XListEntrySource;

//=============================================================================

/** specifies support for indirect manipulation of of a string list
*/
interface XListEntrySink : com::sun::star::uno::XInterface
{
    /** sets the new source for the list entries of the component

        <p>The list represented by this component will be cleared, and initially
        filled with the entries from the new list source.</p>

        @param Source
            the new source for the list entries. May be <NULL/>, in this
            case, the current source is revoked.
    */
    void    setListEntrySource( [in] XListEntrySource Source );

    /** retrieves the current source for the list entries of the component.
    */
    XListEntrySource
            getListEntrySource( );
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
