/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_binding_BindableDatabaseTimeField_idl__
#define __com_sun_star_form_binding_BindableDatabaseTimeField_idl__

#include <com/sun/star/form/component/DatabaseTimeField.idl>
#include <com/sun/star/form/binding/BindableDataAwareControlModel.idl>

//=============================================================================

module com {  module sun {  module star {  module form {  module binding {

//=============================================================================

/** This service specifies a date input field which is data-aware and thus can be bound to a
    database field, and additionally supports binding to arbitrary external values.

    <p>If an <type scope="com::sun::star::form::binding">ValueBinding</type> instance is
    set at the field, it will exchange it's content with the binding as <type scope="com::sun::star::util">Time</type>.
    </p>

    @see com::sun::star::form::binding::XValueBinding::supportsType
    @see com::sun::star::awt::UnoControlTimeFieldModel::Time
 */
service BindableDatabaseTimeField
{
    service com::sun::star::form::component::DatabaseTimeField;

    /** specifies the interaction between an internal binding to a database column,
        and an external value binding.</p>
    */
    service com::sun::star::form::binding::BindableDataAwareControlModel;

};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
