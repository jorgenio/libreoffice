/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_form_binding_XBindableValue_idl__
#define __com_sun_star_form_binding_XBindableValue_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/form/binding/IncompatibleTypesException.idl>

//=============================================================================

module com {  module sun {  module star {  module form { module binding {

interface XValueBinding;

//=============================================================================

/** specifies support for being bound to an external value

    @see XValueBinding
*/
interface XBindableValue : com::sun::star::uno::XInterface
{
    /** sets an external instance which controls the value of the component

        <p>Any previously active binding will be revoked. There can be only one!</p>

        @param aBinding
            the new binding which is to be used by the component. May be <NULL/>,
            in this case only the current binding is revoked.

        @throws <type>IncompatibleTypesException</type>
            if the new binding (provided it's not <NULL/>) supports only types
            which are incompatible with the types of the bindable component.
     */
    void    setValueBinding( [in] XValueBinding aBinding )
                raises ( IncompatibleTypesException );

    /** retrieves the external instance which currently controls the value of the
        component
    */
    XValueBinding
            getValueBinding( );
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
