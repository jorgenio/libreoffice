/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef com_sun_star_graphic_XGraphic_idl
#define com_sun_star_graphic_XGraphic_idl

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/graphic/GraphicType.idl>

module com { module sun { module star { module graphic
{

/** This interface acts as a container for the loaded graphic.

    <p>The interface itself can be retrieved by using the appropriate
    methods of <type>XGraphicProvider</type> interface.
    <type>XGraphicProvider</type> also offers a method to store
    the graphic content at a specific location</p>

    <p>To render the graphic content onto a specific device, you
    have to create a <type>XGraphicRenderer</type> interface and pass
    this interface appropriately</p>

    @see XGraphicProvider
    @see XGraphicRenderer
  */
published interface XGraphic : ::com::sun::star::uno::XInterface
{
    /** Get the type of the contained graphic

        @returns
            The type of the contained graphic

        @see GraphicType
    */
    byte getType();
};

} ; } ; } ; } ;

#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
