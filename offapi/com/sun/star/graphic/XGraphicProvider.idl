/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef com_sun_star_graphic_XGraphicProvider_idl
#define com_sun_star_graphic_XGraphicProvider_idl

#include <com/sun/star/io/XInputStream.idl>
#include <com/sun/star/io/XOutputStream.idl>
#include <com/sun/star/io/IOException.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/lang/WrappedTargetException.idl>
#include <com/sun/star/beans/PropertyValues.idl>
#include <com/sun/star/beans/XPropertySet.idl>
#include <com/sun/star/graphic/XGraphic.idl>

module com { module sun { module star { module graphic
{

/** This interface acts as the main interface to handle graphic
    content. It is used to load graphics, store graphics and
    to get information about unloaded graphics
  */
published interface XGraphicProvider : ::com::sun::star::uno::XInterface
{
    /** Calling this method returns a
        <type scope="com::sun::star::beans">XPropertySet</type>
        interface that gives access to the properties of the
        unloaded graphic

        <p>In most cases, this method will be used to query the
        mime type of the graphic and, in the case of pixel graphics,
        the resulting size after loading</p>

        @param MediaProperties
            A sequence of property values to describe the location
            of the graphic, for which the attributes should be returned

        @returns
            A <type scope="com::sun::star::beans">XPropertySet</type> interface
            to get access to the different graphic properties

        @see MediaProperties
        @see GraphicDescriptor
        @see com::sun::star::beans::PropertyValues
    */
    ::com::sun::star::beans::XPropertySet   queryGraphicDescriptor( [in] ::com::sun::star::beans::PropertyValues MediaProperties )
                                                raises( ::com::sun::star::io::IOException,
                                                        ::com::sun::star::lang::IllegalArgumentException,
                                                        ::com::sun::star::lang::WrappedTargetException );



    /** Calling this method returns a <type>XGraphic</type> interface
        that holds the graphic content after loading the graphic

        @param MediaProperties
            A sequence of property values to describe the location
            of the graphic from which the graphic is to be loaded

        @returns
            The <type>XGraphic</type> interface

        @see MediaProperties
        @see XGraphic
        @see com::sun::star::beans::PropertyValues
    */
    XGraphic    queryGraphic( [in] ::com::sun::star::beans::PropertyValues MediaProperties )
                     raises( ::com::sun::star::io::IOException,
                              ::com::sun::star::lang::IllegalArgumentException,
                             ::com::sun::star::lang::WrappedTargetException );

    /** Store the graphic content, represented through the <type>XGraphic</type>
        interface at the specified location

        @param Graphic
            The graphic that should be stored

        @param MediaProperties
            A sequence of property values to describe the destination
            location of the graphic

        @see XGraphic
        @see MediaProperties
        @see com::sun::star::beans::PropertyValues
    */
    void        storeGraphic( [in] ::com::sun::star::graphic::XGraphic Graphic,
                              [in] ::com::sun::star::beans::PropertyValues MediaProperties  )
                    raises( ::com::sun::star::io::IOException,
                             ::com::sun::star::lang::IllegalArgumentException,
                            ::com::sun::star::lang::WrappedTargetException );
};

} ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
