/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_animations_XAnimateSet_idl__
#define __com_sun_star_animations_XAnimateSet_idl__

#include <com/sun/star/animations/XAnimate.idl>

//=============================================================================

 module com {  module sun {  module star {  module animations {

//=============================================================================

/** Interface for animation by simply setting the value of the target attribute to a constant value.
    This interface provides a simple means of just setting the value of an attribute for
    a specified duration.

    When using XAnimateSet, the following members are ignored
    <ul>
    <li><member>XAnimate::Values</member></li>
    <li><member>XAnimate::KeyTimes</member></li>
    <li><member>XAnimate::CalcMode</member></li>
    <li><member>XAnimate::Accumulate</member></li>
    <li><member>XAnimate::Additive</member></li>
    <li><member>XAnimate::From</member></li>
    <li><member>XAnimate::By</member></li>
    <li><member>XAnimate::TimeFilter</member></li>
    </ul>

    @see http://www.w3.org/TR/smil20/animation.html#edef-set
*/
interface XAnimateSet : XAnimate
{
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
