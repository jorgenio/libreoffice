/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_document_XEmbeddedScripts_idl__
#define __com_sun_star_document_XEmbeddedScripts_idl__

#include <com/sun/star/script/XStorageBasedLibraryContainer.idl>

//=============================================================================

module com { module sun { module star { module document {

//=============================================================================

/** is supported by <type>OfficeDocument</type>s which allow to embed scripts

    @since OOo 2.4
 */
interface XEmbeddedScripts
{
    /** is the container of <code>StarBasic</code> macro libraries contained in the document
    */
    [attribute, readonly]   ::com::sun::star::script::XStorageBasedLibraryContainer BasicLibraries;

    /** is the container of dialogs libraries contained in the document
    */
    [attribute, readonly]   ::com::sun::star::script::XStorageBasedLibraryContainer DialogLibraries;

    /** determines whether executing macros from this document is allowed.

        <p>Effectively, this attribute is an evaluation of the document's <type>MacroExecMode</type>
        against possibly applicable configuration settings, the document location in relation to the
        trusted location, and the like.</p>

        @see MacroExecMode
    */
    [attribute, readonly]   boolean AllowMacroExecution;
};

//=============================================================================

}; }; }; };

//=============================================================================

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
