/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_DrawPage_idl__
#define __com_sun_star_drawing_DrawPage_idl__

#include <com/sun/star/drawing/GenericDrawPage.idl>

#include <com/sun/star/drawing/XMasterPageTarget.idl>
#include <com/sun/star/form/XFormsSupplier.idl>


//=============================================================================

 module com {  module sun {  module star {  module drawing {

//=============================================================================

// DocMerge from xml: service com::sun::star::drawing::DrawPage
/** This service specifies a page for the actual draw pages to contain
    the drawings.
    @see    GenericDrawPage
    @see    DrawPages
 */
published service DrawPage
{


    // DocMerge from xml: service com::sun::star::drawing::DrawPage: service com::sun::star::drawing::GenericDrawPage
    /** This service describes the base functionality that every
                draw/master page has.
     */
    service com::sun::star::drawing::GenericDrawPage;


    // DocMerge from xml: service com::sun::star::drawing::DrawPage: interface com::sun::star::drawing::XMasterPageTarget
    /** provides the link to a <type>MasterPage</type>.
     */
    interface com::sun::star::drawing::XMasterPageTarget;

    /** provides access to the hierarchy of form components belonging to the draw page.

        <p>Every draw page may contain a form layer - that is, a hierarchy of form elements. The layer can be
        accessed using this interface.</p>

        @see com.sun.star.form.FormComponent
        @see com.sun.star.form.FormComponents
        @see com.sun.star.form.Forms
    */
    [optional] interface com::sun::star::form::XFormsSupplier;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
