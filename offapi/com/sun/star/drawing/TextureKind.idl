/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_TextureKind_idl__
#define __com_sun_star_drawing_TextureKind_idl__


//=============================================================================

 module com {  module sun {  module star {  module drawing {

//=============================================================================

// DocMerge from xml: enum com::sun::star::drawing::TextureKind
/** These enumeration values specify how the texture is applied.
    @deprecated
 */
published enum TextureKind
{
    //-------------------------------------------------------------------------

    // DocMerge from xml: value com::sun::star::drawing::TextureKind::LUMINANCE
    /** With <type>TextureKind</type> <const>LUMINANCE</const>, the texture
        and the lighting information are    mixed to produce the image, so a
        lit, textured object is achieved.
     */
    LUMINANCE,

    //-------------------------------------------------------------------------

    // DocMerge from xml: value com::sun::star::drawing::TextureKind::COLOR
    /** With this mode the lighting is ignored and only the
        texture color information is used.
     */
    COLOR

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
