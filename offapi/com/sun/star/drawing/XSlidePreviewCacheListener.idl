/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_drawing_XSlidePreviewCacheListener_idl__
#define __com_sun_star_drawing_XSlidePreviewCacheListener_idl__

#ifndef __com_sun_star_uno_XInterface_idl__
#include <com/sun/star/uno/XInterface.idl>
#endif

module com { module sun { module star { module drawing {

/** Listener for asynchronous preview creations.  Called when a slide
    preview has been created that was previously requested via a call to
    <method>XSlidePreviewCache::getSlidePreview()</method>.  The
    implementor may then call <method>getSlidePreview()</method> a second
    time to get the up-to-date version of the preview.
*/
interface XSlidePreviewCacheListener
{
    /** Called by a <interface>XSlidePreviewCache</interface> object when a
        preview has been created for the slide with the given index.
        @param nSlideIndex
            The index of the slide for which a new preview has been created.
    */
    void notifyPreviewCreation ([in] long nSlideIndex);
};

}; }; }; }; // ::com::sun::star::drawing

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
