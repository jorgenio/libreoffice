/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_drawing_SlideSorter_idl__
#define __com_sun_star_drawing_SlideSorter_idl__

#include <com/sun/star/drawing/framework/XView.idl>
#include <com/sun/star/drawing/XDrawView.idl>
#include <com/sun/star/awt/XWindow.idl>
#include <com/sun/star/container/XIndexAccess.idl>
#include <com/sun/star/frame/XController.idl>
#include <com/sun/star/util/Color.idl>
#include <com/sun/star/drawing/XSlideSorterBase.idl>

module com {  module sun {  module star {  module drawing {


/** A slide sorter shows previews for a set of slides, typically all slides
    in a document, and allows the selection, reordering, creation, and
    deletion of slides.
    <p>In the drawing framework a slide sorter is regarded as a view.</p>
*/
service SlideSorter : XSlideSorterBase
{
    /** Create a new slide sorter object.
        @param xViewId
            The resource id of the new slide sorter.
        @param xController
            The access point to an impress document.
        @param xParentWindow
            The parent window which will be completely covered by the new
            slide sorter.
    */
    create (
        [in] framework::XResourceId xViewId,
        [in] ::com::sun::star::frame::XController xController,
        [in] ::com::sun::star::awt::XWindow xParentWindow);
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
