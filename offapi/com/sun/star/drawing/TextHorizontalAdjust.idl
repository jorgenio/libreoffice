/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_drawing_TextHorizontalAdjust_idl__
#define __com_sun_star_drawing_TextHorizontalAdjust_idl__


//=============================================================================

 module com {  module sun {  module star {  module drawing {

//=============================================================================

/** This enumeration specifies the horizontal position of text inside a shape in
    relation to the shape.
    <p>This counts for the complete text, not individual lines. </p>
 */
published enum TextHorizontalAdjust
{
    //-------------------------------------------------------------------------

    /** The left edge of the text is adjusted to the left edge of the shape.
     */
    LEFT,

    //-------------------------------------------------------------------------

    /** The text is centered inside the shape.
     */
    CENTER,

    //-------------------------------------------------------------------------

    /** The right edge of the text is adjusted to the right edge of the shape.
     */
    RIGHT,

    //-------------------------------------------------------------------------

    /** The text extends from the left to the right edge of the shape.
     */
    BLOCK
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
