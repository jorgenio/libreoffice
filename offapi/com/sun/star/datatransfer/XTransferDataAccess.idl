/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_XTransferDataInfo_idl__
#define __com_sun_star_datatransfer_XTransferDataInfo_idl__

#include <com/sun/star/datatransfer/DataFlavor.idl>
#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com { module sun { module star { module datatransfer {

//=============================================================================
/** This interface provides direct access to the data in all data flavors. It can
    be used by the clipboard implementation to optimize data transport on
    flush operations.

    @see com::sun::star::datatransfer::XTransferable
*/

published interface XTransferDataAccess: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** To query for the summarized data size in bytes of a sequence of <type>DataFlavor</type>.

        @param aFlavorList
        A sequence of requested <type>DataFlavor</type>.
        <p>An unsupported <type>DataFlavor</type> will be ignored.</p>

        @returns
        The number of bytes of the transfer data in the specified sequence of <type>DataFlavor</type>.
    */
    hyper queryDataSize( [in] sequence < DataFlavor > aFlavorList );

    //-------------------------------------------------------------------------
    /** To get all the data of a sequence of <type>DataFlavor</type>.

        @param aFlavorList
        The sequence of requested <type>DataFlavor</type>.
        <p>An unsupported <type>DataFlavor</type> will be ignored.</p>

        @returns
        The data in the requested <type>DataFlavor</type>.
        <p>For unsupported <type>DataFlavor</type> an
        empty any will be returned.</p>
     */
    sequence < any > getData( [in] sequence < DataFlavor > aFlavorList );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
