/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_dnd_DropTargetDropEvent_idl__
#define __com_sun_star_datatransfer_dnd_DropTargetDropEvent_idl__

#include <com/sun/star/datatransfer/dnd/DropTargetEvent.idl>
#include <com/sun/star/datatransfer/XTransferable.idl>

//=============================================================================

module com { module sun { module star { module datatransfer { module dnd {

 published interface XDropTargetDropContext;

//=============================================================================
/** The <type>DropTargetDropEvent</type> is delivered from the drop target to
    its currently registered drop target listener.

    <p>It contains sufficient information for the originator of the operation to
    provide appropriate feedback to the end user when the operation completes.</p>
*/

published struct DropTargetDropEvent: com::sun::star::datatransfer::dnd::DropTargetEvent
{
    //-------------------------------------------------------------------------
    /** The drop target context of the current drag operation.

        @see com::sun::star::datatransfer::dnd::XDropTargetDropContext
     */

    XDropTargetDropContext Context;

    //-------------------------------------------------------------------------
    /** This value represents the action or actions selected by the user at
        the time of the drop.

        <p>If more than one action is specified, the <type>XDropTargetListener</type>
         should raise a dialog to ask the user which action to use.</p>

         @see com::sun::star::datatransfer::dnd::DNDConstants
     */

    byte DropAction;

    //-------------------------------------------------------------------------
    /** The cursor's current x location within the window's coordinates.
     */

    long LocationX;

    //-------------------------------------------------------------------------
    /** The cursor's current y location within the window's coordinates.
     */

    long LocationY;

    //-------------------------------------------------------------------------
    /** This value represents the action or actions supported by the source.
     */

    byte SourceActions;

    //-------------------------------------------------------------------------
    /** The transferable object associated with the drop.

        @see com::sun::star::datatransfer::XTransferable
     */

    com::sun::star::datatransfer::XTransferable Transferable;
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
