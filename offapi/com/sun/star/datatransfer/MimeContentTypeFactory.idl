/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_MimeContentType_idl__
#define __com_sun_star_datatransfer_MimeContentType_idl__

#include <com/sun/star/lang/XComponent.idl>
#include <com/sun/star/lang/XServiceInfo.idl>
#include <com/sun/star/lang/XTypeProvider.idl>

//=============================================================================

module com { module sun { module star { module datatransfer {

//=============================================================================

 published interface XMimeContentTypeFactory;

//=============================================================================
/** Used to create an instance that implement the interface
    <type>XMimeContentType</type> from a string
    representation of such a MIME content-type that is conform to
    <a href="ftp://ftp.isi.edu/in-notes/rfc2045.txt">Rfc2045</a> and
    <a href="ftp://ftp.isi.edu/in-notes/rfc2046.txt">Rfc2046</a>.

    @see com::sun::star::datatransfer::XMimeContentTypeFactory
*/
published service MimeContentTypeFactory
{
    //-------------------------------------------------------------------------
    /** Creates an instance that implement the interface
        <type>XMimeContentType</type> from the string
        representation of such a MIME content-types that is conform to
        <a href="ftp://ftp.isi.edu/in-notes/rfc2045.txt">Rfc2045</a>,
        <a href="ftp://ftp.isi.edu/in-notes/rfc2046.txt">Rfc2046</a>.
    */
    interface XMimeContentTypeFactory;

    //-------------------------------------------------------------------------
    /** Service should always support this interface.
    */
    interface com::sun::star::lang::XServiceInfo;

    //-------------------------------------------------------------------------
    /** Service should always support this interface.
    */
    interface com::sun::star::lang::XTypeProvider;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
