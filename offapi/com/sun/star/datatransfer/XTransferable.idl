/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_datatransfer_XTransferable_idl__
#define __com_sun_star_datatransfer_XTransferable_idl__

#include <com/sun/star/datatransfer/UnsupportedFlavorException.idl>
#include <com/sun/star/datatransfer/DataFlavor.idl>
#include <com/sun/star/io/IOException.idl>
#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com { module sun { module star { module datatransfer {

//=============================================================================
/** Interface to be implemented by objects used to provide data for a data
    transfer operation.

    @see com::sun::star::datatransfer::DataFlavor
*/

published interface XTransferable: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** Called by a data consumer to obtain data from the source in a specified
        format.

        @param aFlavor
        Describes the requested data format

        @returns
        The data in the specified <type>DataFlavor</type>.

        @throws com::sun::star::io::IOException
        if the data is no longer available in the requested flavor.

        @throws com::sun::star::datatransfer::UnsupportedFlavorException
        if the requested <type>DataFlavor</type> is not supported.
    */
    any getTransferData( [in] DataFlavor aFlavor )
        raises ( UnsupportedFlavorException, com::sun::star::io::IOException );

    //-------------------------------------------------------------------------
    /** Returns a sequence of supported <type>DataFlavor</type>.

        @returns
        The sequence of supported <type>DataFlavor</type>.

        @see com::sun::star::datatransfer::DataFlavor
    */
    sequence < DataFlavor > getTransferDataFlavors();

    //-------------------------------------------------------------------------
    /** Checks if the data object supports the specified data flavor.

        @param aFlavor
        Describes the format that should be checked

        @returns
        A value of <TRUE/> if the <type>DataFlavor</type> is supported by the transfer source.
        <p>A value of <FALSE/> if the <type>DataFlavor</type> is unsupported by the transfer source.</p>

        <br/><br/><p><strong>Note: </strong>This method is only for analogy with the JAVA Clipboard interface. To
        avoid many calls, the caller should instead use
        <member scope="com::sun::star::datatransfer">XTransferable::getTransferDataFlavors()</member>.
    */
    boolean isDataFlavorSupported( [in] DataFlavor aFlavor );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
