/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_XHatchWindow_idl__
#define __com_sun_star_embed_XHatchWindow_idl__

#include <com/sun/star/lang/XComponent.idl>
#include <com/sun/star/embed/XHatchWindowController.idl>
#include <com/sun/star/awt/Size.idl>

//============================================================================

 module com {  module sun {  module star {  module embed {

//============================================================================

/** specifies the operations for a hatch window.

    <p>
    A hatch window is a kind of window that is adopted to contain
    an embedded object window to represent the contained window border
    and to handle resizing/moving in a specific way: after user have
    selected the new size/placement the hatching window sends request
    to owner for resizing/moving.
    Thus the window can not resize/move itself.
    </p>
 */
published interface XHatchWindow: com::sun::star::lang::XComponent
{
    //------------------------------------------------------------------------
    /** sets the object that will control resizing/moving, if the object is
    not set the window can not be resized/moved.
     */
    void setController( [in] XHatchWindowController xController );

    [attribute] com::sun::star::awt::Size HatchBorderSize;
};

//============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
