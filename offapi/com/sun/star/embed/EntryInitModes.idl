/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_EntryInitModes_idl__
#define __com_sun_star_embed_EntryInitModes_idl__


//============================================================================

module com { module sun { module star { module embed {

//============================================================================
/** This constant set contains possible modes to initialize object
    persistence.

    @see XEmbedPersist
*/
published constants EntryInitModes
{
    // -----------------------------------------------------------------------
    /** In case object persistence is created based on existing entry,
        the object should be initialized from this entry. Otherwise
        the object should be initialized as a new one.
     */
    const long DEFAULT_INIT = 0;

    // -----------------------------------------------------------------------
    /** The object should be initialized as a new empty one.
     */
    const long TRUNCATE_INIT = 1;

    // -----------------------------------------------------------------------
    /** The object should be initialized as a new one only in case it still
        was not initialized. If the object initialized already do not
        reinitialize it.
     */
    const long NO_INIT = 2;

    // -----------------------------------------------------------------------
    /** The object should be initialized using additional arguments from
        provided
        <type scope="com::sun::star::document">MediaDescriptor</type>.
     */
    const long MEDIA_DESCRIPTOR_INIT = 3;

    // -----------------------------------------------------------------------
    /** The object should be initialized as a link using URL provided in
        additional arguments.
     */
    const long URL_LINK_INIT = 4;
};

//============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
