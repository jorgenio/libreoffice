/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_EmbedMapUnits_idl__
#define __com_sun_star_embed_EmbedMapUnits_idl__


//============================================================================

module com { module sun { module star { module embed {

//============================================================================
/** contains list of possible map modes supported by embedded object.

    @see XVisualObject
*/
published constants EmbedMapUnits
{
    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one-hundredth
        of millimeter.
     */
    const long ONE_100TH_MM = 0;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one-tenth
        of millimeter.
     */
    const long ONE_10TH_MM = 1;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one
        millimeter.
     */
    const long ONE_MM = 2;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one
        centimeter.
     */
    const long ONE_CM = 3;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one-thousandth
        of inch.
     */
    const long ONE_1000TH_INCH = 4;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one-hundredth
        of inch.
     */
    const long ONE_100TH_INCH = 5;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one-tenth
        of inch.
     */
    const long ONE_10TH_INCH = 6;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one inch.
     */
    const long ONE_INCH = 7;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one
        typographical point.
     */
    const long POINT = 8;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one twentieth
        of typographical point.
     */
    const long TWIP = 9;

    // ----------------------------------------------------------------------
    /** In this type of map mode one logical point is equal to one pixel.
     */
    const long PIXEL = 10;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
