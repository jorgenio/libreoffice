/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_XHierarchicalStorageAccess_idl__
#define __com_sun_star_embed_XHierarchicalStorageAccess_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/io/XStream.idl>
#include <com/sun/star/embed/XExtendedStorageStream.idl>
#include <com/sun/star/io/IOException.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/embed/StorageWrappedTargetException.idl>
#include <com/sun/star/embed/InvalidStorageException.idl>
#include <com/sun/star/packages/WrongPasswordException.idl>
#include <com/sun/star/container/XNameAccess.idl>
#include <com/sun/star/container/NoSuchElementException.idl>
#include <com/sun/star/container/ElementExistException.idl>
#include <com/sun/star/packages/NoEncryptionException.idl>


//============================================================================

 module com {  module sun {  module star {  module embed {

//============================================================================
/** This interface allows hierarchical access to storage tree.

    <p>
    Currently only streams can be opened using this interface.
    </p>

    <p>
    The hierarchical access can not be mixed with a normal access. Thus when
    in a storage a stream with a path "a/b/c" is accessed using hierarchical
    access, another stream "a/b/d" can also be opened with hierarchical
    access ( if it is still not opened ), but the substorage "a" can not be
    opened ( it is locked by hierarchical access ).
    </p>
 */
published interface XHierarchicalStorageAccess
{
    // METHODS
    //
    // -----------------------------------------------------------------------
    /** allows to get access to a child stream of the storage, using
        hierarchical path.

        <p>
        In case the stream is open in readonly mode the
        <method scope="com::sun::star::io">XStream::getOutputStream</method>
        method will return an empty reference.
        </p>

        @param sStreamPath
            the path to the substream that should be open

        @param nOpenMode
            a mode the stream should be open in,
            can be a combination of <type>ElementModes</type> values

        @throws ::com::sun::star::embed::InvalidStorageException
            this storage is in invalid state for any reason

        @throws ::com::sun::star::lang::IllegalArgumentException
            one of provided arguments is illegal

        @throws ::com::sun::star::packages::WrongPasswordException
            the provided password is wrong

        @throws ::com::sun::star::io::IOException
            in case of io errors during stream opening

        @throws ::com::sun::star::embed::StorageWrappedTargetException
            wraps other exceptions
     */
    XExtendedStorageStream  openStreamElementByHierarchicalName(
            [in] string sStreamPath,
            [in] long nOpenMode )
        raises( ::com::sun::star::embed::InvalidStorageException,
                ::com::sun::star::lang::IllegalArgumentException,
                ::com::sun::star::packages::WrongPasswordException,
                ::com::sun::star::io::IOException,
                ::com::sun::star::embed::StorageWrappedTargetException );

    // -----------------------------------------------------------------------
    /** allows to get access to a child encrypted stream with password using
        hierarchical path.

        <p>
        If storage does not allow any encryption this method will always throw
        <type scope="com::sun::star::packages">NoEncryptionException</type>.
        </p>

        <p>
        In case the stream is open in readonly mode the
        <method scope="com::sun::star::io">XStream::getOutputStream</method>
        method will return an empty reference.
        </p>

        @param sStreamPath
            the path to the substream that should be open

        @param nOpenMode
            a mode the stream should be open in,
            can be a combination of <type>ElementModes</type> values

        @param sPassword
            this parameter allows to specify a reading password for the
            stream, the password must be a correct one, otherwise an
            exception will be thrown

        @throws ::com::sun::star::embed::InvalidStorageException
            this storage is in invalid state for any reason

        @throws ::com::sun::star::lang::IllegalArgumentException
            one of provided arguments is illegal

        @throws ::com::sun::star::packages::NoEncryptionException
            the stream is not encrypted

        @throws ::com::sun::star::packages::WrongPasswordException
            the provided password is wrong

        @throws ::com::sun::star::io::IOException
            in case of io errors during stream opening

        @throws ::com::sun::star::embed::StorageWrappedTargetException
            wraps other exceptions
     */
    XExtendedStorageStream  openEncryptedStreamElementByHierarchicalName(
            [in] string sStreamName,
            [in] long nOpenMode,
            [in] string sPassword )
        raises( ::com::sun::star::embed::InvalidStorageException,
                ::com::sun::star::lang::IllegalArgumentException,
                ::com::sun::star::packages::NoEncryptionException,
                ::com::sun::star::packages::WrongPasswordException,
                ::com::sun::star::io::IOException,
                ::com::sun::star::embed::StorageWrappedTargetException );

    // -----------------------------------------------------------------------
    /** removes a stream specified by hierarchical name from a storage.

        @param sElementPath
            the path to the element to remove

        @throws ::com::sun::star::embed::InvalidStorageException
            this storage is in invalid state for any reason

        @throws ::com::sun::star::lang::IllegalArgumentException
            an illegal argument is provided

        @throws ::com::sun::star::container::NoSuchElementException
            there is no element with such name

        @throws ::com::sun::star::io::IOException
            in case of io errors during removing

        @throws ::com::sun::star::embed::StorageWrappedTargetException
            wraps other exceptions

     */
    void removeStreamElementByHierarchicalName( [in] string sElementPath )
        raises( ::com::sun::star::embed::InvalidStorageException,
                ::com::sun::star::lang::IllegalArgumentException,
                ::com::sun::star::container::NoSuchElementException,
                ::com::sun::star::io::IOException,
                ::com::sun::star::embed::StorageWrappedTargetException );

};

//============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
