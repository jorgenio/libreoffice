/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_embed_OLESimpleStorage_idl__
#define __com_sun_star_embed_OLESimpleStorage_idl__

#include <com/sun/star/embed/XOLESimpleStorage.idl>
#include <com/sun/star/io/XInputStream.idl>
#include <com/sun/star/io/XStream.idl>

//============================================================================
module com { module sun { module star { module embed {
//============================================================================
/** This service provides a simple functionality to allow
    read/write the storages in OLE storage format.
*/
published service OLESimpleStorage: XOLESimpleStorage
{
    // CONSTRUCTORS ----------------------------------------------------------
    /** is used to initialize the object on it's creation.

        @param xInputStream
                [in] the InputStream that contains data in OLE storage format.

        @param bNoTempCopy
                [in] specifies whether a temporary copy should be created during
                substreams opening. If the copy is not created the storage must
                stay alive while the opened substreams are used.

     */
    createFromInputStream ([in] ::com::sun::star::io::XInputStream xInputStream, [in] boolean bNoTempCopy);

    /** is used to initialize the object on it's creation.

        @param xStream
                [in] the Stream that contains data in OLE storage format.

        @param bNoTempCopy
                [in] specifies whether a temporary copy should be created during
                substreams opening. If the copy is not created the storage must
                stay alive while the opened substreams are used.

     */
    createFromStream ([in] ::com::sun::star::io::XStream xStream, [in] boolean bNoTempCopy);
};

//============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
