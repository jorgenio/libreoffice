/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_BlendMode_idl__
#define __com_sun_star_rendering_BlendMode_idl__

module com { module sun { module star { module rendering {

/** These constants determine some extra ways how the primitive color
    is combined with the background.<p>

    @see <type>CompositeOperation</type>

    Please refer to the PDF specification for explanations of this
    constants.<p>
 */
constants BlendMode
{
    const byte NORMAL      = 0;
    const byte MULTIPLY    = 1;
    const byte SCREEN      = 2;
    const byte OVERLAY     = 3;
    const byte DARKEN      = 4;
    const byte LIGHTEN     = 5;
    const byte COLOR_DODGE = 6;
    const byte COLOR_BURN  = 7;
    const byte HARD_LIGHT  = 8;
    const byte SOFT_LIGHT  = 9;
    const byte DIFFERENCE  = 10;
    const byte EXCLUSION   = 11;
    const byte HUE         = 12;
    const byte SATURATION  = 13;
    const byte COLOR       = 14;
    const byte LUMINOSITY  = 15;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
