/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_XBufferController_idl__
#define __com_sun_star_rendering_XBufferController_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>

module com { module sun { module star { module rendering {

/** Interface providing access to double/multi-buffer facilities of
    screen devices.<p>

    This interface provides methods to enable and control
    double/multi-buffering facilities on screen devices.<p>

    @since OOo 2.0
 */
interface XBufferController : ::com::sun::star::uno::XInterface
{
    /** Create the given number of background buffers.<p>

        There's one buffer implicitly available, which is the canvas
        surface itself. Thus, calling <code>createBuffers(1)</code>
        creates a double-buffered object.<p>

        @param nBuffers
        The number of background&lt;buffers requested. Must be greater
        than 0.

        @return the number of actually generated buffers, which might
        be between 0 (no double-buffering available) and nBuffers.

        @throws <type>com::sun::star::lang::IllegalArgumentException</type>
        if nBuffers is smaller than one.
      */
    long    createBuffers( [in] long nBuffers )
        raises (com::sun::star::lang::IllegalArgumentException);

    //-------------------------------------------------------------------------

    /** Destroy all buffers generated via this object.
     */
    void    destroyBuffers();

    //-------------------------------------------------------------------------

    /** Switch the display to show the specified buffer.<p>

        The method returns, when the switch is performed and the
        selected buffer is shown on screen, or immediately when an
        error occurs. If the switch was successful, subsequent render
        operations will be directed to the new backbuffer.<p>

        Use this method if you need your screen display to be in sync
        with other things, e.g. sound playback.<p>

        @param bUpdateAll
        When <TRUE/>, update the whole screen. When <FALSE/>,
        implementation is permitted to restrict update to areas the
        canvas itself changed (e.g. because of render operations, or
        changes on the sprites). The former is useful for updates
        after window expose events, the latter for animation display.

        @return whether the switch was performed successfully.

        @throws <type>com::sun::star::lang::IllegalArgumentException</type>
        if nBuffer is outside the permissible range.
     */
    boolean showBuffer( [in] boolean bUpdateAll );

    //-------------------------------------------------------------------------

    /** Schedule the display of the specified buffer.<p>

        The method returns, when the switching of the buffer is
        successfully scheduled, or immediately when an error
        occurs. If the switch was successful, subsequent render
        operations will be directed to the new backbuffer. Note that,
        if the buffer switching is exceedingly slow, or the frequency
        of switchBuffer() is exceedingly high, the buffer scheduled
        for display here might become the current render target
        <em>before</em> it is fully displayed on screen. In this case,
        any rendering operation to this buffer will block, until it is
        safe to perform the operation without visible cluttering.<p>

        Use this method if you favor maximal render speed, but don't
        necessarily require your screen display to be in sync with
        other things, e.g. sound playback.<p>

        @param bUpdateAll
        When <TRUE/>, update the whole screen. When <FALSE/>,
        implementation is permitted to restrict update to areas the
        canvas itself changed (e.g. because of render operations, or
        changes on the sprites). The former is useful for updates
        after window expose events, the latter for animation display.

        @return whether the switch was performed successfully.
     */
    boolean switchBuffer( [in] boolean bUpdateAll );

};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
