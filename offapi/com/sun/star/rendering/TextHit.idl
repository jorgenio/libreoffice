/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_TextHit_idl__
#define __com_sun_star_rendering_TextHit_idl__

module com { module sun { module star { module rendering {

/** This structure contains hit information for <type>XTextLayout</type>.<p>

    This structure is used from the <type>XTextLayout</type> interface
    to transport information regarding hit tests.<p>

    @since OOo 2.0
 */
struct TextHit
{
    /** This contains the entry index.<p>

        The entry index is the index of the insertion point in the
        character sequence. The insertion point denotes positions
        <em>between</em> the actual characters in the string, and can
        thus have values ranging from 0 up to the number of characters
        in the string. Hereby, an index of 0 denotes an insertion
        position <em>before</em> the first character, and an index
        containing the number of characters denotes an insertion
        <em>behind</em> the last character.<p>
     */
    long        EntryIndex;

    /** This member denotes whether the hit was on the leading
        edge.<p>

        Each character is divided in two halves, the leading and the
        trailing part. The leading edge is the part of the glyph
        encountered first when reading text of the corresponding
        language (i.e. the leading edge of an Arabic glyph is the
        right half of it, whereas it is the left half of a Latin
        character). If the hit was on the leading edge, this member is
        set to <TRUE/>.
     */
    boolean     IsLeadingEdge;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
