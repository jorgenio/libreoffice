/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_rendering_PathCapType_idl__
#define __com_sun_star_rendering_PathCapType_idl__

module com { module sun { module star { module rendering {

/** These constants determine which shape to use for start or end of a
    stroked path.<p>

    The start and end of stroked paths can have one out of several
    different shapes (which are, of course, only visible for strokes
    wider than one device pixel).<p>

    @since OOo 2.0
 */
constants PathCapType
{
    /// End the path at its start or end point, without any cap.
    const byte BUTT=0;

    //-------------------------------------------------------------------------

    /// Extend the path with a half circle cap, diameter is the line width.
    const byte ROUND=1;

    //-------------------------------------------------------------------------

    /// Extend the path with a rectangular cap, half the line width long.
    const byte SQUARE=2;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
