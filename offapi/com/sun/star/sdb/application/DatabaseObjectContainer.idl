/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
************************************************************************/

#ifndef __com_sun_star_sdb_application_DatabaseObjectContainer_idl__
#define __com_sun_star_sdb_application_DatabaseObjectContainer_idl__

//=============================================================================

module com { module sun { module star { module sdb { module application {

//=============================================================================

/** denotes different types of (maybe virtual) containers of database objects

    <p>In the database application of OpenOffice.org, database objects (such as
    tables, queries, forms, reports) can be organized in folders. This hierarchy can
    be imposed externally, or internally.

    <p>For example, when you connect to a database which supports catalogs and/or schemas,
    then those impose a natural order on the tables, in that a catalog or a schema
    is a folder of tables.</p>

    <p>On the other hand, for forms and reports, OpenOffice.org Base itself allows the
    user to create folders to organize the documents - in this case, the hierarchy is
    defined in the database document itself.</p>

    @see DatabaseObject

    @since OOo 3.0
 */
constants DatabaseObjectContainer
{
    /** denotes the virtual folder containing all tables of a database, in a context where such a
        folder is displayed to the user.
    */
    const long TABLES = 1000;

    /** denotes the virtual folder containing all queries of a database, in a context where such a
        folder is displayed to the user.
    */
    const long QUERIES = 1001;

    /** denotes the virtual folder containing all forms of a database document, in a context where such a
        folder is displayed to the user.
    */
    const long FORMS = 1002;

    /** denotes the virtual folder containing all reports of a database database, in a context where such a
        folder is displayed to the user.
    */
    const long REPORTS = 1003;

    /** denotes the data source itself, which effectively is the root container for all other
        kind of database objects, including other container types.
    */
    const long DATA_SOURCE = 1004;

    /** denotes a catalog in a database which supports catalogs
    */
    const long CATALOG = 1005;

    /** denotes a schema in a database which supports catalogs
    */
    const long SCHEMA = 1006;

    /** denotes a folder which is used to organize forms in a database document
    */
    const long FORMS_FOLDER = 1007;

    /** denotes a folder which is used to organize reports in a database document
    */
    const long REPORTS_FOLDER = 1008;
};

//=============================================================================

}; }; }; }; };

//=============================================================================

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
