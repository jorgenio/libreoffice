/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdb_DataSettings_idl__
#define __com_sun_star_sdb_DataSettings_idl__

#include <com/sun/star/beans/XPropertySet.idl>

#include <com/sun/star/util/Color.idl>
#include <com/sun/star/awt/FontDescriptor.idl>

 module com {  module sun {  module star {  module sdb {


/** extends the
    <type scope="com::sun::star::sdbcx">Table</type>
    with additional display information, sorting and filtering criteria.

    */
published service DataSettings
{

    /** additional filter for the data object.
     */
    [property] string Filter;


    /** indicates whether the filter should be applied or not,
        default is <FALSE/>.
     */
    [property] boolean ApplyFilter;


    /** is an additional sort order definition.
     */
    [property] string Order;


    /** specifies the font attributes for data displaying.
     */
    [property] com::sun::star::awt::FontDescriptor FontDescriptor;


    /** specifies the height of a data row.
     */
    [property] long RowHeight;


    /** specifies the text color (RGB) for displaying text.
     */
    [property] com::sun::star::util::Color TextColor;

    /** additional having clause for the data object.
     */
    [optional,property] string HavingClause;

    /** additional group by for the data object.
     */
    [optional,property] string GroupBy;
};

//=============================================================================

}; }; }; };

/*===========================================================================
===========================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
