/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdb_DefinitionContent_idl__
#define __com_sun_star_sdb_DefinitionContent_idl__

#include <com/sun/star/ucb/Content.idl>
#include <com/sun/star/container/XHierarchicalName.idl>

//=============================================================================

module com { module sun { module star { module sdb {

//=============================================================================

/** defines the basic functionality for an object in the hierarchy of sub documents
    of a <type>OfficeDatabaseDocument</type>.

    @see DocumentDefinition
    @see DocumentContainer
 */
service DefinitionContent
{
    /** provides the functionality for treating the object as part of a hierarchy
        of objects.

        <p>The following commands are commonly supported by the <type scope="com::sun::star::ucb">XCommandProcessor</type>
        implementation of a <code>DefinitionContent</code>:
        <ul>
            <li><strong>getPropertyValues</strong></li>
            <li><strong>setPropertyValues</strong></li>
            <li><strong>getPropertySetInfo</strong></li>
        </ul>
        </p>

        <p>Additional commands might be supported by derived services.</p>
    */
    service ::com::sun::star::ucb::Content;

    /** provides access to the complete name of the content within its hierarchy
        @since OOo 3.3
    */
    interface ::com::sun::star::container::XHierarchicalName;
};

//=============================================================================

}; }; }; };

//=============================================================================

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
