/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbcx_ViewDescriptor_idl__
#define __com_sun_star_sdbcx_ViewDescriptor_idl__

#include <com/sun/star/sdbcx/Descriptor.idl>

 module com {  module sun {  module star {  module sdbcx {


/** is used to define a new view for a database.
    @see com::sun::star::sdbcx::View
 */
published service ViewDescriptor
{
    service Descriptor;


    /** is the name of the views catalog, may be empty.
     */
    [property] string CatalogName;


    /** is the name of the views schema, may be empty.
     */
    [property] string SchemaName;


    /** is the command for creating the view. After appending a view to it's
        container, the command may be empty. This is typically a
        SQL Select-Statement.
     */
    [property] string Command;


    /** indicates if a check option should be used for the view.
        @see com::sun::star::sdbcx::CheckOption
     */
    [property] long CheckOption;
};

//=============================================================================

}; }; }; };

/*===========================================================================
===========================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
