/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbcx_KeyDescriptor_idl__
#define __com_sun_star_sdbcx_KeyDescriptor_idl__

#include <com/sun/star/sdbcx/Descriptor.idl>

module com { module sun { module star { module sdbcx {

 published interface XColumnsSupplier;

/**
 is used to define a new key for a table.

 @see com::sun::star::sdbcx::Key
 */
published service KeyDescriptor
{
    service Descriptor;

    /** access to the contained key columns.
    */
    interface XColumnsSupplier;

    /** indicates the type of the key.
        @see com::sun::star::sdbcx::KeyType
     */
    [property] long Type;

    /** is the name of the referenced table, only used for foreign keys.
    */
    [property] string ReferencedTable;

    /** is the rule which is applied for updates; only used for foreign keys.
        @see com::sun::star::sdbc::KeyRule
     */
    [property] long UpdateRule;

    /** is the rule which is applied for deletions; only used for foreign keys.
        @see com::sun::star::sdbc::KeyRule
    */
    [property] long DeleteRule;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
