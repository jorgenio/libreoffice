/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_script_browse_XBrowseNode_idl__
#define __com_sun_star_script_browse_XBrowseNode_idl__

#include <com/sun/star/uno/XInterface.idl>

module com { module sun { module star { module script { module browse {
//==============================================================================
/**
    This interface represents a node in the hierarchy used to browse
    available scripts.
    Objects implementing this interface are expected to also implement
    com.sun.star.beans.XPropertySet and, optionally,
    com.sun.star.script.XInvocation (see the Developer's Guide for
    more details).
*/
interface XBrowseNode : ::com::sun::star::uno::XInterface
{
  //-----------------------------------------------------------------------
  /**
    Get the name of the node

    @return
     The <atom>string</atom> name of this node
  */
  string getName();

  //----------------------------------------------------------------------
  /**
    Get the children of this node

    @returns
    <type scope="::com::sun::star::script::browse">XBrowseNode</type> sequence of child nodes
  */
    sequence < XBrowseNode > getChildNodes();

  //----------------------------------------------------------------------
  /**
    Indicates if this node contains any children

    @returns
    <atom> boolean </atom> true if there are child nodes.
  */
    boolean hasChildNodes();

    //-------------------------------------------------------------------
    /** the type of the node.
        @returns A <atom>short</atom> representing the type of this node.
    */
    short getType();

};

};  };  };  }; };
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
