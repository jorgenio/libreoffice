/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_XLibraryContainerExport_idl__
#define __com_sun_star_script_XLibraryContainerExport_idl__

#include <com/sun/star/task/XInteractionHandler.idl>
#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/container/NoSuchElementException.idl>


//=============================================================================

 module com {  module sun {  module star {  module script {

//=============================================================================

/**
    Extension of XLibraryContainer to provide functionality to
    store a library to a location represented by a URL.
 */
interface XLibraryContainerExport : com::sun::star::uno::XInterface
{
    /**
        Exports a library specified by Name to the location
        specified by the passed URL string.

        An interaction handler can be passed to be used for internal
        ucb operations. Exceptions not processed by this handler will
        be passed as com::sun::star::uno::Exception. If this parameter
        is null this applies to all exceptions thrown by ucb.

        @see com::sun::star::task::InteractionHandler

        If a library with the this name doesn't exist a
        com::sun::star::container::NoSuchElementException is thrown.
     */
    void exportLibrary( [in] string Name, [in] string URL,
        [in] com::sun::star::task::XInteractionHandler Handler )
            raises( com::sun::star::uno::Exception,
                    com::sun::star::container::NoSuchElementException );
};

//=============================================================================


}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
