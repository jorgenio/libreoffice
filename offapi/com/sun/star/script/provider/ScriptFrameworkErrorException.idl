/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_script_provider_ScriptFrameworkErrorException_idl__
#define __com_sun_star_script_provider_ScriptFrameworkErrorException_idl__

#include <com/sun/star/uno/Exception.idl>

//===========================================================================

module com { module sun { module star { module script { module provider {

//===========================================================================
/**
    is a checked exception that represents an error encountered
        by the Scripting Framework whilst executing a script
*/
exception ScriptFrameworkErrorException : com::sun::star::uno::Exception {
    /** Name of script where error occurred
     */
    string scriptName;
    /** Scripting language of script that generated exception
     */
    string language;
    /** error type  <type scope="::com::sun::star::script::provider">ScriptFrameworkErrorType</type>
     */
    long errorType;
};

//===========================================================================
}; }; }; }; }; // ::com::sun::star::script::provider

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
