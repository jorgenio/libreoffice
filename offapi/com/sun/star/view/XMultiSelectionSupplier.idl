/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_XMultiSelectionSupplier_idl__
#define __com_sun_star_view_XMultiSelectionSupplier_idl__

#include <com/sun/star/view/XSelectionSupplier.idl>
#include <com/sun/star/container/XEnumeration.idl>

//=============================================================================

module com {  module sun {  module star {  module view {

//=============================================================================

/** makes it possible to append and remove objects from a selection.

    <p>The method <method>XSelectionSupplier::setSelection</method> for an
    instance that also supports <type>XMultiSelectionSupplier</method> should
    be implemented that it also takes either a selectable object or a sequence
    of selectable objects.</p>

    <p>Adding an object more than once to a selection should not toggle the
    selection for that object but only select it once</p>
 */
interface XMultiSelectionSupplier: XSelectionSupplier
{
    //-------------------------------------------------------------------------

    /** adds the object or the objects represented by <var>Selection</var> to the selection
        of this <type>XMultiSelectionSupplier</type>.

        @param Selection
            either an Object that is selectable or a sequence of objects that are selectable.

        @returns
            <TRUE/>/, if <var>Selection</var> was added to the current selection.
            <FALSE/>, if <var>Selection</var> or parts of <var>Selection</var> could not be
            added to the current selection. For example, if the selection already contains
            objects that are forbidden to be selected together with <var>Selection</var>

        @throws com::sun::star::lang::IllegalArgumentException
            If <var>Selection</var> is not a selectable object for this <type>XMultiSelectionSupplier</type>.
            <p>Adding an object to the selection that is already part of the selection should not raise this exception</p>
     */
    boolean addSelection( [in] any Selection )
                raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------

    /** remove the object or objects represented by <var>Selection</var> from the
        selection of this <type>XMultiSelectionSupplier</type>.

        @param Selection
            either an Object that is selectable or a sequence of objects that are selectable.

        @returns
            <TRUE/>/, if <var>Selection</var> was added to the current selection.
            <FALSE/>, if <var>Selection</var> or parts of <var>Selection</var> could not be
            added to the current selection. For example, if the selection already contains
            objects that are forbidden to be selected together with <var>Selection</var>.

        @throws com::sun::star::lang::IllegalArgumentException
            If <var>Selection</var> is not a selectable object for this <type>XMultiSelectionSupplier</type>.
            <p>Removing an object from the selection that is not part of the selection should not raise this exception</p>
     */
    void removeSelection( [in] any Selection )
                raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------

    /** clears the selection of this <type>XMultiSelectionSupplier</type>.
    */
    void clearSelection();

    //-------------------------------------------------------------------------

    /** returns the number of selected objects of this <type>XMultiSelectionSupplier</type>.
    */
    long getSelectionCount();

    //-------------------------------------------------------------------------

    /** @returns
            a new object to enumerate the selection of this <type>XMultiSelectionSupplier</type>.
            It returns NULL if there are no objects in the selection.
     */
    com::sun::star::container::XEnumeration createSelectionEnumeration();


    /** @returns
            a new object to enumerate the selection of this <type>XMultiSelectionSupplier</type>
            in reverse order. If the order of the selected objects
            It returns NULL if there are no objects in the selection.
     */
    com::sun::star::container::XEnumeration createReverseSelectionEnumeration();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
