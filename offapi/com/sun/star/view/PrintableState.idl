/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_view_PrintableState_idl__
#define __com_sun_star_view_PrintableState_idl__


//=============================================================================

module com {  module sun {  module star {  module view {

//=============================================================================

/** specifies the print progress of an <type>XPrintable</type>.

    <p>Printing consists of two abstract phases: rendering the document
    for the printer and then sending it to the printer (spooling).
    <type>PrintableState</type> describes which phase is currently
    progressing or has failed.</p>

    @see PrintableStateEvent
 */
published enum PrintableState
{
    /** printing (rendering the document) has begun
     */
    JOB_STARTED,

    /** printing (rendering the document) has finished, spooling has begun
     */
    JOB_COMPLETED,

    /** spooling has finished successfully. This is the only state
        that can be considered as "success" for a print job.
    */
    JOB_SPOOLED,

    /** printing was aborted (e.g., by the user) while either printing or spooling.
     */
    JOB_ABORTED,

    /** printing ran into an error.
     */
    JOB_FAILED,

    /** the document could be printed but not spooled.
     */
    JOB_SPOOLING_FAILED
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
