/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_geometry_AffineMatrix2D_idl__
#define __com_sun_star_geometry_AffineMatrix2D_idl__

module com {  module sun {  module star {  module geometry {

/** This structure defines a 2 by 3 affine matrix.<p>

    The matrix defined by this structure constitutes an affine mapping
    of a point in 2D to another point in 2D. The last line of a
    complete 3 by 3 matrix is omitted, since it is implicitly assumed
    to be [0,0,1].<p>

    An affine mapping, as performed by this matrix, can be written out
    as follows, where <code>xs</code> and <code>ys</code> are the source, and
    <code>xd</code> and <code>yd</code> the corresponding result coordinates:

    <code>
        xd = m00*xs + m01*ys + m02;
        yd = m10*xs + m11*ys + m12;
    </code><p>

    Thus, in common matrix language, with M being the
    <type>AffineMatrix2D</type> and vs=[xs,ys]^T, vd=[xd,yd]^T two 2D
    vectors, the affine transformation is written as
    vd=M*vs. Concatenation of transformations amounts to
    multiplication of matrices, i.e. a translation, given by T,
    followed by a rotation, given by R, is expressed as vd=R*(T*vs) in
    the above notation. Since matrix multiplication is associative,
    this can be shortened to vd=(R*T)*vs=M'*vs. Therefore, a set of
    consecutive transformations can be accumulated into a single
    AffineMatrix2D, by multiplying the current transformation with the
    additional transformation from the left.<p>

    Due to this transformational approach, all geometry data types are
    points in abstract integer or real coordinate spaces, without any
    physical dimensions attached to them. This physical measurement
    units are typically only added when using these data types to
    render something onto a physical output device, like a screen or a
    printer, Then, the total transformation matrix and the device
    resolution determine the actual measurement unit.<p>

    @since OOo 2.0
 */
struct AffineMatrix2D
{
    /// The top, left matrix entry.
    double m00;

    /// The top, middle matrix entry.
    double m01;

    /// The top, right matrix entry.
    double m02;

    /// The bottom, left matrix entry.
    double m10;

    /// The bottom, middle matrix entry.
    double m11;

    /// The bottom, right matrix entry.
    double m12;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
