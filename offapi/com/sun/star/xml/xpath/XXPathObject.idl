/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_xml_XPath_XXPathObject_idl__
#define __com_sun_star_xml_XPath_XXPathObject_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/xml/dom/XNode.idl>
#include <com/sun/star/xml/dom/XNodeList.idl>
#include <com/sun/star/xml/xpath/XPathObjectType.idl>

module com { module sun { module star { module xml { module xpath {


interface XXPathObject : com::sun::star::uno::XInterface
{
    /**
        get object type
    */
    XPathObjectType getObjectType();

    /**
        get the nodes from a node list type object
    */
    com::sun::star::xml::dom::XNodeList getNodeList();

    /**
        get value of a boolean object
    */
    boolean getBoolean();

    /**
        get number as byte
    */
    byte getByte();

    /**
        get number as short
    */
    short getShort();

    /**
        get number as long
    */
    long getLong();

    /**
        get number as hyper
    */
    hyper getHyper();

    /**
        get number as float
    */
    float getFloat();

    /**
        get number as double
    */
    double getDouble();

    /**
        get string value
    */
    string getString();
};

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
