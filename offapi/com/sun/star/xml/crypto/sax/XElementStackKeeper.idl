/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

//i20156 - new file for XML security module

/** -- idl definition -- **/

#ifndef __com_sun_star_xml_crypto_sax_xelementstackkeeper_idl_
#define __com_sun_star_xml_crypto_sax_xelementstackkeeper_idl_

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/uno/Exception.idl>
#include <com/sun/star/xml/sax/XDocumentHandler.idl>
#include <com/sun/star/xml/crypto/sax/ElementStackItem.idl>

module com { module sun { module star { module xml { module crypto { module sax {

/**
 * Manipulate the "key SAX events" in a SAX event stream.
 *
 * @see   XSecurityController
 */
interface XElementStackKeeper : com::sun::star::uno::XInterface
{
    /**
     * Starts to buffer key SAX events.
     */
    void start();

    /**
     * Stops buffering key SAX events.
     */
    void stop();

    /**
     * Transfers the buffered key SAX events to a document handler.
     * <p>
     * All transferred events are removed from the buffer.
     *
     * @param handler                the document to receive key SAX events
     * @param includingTheLastEvent  whether to transfer the last key SAX event
     */
    void retrieve( [in] com::sun::star::xml::sax::XDocumentHandler handler, [in] boolean includingTheLastEvent);
};

} ; } ; } ; } ; } ; } ;


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
