/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

//i20156 - new file for XML security module

/** -- idl definition -- **/

#ifndef __com_sun_star_xml_crypto_xuribinding_idl_
#define __com_sun_star_xml_crypto_xuribinding_idl_

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/uno/Exception.idl>
#include <com/sun/star/io/XInputStream.idl>

module com { module sun { module star { module xml { module crypto {

/**
 * Interface of Uri Binding.
 * <p>
 * This interface is used to dynamically bind a uri with a
 * XInputStream interface.
 */
interface XUriBinding : com::sun::star::uno::XInterface
{
    /**
     * Sets the XInputStream interface for a uri.
     *
     * @param uri       the uri to bind
     * @param aStream   the XInputStream to be binded
     */
    void setUriBinding([in] string uri, [in] com::sun::star::io::XInputStream aInputStream)
        raises( com::sun::star::uno::Exception );

    /**
     * Gets the XInputStream interface for a uri.
     *
     * @param uri   the uri whose binding is to get
     * @return      the XInputStream binded with the uri
     */
    com::sun::star::io::XInputStream getUriBinding([in] string uri)
        raises( com::sun::star::uno::Exception );
};

} ; } ; } ; } ; } ;


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
