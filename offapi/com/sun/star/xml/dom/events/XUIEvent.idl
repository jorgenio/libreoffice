/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_xml_dom_events_XUIEvent_idl__
#define __com_sun_star_xml_dom_events_XUIEvent_idl__

#include <com/sun/star/xml/dom/events/XEvent.idl>

module com { module sun { module star { module xml { module dom { module views {
    interface XAbstractView;
}; }; }; }; }; };

module com { module sun { module star { module xml { module dom { module events {

// Introduced in DOM Level 2:
interface XUIEvent : XEvent {
    com::sun::star::xml::dom::views::XAbstractView getView();
    long getDetail();
    void initUIEvent([in] string typeArg,
                     [in] boolean canBubbleArg,
                     [in] boolean cancelableArg,
                     [in] com::sun::star::xml::dom::views::XAbstractView viewArg,
                     [in] long detailArg);
};
}; }; }; }; }; };
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
