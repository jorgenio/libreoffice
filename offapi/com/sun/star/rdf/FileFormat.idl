/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_rdf_FileFormat_idl__
#define __com_sun_star_rdf_FileFormat_idl__


//=============================================================================

module com {   module sun {   module star {   module rdf {

//=============================================================================
/** Constants to specify RDF file formats.

    <p>
    These constants are mainly for use with
    <member>XRepository::importGraph</member>
    and <member>XRepository::exportGraph</member>.
    </p>

    <p>
    Note that these are integers because UNO IDL does not permit
    <atom>string</atom> constants.
    </p>

    @since OOo 3.0

    @see XRepository::importGraph
    @see XRepository::exportGraph
 */
constants FileFormat
{

    /// <a href="http://www.w3.org/TR/rdf-syntax-grammar/">RDF/XML</a>
    const short RDF_XML     = 0;    // "application/rdf+xml";

    /// <a href="http://www.w3.org/DesignIssues/Notation3">N3 (Notation-3)</a>
    const short N3          = 1;    // "text/rdf+n3";

    /// <a href="http://www.w3.org/TR/rdf-testcases/#ntriples">N-Triples</a>
    const short NTRIPLES    = 2;    // "text/plain";

    /// <a href="http://www.wiwiss.fu-berlin.de/suhl/bizer/TriG/Spec/">TriG</a>
    const short TRIG        = 3;    // "application/x-trig";

    /// <a href="http://sw.nokia.com/trix/">TriX</a>
    const short TRIX        = 4;    // "if only the damn server were up I'd know";

    /// <a href="http://www.dajobe.org/2004/01/turtle/">Turtle</a>
    const short TURTLE      = 5;    // "application/turtle";

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
