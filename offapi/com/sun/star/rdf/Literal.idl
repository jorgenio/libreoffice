/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_rdf_Literal_idl__
#define __com_sun_star_rdf_Literal_idl__

#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/rdf/XLiteral.idl>


//=============================================================================

module com {   module sun {   module star {   module rdf {

//=============================================================================
/** represents a literal that may occur in a RDF graph.

    @since OOo 3.0

    @see XRepository
 */
service Literal : XLiteral
{

    //-------------------------------------------------------------------------
    /** creates a plain literal RDF node.

        @param Value
            the string value of the literal
     */
    create( [in] string Value );

    //-------------------------------------------------------------------------
    /** creates a typed literal RDF node.

        @param Value
            the string value of the literal

        @param Type
            the data type of the literal
     */
    createWithType( [in] string Value, [in] XURI Type );

    //-------------------------------------------------------------------------
    /** creates a literal RDF node with a language.

        @param Value
            the string value of the literal

        @param Language
            the language of the literal
     */
    createWithLanguage( [in] string Value, [in] string Language );

    // NOTE: there is no createWithTypeAndLanguage!
};


//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
