/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_linguistic2_XThesaurus_idl__
#define __com_sun_star_linguistic2_XThesaurus_idl__

#include <com/sun/star/linguistic2/XSupportedLocales.idl>
#include <com/sun/star/linguistic2/XMeaning.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/beans/PropertyValues.idl>

//=============================================================================

module com { module sun { module star { module linguistic2 {

//=============================================================================
/** allows for the retrieval of possible meanings for a given word and language.

    <P>
     The meaning of a word is in essence a descriptive text for that word.
     Each meaning may have several synonyms where a synonym is a word
     (or small text) with the same or similar meaning.
    </P>

    @see    <type scope="com::sun::star::linguistic2">XSupportedLocales</type>
*/
published interface XThesaurus : com::sun::star::linguistic2::XSupportedLocales
{
    //-------------------------------------------------------------------------
    /**
        @returns
            a list of meanings for the given word and language.

         @param aTerm
             the word to query for its meanings.

        @param  aLocale
            specifies the language of the word.

            <P>If the language is not supported, an
            <type scope="com::sun::star::lang">IllegalArgumentException</type>
            exception is raised.</P>

        @param aProperties
            provides property values to be used for this function call only.
            It is usually empty in order to use the default values supplied with
            the property set.

        @see    <type scope="com::sun::star::lang">Locale</type>
        @see    <type scope="com::sun::star::linguistic2">XMeaning</type>
    */
    sequence< com::sun::star::linguistic2::XMeaning > queryMeanings(
            [in] string aTerm,
             [in] com::sun::star::lang::Locale aLocale,
            [in] com::sun::star::beans::PropertyValues aProperties )
        raises( com::sun::star::lang::IllegalArgumentException );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
