/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_linguistic2_ConversionDictionaryList_idl__
#define __com_sun_star_linguistic2_ConversionDictionaryList_idl__

#include <com/sun/star/linguistic2/XConversionDictionaryList.idl>
#include <com/sun/star/lang/XComponent.idl>

//=============================================================================

module com { module sun { module star { module linguistic2 {

//=============================================================================
/** represents a list of available conversion dictionaries.

    <p>There will be only one list that may hold different types of
    conversion dictionaries. That is e.g. it may hold dictionaries
    for Korean Hangul/Hanja conversion along with ones for
    Chinese traditional/simplified conversion or conversion between
    different Indic script types.</p>

    <p>The list will be used by the text conversion service to
    check for user supplied text conversions.</p>

    @see com::sun::star::linguistic2::ConversionDictionary
    @see com::sun::star::linguistic2::HangulHanjaConversionDictionary
    @see com::sun::star::i18n::TextConversion

     @since OOo 1.1.2
*/
published service ConversionDictionaryList
{
    interface com::sun::star::linguistic2::XConversionDictionaryList;
    interface com::sun::star::lang::XComponent;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
