/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_linguistic2_XProofreader_idl__
#define __com_sun_star_linguistic2_XProofreader_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/lang/Locale.idl>
#include <com/sun/star/linguistic2/XSupportedLocales.idl>
#include <com/sun/star/text/XFlatParagraph.idl>
#include <com/sun/star/linguistic2/ProofreadingResult.idl>
#include <com/sun/star/beans/PropertyValue.idl>

//=============================================================================

module com {  module sun {  module star {  module linguistic2 {

//=============================================================================
/** API for proofreading a text

    @since  OOo 3.0.1
*/
interface XProofreader: com::sun::star::linguistic2::XSupportedLocales
{

    //-------------------------------------------------------------------------
    /** whether is the text checked by the spell checker

        @returns true if it is also a spell checker
    */
    boolean isSpellChecker();

    //-------------------------------------------------------------------------
    /** start checking

        @param  nDocId
                the Document ID.

        @param  rFlatParaText
                the flat text to be checked.

        @param  xFlatPara
                the flat paragraph to be checked.

        @param  aLocale
                Language used in the text.

        @param  nStartOfSentencePos
                Start Index of the text.

        @param  nSuggestedSentenceEndPos
                Probable end position of the text.

        @throws IllegalArgumentException
                when any argument is wrong.
    */
    ProofreadingResult doProofreading( [in] string aDocumentIdentifier,
                            [in] string aText,
                            [in] com::sun::star::lang::Locale aLocale,
                            [in] long nStartOfSentencePosition,
                            [in] long nSuggestedBehindEndOfSentencePosition,
                            [in] sequence< com::sun::star::beans::PropertyValue > aProperties )
            raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /** disables a specific rule for a given locale.

        <p>If the locale is empty the rule should be ignored for all languages.</p>
    */
    void ignoreRule( [in] string aRuleIdentifier,
                     [in] com::sun::star::lang::Locale aLocale )
            raises( com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /** sets all rules back to their default settings.
    */
    void resetIgnoreRules();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
