/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_table_TableColumn_idl__
#define __com_sun_star_table_TableColumn_idl__

#include <com/sun/star/table/XCellRange.idl>
#include <com/sun/star/beans/XPropertySet.idl>
#include <com/sun/star/container/XNamed.idl>

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

/** represents a special cell range containing all cells of a single
    specific column in a table or spreadsheet.
 */
published service TableColumn
{
    //-------------------------------------------------------------------------

    /** provides access to the cells of this column.

        @see com::sun::star::table::CellRange
     */
    interface com::sun::star::table::XCellRange;

    //-------------------------------------------------------------------------

//!published service PropertySet
    /** provides access to the cell properties of all cells of this column.
     */
    interface com::sun::star::beans::XPropertySet;

    //-------------------------------------------------------------------------

    /** provides methods to access the name of the column.

        <p>In spreadsheets, the name of a column cannot be changed.<p/>
     */
    interface com::sun::star::container::XNamed;

    //=========================================================================

    /** contains the width of the column (in 1/100th mm).

        <p>When hidden, it returns the width which the column would have,
        if it were visible. </p>
     */
    [property] long Width;

    //-------------------------------------------------------------------------

    /** is <TRUE/>, if the column always keeps its optimal width.
     */
    [property] boolean OptimalWidth;

    //-------------------------------------------------------------------------

    /** is <TRUE/>, if the column is visible.
     */
    [property] boolean IsVisible;

    //-------------------------------------------------------------------------

    /** is <TRUE/>, if there is a manual horizontal page break attached
        to the column.
     */
    [property] boolean IsStartOfNewPage;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
