/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_table_BorderLineStyle_idl__
#define __com_sun_star_table_BorderLineStyle_idl__

//=============================================================================

module com {  module sun {  module star {  module table {

//=============================================================================

constants BorderLineStyle
{
    /** No border line
     */
    const short NONE = 0x7FFF;

    /** Solid border line.
     */
    const short SOLID = 0;

    /** Dotted border line.
     */
    const short DOTTED = 1;

    /** Dashed border line.
     */
    const short DASHED = 2;

    /** Double border line.
     */
    const short DOUBLE = 3;

    /** Double border line with a thin line outside and a thick line
        inside separated by a small gap.
     */
    const short THINTHICK_SMALLGAP = 4;

    /** Double border line with a thin line outside and a thick line
        inside separated by a medium gap.
     */
    const short THINTHICK_MEDIUMGAP = 5;

    /** Double border line with a thin line outside and a thick line
        inside separated by a large gap.
     */
    const short THINTHICK_LARGEGAP = 6;

    /** Double border line with a thick line outside and a thin line
        inside separated by a small gap.
     */
    const short THICKTHIN_SMALLGAP = 7;

    /** Double border line with a thick line outside and a thin line
        inside separated by a medium gap.
     */
    const short THICKTHIN_MEDIUMGAP = 8;

    /** Double border line with a thick line outside and a thin line
        inside separated by a large gap.
     */
    const short THICKTHIN_LARGEGAP = 9;

    /** 3D embossed border line.
     */
    const short EMBOSSED = 10;

    /** 3D engraved border line.
     */
    const short ENGRAVED = 11;

    /** Outset border line.
     */
    const short OUTSET = 12;

    /** Inset border line.
     */
    const short INSET = 13;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
