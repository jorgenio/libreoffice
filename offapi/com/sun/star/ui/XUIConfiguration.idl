/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ui_XUIConfiguration_idl__
#define __com_sun_star_ui_XUIConfiguration_idl__

#include <com/sun/star/ui/XUIConfigurationListener.idl>

module com { module sun { module star { module ui {

/** supports to notify other implementations about changes of a user
    interface configuration manager.

    <p>
    The <type>XUIConfiguration</type> interface is provided for user
    interface configuration managers which need to broadcast changes
    within the container; that means the actions of adding, replacing
    and removing elements are broadcast to listeners.
    </p>

    <p>This can be useful for UI to enable/disable some functions without
    actually accessing the data.</p>

    @since OOo 2.0
*/

interface XUIConfiguration : ::com::sun::star::uno::XInterface
{
    /** adds the specified listener to receive events when elements are
        changed, inserted or removed.

        <p>
        It is suggested to allow multiple registration of the same
        listener, thus for each time a listener is added, it has to be
        removed.

        @see XUIConfigurationListener
        </p>
    */
    void addConfigurationListener( [in] ::com::sun::star::ui::XUIConfigurationListener Listener );

    /** removes the specified listener so it does not receive any events
        from this user interface configuration manager.

        <p>It is suggested to allow multiple registration of the same
        listener, thus for each time a listener is added, it has to be
        removed.

        @see XUIConfigurationListener
        </p>
     */
    void removeConfigurationListener( [in] ::com::sun::star::ui::XUIConfigurationListener Listener );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
