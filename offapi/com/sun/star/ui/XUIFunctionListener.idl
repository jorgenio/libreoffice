/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_XUIFunctionListener_idl__
#define __com_sun_star_frame_XUIFunctionListener_idl__

#include <com/sun/star/lang/XEventListener.idl>

//=============================================================================

 module com {  module sun {  module star {  module ui {

//=============================================================================
/** special interface to receive notification that a user interface
    element will execute a function.

    @since OOo 2.0
 */
interface XUIFunctionListener : com::sun::star::lang::XEventListener
{
    //=============================================================================
    /** gets called to notify a component that a user interface element
        wants to execute a function.

        @param aUIElementName
            a string which identifies the user interface element that wants
            to execute a function.

        @param aCommand
            a string which identifies the function that has been selected by
            a user.
    */
    void functionExecute( [in] string aUIElementName, [in] string aCommand );

};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
