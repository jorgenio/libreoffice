/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ui_dialogs_FilterOptionsDialog_idl__
#define __com_sun_star_ui_dialogs_FilterOptionsDialog_idl__

#include <com/sun/star/ui/dialogs/XExecutableDialog.idl>
#include <com/sun/star/beans/XPropertyAccess.idl>

//=============================================================================

module com { module sun { module star { module ui { module dialogs {

//=============================================================================

/** This service enables a filter developer to register a dialog to query
    for user options before the filter operation is performed.

    <p>The user options are stored inside the <type scope="com::sun::star::document">MediaDescriptor</type>
    and can be queried from the <type scope="com::sun::star::document">MediaDescriptor</type> by
    the component that implements <type scope="com::sun::star::document">XFilter</type>.</p>

    <p>The application will set the <type scope="com::sun::star::document">MediaDescriptor</type>
    using the interface <type scope="com::sun::star::beans">XPropertyAccess</type> and then
    call <method>XExecutableDialog::execute<method>.</p>

    <p>If that method returns <code>ExecutableDialogResults::OK</code>,
    the application will retrieve the changed <type scope="com::sun::star::document">MediaDescriptor</type>
    back using the interface <type scope="com::sun::star::beans">XPropertyAccess</type>. The filter
    operation is than continued, using the new <type scope="com::sun::star::document">MediaDescriptor</type>.</p>

    <p>Otherwise, the filter operation is canceled.</p>

    @since OOo 1.1.2
 */
published service FilterOptionsDialog
{
    //-------------------------------------------------------------------------
    /** this interface is used to set the property values of the
        <type scope="com::sun::star::document">MediaDescriptor</type> before executing
        the dialog and to retrieve the changed <type scope="com::sun::star::document">MediaDescriptor</type>

        afterwards.
     */
    interface com::sun::star::beans::XPropertyAccess;

    //-------------------------------------------------------------------------
    /** this interface executes a dialog that displays the options
        for a filter.
        If <method>XExecutableDialog::execute<method>
        returns <code>ExecutableDialogResults::OK</code>,
        the options can be retrieved by calling <method scope="com::sun::star::beans">XPropertyAccess::getPropertyValues<method>.
     */
    interface ::com::sun::star::ui::dialogs::XExecutableDialog;
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
