/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ui_dialogs_XFilePickerControlAccess_idl__
#define __com_sun_star_ui_dialogs_XFilePickerControlAccess_idl__

#include <com/sun/star/ui/dialogs/XFilePicker.idl>

//=============================================================================

module com { module sun { module star { module ui { module dialogs {

//=============================================================================
/** Provides access to the controls of a FilePicker.

    <p> A FilePicker may contain additional elements according to the needs
        of the different applications. These additional elements can be
        addressed by this interface.</p>

    @see com::sun::star::ui::dialogs::FilePicker
*/
published interface XFilePickerControlAccess: com::sun::star::ui::dialogs::XFilePicker
{

//-------------------------------------------------------------------------
/** Set the value of an additional element within a FilePicker.

    @param ControlId
    Identifies the element which value is to be set.

    @param aControlAction
        Specifies an action to perform with the given value.
        aControlAction has to be one of the values defined in <type>ControlActions</type>.
        Not all of the values are valid for all controls.
        To add a new filter to the FilePicker use the interface XFilterManager, but
        optionally an implementation may also support adding new filter using this
        method.

    @param aValue
    The value to set. For checkboxes aValue should be a boolean value that
    should be <TRUE/> if the checkbox should be checked and <FALSE/> otherwise.

    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
    @see com::sun::star::ui::dialogs::ControlActions
*/
void setValue( [in] short aControlId, [in] short aControlAction, [in] any aValue );

//-------------------------------------------------------------------------
/** Get the value of an additional element within a FilePicker

    @param aControlId
    Identifies the element for which value is requested.

    @param aControlAction
    Specifies which value to retrieve.
    aControlAction has to be one of the values defined in <type>ControlActions</type>.
    Not all of the values are valid for all controls.

    @returns
    The value of the specified element. If the specified control
    is a checkbox the returned value is a boolean that is <TRUE/>
    if the checkbox is checked <FALSE/> otherwise.
    If the specified element doesn't exist or the specified element
    doesn't support the specified control action an empty any will be
    returned.

    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
    @see com::sun::star::ui::dialogs::ControlActions
*/
any getValue( [in] short aControlId, [in] short aControlAction );

//-------------------------------------------------------------------------
/** Set the label of the specified element. If the specified element
    doesn't support setting a label, this method has no effect.

    @param aControlId
    Identifies the element for which the label should be set.

    @param aLabel
    The label to be set.

    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
*/
void setLabel( [in] short aControlId, [in] string aLabel );

//-------------------------------------------------------------------------
/** Returns the label of the specified element.

    @param aControlId
    Identifies the element for which the label should be returned.

    @returns
    The label of the specified element or an empty string if
    the specified element has no or supports no label or the
    specified element doesn't exist.

    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
*/
string getLabel( [in] short aControlId );

//-------------------------------------------------------------------------
/** Enables or disables a control.

    @param ControlId
    Identifies the control.

    @param bEnable
    <p>If <TRUE/> the specified control will be enabled.</p>
    <p>If <FALSE/> the specified control will be disabled.</p>

    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
*/
void enableControl( [in] short aControlId, [in] boolean bEnable );
};

//=============================================================================

}; }; }; }; };


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
