/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ui_dialog_ControlActions_idl__
#define __com_sun_star_ui_dialog_ControlActions_idl__


module com { module sun { module star { module ui { module dialogs {

//=============================================================================
/** Control actions for common and extended controls of a FilePicker.

    @see com::sun::star::ui::dialogs::XFilePickerControlAccess
    @see com::sun::star::ui::dialogs::CommonFilePickerElementIds
    @see com::sun::star::ui::dialogs::ExtendedFilePickerElementIds
 */

published constants ControlActions
{
    //---------------------------------------------------------------------
    /** Adds an item to the content of the listbox. The given item has to
        be a string.
     */
    const short ADD_ITEM        = 1;

    //---------------------------------------------------------------------
    /** Adds a sequence of strings to the content of the listbox.
     */
    const short ADD_ITEMS       = 2;

    //---------------------------------------------------------------------
    /** Removes an item from a listbox. The given value has to be a
        position. If the position is invalid an exception will be thrown.
        The index of the first position is 0.
        The value should be a sal_Int32.
     */
    const short DELETE_ITEM     = 3;

    //---------------------------------------------------------------------
    /** Removes all items from the listbox.
    */
    const short DELETE_ITEMS    = 4;

    //---------------------------------------------------------------------
    /** Selects an item in a listbox. The given value has to be a position.
        The index of the first position is 0. A value of -1 removes the
        selection.
        If the given position is invalid an exception will be thrown.
        The value should be a sal_Int32.
     */
    const short SET_SELECT_ITEM = 5;

    //---------------------------------------------------------------------
    /** Returns all items of the listbox as a sequence of strings.
    */
    const short GET_ITEMS       = 6;

    //---------------------------------------------------------------------
    /** Returns the currently selected item. The returned item is an empty
        string if the listbox is empty or no item is selected.
     */
    const short GET_SELECTED_ITEM = 7;

    //---------------------------------------------------------------------
    /** Returns the zero based index of the currently selected item.
        If the listbox is empty or there is no item selected -1 will be
        returned. The returned value is a sal_Int32.
     */

    const short GET_SELECTED_ITEM_INDEX = 8;

    //---------------------------------------------------------------------
    /** Sets the help URL of a control.
     */
    const short SET_HELP_URL = 100;

    //---------------------------------------------------------------------
    /** Retrieves the help URL of a control.
     */
    const short GET_HELP_URL = 101;
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
