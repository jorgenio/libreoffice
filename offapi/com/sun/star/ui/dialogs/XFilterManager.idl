/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_ui_dialogs_XFilterManager_idl__
#define __com_sun_star_ui_dialogs_XFilterManager_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>

//=============================================================================

module com { module sun { module star { module ui { module dialogs {

//=============================================================================
/** Specifies a filter manager interface for a FilePicker
*/

published interface XFilterManager: com::sun::star::uno::XInterface
{

    //-------------------------------------------------------------------------
    /** Adds a filter identified by a title.

        @param aTitle
        Specifies the name of the filter as shown in the filter box of the
        FilePicker dialog.

        @param aFilter
        Specifies the extensions of the filter. Multiple filters should be
        semicolon separated. The semicolon may not be used as character in
        a filter expression. A typical filter is for instance "*.txt".

        @throws ::com::sun::star::lang::IllegalArgumentException
        If a filter with the specified title already exists.

    */
    void appendFilter( [in] string aTitle, [in] string aFilter )
        raises( ::com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /** Sets the current filter.

        @param aTitle
        Specifies the name of the filter to be set.

        @throws com::sun::star::lang::IllegalArgumentException
        If the specified filter was not found.
    */
    void setCurrentFilter( [in] string aTitle )
        raises( ::com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------
    /** Returns the currently selected filter.

        @returns
        The name of the selected filter or an empty string if
        there is no filter or no filter is currently selected.
    */
    string getCurrentFilter( );
};

//=============================================================================

}; }; }; }; };


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
