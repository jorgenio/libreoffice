/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart2_XRegressionCurveCalculator_idl
#define com_sun_star_chart2_XRegressionCurveCalculator_idl

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/util/XNumberFormatsSupplier.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>
#include <com/sun/star/geometry/RealPoint2D.idl>
#include <com/sun/star/chart2/XScaling.idl>

module com
{
module sun
{
module star
{
module chart2
{

interface XRegressionCurveCalculator : com::sun::star::uno::XInterface
{
    /** recalculates the parameters of the internal regression curve according to
        the <it>x</it>- and <it>y</it>-values given.

        @param aXValues
            All x-values that represent the measurement points on
            which the regression is based

        @param aYValues
            All y-values that represent the measurement points on
            which the regression is based
    */
    void recalculateRegression( [in] sequence< double > aXValues,
                                [in] sequence< double > aYValues );


    /** calculates the value of the regression curve for <it>x</it>.

        @param x
            The abscissa value for which the value of the regression
            curve should be calculated.  All numbers that are part of
            the domain of the regression function are valid.

        @return
            If <it>x</it> is element of the domain of the regression
            curve function, the result is its value.

        @throws com::sun::star::lang::IllegalArgumentException
            If <it>x</it> is not part of the domain of the regression
            function.
     */
    double getCurveValue( [in] double x )
        raises( com::sun::star::lang::IllegalArgumentException );

    /** calculate multiple points of a regression curve at once. Note
        that this method may optimize the output by returning less
        points, e.g. for a line you may get only two resulting points
        instead of <member>nPointCount</member> points.  This is only
        allowed if the parameter
        <member>bMaySkipPointsInCalculation</member> is set to
        <TRUE/>.

        <p>It is important that a renderer takes the scalings into
        account. When one of these parameters is unknown, no
        optimization must be done.</p>

        @param bMaySkipPointsInCalculation determines whether it is
               allowed to skip points in the calculation. When this
               parameter is <TRUE/> it is assumed that the underlying
               coordinate system is Cartesian.

        @param xScalingX a scaling that is used for the values in
               x-direction

        @param xScalingY a scaling that is used for the values in
               y-direction
     */
    sequence< com::sun::star::geometry::RealPoint2D > getCurveValues(
        [in] double min,
        [in] double max,
        [in] long nPointCount,
        [in] XScaling xScalingX,
        [in] XScaling xScalingY,
        [in] boolean bMaySkipPointsInCalculation )
        raises( com::sun::star::lang::IllegalArgumentException );

    /** Returns the value of the correlation coefficient for the given
        regression.  This value is often denoted as <it>r</it> or
        <it>R</it>.

        <p>The value of <it>r</it> is signed.  Often
        <it>r</it><sup>2</sup> is used instead of <it>r</it> to denote
        a regression curve's accuracy.</p>

        @return
            The return value is the fraction of the variance in the
            data that is explained by the regression.
     */
    double getCorrelationCoefficient();

    /** Retrieve a string showing the regression curve's function with
        calculated parameters.

        @return
            The string returned contains the regression curve's
            formula in a form <pre>"f(x) = ..."</pre>, where the
            calculated parts are filled out.  For a linear regression
            you might get <pre>"f(x) = 0.341 x + 1.45"</pre>.
     */
    string getRepresentation();

    /** Returns a representation using the given number format for formatting all numbers
        contained in the formula.

        @see getRepresentation
     */
    string getFormattedRepresentation( [in] com::sun::star::util::XNumberFormatsSupplier xNumFmtSupplier,
                                       [in] long nNumberFormatKey );
};

} ; // chart2
} ; // com
} ; // sun
} ; // star

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
