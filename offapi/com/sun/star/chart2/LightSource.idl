/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart_LightSource_idl
#define com_sun_star_chart_LightSource_idl

#include <com/sun/star/drawing/Direction3D.idl>

module com
{
module sun
{
module star
{
module chart2
{

/**
 */
struct LightSource
{
    /** the light source's color
     */
    long                        nDiffuseColor;

    /** the direction into which the light-source points
     */
    ::com::sun::star::drawing::Direction3D   aDirection;

    /**
     */
    boolean                     bIsEnabled;

    /** When <TRUE/>, the specularity of material is taken into
        account when lighting an object.
     */
    boolean                     bSpecular;
};

} ; // chart2
} ; // com
} ; // sun
} ; // star

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
