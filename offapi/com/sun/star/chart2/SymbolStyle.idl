/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart2_SymbolStyle_idl
#define com_sun_star_chart2_SymbolStyle_idl

//=============================================================================

module com {  module sun {  module star {  module chart2 {

//=============================================================================

/** determines what kind of symbol to use
 */
enum SymbolStyle
{
    /** The symbol is invisible
     */
    NONE,

    /** The symbol is taken automatically.

        <p>This will typically be the nth standard symbol for the nth
        data series.</p>
     */
    AUTO,

    /** uses one of the standard symbols.  Which standard symbol is
        given in <member>Symbol::StandardSymbol</member>.
     */
    STANDARD,

    /** uses the symbol given in the
        <type scope="com::sun::star::drawing">PolyPolygonBezierCoords</type>
        given in <member>Symbol::PolygonCoords</member>.
     */
    POLYGON,

    /** uses the graphic given in
        <member>Symbol::Graphic</member> as symbol.
     */
    GRAPHIC
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
