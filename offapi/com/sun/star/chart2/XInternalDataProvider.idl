/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart2_XInternalDataProvider_idl
#define com_sun_star_chart2_XInternalDataProvider_idl

#include <com/sun/star/chart2/data/XDataProvider.idl>
#include <com/sun/star/chart2/data/XDataSequence.idl>

module com
{
module sun
{
module star
{
module chart2
{

/** An internal DataProvider that has more access to data than a plain
    DataProvider.
 */
interface XInternalDataProvider  : com::sun::star::chart2::data::XDataProvider
{
    boolean         hasDataByRangeRepresentation( [in] string aRange );
    sequence< any > getDataByRangeRepresentation( [in] string aRange );
    void            setDataByRangeRepresentation( [in] string aRange, [in] sequence< any > aNewData );

    /** @param Note that -1 is allowed as the sequence is inserted
               after the given index. So to insert a sequence as the
               new first sequence (index 0), you would pass -1 here.
     */
    void insertSequence( [in] long nAfterIndex );
    void deleteSequence( [in] long nAtIndex );
    /** same as insertSequence with nAfterIndex being the largest
        current index of the data, i.e. (size - 1)
     */
    void appendSequence();

    void insertDataPointForAllSequences( [in] long nAfterIndex );
    void deleteDataPointForAllSequences( [in] long nAtIndex );
    void swapDataPointWithNextOneForAllSequences( [in] long nAtIndex );

    /** If range representations of data sequences change due to
        internal structural changes, they must be registered at the
        data provider.

        <p>Sequences that are directly retrieved via the methods of
        the XDataProvider interface are already registered.  If a
        labeled data sequence was created by cloning an existing one,
        it has to be explicitly registered via this method.</p>
     */
    void registerDataSequenceForChanges( [in] com::sun::star::chart2::data::XDataSequence xSeq );

    /** insert an additional sequence for categories nLevel>=1;
    categories at level 0 are always present and cannot be inserted or deleted
    @since OOo 3.3
    */
    void insertComplexCategoryLevel( [in] long nLevel );
    /** deletes an additional sequence for categories at nLevel>=1;
    categories at level 0 are always present and cannot be deleted
    @since OOo 3.3
    */
    void deleteComplexCategoryLevel( [in] long nLevel );
};

} ; // chart2
} ; // com
} ; // sun
} ; // star


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
