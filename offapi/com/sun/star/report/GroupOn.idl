/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_report_GroupOn_idl__
#define __com_sun_star_report_GroupOn_idl__


//=============================================================================

 module com {  module sun {  module star {  module report {

//=============================================================================

/** Specifies how to group data.
    @see XGroup
 */
constants GroupOn
{
    //-------------------------------------------------------------------------

    /** The same value in the column value or expression.
     */
    const short DEFAULT = 0;

    /** The same first nth of characters in the column value or expression.
     */
    const short PREFIX_CHARACTERS = 1;

    /** Dates in the same calendar year.
    */
    const short YEAR= 2;

    /** Dates in the same calendar quarter.
    */
    const short QUARTAL = 3;

    /** Dates in the same month.
    */
    const short MONTH = 4;

    /** Dates in the same week.
    */
    const short WEEK = 5;

    /** Dates on the same date.
    */
    const short DAY = 6;

    /** Times in the same hour.
    */
    const short HOUR = 7;

    /** Times in the same minute.
    */
    const short MINUTE = 8;

    /** Values within an interval you specify.
    */
    const short INTERVAL = 9;
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
