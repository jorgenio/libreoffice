/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_report_XGroup_idl__
#define __com_sun_star_report_XGroup_idl__

#include <com/sun/star/container/NoSuchElementException.idl>
#include <com/sun/star/report/XSection.idl>
#include <com/sun/star/report/XGroups.idl>
#include <com/sun/star/report/XFunctions.idl>
//=============================================================================

 module com {  module sun {  module star {  module report {

//=============================================================================

/** identifies a <type>XGroup</type>.
    A group is always a child of the groups collection in the report.
    @see XReportDefinition
    @see XGroups
 */
interface XGroup
{
    /** allows access to the groups collection of the report.
    */
    interface com::sun::star::container::XChild;

    /** allows access to the properties of the instance.
    */
    interface com::sun::star::beans::XPropertySet;

    /** allows life-time control of group instances.
     */
    interface com::sun::star::lang::XComponent;

    /** gives access to functions defined in the group definition.
    */
    interface XFunctionsSupplier;

    /** Defines if the group is sorted ascending or descending.
        The default is <TRUE/>.
     */
    [attribute,bound] boolean SortAscending;

    /** Defines if a group has a header.
    */
    [attribute,bound] boolean HeaderOn;

    /** Defines if a group has a footer.
    */
    [attribute,bound] boolean FooterOn;

    /** returns the group header.
        @throws <type scope="com::sun::star::container">NoSuchElementException</type>
            If the group has the header disabled.
        @see XSection
    */

    [attribute,readonly] XSection Header
    {
        get raises (com::sun::star::container::NoSuchElementException);
    };

    /** returns the group footer.
        @throws <type scope="com::sun::star::container">NoSuchElementException</type>
            If the group has the footer disabled.
        @see XSection
    */
    [attribute,readonly,bound] XSection Footer
    {
        get raises (com::sun::star::container::NoSuchElementException);
    };

    /** Specifies how to group data.
        @see GroupOn
    */
    [attribute,bound] short GroupOn
    {
        set raises (com::sun::star::lang::IllegalArgumentException);
    };

    /** Defines an interval value that rows are grouped by.
    */
    [attribute,bound] long GroupInterval;

    /** Specifies if a group header, detail, and footer section is printed on the same page.
        @see KeepTogether
    */
    [attribute,bound] short KeepTogether
    {
        set raises (com::sun::star::lang::IllegalArgumentException);
    };

    /** Specifies the parent of the group.
        @see XChild
    */
    [attribute,readonly] com::sun::star::report::XGroups Groups;

    /** Defines either a column name or a expression.
    */
    [attribute,bound] string Expression;

    /** Specifies that the group header should always be printed on a new column.
    */
    [attribute,bound] boolean StartNewColumn;

    /** Specifies that the group header should always be printed on a new page and the reset of the page number to zero.
    */
    [attribute,bound] boolean ResetPageNumber;
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
