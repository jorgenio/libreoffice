/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_report_ForceNewPage_idl__
#define __com_sun_star_report_ForceNewPage_idl__


//=============================================================================

 module com {  module sun {  module star {  module report {

//=============================================================================

/** Specifies if the section will be printed on an separate page.
    <p>This does not apply to page header or page footer.</p>
    @see XSection
 */
constants ForceNewPage
{
    //-------------------------------------------------------------------------

    /** The current section is printed on the current page.
     */
    const short NONE = 0;

    /** The current section is printed at the top of a new page.
     */
    const short BEFORE_SECTION = 1;

    /** The next section following the current section is printed at the top of a new page.
     */
    const short AFTER_SECTION = 2;

    /** The current section is printed at the top of a new page as well as the next section.
     */
    const short BEFORE_AFTER_SECTION = 3;
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
