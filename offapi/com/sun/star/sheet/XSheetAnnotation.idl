/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSheetAnnotation_idl__
#define __com_sun_star_sheet_XSheetAnnotation_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/table/CellAddress.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides methods to query data of the annotation and to show and
    hide it.

    @see com::sun::star::sheet::CellAnnotation
 */
published interface XSheetAnnotation: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** returns the position of cell in the spreadsheet document that
        contains this annotation.
     */
    com::sun::star::table::CellAddress getPosition();

    //-------------------------------------------------------------------------

    /** returns the name of the user who last changed the annotation.
     */
    string getAuthor();

    //-------------------------------------------------------------------------

    /** returns a formatted string representing the date when the
        annotation was last changed.
     */
    string getDate();

    //-------------------------------------------------------------------------

    /** returns, whether the annotation is permanently visible.
     */
    boolean getIsVisible();

    //-------------------------------------------------------------------------

    /** specifies whether the annotation is permanently visible.
     */
    void setIsVisible( [in] boolean bIsVisible );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
