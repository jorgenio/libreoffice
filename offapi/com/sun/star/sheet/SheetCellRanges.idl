/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_SheetCellRanges_idl__
#define __com_sun_star_sheet_SheetCellRanges_idl__


#include <com/sun/star/table/CellProperties.idl>
#include <com/sun/star/style/CharacterProperties.idl>
#include <com/sun/star/style/CharacterPropertiesAsian.idl>
#include <com/sun/star/style/CharacterPropertiesComplex.idl>
#include <com/sun/star/style/ParagraphProperties.idl>
#include <com/sun/star/sheet/SheetRangesQuery.idl>


#include <com/sun/star/util/XReplaceable.idl>
#include <com/sun/star/util/XIndent.idl>
#include <com/sun/star/sheet/XSheetOperation.idl>
#include <com/sun/star/chart/XChartDataArray.idl>
#include <com/sun/star/sheet/XSheetCellRangeContainer.idl>
#include <com/sun/star/container/XEnumerationAccess.idl>
#include <com/sun/star/container/XNameContainer.idl>


#include <com/sun/star/sheet/XSheetConditionalEntries.idl>
#include <com/sun/star/beans/XPropertySet.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a collection of cell ranges in a spreadsheet document.

    @see com::sun::star::sheet::SheetCellRange
 */
published service SheetCellRanges
{
    //-------------------------------------------------------------------------

    /** contributes common cell formatting properties.
     */
    service com::sun::star::table::CellProperties;

    //-------------------------------------------------------------------------

    /** contributes properties for character formatting of Western text.
     */
    service com::sun::star::style::CharacterProperties;

    //-------------------------------------------------------------------------

    /** contributes properties for character formatting of Asian text.
     */
    service com::sun::star::style::CharacterPropertiesAsian;

    //-------------------------------------------------------------------------

    /** contributes properties for character formatting of Complex text.
     */
    service com::sun::star::style::CharacterPropertiesComplex;

    //-------------------------------------------------------------------------

    /** contributes properties for paragraph formatting.
     */
    service com::sun::star::style::ParagraphProperties;

    //-------------------------------------------------------------------------

    /** provides interfaces to find cells with specific properties.
     */
    service com::sun::star::sheet::SheetRangesQuery;

    //=========================================================================

    /** provides "Find & Replace" functionality.

        <p>The property
        <member scope="com::sun::star::util">SearchDescriptor::SearchWords</member>
        has a different meaning in spreadsheets: If set to <TRUE/>, the
        spreadsheet searches for cells containing the search text only.</p>
     */
    interface com::sun::star::util::XReplaceable;

    //-------------------------------------------------------------------------

    /** provides modifying the cell indentation.
     */
    interface com::sun::star::util::XIndent;

    //-------------------------------------------------------------------------

    /** provides computation of a value based on the contents of all
        cells of this range and to clear specific cells.
     */
    interface com::sun::star::sheet::XSheetOperation;

    //-------------------------------------------------------------------------

    /** provides modifying the source data cells of a chart and makes
        it possible to learn about changes in the cell values.
     */
    interface com::sun::star::chart::XChartDataArray;

    //-------------------------------------------------------------------------

    /** provides methods to access cell ranges via index and to add and
        remove cell ranges.
     */
    interface com::sun::star::sheet::XSheetCellRangeContainer;

    //-------------------------------------------------------------------------

    /** creates an enumeration of all cell ranges.

        @see com::sun::star::sheet::SheetCellRangesEnumeration
     */
    interface com::sun::star::container::XEnumerationAccess;

    //-------------------------------------------------------------------------

    /** provides access of the cell ranges via an user-defined name.

        <p>Later the range can be found, replaced or removed using that
        name.</p>
     */
    interface com::sun::star::container::XNameContainer;

    //=========================================================================

    /** contains the conditional formatting settings for this cell.

        <p>After a conditional format has been changed it has to be
        reinserted into the property set.</p>

        @see com::sun::star::sheet::TableConditionalFormat
     */
    [property] com::sun::star::sheet::XSheetConditionalEntries
        ConditionalFormat;

    //-------------------------------------------------------------------------

    /** contains the conditional formatting settings for this cell,
        using localized formulas.

        <p>After a conditional format has been changed it has to be
        reinserted into the property set.</p>

        @see com::sun::star::sheet::TableConditionalFormat
     */
    [optional, property] com::sun::star::sheet::XSheetConditionalEntries
        ConditionalFormatLocal;

    //-------------------------------------------------------------------------

    /** contains the data validation settings for this cell.

        <p>After the data validation settings have been changed the
        validation has to be reinserted into the property set.</p>

        @see com::sun::star::sheet::TableValidation
     */
    [property] com::sun::star::beans::XPropertySet Validation;

    //-------------------------------------------------------------------------

    /** contains the data validation settings for this cell,
        using localized formulas.

        <p>After the data validation settings have been changed the
        validation has to be reinserted into the property set.</p>

        @see com::sun::star::sheet::TableValidation
     */
    [optional, property] com::sun::star::beans::XPropertySet ValidationLocal;

    //-------------------------------------------------------------------------

    /** Returns the absolute address of the ranges as string, e.g. "$Sheet1.$B$2:$D$5".
     */
    [optional, readonly, property] string AbsoluteName;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
