/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSheetAuditing_idl__
#define __com_sun_star_sheet_XSheetAuditing_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/table/CellAddress.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides methods to access auditing (detective) features in a
    spreadsheet.
 */
published interface XSheetAuditing: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** removes arrows for one level of dependents of a formula cell.

        <p>If the method is executed again for the same cell, the previous
        level of dependent cells is removed.</p>

        @param aPosition
            the address of the formula cell.
     */
    boolean hideDependents( [in] com::sun::star::table::CellAddress aPosition );

    //-------------------------------------------------------------------------

    /** removes arrows for one level of precedents of a formula cell.

        <p>If the method is executed again for the same cell, the previous
        level of dependent cells is removed.</p>

        @param aPosition
            the address of the formula cell.
     */
    boolean hidePrecedents( [in] com::sun::star::table::CellAddress aPosition );

    //-------------------------------------------------------------------------

    /** draws arrows between a formula cell and its dependents.

        <p>If the method is executed again for the same cell, the next
        level of dependent cells is marked.</p>

        @param aPosition
            the address of the formula cell.
     */
    boolean showDependents( [in] com::sun::star::table::CellAddress aPosition );

    //-------------------------------------------------------------------------

    /** draws arrows between a formula cell and its precedents.

        <p>If the method is executed again for the same cell, the next
        level of dependent cells is marked.</p>

        @param aPosition
            the address of the formula cell.
     */
    boolean showPrecedents( [in] com::sun::star::table::CellAddress aPosition );

    //-------------------------------------------------------------------------

    /** draws arrows between a formula cell containing an error
        and the cells causing the error.
     */
    boolean showErrors( [in] com::sun::star::table::CellAddress aPosition );

    //-------------------------------------------------------------------------

    /** marks all cells containing invalid values.
     */
    boolean showInvalid();

    //-------------------------------------------------------------------------

    /** removes all auditing arrows from the spreadsheet.
     */
    void clearArrows();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
