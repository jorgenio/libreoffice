/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XCalculatable_idl__
#define __com_sun_star_sheet_XCalculatable_idl__

#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents something that can recalculate.
 */
published interface XCalculatable: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** recalculates all dirty cells.

        <p>This calculates all formula cells which have not yet been
        calculated after their precedents have changed.</p>
     */
    void calculate();

    //-------------------------------------------------------------------------

    /** recalculates all cells.
     */
    void calculateAll();

    //-------------------------------------------------------------------------

    /** returns whether automatic calculation is enabled.

        <p>With automatic calculation, each formula cell is recalculated
        whenever its value is needed after its precedents have changed.
        The value is needed if the cell is displayed or used in another
        calculation.</p>

        @returns
            <TRUE/>, if automatic calculation is enabled.
     */
    boolean isAutomaticCalculationEnabled();

    //-------------------------------------------------------------------------

    /** enables automatic calculation.

        <p>With automatic calculation, each formula cell is recalculated
        whenever its value is needed after its precedents have changed.
        The value is needed if the cell is displayed or used in another
        calculation.</p>

        @param bEnabled
            <TRUE/> to enable automatic calculation, <FALSE/> to disable.
     */
    [oneway] void enableAutomaticCalculation( [in] boolean bEnabled );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
