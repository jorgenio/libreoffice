/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XDrillDownDataSupplier_idl__
#define __com_sun_star_sheet_XDrillDownDataSupplier_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/sheet/DataPilotFieldFilter.idl>
#include <com/sun/star/table/CellAddress.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** supplies a filtered subset of the original data source based on filtering criteria.

    A service that acts as a DataPilot data source can optionally implement this
    interface to allow drill-down of result data.  The method this interface provides
    is used internally when calling <method>XDataPilotTable2::getDrillDownData</method>
    or <method>XDataPilotTable2::insertDrillDownSheet</method>.  If the data source
    service does not implement this interface, then the aforementioned two methods
    will have no effect.

    @see com::sun::star::sheet::DataPilotSource

    @since OOo 3.0
 */
interface XDrillDownDataSupplier: com::sun::star::uno::XInterface
{
    /** This method returns filtered subset of the original source data based on
        a given set of filtering criteria.

        @param aFilters filtering criteria

        @returns a filtered subset of the original source data as 2-dimensional
        sequences of <type scope="com::sun::star::uno">Any</any>.  The first row
        must be the header row.  Each <type scope="com::sun::star::uno">Any</type>
        instance must contain either <type>double</type> value for a numeric cell, or
        a <type>string</type> value for a string cell.

        @see com::sun::star::sheet::DataPilotFieldFilter
        @see com::sun::star::sheet::XDataPilotTable2
     */
    sequence< sequence< any > > getDrillDownData(
        [in] sequence< com::sun::star::sheet::DataPilotFieldFilter > aFilters );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
