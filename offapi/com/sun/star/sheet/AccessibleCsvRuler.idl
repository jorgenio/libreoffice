/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_AccessibleCsvRuler_idl__
#define __com_sun_star_sheet_AccessibleCsvRuler_idl__

#include <com/sun/star/accessibility/XAccessibleContext.idl>
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#include <com/sun/star/accessibility/XAccessibleText.idl>


//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** The accessible object of the ruler in the CSV import dialog.
    @see ::com::sun::star::sheet::AccessibleCsvTable

     @since OOo 1.1.2
 */
published service AccessibleCsvRuler
{
    //-------------------------------------------------------------------------

    /** This interface gives access to the whole content of the ruler.
     */
    interface ::com::sun::star::accessibility::XAccessibleContext;

    //-------------------------------------------------------------------------

    /** This interface gives access to the visibility of the ruler.
     */
    interface ::com::sun::star::accessibility::XAccessibleComponent;

    //-------------------------------------------------------------------------

    /** This interface gives access to the text representation of the ruler.
     */
    interface ::com::sun::star::accessibility::XAccessibleText;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
