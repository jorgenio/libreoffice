/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XCellFormatRangesSupplier_idl__
#define __com_sun_star_sheet_XCellFormatRangesSupplier_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/container/XIndexAccess.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides access to a collection of equal-formatted cell ranges.

    @see com::sun::star::sheet::SheetCellRange
    @see com::sun::star::sheet::SheetCellRanges
 */
published interface XCellFormatRangesSupplier: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** returns a collection of equal-formatted cell ranges.

        <p>Each cell of the original range is contained in one of the
        ranges (even unformatted cells). If there is a non-rectangular
        equal-formatted cell area, it will be split into several
        rectangular ranges.</p>

        @returns
            the collection of equal-formatted cell ranges.

        @see com::sun::star::sheet::CellFormatRanges
     */
    com::sun::star::container::XIndexAccess getCellFormatRanges();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
