/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XGoalSeek_idl__
#define __com_sun_star_sheet_XGoalSeek_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/sheet/GoalResult.idl>
#include <com/sun/star/table/CellAddress.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides seeking a goal value for a cell.
 */
published interface XGoalSeek: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** calculates a value which gives a specified result in a formula.

        @param aFormulaPosition
            is the address of the formula cell used for the calculation.

        @param aVariablePosition
            is the address of the cell that is used in the formula as
            variable.

        @param aGoalValue
            is the value which should be reached during the goal seek.

        @returns
            the result of the goal seek, including the value that results
            in the specified goal, using the specified formula.
     */
    com::sun::star::sheet::GoalResult seekGoal(
            [in] com::sun::star::table::CellAddress aFormulaPosition,
            [in] com::sun::star::table::CellAddress aVariablePosition,
            [in] string aGoalValue );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
