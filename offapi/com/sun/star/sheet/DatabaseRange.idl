/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sheet_DatabaseRange_idl__
#define __com_sun_star_sheet_DatabaseRange_idl__

#include <com/sun/star/sheet/XDatabaseRange.idl>
#include <com/sun/star/sheet/XCellRangeReferrer.idl>
#include <com/sun/star/beans/XPropertySet.idl>
#include <com/sun/star/container/XNamed.idl>
#include <com/sun/star/util/XRefreshable.idl>
#include <com/sun/star/table/CellRangeAddress.idl>


//=============================================================================

 module com {  module sun {  module star {  module sheet {

//=============================================================================

/** represents a database range in a spreadsheet document.

    <p>A database range is a name for a cell range that also stores filtering,
    sorting, subtotal and data import settings and options.</p>

    @see com::sun::star::sheet::DatabaseRanges
 */
published service DatabaseRange
{
    // DocMerge: empty anyway
    interface com::sun::star::sheet::XDatabaseRange;

    // DocMerge: empty anyway
    interface com::sun::star::sheet::XCellRangeReferrer;

    // DocMerge: empty anyway
    interface com::sun::star::beans::XPropertySet;

    // DocMerge: empty anyway
    interface com::sun::star::container::XNamed;

    [optional] interface com::sun::star::util::XRefreshable;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::sheet::DatabaseRange::MoveCells
    /** if this property is set, columns or rows are inserted or deleted
        when the size of the range is changed by an update operation.
     */
    [property] boolean MoveCells;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::sheet::DatabaseRange::KeepFormats
    /** if this property is set, cell formats are extended
        when the size of the range is changed by an update operation.
     */
    [property] boolean KeepFormats;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::sheet::DatabaseRange::StripData
    /** if this property is set, the cell contents within the database
        range are left out when the document is saved.
     */
    [property] boolean StripData;

    //-------------------------------------------------------------------------

    /** specifies whether the AutoFilter is enabled or not.

        @since OOo 1.1.2
     */
    [optional, property] boolean AutoFilter;

    //-------------------------------------------------------------------------

    /** specifies whether the filter criteria should be taken from a CellRange.

        @since OOo 1.1.2
     */
    [optional, property] boolean UseFilterCriteriaSource;

    //-------------------------------------------------------------------------

    /** specifies the range where the filter can find the filter criteria.
        <p>This is only used if <member>SheetFilterDescriptor::UseFilterCriteriaSource</member> is <TRUE/>.</p>

        @since OOo 1.1.2
     */
    [optional, property] com::sun::star::table::CellRangeAddress FilterCriteriaSource;

    //-------------------------------------------------------------------------

    /** specifies the time between two refresh actions in seconds.

        @since OOo 2.0
     */
    [optional, property] long RefreshPeriod;

    //-------------------------------------------------------------------------

    /** specifies whether the imported data is only a selection of the database.

        @since OOo 2.0
     */
    [optional, property] boolean FromSelection;

    //-------------------------------------------------------------------------

    /** returns the index used to refer to this range in token arrays.

        <p>A token describing a database range shall contain the op-code
        obtained from the <const>FormulaMapGroupSpecialOffset::DB_AREA</const>
        and this index as data part.</p>

        @see com::sun::star::sheet::FormulaToken
        @see com::sun::star::sheet::FormulaMapGroupSpecialOffset::DB_AREA

        @since OOo 3.0
     */
    [optional, readonly, property] long TokenIndex;
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
