/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_sheet_XSheetPageBreak_idl__
#define __com_sun_star_sheet_XSheetPageBreak_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/sheet/TablePageBreakData.idl>

//=============================================================================

module com {  module sun {  module star {  module sheet {

//=============================================================================

/** provides access to page breaks in a sheet.

    @deprecated
 */
published interface XSheetPageBreak: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** returns a sequence of descriptions of all horizontal page breaks
        on the sheet.

        <P>This includes manual and automatic page breaks. To add or
        remove manual breaks, use the
        <member scope="com::sun::star::table">TableColumn::IsStartOfNewPage</member>
        property of the column.</P>

        @returns
            a sequence of structs containing column page break data.
     */
    sequence< com::sun::star::sheet::TablePageBreakData > getColumnPageBreaks();

    //-------------------------------------------------------------------------

    /** returns a sequence of descriptions of all vertical page breaks
        on the sheet.

        <P>This includes manual and automatic page breaks. To add or
        remove manual breaks, use the
        <member scope="com::sun::star::table">TableRow::IsStartOfNewPage</member>
        property of the row.</P>

        @returns
            a sequence of structs containing row page break data.
     */
    sequence< com::sun::star::sheet::TablePageBreakData > getRowPageBreaks();

    //-------------------------------------------------------------------------

    /** removes all manual page breaks on the sheet.
     */
    void removeAllManualPageBreaks();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
