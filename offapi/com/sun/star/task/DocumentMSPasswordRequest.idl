/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright IBM Corporation 2009.
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_task_DocumentMSPasswordRequest_idl__
#define __com_sun_star_task_DocumentMSPasswordRequest_idl__

#include <com/sun/star/task/PasswordRequest.idl>


//=============================================================================

module com { module sun { module star { module task {

//=============================================================================
/** this request specifies the mode in which the password for Microsoft Office file format encryption should be asked

    <P>
    It is supported by <type>InteractionHandler</type> service, and can
    be used to interact for a document password. Continuations for using with
    the mentioned service are Abort and Approve.
    </P>

    @since OOo 3.2
*/
published exception DocumentMSPasswordRequest: PasswordRequest
{
    //-------------------------------------------------------------------------
    /** the name of the document
    */
    string Name;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
