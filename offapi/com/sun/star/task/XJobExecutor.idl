/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_task_XJobExecutor_idl__
#define __com_sun_star_task_XJobExecutor_idl__

#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com {  module sun {  module star {  module task {

//=============================================================================
/** starts action for any triggered event from outside

    <p>
    If somewhere from outside trigger an event on this interface
    it will be used to find any registered service inside configuration
    of this executor. If somewhere could be found it will be
    started and controlled by this instance. After it finish his work
    it's possible to deactivate further startups or let him run again if
    a new event will be detected later.
    </p>

    @see JobExecutor
 */
published interface XJobExecutor : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** trigger event to start registered jobs

        <p>
        Jobs are registered in configuration and will be started by executor
        automatically, if they are registered for triggered event.
        The meaning of given string <var>Event</var> mustn't be known. Because
        for the executor it's enough to use it for searching a registered job
        inside his own configuration. So no special events will be defined here.
        </p>

        @param Event
                describe the event for which jobs can be registered and should be started
    */
    [oneway] void trigger( [in] string Event );
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
