/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_text_textfield_TableFormula_idl__
#define __com_sun_star_text_textfield_TableFormula_idl_

#include <com/sun/star/text/TextField.idl>

//=============================================================================

module com { module sun { module star { module text { module textfield {

//=============================================================================
/** specifies service of a table formula text field.
    @deprecated
    @see com::sun::star::text::TextField
*/
published service TableFormula
{
    service  com::sun::star::text::TextField;

    /** contains the formula.     */
    [property] string Formula;

    /** contains the current content of the text field.
        <p> This property is especially useful for import/export purposes. </p>
     */
    [property] string CurrentPresentation;

    /** determines whether the formula displayed as text or evaluated.
    */
    [property] boolean IsShowFormula;
    /** this is the number format for this field.
        @see com::sun::star::util::NumberFormatter
    */
    [property] short NumberFormat;
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
