/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_AccessibleTextDocumentPageView_idl__
#define __com_sun_star_text_AccessibleTextDocumentPageView_idl__

#include <com/sun/star/accessibility/XAccessibleContext.idl>
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#include <com/sun/star/accessibility/XAccessibleSelection.idl>
#include <com/sun/star/accessibility/XAccessibleEventBroadcaster.idl>


//=============================================================================

module com {  module sun {  module star {  module text {

//=============================================================================

/** The accessible page preview of a text document.

     @since OOo 1.1.2
 */
published service AccessibleTextDocumentPageView
{
    /** This interface gives access to page that are visible in the
          page preview of a text document.
        <ul>
            <li>The children returned by
                <method scope="::com::sun::star::accessibility"
                >XAccessibleContext::getAccessibleChild</type> all support
                the interface XAccessible. Calling
                <method scope="::com::sun::star::accessibility"
                >XAccessible::getAccessibleContext</type> for these children
                returns an object that supports one of the service
                <type scope="::com::sun::star::text"
                >AccessiblePageView</type>.
            <li>The role is <const scope="::com::sun::star::accessibility"
                >AccessibleRole::DOCUMENT</const>.
            <li>The name is "document view" (or the equivalent term
                in application's language).
            <li>The description also is "document view" (or the equivalent term
                in application's language).
            <li>There are no relation sets.
            <li>The following states might be contained in the state set
                returned by <method scope="::com::sun::star::accessibility"
                >XAccessibleContext::getAccessibleStateSet</method>:
                <ul>
                    <li><const scope="::com::sun::star::accessibility"
                        >AccessibleStateType::DEFUNC</const> (indicates that
                        the document window has been closed)
                    <li><const scope="::com::sun::star::accessibility"
                        >AccessibleStateType::ENABLED</const> (always
                        contained)
                    <li><const scope="::com::sun::star::accessibility"
                        >AccessibleStateType::OPAQUE</const> (always
                        contained)
                    <li><const scope="::com::sun::star::accessibility"
                        >AccessibleStateType::SHOWING</const>
                    <li><const scope="::com::sun::star::accessibility"
                        >AccessibleStateType::VISIBLE</const>
                </ul>
            <li>The locale is the application's locale.
        </ul>

    */
    interface ::com::sun::star::accessibility::XAccessibleContext;

    /** This interface describes the graphical representation of a text
          document view.
    */
    interface ::com::sun::star::accessibility::XAccessibleComponent;

    /** This is the interface for listeners
    */
    interface ::com::sun::star::accessibility::XAccessibleEventBroadcaster;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
