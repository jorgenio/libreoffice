/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_TextTableCursor_idl__
#define __com_sun_star_text_TextTableCursor_idl__

#include <com/sun/star/text/XTextTableCursor.idl>
#include <com/sun/star/beans/XPropertySet.idl>
#include <com/sun/star/style/CharacterProperties.idl>
#include <com/sun/star/style/CharacterPropertiesAsian.idl>
#include <com/sun/star/style/CharacterPropertiesComplex.idl>
#include <com/sun/star/style/ParagraphProperties.idl>
#include <com/sun/star/xml/UserDefinedAttributesSupplier.idl>


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

/** specifies a cursor in text tables.

    <p>This cursor can be used to:</p>
    <ul>
        <li>travel through text table cells</li>
        <li>select text table cells</li>
        <li>get property values from the selected cells</li>
        <li>set property values in the selected cells</li>
    </ul>

    @see    com::sun::star::text::TextTable
 */
published service TextTableCursor
{
    interface com::sun::star::text::XTextTableCursor;

    interface com::sun::star::beans::XPropertySet;

    service com::sun::star::style::CharacterProperties;
    service com::sun::star::style::CharacterPropertiesAsian;
    service com::sun::star::style::CharacterPropertiesComplex;
    service com::sun::star::style::ParagraphProperties;

    [optional] service com::sun::star::xml::UserDefinedAttributesSupplier;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
