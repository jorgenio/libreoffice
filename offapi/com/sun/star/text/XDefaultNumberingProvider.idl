/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XDefaultNumberingProvider_idl__
#define __com_sun_star_text_XDefaultNumberingProvider_idl__

#include <com/sun/star/container/XIndexAccess.idl>
#include <com/sun/star/lang/Locale.idl>
#include <com/sun/star/beans/PropertyValues.idl>

module com { module sun { module star { module text {

/**
    provides access to default <type scope="com::sun::star::text">NumberingRules</type>
     according to a given locale information.
    <p></p>
    @see <type scope="com::sun::star::text">NumberingRules</type>
    @see <type scope="com::sun::star::lang">Locale</type>
    @version    1.0
    @author     <a href="mailto:oliver.specht@germany.sun.com">Oliver Specht</a>
*/
published interface XDefaultNumberingProvider : com::sun::star::uno::XInterface
{
    /**
        provides access to outline numberings according to
        a given <type scope="com::sun::star::lang">Locale</type>.
        <p>Outline numberings usually consist of levels
            with different settings. </p>
        @see <type scope="com::sun::star::text">NumberingLevel</type>
    */
    sequence<com::sun::star::container::XIndexAccess>
        getDefaultOutlineNumberings([in]com::sun::star::lang::Locale aLocale);
    /**
        provides access to outline numberings according
        to a given <type scope="com::sun::star::lang">Locale</type>.
        <p>In contrast to outline numberings the continuous
            numberings consist of
            level using the equal settings in all numbering levels.</P>
        @see <type scope="com::sun::star::text">NumberingLevel</type>
        @see <type scope="com::sun::star::lang">Locale</type>
    */
    sequence<com::sun::star::beans::PropertyValues>
        getDefaultContinuousNumberingLevels([in]com::sun::star::lang::Locale aLocale);
};
};};};};

#endif



/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
