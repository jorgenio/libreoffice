/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_BibliographyDataField_idl__
#define __com_sun_star_text_BibliographyDataField_idl__

//=============================================================================

 module com {  module sun {  module star {  module text {
//=============================================================================

// DocMerge from idl: constants com::sun::star::text::BibliographyDataField
/** These values define parts of bibliographic data. They are used to create a
        bibliography in a text document.
     <p> Depending on the type of the data some of the fields will usually be left empty.
     </p>
 */
published constants BibliographyDataField
{
    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::IDENTIFIER
    /** This field contains a unique identifier for the bibliographic data.
     */
    const short IDENTIFIER      = 0;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::BIBILIOGRAPHIC_TYPE
    /** This field contains the type of the bibliographic reference. It is of the
             type BibliographyDataType.
             @see BibliographyDataType
     */
    const short BIBILIOGRAPHIC_TYPE     = 1;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::ADDRESS
    /** This field contains the address of the publisher.
     */
    const short ADDRESS         = 2;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::ANNOTE
    /** This field contains an annotation.
     */
    const short ANNOTE          = 3;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::AUTHOR
    /** This field contains the name(s) of the author(s)
     */
    const short AUTHOR          = 4;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::BOOKTITLE
    /** This field contains the title of the book.
     */
    const short BOOKTITLE       = 5;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CHAPTER
    /** This field contains the name or number of the chapter.
     */
    const short CHAPTER         = 6;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::EDITION
    /** This field contains the number or name of the edition.
     */
    const short EDITION         = 7;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::EDITOR
    /** This field contains the name(s) of the editor(s)
     */
    const short EDITOR          = 8;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::HOWPUBLISHED
    /** This field contains a description of the type of the publishing.
     */
    const short HOWPUBLISHED    = 9;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::INSTITUTION
    /** This field contains the name of the institution where the publishing was created.
     */
    const short INSTITUTION     = 10;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::JOURNAL
    /** This field contains the name of the journal.
     */
    const short JOURNAL         = 11;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::MONTH
    /** This field contains number or name of the month of the publishing.
     */
    const short MONTH           = 12;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::NOTE
    /** This field contains a note.
     */
    const short NOTE            = 13;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::NUMBER
    /** This field contains the number of the publishing.
     */
    const short NUMBER          = 14;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::ORGANIZATIONS
    /** This field contains the name of the organizations where the publishing was created.
     */
    const short ORGANIZATIONS   = 15;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::PAGES
    /** This field contains the number(s) of the page(s) of the reference into a publishing.
     */
    const short PAGES           = 16;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::PUBLISHER
    /** This field contains the name of the publisher.
     */
    const short PUBLISHER       = 17;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::SCHOOL
    /** This field contains the name of the university or school where the publishing was created.
     */
    const short SCHOOL          = 18;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::SERIES
    /** This field contains the series of the publishing.
     */
    const short SERIES          = 19;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::TITLE
    /** This field contains the title of the publishing.
     */
    const short TITLE           = 20;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::REPORT_TYPE
    /** This field contains a description of the type of the report.
     */
    const short REPORT_TYPE     = 21;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::VOLUME
    /** This field contains the volume of the publishing.
     */
    const short VOLUME          = 22;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::YEAR
    /** This field contains the year when the publishing was created.
     */
    const short YEAR            = 23;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::URL
    /** This field contains URL of the publishing.
     */
    const short URL             = 24;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CUSTOM1
    /** This field contains user defined data.
     */
    const short CUSTOM1         = 25;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CUSTOM2
    /** This field contains user defined data.
     */
    const short CUSTOM2         = 26;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CUSTOM3
    /** This field contains user defined data.
     */
    const short CUSTOM3         = 27;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CUSTOM4
    /** This field contains user defined data.
     */
    const short CUSTOM4         = 28;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::CUSTOM5
    /** This field contains user defined data.
     */
    const short CUSTOM5         = 29;

    // DocMerge from idl: value com::sun::star::text::BibliographyDataField::ISBN
    /** This field contains the ISBN data of the publishing.
     */
    const short ISBN            = 30;
};
//=============================================================================

}; }; }; };

#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
