/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XParagraphAppend_idl__
#define __com_sun_star_text_XParagraphAppend_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/beans/PropertyValues.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>

#include <com/sun/star/beans/UnknownPropertyException.idl>

#include <com/sun/star/text/XTextRange.idl>

//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

/** .
 */
interface XParagraphAppend : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
        /** appends a new and empty paragraph at the end of the text.

                <p>The properties are applied to the new paragraph.
        </p>

        @param
            CharacterAndParagraphProperties can contain all the properties defined by the service
                      <type scope="com::sun::star::text">Paragraph.

     */
         com::sun::star::text::XTextRange appendParagraph(  [in] com::sun::star::beans::PropertyValues CharacterAndParagraphProperties )
                        raises( com::sun::star::lang::IllegalArgumentException,
                                       com::sun::star::beans::UnknownPropertyException );
         /** appends a new and empty paragraph at the end of the text.

                <p>The properties are applied to the last paragraph before the new paragraph is inserted.
                </p>

        @param
            CharacterAndParagraphProperties can contain all the properties defined by the service
                      <type scope="com::sun::star::text">Paragraph.

         */
         com::sun::star::text::XTextRange finishParagraph(  [in] com::sun::star::beans::PropertyValues CharacterAndParagraphProperties )
                        raises( com::sun::star::lang::IllegalArgumentException,
                                       com::sun::star::beans::UnknownPropertyException );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
