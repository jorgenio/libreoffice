/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_DateDisplayFormat_idl__
#define __com_sun_star_text_DateDisplayFormat_idl__


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

/** This constants define how a date field is formated before it is displayed.
    The format may also depend on the system or document locale. The samples
    are in German.

    @deprecated
 */
published constants DateDisplayFormat
{
    /** the shortest system standard
     */
    const short STANDARD_SHORT = 0;

    //-------------------------------------------------------------------------

    /** the longest system standard
     */
    const short STANDARD_LONG = 1;

    //-------------------------------------------------------------------------

    /** 22.11.73
     */
    const short MMDDYY = 2;

    //-------------------------------------------------------------------------

    /** 22.11.1973
     */
    const short MMDDYYYY = 3;

    //-------------------------------------------------------------------------

    /** 22. Nov 1973
     */
    const short DDMMMYYYY = 4;

    //-------------------------------------------------------------------------

    /** 22. November 1973
     */
    const short DDMMMMYYYY = 5;

    //-------------------------------------------------------------------------

    /** Do, 22. November 1973
     */
    const short NNDDMMMMYYYY = 6;

    //-------------------------------------------------------------------------

    /** Donnerstag, 22. November 1973
     */
    const short NNNNDDMMMMYYYY = 7;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
