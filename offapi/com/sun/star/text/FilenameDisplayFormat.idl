/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_FilenameDisplayFormat_idl__
#define __com_sun_star_text_FilenameDisplayFormat_idl__


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

// DocMerge from idl: constants com::sun::star::text::FilenameDisplayFormat
/** These constants are used to specify which parts of an URL are displayed
        in a field.
 */
published constants FilenameDisplayFormat
{
    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::text::FilenameDisplayFormat::FULL
    /** The content of the URL is completely displayed.
     */
    const short FULL = 0;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::text::FilenameDisplayFormat::PATH
    /** Only the path of the file is displayed.
     */
    const short PATH = 1;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::text::FilenameDisplayFormat::NAME
    /** Only the name of the file without the file extension is displayed.
     */
    const short NAME = 2;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::text::FilenameDisplayFormat::NAME_AND_EXT
    /** The file name including the file extension is displayed.
     */
    const short NAME_AND_EXT = 3;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
