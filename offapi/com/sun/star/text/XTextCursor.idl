/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_XTextCursor_idl__
#define __com_sun_star_text_XTextCursor_idl__

#include <com/sun/star/text/XTextRange.idl>


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

/** extends a text range by method to modify its position.
 */
published interface XTextCursor: com::sun::star::text::XTextRange
{
    //-------------------------------------------------------------------------

    /** sets the end of the position to the start.
     */
    [oneway] void collapseToStart();

    //-------------------------------------------------------------------------

    /** sets the start of the position to the end.
     */
    [oneway] void collapseToEnd();

    //-------------------------------------------------------------------------

    /** determines if the start and end positions are the same.
     */
    boolean isCollapsed();

    //-------------------------------------------------------------------------

    /** moves the cursor the specified number of characters to the left.

        @param nCount
            the number of characters to move.

        @param bExpand
            specifies if the current selection of the cursor should
            be expanded or not.

        @returns
            <TRUE/> if the command was successfully completed.
            <FALSE/> otherwise.

            <p>Note: Even if the command was not completed successfully
            it may be completed partially. E.g. if it was required to move
            5 characters but it is only possible to move 3 <FALSE/> will
            be returned and the cursor moves only those 3 characters.<p>
     */
    boolean goLeft( [in] short nCount,
             [in] boolean bExpand );

    //-------------------------------------------------------------------------

    /** moves the cursor the specified number of characters to the right.

        @param nCount
            the number of characters to move.

        @param bExpand
            specifies if the current selection of the cursor should
            be expanded or not.

        @returns
            <TRUE/> if the command was successfully completed.
            <FALSE/> otherwise.

            <p>Note: Even if the command was not completed successfully
            it may be completed partially. E.g. if it was required to move
            5 characters but it is only possible to move 3 <FALSE/> will
            be returned and the cursor moves only those 3 characters.<p>
     */
    boolean goRight( [in] short nCount,
             [in] boolean bExpand );

    //-------------------------------------------------------------------------

    /** moves the cursor to the start of the text.
     */
    void gotoStart( [in] boolean bExpand );

    //-------------------------------------------------------------------------

    /** moves the cursor to the end of the text.
     */
    void gotoEnd( [in] boolean bExpand );

    //-------------------------------------------------------------------------

    /** moves or expands the cursor to a specified <type>TextRange</type>.
     */
    void gotoRange( [in] com::sun::star::text::XTextRange xRange,
             [in] boolean bExpand );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
