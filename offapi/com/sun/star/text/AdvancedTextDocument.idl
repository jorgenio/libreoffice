/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_text_AdvancedTextDocument_idl__
#define __com_sun_star_text_AdvancedTextDocument_idl__

#include <com/sun/star/lang/Locale.idl>

#include <com/sun/star/text/HypertextDocument.idl>

#include <com/sun/star/text/XFootnotesSupplier.idl>

#include <com/sun/star/text/XEndnotesSupplier.idl>

#include <com/sun/star/util/XReplaceable.idl>

#include <com/sun/star/text/XPagePrintable.idl>

#include <com/sun/star/text/XReferenceMarksSupplier.idl>

#include <com/sun/star/text/XLineNumberingSupplier.idl>

#include <com/sun/star/text/XChapterNumberingSupplier.idl>

#include <com/sun/star/beans/XPropertySet.idl>


//=============================================================================

 module com {  module sun {  module star {  module text {

//=============================================================================

// DocMerge from xml: service com::sun::star::text::AdvancedTextDocument
/** An advanced text document is an extension of a hypertext document
    which can also contain text-frames, foot- and endnotes, and some
    other advanced contents.

    @deprecated
        use <type>TextDocument</type> instead.
 */
published service AdvancedTextDocument
{
    // DocMerge: empty anyway
    service com::sun::star::text::HypertextDocument;

    // DocMerge: empty anyway
    interface com::sun::star::text::XFootnotesSupplier;

    // DocMerge: empty anyway
    interface com::sun::star::text::XEndnotesSupplier;

    // DocMerge: empty anyway
    interface com::sun::star::util::XReplaceable;

    // DocMerge: empty anyway
    interface com::sun::star::text::XPagePrintable;

    // DocMerge: empty anyway
    interface com::sun::star::text::XReferenceMarksSupplier;

    // DocMerge: empty anyway
    interface com::sun::star::text::XLineNumberingSupplier;

    // DocMerge: empty anyway
    interface com::sun::star::text::XChapterNumberingSupplier;

    // DocMerge: empty anyway
    interface com::sun::star::beans::XPropertySet;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::text::AdvancedTextDocument::CharLocale
    /** contains the identifier of the default locale of the document.
     */
    [property] com::sun::star::lang::Locale CharLocale;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::text::AdvancedTextDocument::CharacterCount
    /** contains the count of all characters in the document.
     */
    [readonly, property] long CharacterCount;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::text::AdvancedTextDocument::ParagraphCount
    /** contains the count of all paragraphs in the document.
     */
    [readonly, property] long ParagraphCount;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::text::AdvancedTextDocument::WordCount
    /** contains the count of all words in the document.@see WordSeparator
     */
    [readonly, property] long WordCount;

    //-------------------------------------------------------------------------

    // DocMerge from xml: property com::sun::star::text::AdvancedTextDocument::WordSeparator
    /** contains a string that consists of characters that mark the
        separation of words in counting the words in a document.



        <p> I.e., slash and backslash. Whitespace (tab stop,
        space, paragraph break, or line break) always separate
        words.</p>@see WordCount
     */
    [property] string WordSeparator;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
