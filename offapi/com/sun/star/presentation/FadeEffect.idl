/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_presentation_FadeEffect_idl__
#define __com_sun_star_presentation_FadeEffect_idl__


//=============================================================================

 module com {  module sun {  module star {  module presentation {

//=============================================================================

/** specifies the fade effects to fade one page into another.
 */
published enum FadeEffect
{
    //-------------------------------------------------------------------------

    /** use no fade effects.
     */
    NONE,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Left".
     */
    FADE_FROM_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Top".
     */
    FADE_FROM_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Right".
     */
    FADE_FROM_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Bottom".
     */
    FADE_FROM_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade to Center".
     */
    FADE_TO_CENTER,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Center".
     */
    FADE_FROM_CENTER,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Left".
     */
    MOVE_FROM_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Top".
     */
    MOVE_FROM_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Right".
     */
    MOVE_FROM_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Bottom".
     */
    MOVE_FROM_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Roll from Left".
     */
    ROLL_FROM_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Roll from Top".
     */
    ROLL_FROM_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Roll from Right".
     */
    ROLL_FROM_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Roll from Bottom".
     */
    ROLL_FROM_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Vertical Stripes".
     */
    VERTICAL_STRIPES,

    //-------------------------------------------------------------------------

    /** use the fade effect "Horizontal Stripes".
     */
    HORIZONTAL_STRIPES,

    //-------------------------------------------------------------------------

    /** use the fade effect "Clockwise".
     */
    CLOCKWISE,

    //-------------------------------------------------------------------------

    /** use the fade effect "Counter Clockwise".
     */
    COUNTERCLOCKWISE,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Upper Left".
     */
    FADE_FROM_UPPERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Upper Right".
     */
    FADE_FROM_UPPERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Lower Left".
     */
    FADE_FROM_LOWERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Fade from Lower Right".
     */
    FADE_FROM_LOWERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Close Vertical".
     */
    CLOSE_VERTICAL,

    //-------------------------------------------------------------------------

    /** use the fade effect "Close Horizontal".
     */
    CLOSE_HORIZONTAL,

    //-------------------------------------------------------------------------

    /** use the fade effect "Open Vertical".
     */
    OPEN_VERTICAL,

    //-------------------------------------------------------------------------

    /** use the fade effect "Open Horizontal".
     */
    OPEN_HORIZONTAL,

    //-------------------------------------------------------------------------

    /** use the fade effect "Spiral Inward Left".
     */
    SPIRALIN_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Spiral Inward Right".
     */
    SPIRALIN_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Spiral Outward Left".
     */
    SPIRALOUT_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Spiral Outward Right".
     */
    SPIRALOUT_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Dissolve".
     */
    DISSOLVE,

    //-------------------------------------------------------------------------

    /** use the fade effect "Wavy Line from Left".
     */
    WAVYLINE_FROM_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Wavy Line from Top".
     */
    WAVYLINE_FROM_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Wavy Line from Right".
     */
    WAVYLINE_FROM_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Wavy Line from Bottom".
     */
    WAVYLINE_FROM_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Random".
     */
    RANDOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Stretch from Left".
     */
    STRETCH_FROM_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Stretch from Top".
     */
    STRETCH_FROM_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Stretch from Right".
     */
    STRETCH_FROM_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Stretch from Bottom".
     */
    STRETCH_FROM_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Vertical Lines".
     */
    VERTICAL_LINES,

    //-------------------------------------------------------------------------

    /** use the fade effect "Horizontal Lines".
     */
    HORIZONTAL_LINES,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Upper Left".
     */
    MOVE_FROM_UPPERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Upper Right".
     */
    MOVE_FROM_UPPERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Lower Right".
     */
    MOVE_FROM_LOWERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Move from Lower Left".
     */
    MOVE_FROM_LOWERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Left".
     */
    UNCOVER_TO_LEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Upper Left".
     */
    UNCOVER_TO_UPPERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Top".
     */
    UNCOVER_TO_TOP,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Upper Right".
     */
    UNCOVER_TO_UPPERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Right".
     */
    UNCOVER_TO_RIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Lower Right".
     */
    UNCOVER_TO_LOWERRIGHT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Bottom".
     */
    UNCOVER_TO_BOTTOM,

    //-------------------------------------------------------------------------

    /** use the fade effect "Uncover to Lower Left".
     */
    UNCOVER_TO_LOWERLEFT,

    //-------------------------------------------------------------------------

    /** use the fade effect "Vertical Checkerboard".
     */
    VERTICAL_CHECKERBOARD,

    //-------------------------------------------------------------------------

    /** use the fade effect "Horizontal Checkerboard".
     */
    HORIZONTAL_CHECKERBOARD
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
