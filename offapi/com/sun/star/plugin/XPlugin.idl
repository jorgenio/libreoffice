/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_plugin_XPlugin_idl__
#define __com_sun_star_plugin_XPlugin_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/io/XActiveDataSource.idl>

#include <com/sun/star/plugin/PluginException.idl>


//=============================================================================

 module com {  module sun {  module star {  module plugin {

//=============================================================================

/** This interface allows the container, e.g. document to control the plugin.
*/
published interface XPlugin: com::sun::star::uno::XInterface
{
    /** Tells the plugin instance that a new stream is requested.

        @param mimetype
               mime type of provided data
        @param aSource
               data source
        @param url
               url
        @param length
               number of bytes (if file)
        @param lastmodified
               time stamp of last modification (if file)
        @param isfile
               whether data comes from a file
        @return true upon success
    */
    boolean provideNewStream( [in] string mimetype,
             [in] com::sun::star::io::XActiveDataSource aSource,
             [in] string url,
             [in] long length,
             [in] long lastmodified,
             [in] boolean isfile )
            raises( com::sun::star::plugin::PluginException );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
