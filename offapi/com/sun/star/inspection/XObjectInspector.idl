/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_inspection_XObjectInspector_idl__
#define __com_sun_star_inspection_XObjectInspector_idl__

#include <com/sun/star/frame/XController.idl>
#include <com/sun/star/frame/XDispatchProvider.idl>
#include <com/sun/star/util/VetoException.idl>

//=============================================================================
module com {  module sun {  module star {  module inspection {

interface XObjectInspectorModel;
interface XObjectInspectorUI;

//-----------------------------------------------------------------------------
/** describes the main interface of an <type>ObjectInspector</type>.

    @see ObjectInspector

    @since OOo 2.0.3
*/
interface XObjectInspector
{
    /** allows to plug the inspector into an <type scope="com::sun::star::frame">XFrame</type>

        <p>The <member scope="com::sun::star::frame">XController::setModel</member> method
        accepts only <type>XObjectInspectorModel</type>s, and will return <FALSE/> if the
        to-be-attached model is not <NULL/>, but does not support this interface.</p>

        <p>If you do not want to support a full-blown <type scope="com::sun::star::frame">XModel</type>,
        use the <member>InspectorModel</member> attribute instead.</p>

        <p>The <member scope="com::sun::star::frame">XController::getViewData</member> and
        <member scope="com::sun::star::frame">XController::restoreViewData</member> exchange
        string values, which describes as much of the current view state as possible.</p>
    */
    interface com::sun::star::frame::XController;

    /** allows to intercept functionality

        <p>This interface is inherited from the <type scope="com::sun::star::frame">Controller</type>
        service, currently, there is no functionality to intercept at an <type>XObjectInspector</type>.</p>
    */
    interface com::sun::star::frame::XDispatchProvider;

    /** provides access to the current model of the inspector

        <p>The model is mainly responsible for providing the property handlers. Additionally,
        it can provide user interface names and help URLs for property categories.</p>

        <p>Note that there are two ways of setting or retrieving the current model: You can either
        use <member scope="com::sun::star::frame">XModel::setModel</member>, or, if you do not want
        or need to implement the full-blown <type scope="com::sun::star::frame">XModel</type> interface,
        you can use this property directly. Both approaches are semantically equivalent.</p>

        <p>If a new model is set at the inspector, the complete UI will be rebuilt to reflect
        the change, using the new property handlers provided by the new model.</p>
    */
    [attribute] XObjectInspectorModel InspectorModel;

    /** provides access to the user interface of the object inspector.

        <p>This interface can be used to access and manipulate various aspects of
        the user interface. For instance, you can enable and disable certain
        property controls (or parts thereof), or register observers for all property
        controls.</p>

        @since OOo 2.2
    */
    [attribute, readonly] XObjectInspectorUI InspectorUI;

    /** inspects a new collection of one or more objects.

        <p>If the sequence is empty, the UI of the <type>ObjectInspector</type> will be
        cleared.</p>

        <p>If the sequence contains more than one object, the <type>XObjectInspector</type>
        will create a complete set of property handlers (as indicated by
        <member>XObjectInspectorModel::HandlerFactories</member>) for <em>every</em> of
        the objects, and compose their output.</p>

        @throws com::sun::star::util::VetoException
            if the inspector cannot switch to another object set. This typically happens if
            one of the active <type>XPropertyHandler</type>'s raised a non-modal user interface,
            and vetoed suspension of this UI.

        @see XPropertyHandler::isComposable
        @see XPropertyHandler::onInteractivePropertySelection
        @see XPropertyHandler::suspend
    */
    void inspect( [in] sequence< com::sun::star::uno::XInterface > Objects )
        raises (com::sun::star::util::VetoException);
};

//=============================================================================

}; }; }; };

#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
