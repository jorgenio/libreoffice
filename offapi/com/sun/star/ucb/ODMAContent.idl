/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_ODMAContent_idl__
#define __com_sun_star_ucb_ODMAContent_idl__

#include <com/sun/star/lang/XComponent.idl>
#include <com/sun/star/ucb/XContent.idl>
#include <com/sun/star/ucb/XCommandProcessor.idl>
#include <com/sun/star/ucb/XCommandProcessor2.idl>
#include <com/sun/star/beans/XPropertiesChangeNotifier.idl>
#include <com/sun/star/beans/XPropertyContainer.idl>
#include <com/sun/star/beans/XPropertySetInfoChangeNotifier.idl>
#include <com/sun/star/ucb/XCommandInfoChangeNotifier.idl>
#include <com/sun/star/container/XChild.idl>

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** A OCP content is representation of a document saved in a DMS.

    <p>The document Content corresponds to a document stored in a Document Management System.
    </p>

    @see com::sun::star::ucb::ODMAContentProvider
    @see com::sun::star::ucb::Content

    @since OOo 1.1.2
*/
published service ODMAContent
{
    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::lang::XComponent;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::ucb::XContent;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.

        <p>

        <b>Supported Commands</b>
        <ul>
        <li>
        getCommandInfo
        </li>
        <li>
        getPropertySetInfo
        </li>
        <li>
        getPropertyValues
        </li>
        <li>
        setPropertyValues
        </li>
        <li>
        delete
        </li>
        <li>
        open
        </li>
        <li>
        close
        </li>
        </ul>

        <b>Supported Properties</b>
        <ul>
        <li>
        string ContentType ( read-only, always "application/vnd.sun.star.odma" )
        </li>
        <li>
        boolean IsDocument
        </li>
        <li>
        boolean IsFolder
        </li>
        <li>
        <type scope="com::sun::star::util">DateTime</type> DateCreated ( read-only )
        </li>
        <li>
        <type scope="com::sun::star::util">DateTime</type> DateModified ( read-only )
        </li>
        <li>
        string Author
        </li>
        <li>
        string Subject
        </li>
        <li>
        string Keywords
        </li>
        <li>
        string Size ( read-only )
        </li>
        <li>
        string Title
        </li>
        </ul>

        </p>
     */
    interface com::sun::star::ucb::XCommandProcessor;

    //-------------------------------------------------------------------------
    /** is an enhanced version of <type>XCommandProcessor</type> that has an
        additional method for releasing command identifiers obtained via
        <member>XCommandProcessor::createCommandIdentifier</member> to avoid
        resource leaks. For a detailed description of the problem refer to
        <member>XCommandProcessor2::releaseCommandIdentifier</member>.

        <p>Where many existing <type>Content</type> implementations do not
        (yet), every new implementation should support this interface.
     */
    [optional] interface com::sun::star::ucb::XCommandProcessor2;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertiesChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertyContainer;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertySetInfoChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::ucb::XCommandInfoChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::container::XChild;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
