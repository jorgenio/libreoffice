/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_ContentCreationError_idl__
#define __com_sun_star_ucb_ContentCreationError_idl__


//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** Codes for content creation errors.
*/
published enum ContentCreationError
{
    //-------------------------------------------------------------------------
    /** Unknown.
    */
    UNKNOWN,

    //-------------------------------------------------------------------------
    /** @deprecated
    */
    NO_CONTENT_BROKER,

    //-------------------------------------------------------------------------
    /** @deprecated
    */
    NO_IDENTIFIER_FACTORY,

    //-------------------------------------------------------------------------
    /** Creation of content identifier failed.
    */
    IDENTIFIER_CREATION_FAILED,

    //-------------------------------------------------------------------------
    /** No Content Provider for given content identifier available.
    */
    NO_CONTENT_PROVIDER,

    //-------------------------------------------------------------------------
    /** Provider was unable to create the content instance.
    */
    CONTENT_CREATION_FAILED
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
