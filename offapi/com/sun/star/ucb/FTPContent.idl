/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_FTPContent_idl__
#define __com_sun_star_ucb_FTPContent_idl__

#include <com/sun/star/lang/XComponent.idl>
#include <com/sun/star/ucb/XContent.idl>
#include <com/sun/star/ucb/XContentCreator.idl>
#include <com/sun/star/ucb/XCommandProcessor.idl>
#include <com/sun/star/beans/XPropertiesChangeNotifier.idl>
#include <com/sun/star/beans/XPropertyContainer.idl>
#include <com/sun/star/beans/XPropertySetInfoChangeNotifier.idl>
#include <com/sun/star/ucb/XCommandInfoChangeNotifier.idl>
#include <com/sun/star/container/XChild.idl>

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** A FTPContent provides an object representing either a (ftp-server-side)
    directory object allowing listing of children or a content object providing
    access to a (ftp-server-side) file.

    @see com::sun::star::ucb::FTPContentProvider

    @since OOo 1.1.2
*/
published service FTPContent
{
    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::lang::XComponent;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::ucb::XContent;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.

        <p>An object representing a directory in the file system is able
        to create a new object, which in turn can become the representation
        of a physical file/directory being contained in the initial directory.
        The precise way is the following:

        <p>

        <ol>
        <li>
        Let the parent folder create a new content by calling
        <member>XContentCreator::createNewContent</member> on it. The content
        type to use for new directories is
        "application/vnd.sun.staroffice.ftp-folder". To create a new file
        use the type "application/vnd.sun.staroffice.ftp-file".
        As return value you get a new file content object.
        </li>
        <li>
        Set a title at the new object by letting it execute
        the command "setPropertyValues", which sets at least the property
        "Title" to a non-empty value ).
        </li>
        <li>
        Let the new object ( not the parent! ) execute the command "insert".
        This will create the corresponding physical file or directory.
        For files, you need to supply the implementation of
        an <type scope="com::sun::star::io">XInputStream</type> with the
        command's parameters, that provides access to the stream data.
        </li>
        </ol>

        </p>
     */
    interface com::sun::star::ucb::XContentCreator;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.

        <p>

        <b>Supported Commands</b>

        <ul>
        <li>
        getCommandInfo
        </li>
        <li>
        getPropertySetInfo
        </li>
        <li>
        getPropertyValues
        </li>
        <li>
        setPropertyValues
        </li>
        <li>
        open
        </li>
        <li>
        insert
        </li>
        <li>
        delete
        </li>
        </ul>

        <b>Supported Properties</b>
        <ul>
        <li>
        string ContentType ( read-only, either
        "application/vnd.sun.staroffice.ftp-folder" or
        "application/vnd.sun.staroffice.ftp-file" )
        </li>
        <li>
        boolean IsDocument ( read-only, always <true/> for files, always
        <false/> for directories )
        </li>
        <li>
        boolean IsFolder ( read-only ), always <false/> for files, always
        <true/> for directories )
        </li>
        <li>
        boolean IsReadOnly ( read-only attribute )
        </li>
        <li>
        string Title ( file name )
        </li>
        <li>
        hyper Size ( file size )
        </li>
        <li>
        string Title ( file name )
        </li>
        <li>
        <type scope="com::sun::star::util">DateTime</type>DateCreated ( read-only )
        </li>
        </ul>
        </p>
     */
    interface com::sun::star::ucb::XCommandProcessor;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertiesChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertyContainer;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::beans::XPropertySetInfoChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::ucb::XCommandInfoChangeNotifier;

    //-------------------------------------------------------------------------
    /** This interface is implemented according to the specification of
        service <type>Content</type>.
     */
    interface com::sun::star::container::XChild;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
