/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_RemoteAccessContentProvider_idl__
#define __com_sun_star_ucb_RemoteAccessContentProvider_idl__

#include <com/sun/star/ucb/ContentProvider.idl>
#include <com/sun/star/ucb/XParameterizedContentProvider.idl>

//=============================================================================

module com { module sun { module star { module ucb {

//=============================================================================
/** A <type>RemoteAccessContentProvider</type> is a
    <type>ContentProvider</type> that provides access to contents available at
    other (remote) UCBs.
*/
published service RemoteAccessContentProvider
{
    //-------------------------------------------------------------------------
    /** A <type>RemoteAccessContentProvider</type> is a specialized
        <type>ContentProvider</type>.
     */
    service com::sun::star::ucb::ContentProvider;

    //-------------------------------------------------------------------------
    /** allows configuration of the <type>RemoteAccessContentProvider</type>,
        to specify that certain <type>XContentIdentifier</type>s map to
        <type>XContent</type>s from a certain remote UCB.  The Arguments
        string in the
        <member>XParameterizedContentProvider::registerInstance</member>
        call must contain the UNO URL of the remote UCB.

        @descr
        The <type>XContentProvider</type>s obtained through this interface
        implement the service <type>ContentProvider</type> and, if
        appropriate, also the interface <type>XFileIdentifierConverter</type>.
     */
    interface com::sun::star::ucb::XParameterizedContentProvider;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
