/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_ucb_ContentEvent_idl__
#define __com_sun_star_ucb_ContentEvent_idl__

#include <com/sun/star/lang/EventObject.idl>

//=============================================================================

module com { module sun { module star { module ucb {

 published interface XContent;
 published interface XContentIdentifier;

//=============================================================================
/** A structure for content events.
*/
published struct ContentEvent: com::sun::star::lang::EventObject
{
    //-------------------------------------------------------------------------
    /** The action.

        <p>The value can be one of the <type>ContentAction</type> constants.
    */
    long Action;

    //-------------------------------------------------------------------------
    /** The content to that the action is related (e.g., the content that was
        just physically destroyed, the content that was just inserted into a
        folder content).

        <p>This member must be filled as follows:

        <table border=1>
        <tr align=left>
            <td><member>ContentAction::INSERTED</member></td>
            <td>The content inserted into a folder</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::REMOVED</member></td>
            <td>The content removed from a folder</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::DELETED</member></td>
            <td>The deleted content</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::EXCHANGED</member></td>
            <td>The exchanged content (that already has the new content id)</td>
        </tr>
        </table>
    */
    XContent Content;

    //-------------------------------------------------------------------------
    /** A content identifier, which must be filled according to the action
        notified (e.g., the id of the folder content into which another content
        was inserted).

        <p>This member must be filled as follows:

        <table border=1>
        <tr align=left>
            <td><member>ContentAction::INSERTED</member></td>
            <td>Id of the folder the content was inserted into</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::REMOVED</member></td>
            <td>Id of the folder the content was removed from</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::DELETED</member></td>
            <td>Id of the deleted content</td>
        </tr>
        <tr align=left>
            <td><member>ContentAction::EXCHANGED</member></td>
            <td>Previous(!) id of the exchanged content</td>
        </tr>
        </table>
    */
    XContentIdentifier Id;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
