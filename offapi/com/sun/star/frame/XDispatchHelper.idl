/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_XDispatchHelper_idl__
#define __com_sun_star_frame_XDispatchHelper_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/frame/XDispatchProvider.idl>
#include <com/sun/star/beans/PropertyValue.idl>

//=============================================================================

 module com {  module sun {  module star {  module frame {

//=============================================================================
/** provides an easy way to dispatch functions useful at UI level.

    @see XDispatch

    @since OOo 1.1.2
 */
published interface XDispatchHelper : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
        /** executes the dispatch.

        Listeners are not supported here!

        @param DispatchProvider
             points to the provider, which should be asked for valid dispatch objects

        @param URL
            describes the feature which should be supported by internally used dispatch object

        @param TargetFrameName
            specifies the frame which should be the target for this request

        @param SearchFlags
            optional search parameter for finding the frame if no special
            <var>TargetFrameName</var> was used

        @param Arguments
            optional arguments for this request
            They depend on the real implementation of the dispatch object.

        @return
            A possible result of the executed internal dispatch.
            The information behind this <atom>any</atom> depends on the dispatch!

        @see XDispatch::dispatch()
        */

        any executeDispatch(
                [in] XDispatchProvider DispatchProvider,
                [in] string URL,
                [in] string TargetFrameName,
                [in] long SearchFlags,
                [in] sequence< com::sun::star::beans::PropertyValue > Arguments );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
