/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_frame_XDispatchInformationProvider_idl__
#define __com_sun_star_frame_XDispatchInformationProvider_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/frame/DispatchInformation.idl>
#include <com/sun/star/container/NoSuchElementException.idl>

//=============================================================================

module com {  module sun {  module star {  module frame {

//=============================================================================
/** provides information about supported commands

    <p>
    This interface can be used to retrieve additional information about
    supported commands. This interface is normally used by configuration
    implementations to retrieve all supported commands.
    A dispatch information provider is normally supported by a
    <type>Frame</type> service.
    </p>

    @see Frame

    @since OOo 2.0
 */
published interface XDispatchInformationProvider: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** returns all supported command groups.

        @returns
            a sequence of supported command groups.

        @see CommandGroup
    */
    sequence < short > getSupportedCommandGroups();

    //-------------------------------------------------------------------------
    /** returns additional information about supported commands of a
        given command group.

        @param CommandGroup
            specifies a command group.

        @returns
            name and group name of every command supported. A group ID which is
            not supported returns an empty <type scope="com::sun::star::uno">Sequence</type>.
    */
    sequence < DispatchInformation > getConfigurableDispatchInformation( [in] short CommandGroup );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
