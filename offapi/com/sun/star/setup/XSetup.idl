/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_setup_XSetup_idl__
#define __com_sun_star_setup_XSetup_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/setup/OSType.idl>
#include <com/sun/star/setup/UpdateType.idl>
#include <com/sun/star/setup/ModuleInfo.idl>
#include <com/sun/star/setup/InstallEnvironment.idl>
#include <com/sun/star/setup/InstallResponse.idl>
#include <com/sun/star/setup/VersionIdentifier.idl>
#include <com/sun/star/setup/SizeInfo.idl>
#include <com/sun/star/setup/MirrorEntry.idl>

module com {  module sun {  module star {  module setup {

/// @deprecated
published interface XSetup: com::sun::star::uno::XInterface
{
    // DocMerge: empty anyway
    boolean                 isVersionSupported          ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    sequence <short>        getAvailableLanguages       ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    ModuleInfo              getRootModule               ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    void                    setHiddenModule             ( [in] VersionIdentifier aVerIdentifier,
                                                          [in] string strModuleID,
                                                          [in] boolean bHidden );

    // DocMerge: empty anyway
    string                  getProductname              ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    string                  getVendorname               ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    string                  getDefaultDestinationPath   ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    SizeInfo                getStandardSizeValues       ( [in] VersionIdentifier aVerIdentifier,
                                                          [in] sequence <short> seqLanguages );

    // DocMerge: empty anyway
    string                  getHelpText                 ( [in] VersionIdentifier aVerIdentifier,
                                                          [in] string strPagename );

    // DocMerge: empty anyway
    string                  getReadmeText               ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    string                  getLicenseText              ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    string                  getNativeLocation           ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    sequence < any >        getMirrorList               ( [in] VersionIdentifier aVerIdentifier );

    // DocMerge: empty anyway
    UpdateType              isUpdateAvailable           ( [in] VersionIdentifier aVerIdentifier,
                                                          [out] VersionIdentifier aNewVerIdentifier );

    // DocMerge: empty anyway
    boolean                 isModuleAvailable           ( [in] VersionIdentifier aVerIdentifier,
                                                          [in] string strModuleID );

    // DocMerge: empty anyway
    sequence < any >        getActionListForInstall     ( [in] VersionIdentifier aVerIdentifier,
                                                          [in] VersionIdentifier aOldVerIdentifier,
                                                          [in] InstallEnvironment aEnvironment,
                                                          [out] InstallResponse aResponse );
    };

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
