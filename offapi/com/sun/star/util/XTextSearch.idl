/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_lang_XTextSearch_idl__
#define __com_sun_star_lang_XTextSearch_idl__


#include <com/sun/star/lang/Locale.idl>
#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/util/SearchOptions.idl>
#include <com/sun/star/util/SearchResult.idl>


module com { module sun { module star { module util {

/** enables an object to search in its content.
 */
published interface XTextSearch : com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** set the options for the forward or backward search.

    */
    void setOptions ([in] SearchOptions options);
    //-------------------------------------------------------------------------
    /** search forward in the searchStr, starts at startPos and ends by endpos.
        The result is returned in the SearchResult.

    */
    SearchResult  searchForward  ([in] string searchStr, [in] long startPos, [in] long endPos );
    //-------------------------------------------------------------------------
    /** search backward in the searchStr, starts at startPos and ends by endpos.
        The endpos must be lower then the startpos, because the function searches backward!
        The result is returned in the SearchResult.

    */
    SearchResult  searchBackward ([in] string searchStr, [in] long startPos, [in] long endPos );
};

//=============================================================================
}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
