/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_util_XCloseBroadcaster_idl__
#define __com_sun_star_util_XCloseBroadcaster_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/util/XCloseListener.idl>

//=============================================================================

 module com {  module sun {  module star {  module util {

//=============================================================================
/** broadcasts each tried closing of an object to all interest listener

    <p>
    The called object for closing must post the closing events immediately
    and before any internal cancel operations will be started.
    If a listener disagree with that it should throw a <type>CloseVetoException</type>
    and called function <method>XCloseable::close</method> must be broken immediately.
    It's not allowed to catch it inside the close() request.
    If no listener nor internal processes hinder the object on closing
    all listeners get a notification about real closing.
    </p>

    @see
 */
published interface XCloseBroadcaster: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** adds the specified listener to receive or have a veto for "close" events

        @param Listener
            the listener which is interest on closing events
     */
    [oneway] void addCloseListener( [in] XCloseListener Listener );

    //-------------------------------------------------------------------------
    /** removes the specified listener

        @param Listener
            the listener which isn't interest on closing events any longer
     */
    [oneway] void removeCloseListener( [in] XCloseListener Listener );
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
