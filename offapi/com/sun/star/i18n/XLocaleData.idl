/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_i18n_XLocaleData_idl__
#define __com_sun_star_i18n_XLocaleData_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/lang/Locale.idl>
#include <com/sun/star/i18n/LanguageCountryInfo.idl>
#include <com/sun/star/i18n/Currency.idl>
#include <com/sun/star/i18n/Calendar.idl>
#include <com/sun/star/i18n/LocaleDataItem.idl>
#include <com/sun/star/i18n/ForbiddenCharacters.idl>
#include <com/sun/star/i18n/FormatElement.idl>
#include <com/sun/star/i18n/Implementation.idl>

//============================================================================

module com { module sun { module star { module i18n {

//============================================================================

/**
    Access locale specific data as it is defined in XML locale data
    files compiled into the binary data libraries liblocaledata*.so
    respectively localedata*.dll.

    <p> For XML locale data files definitions see <a
    href="http://cgit.freedesktop.org/libreoffice/core/tree/i18npool/source/localedata/data/locale.dtd">
    the DTD file</a>. </p>
 */

published interface XLocaleData: com::sun::star::uno::XInterface
{
    //------------------------------------------------------------------------
    /** returns the LC_INFO locale information.
     */
    LanguageCountryInfo getLanguageCountryInfo(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns LC_CTYPE separators and markers.
     */
    LocaleDataItem      getLocaleItem(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_CALENDAR calendars for a locale.
     */
    sequence<Calendar>  getAllCalendars(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_CURRENCY currencies for a locale.
     */
    sequence<Currency>  getAllCurrencies(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_FORMAT format elements for a locale.
     */
    sequence<FormatElement> getAllFormats(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_COLLATION collators for a locale.
     */
    sequence<Implementation> getCollatorImplementations(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_SEARCH search options for a locale.
     */
    sequence<string>    getSearchOptions(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_COLLATION collation options for a locale.
     */
    sequence<string>    getCollationOptions(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_TRANSLITERATION transliterations for a locale.
     */
    sequence<string>    getTransliterations(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_MISC forbidden characters for a locale.
     */
    ForbiddenCharacters getForbiddenCharacters (
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all LC_MISC reserved words for a locale.

        @see reservedWords
     */
    sequence<string>    getReservedWord(
                                [in] com::sun::star::lang::Locale aLocale );

    //------------------------------------------------------------------------
    /** returns all available locales.
     */
    sequence<com::sun::star::lang::Locale> getAllInstalledLocaleNames();
};

//============================================================================
}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
