/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbc_XResultSetUpdate_idl__
#define __com_sun_star_sdbc_XResultSetUpdate_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/sdbc/SQLException.idl>

 module com {  module sun {  module star {  module sdbc {


/** provides the possibility to write changes made on a result set back to
    database.
 */
published interface XResultSetUpdate: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** inserts the contents of the insert row into the result set and
        the database. Must be on the insert row when this method is called.
        @throws SQLException
                if a database access error occurs.
     */
    void insertRow() raises (SQLException);
    //-------------------------------------------------------------------------

    /** updates the underlying database with the new contents of the
        current row. Cannot be called when on the insert row.
        @throws SQLException
                if a database access error occurs.
     */
    void updateRow() raises (SQLException);
    //-------------------------------------------------------------------------

    /** deletes the current row from the result set and the underlying
        database. Cannot be called when on the insert row.
        @throws SQLException
                if a database access error occurs.
     */
    void deleteRow() raises (SQLException);
    //-------------------------------------------------------------------------

    /** cancels the updates made to a row.


        <p>
        This method may be called after calling an
        <code>updateXXX</code>
        method(s) and before calling
        <member scope="com::sun::star::sdbc">XResultSetUpdate::updateRow()</member>
        to rollback the updates made to a row. If no updates have been made or
        <code>updateRow</code>
        has already been called, then this method has no
        effect.
        </p>
        @throws SQLException
                if a database access error occurs.
     */
    void cancelRowUpdates() raises (SQLException);
    //-------------------------------------------------------------------------

    /** moves the cursor to the insert row.  The current cursor position is
        remembered while the cursor is positioned on the insert row.


        <p>
        The insert row is a special row associated with an updatable
        result set. It is essentially a buffer where a new row may
        be constructed by calling the
        <code>updateXXX</code>
        methods prior to
        inserting the row into the result set.
        </p>
        <p>
        Only the
        <code>updateXXX</code>
        ,
        <code>getXXX</code>
        ,
        and
        <member scope="com::sun::star::sdbc">XResultSetUpdate::insertRow()</member>
        methods may be
        called when the cursor is on the insert row.  All of the columns in
        a result set must be given a value each time this method is
        called before calling
        <code>insertRow</code>
        . The method
        <code>updateXXX</code>
        must be called before a
        <code>getXXX</code>
        method can be called on a column value.
        </p>
        @throws SQLException
                if a database access error occurs.
     */
    void moveToInsertRow() raises (SQLException);
    //-------------------------------------------------------------------------

    /** moves the cursor to the remembered cursor position, usually the
        current row. This method has no effect if the cursor is not on the insert
        row.
        @throws SQLException
                if a database access error occurs.
     */
    void moveToCurrentRow() raises (SQLException);
};

//=============================================================================

}; }; }; };

/*===========================================================================
===========================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
