/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_sdbc_CallableStatement_idl__
#define __com_sun_star_sdbc_CallableStatement_idl__

#include <com/sun/star/sdbc/PreparedStatement.idl>

 module com {  module sun {  module star {  module sdbc {

 published interface XRow;
 published interface XOutParameters;


/** is used to execute SQL stored procedures.


    <p>
    SDBC provides a stored procedure SQL escape that allows stored procedures
    to be called in a standard way for all RDBMSs. This escape syntax has one
    form that includes a result parameter and one that does not. If used, the
    result parameter must be registered as an OUT parameter. The other parameters
    can be used for input, output, or both. Parameters are referred to sequentially,
    by number. The first parameter is 1.
    </p>
    <p>
    <code>{?=call&amp;lt;procedure-name&amp;gt;[&amp;lt;arg1&amp;gt;,&amp;lt;arg2&amp;gt;,...]}<br/>{call&amp;lt;procedure-name&amp;gt;[&amp;lt;arg1&amp;gt;,&amp;lt;arg2&amp;gt;,...]}
    </code>
    </p>
    <p>
    IN parameter values are set using the set methods inherited from
    <type scope="com::sun::star::sdbc">PreparedStatement</type>
    . The type of all
    OUT parameters must be registered prior to executing the stored procedure;
    their values are retrieved after execution via the
    <code>get</code>
    methods
    provided by the
    <type scope="com::sun::star::sdbc">XRow</type>
    .
    </p>
    <p>
    A
    <code>CallableStatement</code>
    can return one
    <type scope="com::sun::star::sdbc">XResultSet</type>
    or multiple
    <type scope="com::sun::star::sdbc">ResultSet</type>
    objects. Multiple
    <code>ResultSet</code>
    objects are
    handled using operations inherited from
    <type scope="com::sun::star::sdbc">XPreparedStatement</type>
    .
    </p>
    <p>
    For maximum portability, a call's
    <type scope="com::sun::star::sdbc">ResultSet</type>
    objects and
    update counts should be processed prior to getting the values of output
    parameters.
    </p>
*/
published service CallableStatement
{
    service PreparedStatement;


    /** is used for retrieve the values of OUT parameters.
     */
    interface XRow;


    /** is used to declare parameters as OUT parameters.
     */
    interface XOutParameters;
};

//=============================================================================

}; }; }; };

/*===========================================================================
===========================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
