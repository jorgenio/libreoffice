/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_accessibility_XAccessibleHypertext_idl__
#define __com_sun_star_accessibility_XAccessibleHypertext_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/lang/IndexOutOfBoundsException.idl>
#include <com/sun/star/accessibility/XAccessibleText.idl>

module com { module sun { module star { module accessibility {

 published interface XAccessibleHyperlink;


/** Implement this interface to expose the hypertext structure of a document.

    <p>The <type>XAccessibleHypertext</type> interface is the main interface
    to expose hyperlinks in a document, typically a text document, that are
    used to reference other (parts of) documents.  For supporting the
    <member>XAccessibleHypertext::getLinkIndex</member> method of this
    interface and other character related methods of the
    <type>XAccessibleHyperlink</type> interface, it is necessary to also
    support the <type>XAccessibleText</type> interface.</p>

    @see XAccessibleHyperlink, XAccessibleText

    @since OOo 1.1.2
*/
published interface XAccessibleHypertext
    : ::com::sun::star::accessibility::XAccessibleText
{
    /** Returns the number of links and link groups contained within this
        hypertext document.

        @return
            The number of links and link groups within this hypertext
            document.  Returns 0 if there is no link.
    */
    long getHyperLinkCount ();

    /** Return the specified link.

        <p>The returned <type>XAccessibleHyperlink</type> object
        encapsulates the hyperlink and provides several kinds of information
        describing it.</p>

        @param nLinkIndex
            This index specifies the hyperlink to return.

        @return
            If the given index is valid, i.e. lies in the interval from 0
            to the number of links minus one, a reference to the specified
            hyperlink object is returned.  If the index is invalid then an
            empty reference is returned.
    */
    XAccessibleHyperlink getHyperLink ([in] long nLinkIndex)
        raises (::com::sun::star::lang::IndexOutOfBoundsException);


    /** Returns the index of the hyperlink that is associated with this
        character index.

        <p>In a HTML document this is the case when a &lt;a href&gt; tag
        spans (includes) the given character index.</p>

        @param nCharIndex
            Index of the character for which to return the link index.  If
            the <type>XAccessibleText</type> interface is used to represent
            the text containing the link, then the character index is only
            valid if it is greater than or equal to zero and lower then the
            number of characters in the text.  See that interface's
            documentation for more details.

        @return
            Returns the index of the hyperlink that is associated with this
            character index, or throws an exception if there is no hyperlink
            associated with this index.

        @see XAccessibleText.
    */
    long getHyperLinkIndex ([in] long nCharIndex)
        raises (::com::sun::star::lang::IndexOutOfBoundsException);
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
