/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_accessibility_XAccessibleValue_idl__
#define __com_sun_star_accessibility_XAccessibleValue_idl__

#include <com/sun/star/uno/XInterface.idl>

module com { module sun { module star { module accessibility {

/** Implement this interface to give access to a single numerical value.

    <p>The <type>XAccessibleValue</type> interface represents a single
    numerical value and should be implemented by any class that supports
    numerical value like scroll bars and spin boxes.  This interface lets
    you access the value and its upper and lower bounds.</p>

    @since OOo 1.1.2
*/
published interface XAccessibleValue : ::com::sun::star::uno::XInterface
{
    /** Returns the value of this object as a number.

        <p>The exact return type is implementation dependent.  Typical types
        are long and double.</p>

        @return
            Returns the current value represented by this object.
    */
    any getCurrentValue ();

    /** Sets the value of this object to the given number.

        <p>The argument is clipped to the valid interval whose upper and
        lower bounds are returned by the methods
        <member>getMaximumAccessibleValue</member> and
        <member>getMinimumAccessibleValue</member>, i.e. if it is lower than
        the minimum value the new value will be the minimum and if it is
        greater than the maximum then the new value will be the maximum.</p>

        @param aNumber
            The new value represented by this object.  The set of admissible
            types for this argument is implementation dependent.

        @return
            Returns <TRUE/> if the new value could successfully be set and
            <FALSE/> otherwise.
    */
    boolean setCurrentValue ([in] any aNumber);

    /** Returns the maximal value that can be represented by this object.

        <p>The type of the returned value is implementation dependent.  It
        does not have to be the same type as that returned by
        <member>getCurrentAccessibleValue</member>.</p>

        @return
            Returns the maximal value in an implementation dependent type.
            If this object has no upper bound then an empty object is
            returned.
    */
    any getMaximumValue ();

    /** Returns the minimal value that can be represented by this object.

        <p>The type of the returned value is implementation dependent.  It
        does not have to be the same type as that returned by
        <member>getCurrentAccessibleValue</member>.</p>

        @return
            Returns the minimal value in an implementation dependent type.
            If this object has no upper bound then an empty object is
            returned.
    */
    any getMinimumValue ();
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
