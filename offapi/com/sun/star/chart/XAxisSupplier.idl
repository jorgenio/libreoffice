/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef com_sun_star_chart_XAxisSupplier_idl
#define com_sun_star_chart_XAxisSupplier_idl

#include <com/sun/star/chart/XAxis.idl>

module com {  module sun {  module star {  module chart {

/** Easier access to the different axes within a chart.
@since OOo 3.4
*/

interface XAxisSupplier : ::com::sun::star::uno::XInterface
{
    /** @returns
            the primary axis of the specified dimension. The returned object supports service <type>ChartAxis</type>.

        @param nDimensionIndex
            Parameter nDimensionIndex says whether it is a x, y or z-axis (0 for x).
     */
    com::sun::star::chart::XAxis getAxis( [in] long nDimensionIndex );

    /** @returns
            the secondary axis of the specified dimension. The returned object supports service <type>ChartAxis</type>.

        @param nDimensionIndex
            Parameter nDimensionIndex says whether it is a x, y or z-axis (0 for x).
     */
    com::sun::star::chart::XAxis getSecondaryAxis( [in] long nDimensionIndex );
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
