/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_XAxisYSupplier_idl__
#define __com_sun_star_chart_XAxisYSupplier_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/drawing/XShape.idl>

#include <com/sun/star/beans/XPropertySet.idl>


//=============================================================================

 module com {  module sun {  module star {  module chart {

//=============================================================================

/** gives access to the <i>y</i>-axis of a chart.

    <p>Note that not all diagrams are capable of displaying a
    <i>y</i>-axis, e.g., the <type>PieDiagram</type>.</p>

    @see XDiagram
 */
published interface XAxisYSupplier: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** @returns
            the <i>y</i>-axis title shape.

        @see ChartTitle
     */
    com::sun::star::drawing::XShape getYAxisTitle();

    //-------------------------------------------------------------------------

    /** @returns
            the properties of the <i>y</i>-axis of the diagram.

        <p>The returned property set contains scaling properties as
        well as formatting properties.</p>

        @see ChartAxis
     */
    com::sun::star::beans::XPropertySet getYAxis();

    //-------------------------------------------------------------------------

    /** @returns
            the properties of the help grid (minor grid) of the
            <i>y</i>-axis of the diagram.

        @see ChartGrid
     */
    com::sun::star::beans::XPropertySet getYHelpGrid();

    //-------------------------------------------------------------------------

    /** @returns
            the properties of the main grid (major grid) of the
            <i>y</i>-axis of the diagram.

        @see ChartGrid
     */
    com::sun::star::beans::XPropertySet getYMainGrid();

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
