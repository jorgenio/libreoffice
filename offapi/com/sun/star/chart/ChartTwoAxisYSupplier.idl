/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartTwoAxisYSupplier_idl__
#define __com_sun_star_chart_ChartTwoAxisYSupplier_idl__

#include <com/sun/star/chart/XTwoAxisYSupplier.idl>

#include <com/sun/star/chart/ChartAxisYSupplier.idl>


//=============================================================================

 module com {  module sun {  module star {  module chart {

//=============================================================================

/** a helper service for chart documents which supply
    primary and secondary y-axes.
 */
published service ChartTwoAxisYSupplier
{
    /** offers access to the axis object
     */
    interface com::sun::star::chart::XTwoAxisYSupplier;

    service com::sun::star::chart::ChartAxisYSupplier;

    //-------------------------------------------------------------------------

    /** determines if the secondary y-axis is shown or
        hidden.

        @see ChartAxis
     */
    [property] boolean HasSecondaryYAxis;

    //-------------------------------------------------------------------------

    /** determines for the secondary y-axis
        if the labels at the tick marks are shown or hidden.
    */
    [property] boolean HasSecondaryYAxisDescription;

    //-------------------------------------------------------------------------

    /** determines if the title of the secondary y-axis is shown or hidden.

        @see ChartTitle

        @since OOo 3.0
    */
    [optional, property] boolean HasSecondaryYAxisTitle;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
