/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_ChartAxisArrangeOrderType_idl__
#define __com_sun_star_chart_ChartAxisArrangeOrderType_idl__


//=============================================================================

 module com {  module sun {  module star {  module chart {

//=============================================================================

/** Values specify the arrangement of the axes descriptions.
 */
published enum ChartAxisArrangeOrderType
{
    //-------------------------------------------------------------------------

    /** The descriptions are arranged automatically.

        <p>If there is enough space to put them side by side, this
        arrangement is preferred. If the descriptions would overlap
        when arranged side by side, they are staggered.</p>
     */
    AUTO,

    //-------------------------------------------------------------------------

    /** The descriptions are arranged side by side.
     */
    SIDE_BY_SIDE,

    //-------------------------------------------------------------------------

    /** The descriptions are alternately put on two lines with the even
        values out of the normal line.
     */
    STAGGER_EVEN,

    //-------------------------------------------------------------------------

    /** The descriptions are alternately put on two lines with the odd
        values out of the normal line.
     */
    STAGGER_ODD
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
