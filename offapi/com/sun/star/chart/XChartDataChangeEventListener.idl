/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_chart_XChartDataChangeEventListener_idl__
#define __com_sun_star_chart_XChartDataChangeEventListener_idl__

#include <com/sun/star/lang/XEventListener.idl>

#include <com/sun/star/chart/ChartDataChangeEvent.idl>


//=============================================================================

 module com {  module sun {  module star {  module chart {

//=============================================================================

/** makes it possible to receive events when chart data changes.
 */
published interface XChartDataChangeEventListener: com::sun::star::lang::XEventListener
{
    //-------------------------------------------------------------------------

    /** is called whenever chart data changes in value or structure.

        <p>This interface must be implemented by components that wish
        to get notified of changes in chart data.  They can be
        registered at an <type>XChartData</type> component.</p>

        @param aEvent
            the event that gives further information on what changed.

        @see ChartDataChangeEvent
        @see XChartData
     */
    void chartDataChanged( [in] com::sun::star::chart::ChartDataChangeEvent aEvent );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
