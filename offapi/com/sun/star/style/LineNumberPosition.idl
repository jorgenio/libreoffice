/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_style_LineNumberPosition_idl__
#define __com_sun_star_style_LineNumberPosition_idl__


//=============================================================================

 module com {  module sun {  module star {  module style {

//=============================================================================

// DocMerge from idl: constants com::sun::star::style::LineNumberPosition
/** These constants are used to specify the position of the numbering
        of lines.
 */
published constants LineNumberPosition
{
    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::style::LineNumberPosition::LEFT
    /** the number occurs on the left side of the pages.
     */
    const short LEFT = 0;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::style::LineNumberPosition::RIGHT
    /** the number occurs on the right side of the pages.
     */
    const short RIGHT = 1;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::style::LineNumberPosition::INSIDE
    /** the number occurs alternating on the inner side of the pages.
     */
    const short INSIDE = 2;

    //-------------------------------------------------------------------------

    // DocMerge from idl: value com::sun::star::style::LineNumberPosition::OUTSIDE
    /** the number occurs alternating on the outside side of the pages.
     */
    const short OUTSIDE = 3;

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
