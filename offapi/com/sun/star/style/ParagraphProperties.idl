/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_style_ParagraphProperties_idl__
#define __com_sun_star_style_ParagraphProperties_idl__

#include <com/sun/star/style/ParagraphAdjust.idl>
#include <com/sun/star/util/Color.idl>
#include <com/sun/star/style/LineSpacing.idl>
#include <com/sun/star/style/GraphicLocation.idl>
#include <com/sun/star/style/TabStop.idl>
#include <com/sun/star/style/DropCapFormat.idl>
#include <com/sun/star/container/XIndexReplace.idl>
#include <com/sun/star/table/ShadowFormat.idl>
#include <com/sun/star/table/BorderLine.idl>
#include <com/sun/star/style/BreakType.idl>
#include <com/sun/star/container/XNameContainer.idl>

//=============================================================================

 module com {  module sun {  module star {  module style {

//=============================================================================

/** describes the style of paragraphs.
 */
published service ParagraphProperties
{
    //-------------------------------------------------------------------------

    /** determines the adjustment of a paragraph.
     */
    [property] com::sun::star::style::ParagraphAdjust ParaAdjust;

    //-------------------------------------------------------------------------

    /** contains the type of the line spacing of a paragraph.
     */
    [optional, property] com::sun::star::style::LineSpacing ParaLineSpacing;

    //-------------------------------------------------------------------------

    /** contains the paragraph background color.
     */
    [optional, property] com::sun::star::util::Color ParaBackColor;

    //-------------------------------------------------------------------------

    /** This value is <TRUE/> if the paragraph background color
        is set to transparent.
     */
    [optional, property] boolean ParaBackTransparent;

    //-------------------------------------------------------------------------

    /** contains the value of a link for the background graphic of a paragraph.
     */
    [optional, property] string ParaBackGraphicURL;

    //-------------------------------------------------------------------------

    /** contains the name of the graphic filter for the background graphic of a paragraph.
     */
    [optional, property] string ParaBackGraphicFilter;

    //-------------------------------------------------------------------------

    /** contains the value for the position of a background graphic.

        @see com::sun::star::style::GraphicLocation
     */
    [optional, property] com::sun::star::style::GraphicLocation ParaBackGraphicLocation;

    //-------------------------------------------------------------------------

    /** determines the adjustment of the last line.
        <p>It is only valid if <member>ParagraphProperties::ParaAdjust</member>
        is set to <const>ParagraphAdjust::BLOCK</const>.</p>
     */
    [property] short ParaLastLineAdjust;

    //-------------------------------------------------------------------------

    /** determines if single words are stretched.

        <p>It is only valid if <member>ParagraphProperties::ParaAdjust</member> and
        <member>ParagraphProperties::ParaLastLineAdjust</member> are also valid.</p>
     */
    [optional, property] boolean ParaExpandSingleWord;

    //-------------------------------------------------------------------------

    /** determines the left margin of the paragraph in 100th mm.
     */
    [property] long ParaLeftMargin;

    //-------------------------------------------------------------------------

    /** determines the right margin of the paragraph in 100th mm.
     */
    [property] long ParaRightMargin;

    //-------------------------------------------------------------------------

    /** determines the top margin of the paragraph in 100th mm.

        <p>The distance between two paragraphs is specified by:</p>
        <ul>
        <li>either the bottom margin of the previous paragraph.</li>
        <li>or the top margin of the following paragraph.</li>
        </ul>
        The greater one is chosen.
     */
    [property] long ParaTopMargin;

    //-------------------------------------------------------------------------

    /** determines the bottom margin of the paragraph in 100th mm.

        <p>The distance between two paragraphs is specified by:</p>
        <ul>
        <li>either the bottom margin of the previous paragraph
        </li>
        <li>or the top margin of the following paragraph.
        </li>
        </ul>
        The greater one is chosen.
     */
    [property] long ParaBottomMargin;

    /** determines if contextual spacing is used.

        @since LibreOffice 3.6

        <p>If true, the top and bottom margins of the paragraph should not be
        applied when the previous and next paragraphs have the same style.</p>
     */
    [optional, property] boolean ParaContextMargin;

    //-------------------------------------------------------------------------

    /** determines if the paragraph is included in the
        line numbering.
     */
    [optional, property] boolean ParaLineNumberCount;

    //-------------------------------------------------------------------------

    /** contains the start value for the line numbering.
     */
    [optional, property] long ParaLineNumberStartValue;

    //-------------------------------------------------------------------------

    /** If this property is set, it creates a page break before the
        paragraph it belongs to and assigns the value as the name
        of the new page style sheet to use.
     */
    [optional, property] string PageDescName;

    //-------------------------------------------------------------------------

    /** If a page break property is set at a paragraph, this property contains the new value for the page number.
     */
    [optional, property] short PageNumberOffset;

    //-------------------------------------------------------------------------

    /** determines if the register mode is applied to a
        paragraph.



        <p>Remark: Register mode is only used if the register
        mode property of the page style is switched on.</p>
     */
    [optional, property] boolean ParaRegisterModeActive;

    //-------------------------------------------------------------------------

    /** specifies the positions and kinds of the tab stops within this
                paragraph.
     */
    [optional, property] sequence<com::sun::star::style::TabStop> ParaTabStops;

    //-------------------------------------------------------------------------

    /** contains the name of the current paragraph style.
     */
    [optional, property] string ParaStyleName;

        //-------------------------------------------------------------------------

        /** contains the name of the current page style.
         */
        [optional, property, maybevoid, readonly] string PageStyleName;

        //-------------------------------------------------------------------------

    /** specifies whether the first characters of the
        paragraph are displayed in capital letters and how they are
        formatted.
     */
    [optional, property] com::sun::star::style::DropCapFormat DropCapFormat;

    //-------------------------------------------------------------------------

    /** specifies if the property <var>DropCapFormat</var>
        is applied to the whole first word.
     */
    [optional, property] boolean DropCapWholeWord;

    //-------------------------------------------------------------------------

    /** Setting this property to <TRUE/> prevents page or column
        breaks between this and the following paragraph.


        <p>This feature is useful for preventing title paragraphs to be
        the last line on a page or column.</p>
     */
    [optional, property] boolean ParaKeepTogether;

    //-------------------------------------------------------------------------

    /** Setting this property to <FALSE/> prevents the paragraph from
        getting split into two pages or columns.
     */
    [optional, property] boolean ParaSplit;

    //-------------------------------------------------------------------------

    /** specifies the numbering level of the    paragraph.
     */
    [optional, property] short NumberingLevel;

    //-------------------------------------------------------------------------
    /** contains the numbering rules applied to this paragraph.
     */
        [optional, property] com::sun::star::container::XIndexReplace NumberingRules;

    //-------------------------------------------------------------------------

    /** specifies the start value for numbering if a new numbering starts at this paragraph.
     */
    [optional, property] short NumberingStartValue;

    //-------------------------------------------------------------------------
    /** determines if the numbering rules restart, counting at the current paragraph.
     */
    [optional, property] boolean ParaIsNumberingRestart;
    //-------------------------------------------------------------------------

    /** specifies the name of the style for the numbering.


        <p>The name must be one of the names which are available via
        <type>XStyleFamiliesSupplier</type>.</p>
     */
    [optional, property] string NumberingStyleName;
    //-------------------------------------------------------------------------

    /** specifies the minimum number of lines of the paragraph that have
        to be at bottom of a page if the paragraph is spread over more than
        one page.
     */
    [optional, property] byte ParaOrphans;
    //-------------------------------------------------------------------------

    /** specifies the minimum number of lines of the paragraph that have
        to be at top of a page if the paragraph is spread over more than
        one page.
     */
    [optional, property] byte ParaWidows;
    //-------------------------------------------------------------------------
    /** determines the type, color, and size of the shadow.
     @see com.sun.star.table.ShadowFormat
     */
    [optional, property] com::sun::star::table::ShadowFormat ParaShadowFormat;
    //-------------------------------------------------------------------------
    /** contains the left border of the object.
     */
        [property, optional] com::sun::star::table::BorderLine LeftBorder;

    //-------------------------------------------------------------------------
    /** contains the right border of the object.
     */
        [property, optional] com::sun::star::table::BorderLine RightBorder;

    //-------------------------------------------------------------------------
    /** contains the top border of the object.
     */
        [property, optional] com::sun::star::table::BorderLine TopBorder;

    //-------------------------------------------------------------------------
    /** contains the bottom border of the object.
     */
        [property, optional] com::sun::star::table::BorderLine BottomBorder;

    //-------------------------------------------------------------------------
    /** contains the distance from the border to the object.
     */
        [property, optional] long BorderDistance;

    //-------------------------------------------------------------------------
    /** contains the distance from the left border to the object.
     */
        [property, optional] long LeftBorderDistance;

    //-------------------------------------------------------------------------
    /** contains the distance from the right border to the object.
     */
        [property, optional] long RightBorderDistance;

    //-------------------------------------------------------------------------
    /** contains the distance from the top border to the object.
     */
        [property, optional] long TopBorderDistance;

    //-------------------------------------------------------------------------
    /** contains the distance from the bottom border to the object.
     */
        [property, optional] long BottomBorderDistance;

    //-------------------------------------------------------------------------
        /** determines the type of break that is applied at the beginning of the table.
            @see com.sun.star.style.BreakType
         */
        [optional, property] com::sun::star::style::BreakType BreakType;

        //-------------------------------------------------------------------------
        /** specifies the character style name for drop caps.
         */
        [optional, property] string DropCapCharStyleName;

        //-------------------------------------------------------------------------
        /** specifies the indent for the first line.
         */
        [optional, property] long ParaFirstLineIndent;

        //-------------------------------------------------------------------------
        /** determines if the first line should be indented automatically.
         */
        [optional, property] boolean ParaIsAutoFirstLineIndent;

        //-------------------------------------------------------------------------
        /** specifies if automatic hyphenation is applied.
         */
        [property] boolean ParaIsHyphenation;

        //-------------------------------------------------------------------------
        /** specifies the maximum number of consecutive hyphens.
         */
        [optional, property] short ParaHyphenationMaxHyphens;

        //-------------------------------------------------------------------------
        /** specifies the maximum number of characters to remain before the
            hyphen character (when hyphenation is applied).
         */
        [optional, property] short ParaHyphenationMaxLeadingChars;

        //-------------------------------------------------------------------------
        /** specifies the maximum number of characters to remain after the
            hyphen character (when hyphenation is applied).
         */
        [optional, property] short ParaHyphenationMaxTrailingChars;

        //-------------------------------------------------------------------------
        /** specifies the vertical alignment of a paragraph.

            @see com::sun::star::text::ParagraphVertAlign
         */
        [optional, property] short ParaVertAlignment;

        //-------------------------------------------------------------------------
        /** this property stores xml attributes.
            They will be saved to and restored from automatic styles inside xml files.

            @see com::sun::star::xml::AttributeContainer
         */
        [optional, property] com::sun::star::container::XNameContainer ParaUserDefinedAttributes;

        //-------------------------------------------------------------------------
        /** returns <FALSE/> if the paragraph is part of a numbering, but has no
            numbering label.

            <p>A paragraph is part of a numbering, if a style for a numbering is
            set - see NumberingStyleName.</p>
            <p>If the paragraph is not part of a numbering the property is void.</p>
         */
        [optional, property, maybevoid] boolean NumberingIsNumber;

        //-------------------------------------------------------------------------
        /** the property determines if borders set at a paragraph are merged with the
         next paragraph.
         <p>Borders are only merged if they are identical.</p>
         */
        [optional, property, maybevoid] boolean ParaIsConnectBorder;

        //-------------------------------------------------------------------------
        /** specifies the id of the list to which the paragraph belongs

         */
        [optional, property] string ListId;

        //-------------------------------------------------------------------------
        /** specifies the outline level to which the paragraph belongs

            @since OOo 3.1

            <p>Value 0 indicates that the paragraph belongs to the body text.</p>
            <p>Values [1..10] indicates that the paragraph belongs to the corresponding outline level.</p>
         */
        [optional, property] short OutlineLevel;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
