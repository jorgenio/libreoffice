/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_style_XStyleLoader_idl__
#define __com_sun_star_style_XStyleLoader_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/beans/PropertyValue.idl>
#include <com/sun/star/io/IOException.idl>


//=============================================================================

 module com {  module sun {  module star {  module style {

//=============================================================================

// DocMerge from xml: interface com::sun::star::style::XStyleLoader
/** enables the object to import styles from documents.
 */
published interface XStyleLoader: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::style::XStyleLoader::loadStylesFromURL
    /** loads styles from a document at the given URL.
        <p>
        If <var>OverwriteStyles</var> is <TRUE/>, then all styles
        will be loaded.  Otherwise, only styles which are not already
        defined in this document are loaded.</p>
         @param string URL:
             The directory and the filename from document with the styles
         @param aOptions :
             Specifies which of the Style families the method should load.
         The sequence <PropertyValue/> has the following, optional items:
         <ul>
        <li> boolean LoadCellStyles </li>
        <li> boolean LoadTextStyles </li>
        <li> boolean LoadFrameStyles </li>
        <li> boolean LoadPageStyles </li>
        <li> boolean LoadNumberingStyles </li>
        <li> boolean OverwriteStyles</li>
        </ul>
     <p>As the default, all supported style families are loaded and existing styles are overwritten.</p>
     */
    void loadStylesFromURL( [in] string URL,
             [in] sequence<com::sun::star::beans::PropertyValue> aOptions )
            raises( com::sun::star::io::IOException );

    //-------------------------------------------------------------------------
    /** @returns
    a sequence of the supported properties as declared in <member>XStyleLoader::loadStylesFromURL</member>
    with their current values.
    */
    sequence<com::sun::star::beans::PropertyValue> getStyleLoaderOptions();

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
