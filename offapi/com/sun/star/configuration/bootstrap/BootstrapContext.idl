/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_bootstrap_BootstrapContext_idl__
#define __com_sun_star_configuration_bootstrap_BootstrapContext_idl__

#include <com/sun/star/uno/XComponentContext.idl>

//=============================================================================

module com { module sun { module star { module configuration { module bootstrap {

//=============================================================================

/**
    provides access to a component context that is enhanced with
    bootstrap parameters for configuration services from the
    associated metaconfiguration mechanism.

    <p> The contained settings are used to initialize the
        <type scope="com::sun::star::configuration">DefaultProvider</type> and
        <type scope="com::sun::star::configuration::backend">DefaultBackend</type> of the
        component context.
    </p>
    <p> The implementation is usually available as a singleton in the context
        that it wraps..
    </p>

    @since OOo 1.1.2
*/
published service BootstrapContext
{
    //-------------------------------------------------------------------------

    /**
      provides access to metaconfiguration data.

      <p>Some values are retrieved from external metaconfiguration,
        if they are not overridden in the wrapped context.
      </p>
      */
    interface com::sun::star::uno::XComponentContext ;

    //-------------------------------------------------------------------------
} ;

//=============================================================================

} ; } ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
