/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_SetAccess_idl__
#define __com_sun_star_configuration_SetAccess_idl__

#include <com/sun/star/configuration/HierarchyAccess.idl>
#include <com/sun/star/configuration/SimpleSetAccess.idl>
#include <com/sun/star/container/XContainer.idl>

//=============================================================================

module com { module sun { module star { module configuration {

//=============================================================================
/** provides access to a dynamic, homogeneous set of values or nested
 trees within a hierarchy.

 <p>Also provides information about the template for elements.
 Allows normalizing externally generated names.
 </p>

 <p><em>Sets</em> are dynamic containers within the hierarchy.</p>

 <p>The number and names of contained elements are not fixed in advance,
 but all elements have to be of one predetermined type.
 </p>

 @see com::sun::star::configuration::SetElement
    Child objects of this service generally implement <type>SetElement</type>.
    The template name returned by the child from
    <member>XTemplateInstance::getTemplateName()</member>
    corresponds to the name returned by the set from
    <member>XTemplateContainer::getElementTemplateName()</member>.

 @see com::sun::star::configuration::GroupAccess
    A complementary service that provides for static heterogeneous groups of
    elements within the hierarchy.

*/
published service SetAccess
{
/** is the basic service for accessing child and descendent nodes in a hierarchy.

 <p>External names (from foreign namespaces) should be normalized using
 <member scope="com::sun::star::util">XStringEscape::escapeString()</member>
 (if available) before using them as element names.
 </p>
*/
    service HierarchyAccess;

/** is the basic service providing information about and access to elements
 of a not necessarily hierarchical <em>set</em>.

 <p>Interface <type scope="com::sun::star::configuration">XTemplateContainer</type>,
 which is optional in <type>SimpleSetAccess</type> must always be implemented
 in this service, if the elements are of object type.  </p>
*/
    service SimpleSetAccess;

/** allows attaching listeners to this node to monitor changes to the set.

 <p>In this service, support for notifications is mandatory.
 </p>
*/
    interface com::sun::star::container::XContainer;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
