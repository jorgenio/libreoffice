/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_configuration_backend_Schema_idl__
#define __com_sun_star_configuration_backend_Schema_idl__

#include <com/sun/star/configuration/backend/XSchema.idl>

//==============================================================================

module com { module sun { module star { module configuration { module backend {

//==============================================================================

/**
  provides read only access to a configuration component schema.

  <p>A component is a set of hierarchically organized and semantically
  related configuration settings, e.g StarWriter settings.
  </p>
  <p>A component schema contains two separate sections, one which
  describes the templates to be used in the dynamic containers (sets) of
  the component and one which describes the component's data structure.
  </p>

  @see com::sun::star::configuration::backend::Layer
        Service providing access to individual
        configuration data for an entity.

  @since OOo 1.1.2
  */
published service Schema
{
    //--------------------------------------------------------------------------

    /**
      handles access to the schema data for a component.
      <p>The data can be read either in its entirety (templates and component
      description) or each of its parts can be accessed.
      </p>
      */
    interface XSchema ;

    //--------------------------------------------------------------------------

    /**
      The URL of the layer data.

      @since OOo 2.0
      */
    [property,optional,readonly] string URL ;

    //--------------------------------------------------------------------------
} ;

//==============================================================================

} ; } ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
