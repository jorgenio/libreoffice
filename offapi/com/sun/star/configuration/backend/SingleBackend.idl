/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_SingleBackend_idl__
#define __com_sun_star_configuration_backend_SingleBackend_idl__

#include <com/sun/star/configuration/backend/XBackendEntities.idl>
#include <com/sun/star/configuration/backend/XSchemaSupplier.idl>
#include <com/sun/star/configuration/backend/XMultiLayerStratum.idl>

//=============================================================================

module com { module sun { module star { module configuration { module backend {

//=============================================================================

/**
    is a configuration storage backends containing a complete
    configuration database, including user data, default or
    policy layers and schemata.

    <p> Configuration data is organized into layers which are selected
        by components and entities.
    </p>

    <p> Components are characterized by configuration schemas.
        A component contains configuration data for a particular
        application domain or software module.
    </p>
    <p> Entities are organized hierarchically in organizations,
        groups, roles and individual users. Each element of the associated
        hierarchy corresponds to a layer that applies to an entity.
    </p>
    <p> Layers contains data for multiple components
        associated to a single entity.
    <p>

    @since OOo 1.1.2
*/
published service SingleBackend
{
    //-------------------------------------------------------------------------

    /**
      provides access to configuration schemas.
      */
    interface XSchemaSupplier ;

    //-------------------------------------------------------------------------

    /**
      provides access to the layer data.
      */
    interface XMultiLayerStratum ;

    //-------------------------------------------------------------------------

    /**
      provides information about supported and special entities.
      */
    interface XBackendEntities ;

    //-------------------------------------------------------------------------
} ;

//=============================================================================

} ; } ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
