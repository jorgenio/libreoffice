/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_XSchemaSupplier_idl__
#define __com_sun_star_configuration_backend_XSchemaSupplier_idl__

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/configuration/backend/XSchema.idl>
#include <com/sun/star/configuration/backend/BackendAccessException.idl>
#include <com/sun/star/lang/IllegalArgumentException.idl>

//=============================================================================

module com { module sun { module star { module configuration { module backend {

//=============================================================================

/**
  provides access to configuration component schemas.

  @since OOo 1.1.2
*/
published interface XSchemaSupplier : ::com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /**
      Returns the schema information (component + templates)
      for a particular component.

      @param aComponent
                component whose schema will be accessed

      @returns
                an object allowing access to the various parts of the schema,
                <NULL/> if the component doesn't exist.

      @throws   com::sun::star::lang::IllegalArgumentException
                if the component identifier is invalid.

      @throws   com::sun::star::configuration::backend::BackendAccessException
                if an error occurs while accessing the data.
    */
    XSchema getComponentSchema([in] string aComponent)
        raises (BackendAccessException,
                com::sun::star::lang::IllegalArgumentException) ;

    //-------------------------------------------------------------------------
} ;

//=============================================================================

} ; } ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
