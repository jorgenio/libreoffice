/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_configuration_backend_xml_LayerParser_idl__
#define __com_sun_star_configuration_backend_xml_LayerParser_idl__

#include <com/sun/star/configuration/backend/XLayer.idl>
#include <com/sun/star/io/XActiveDataSink.idl>
#include <com/sun/star/lang/XInitialization.idl>

//=============================================================================

module com { module sun { module star { module configuration { module backend {
module xml {

//=============================================================================

/**
    represents a configuration data layer that is stored in a stream in
    OOR Update XML format.

    <p> The configuration layer XML from a given stream is parsed and fed to a
    <type scope="com::sun::star::configuration::backend">XLayerHandler</type>.
    </p>

    @see com::sun::star::configuration::backend::xml::SchemaParser
        Service that represents a configuration schema stored in XML.

    @see com::sun::star::configuration::backend::xml::LayerWriter
        Service that writes OOR Update XML for data described via
        <type scope="com::sun::star::configuration::backend">XLayerHandler</type>.

    @since OOo 1.1.2
*/
published service LayerParser
{
    //-------------------------------------------------------------------------

    /** provides access to the parsed layer data.
      */
    interface com::sun::star::configuration::backend::XLayer ;

    //-------------------------------------------------------------------------

    /** provides access to the source XML stream.
      */
    interface com::sun::star::io::XActiveDataSink ;

    //-------------------------------------------------------------------------

    /** allows initializing the source stream.

        <p> The source can be passed
            as a <type scope="com::sun::star::io">XInputStream</type> or
            as a <type scope="com::sun::star::xml::sax">InputSource</type>.
        </p>
      */
    interface com::sun::star::lang::XInitialization ;

    //-------------------------------------------------------------------------
} ;

//=============================================================================
} ;
} ; } ; } ; } ; } ;

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
