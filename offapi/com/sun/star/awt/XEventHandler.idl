/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_XEventHandler_idl__
#define __com_sun_star_awt_XEventHandler_idl__

#include <com/sun/star/uno/XInterface.idl>

//=============================================================================

module com { module sun { module star { module awt {

//=============================================================================

/** This interface can be implemented by clients that need access to the
    toolkits window message loop.
*/
published interface XEventHandler: com::sun::star::uno::XInterface
{

    //-------------------------------------------------------------------------

    /** requests the implementor of this interface to handle a platform
        dependent event.

        @returns <TRUE/> if the event was handled properly and no further
        handling should take place, <FALSE/> otherwise.

        @param event
        the platform dependent event.
    */
    boolean handleEvent( [in] any event );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
