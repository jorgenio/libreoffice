/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_FontStrikeout_idl__
#define __com_sun_star_awt_FontStrikeout_idl__


//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** These values are used to specify the kind of strikeout.

    <p>They may be expanded in future versions.</p>
 */
published constants FontStrikeout
{
    //-------------------------------------------------------------------------

    /** specifies not to strike out the characters.
     */
    const short NONE = 0;

    //-------------------------------------------------------------------------

    /** specifies to strike out the characters with a single line.
     */
    const short SINGLE = 1;

    //-------------------------------------------------------------------------

    /** specifies to strike out the characters with a double line.
     */
    const short DOUBLE = 2;

    //-------------------------------------------------------------------------

    /** The strikeout mode is not specified.
     */
    const short DONTKNOW = 3;

    //-------------------------------------------------------------------------

    /** specifies to strike out the characters with a bold line.
     */
    const short BOLD = 4;

    //-------------------------------------------------------------------------

    /** specifies to strike out the characters with slashes.
     */
    const short SLASH = 5;

    //-------------------------------------------------------------------------

    /** specifies to strike out the characters with X's.
     */
    const short X = 6;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
