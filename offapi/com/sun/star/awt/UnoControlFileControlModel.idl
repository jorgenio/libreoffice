/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_UnoControlFileControlModel_idl__
#define __com_sun_star_awt_UnoControlFileControlModel_idl__

#include <com/sun/star/awt/FontDescriptor.idl>
#include <com/sun/star/awt/UnoControlModel.idl>

#include <com/sun/star/util/Color.idl>

#include <com/sun/star/style/VerticalAlignment.idl>

//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** specifies the standard model of an <type>UnoControlFileControl</type>.
 */
published service UnoControlFileControlModel
{
    service com::sun::star::awt::UnoControlModel;

    //-------------------------------------------------------------------------

    /** specifies the background color (RGB) of the control.
     */
    [property] com::sun::star::util::Color BackgroundColor;

    //-------------------------------------------------------------------------

    /** specifies the border style of the control.

        <pre>
        0: No border
        1: 3D border
        2: simple border
        </pre>
     */
    [property] short Border;

    //-------------------------------------------------------------------------

    /** specifies the color of the border, if present

        <p>Not every border style (see <member>Border</member>) may support coloring.
        For instance, usually a border with 3D effect will ignore the BorderColor setting.</p>

        @since OOo 2.0
     */
    [optional, property] long BorderColor;

    //-------------------------------------------------------------------------

    /** determines whether a control is enabled or disabled.
     */
    [property] boolean Enabled;

    //-------------------------------------------------------------------------

    /** specifies the font attributes of the text in the control.
     */
    [property] com::sun::star::awt::FontDescriptor FontDescriptor;

    //-------------------------------------------------------------------------

    /** specifies the <type scope="com::sun::star::text">FontEmphasis</type>
        value of the text in the control.
     */
    [property] short FontEmphasisMark;

    //-------------------------------------------------------------------------

    /** specifies the <type scope="com::sun::star::text">FontRelief</type>
        value of the text in the control.
     */
    [property] short FontRelief;

    //-------------------------------------------------------------------------

    /** specifies the help text of the control.
     */
    [property] string HelpText;

    //-------------------------------------------------------------------------

    /** specifies the help URL of the control.
     */
    [property] string HelpURL;

    //-------------------------------------------------------------------------

    /** specifies whether the selection in the control should be hidden when
        the control is not active (focused).

        @since OOo 2.0
    */
    [optional, property] boolean HideInactiveSelection;

    //-------------------------------------------------------------------------

    /** specifies that the control will be printed with the document.
     */
    [property] boolean Printable;

    //-------------------------------------------------------------------------

    /** specifies that the content of the control cannot be modified by the user.

        @since OOo 1.1.2
     */
    [optional, property] boolean ReadOnly;

    //-------------------------------------------------------------------------

    /** specifies that the control can be reached with the TAB key.
     */
    [property] boolean Tabstop;

    //-------------------------------------------------------------------------

    /** specifies the text displayed in the control.
     */
    [property] string Text;

    //-------------------------------------------------------------------------

    /** specifies the text color (RGB) of the control.
     */
    [property] com::sun::star::util::Color TextColor;

    //-------------------------------------------------------------------------

    /** specifies the text line color (RGB) of the control.
     */
    [property] com::sun::star::util::Color TextLineColor;

    //-------------------------------------------------------------------------

    /** specifies the vertical alignment of the text in the control.

        @since OOo 3.3
     */
    [optional, property] com::sun::star::style::VerticalAlignment VerticalAlign;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
