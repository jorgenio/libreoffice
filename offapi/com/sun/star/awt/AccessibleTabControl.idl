/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_AccessibleTabControl_idl__
#define __com_sun_star_awt_AccessibleTabControl_idl__

#include <com/sun/star/accessibility/XAccessibleContext.idl>
#include <com/sun/star/accessibility/XAccessibleEventBroadcaster.idl>
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#include <com/sun/star/accessibility/XAccessibleExtendedComponent.idl>
#include <com/sun/star/accessibility/XAccessibleSelection.idl>

module com { module sun { module star { module awt {

/** specifies accessibility support for a tab control.

    @since OOo 1.1.2
 */
published service AccessibleTabControl
{
    /** This interface gives access to the structural information of a
        tab control:

        <ul>
        <li>Role: The role of a tab control is <const
            scope="com::sun::star::accessibility"
            >AccessibleRole::PAGE_TAB_LIST</const>.</li>
        <li>Name: There is no name.</li>
        <li>Description: The description of a tab control is its localized
            help text.</li>
        <li>Children: The children of a tab control are tab pages,
            whose accessible context supports the service
            <type>AccessibleTabPage</type>.</li>
        <li>Parent: The parent is the window that contains the
            tab control.</li>
        <li>Relations: There are no relations.</li>
        <li>States: The states supported by this service are
            <ul>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::DEFUNC</const>
                is set if the object has already been disposed
                and subsequent calls to this object result in
                <type scope="com::sun::star::lang">DisposedException</type>
                exceptions.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::ENABLED</const> is set
                if the object is enabled.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::FOCUSABLE</const> is always set.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::FOCUSED</const> is set
                if the object currently has the keyboard focus.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::SHOWING</const> is set
                if the object is displayed on the screen.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::VISIBLE</const> is always set.</li>
            </ul>
            </li>
        </ul>
     */
    interface ::com::sun::star::accessibility::XAccessibleContext;

    interface ::com::sun::star::accessibility::XAccessibleEventBroadcaster;

    interface ::com::sun::star::accessibility::XAccessibleComponent;

    interface ::com::sun::star::accessibility::XAccessibleExtendedComponent;

    /** This interface gives access to the selectable children of a
        tab control.
     */
    interface ::com::sun::star::accessibility::XAccessibleSelection;

};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
