/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_UnoControlComboBoxModel_idl__
#define __com_sun_star_awt_UnoControlComboBoxModel_idl__

#include <com/sun/star/awt/FontDescriptor.idl>
#include <com/sun/star/awt/UnoControlModel.idl>

#include <com/sun/star/util/Color.idl>

#include <com/sun/star/awt/XItemList.idl>

//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** specifies the standard model of an <type>UnoControlComboBox</type>.
 */
published service UnoControlComboBoxModel
{
    service com::sun::star::awt::UnoControlModel;

    //-------------------------------------------------------------------------

    /** specifies the horizontal alignment of the text in the control.

        <pre>
        0: left
        1: center
        2: right
        </pre>
     */
    [optional, property] short Align;

    //-------------------------------------------------------------------------

    /** specifies whether automatic completion of text is enabled.
     */
    [property] boolean Autocomplete;

    //-------------------------------------------------------------------------

    /** specifies the background color (RGB) of the control.
     */
    [property] com::sun::star::util::Color BackgroundColor;

    //-------------------------------------------------------------------------

    /** specifies the border style of the control.

        <pre>
        0: No border
        1: 3D border
        2: simple border
        </pre>
     */
    [property] short Border;

    //-------------------------------------------------------------------------

    /** specifies the color of the border, if present

        <p>Not every border style (see <member>Border</member>) may support coloring.
        For instance, usually a border with 3D effect will ignore the BorderColor setting.</p>

        @since OOo 2.0
     */
    [optional, property] long BorderColor;

    //-------------------------------------------------------------------------

    /** specifies if the control has a drop down button.
     */
    [property] boolean Dropdown;

    //-------------------------------------------------------------------------

    /** determines whether the control is enabled or disabled.
     */
    [property] boolean Enabled;

    //-------------------------------------------------------------------------

    /** specifies the font attributes of the text in the control.
     */
    [property] com::sun::star::awt::FontDescriptor FontDescriptor;

    //-------------------------------------------------------------------------

    /** specifies the <type scope="com::sun::star::text">FontEmphasis</type>
        value of the text in the control.
     */
    [property] short FontEmphasisMark;

    //-------------------------------------------------------------------------

    /** specifies the <type scope="com::sun::star::text">FontRelief</type>
        value of the text in the control.
     */
    [property] short FontRelief;

    //-------------------------------------------------------------------------

    /** specifies the help text of the control.
     */
    [property] string HelpText;

    //-------------------------------------------------------------------------

    /** specifies the help URL of the control.
     */
    [property] string HelpURL;

    //-------------------------------------------------------------------------

    /** specifies whether the selection in the control should be hidden when
        the control is not active (focused).

        @since OOo 2.0
     */
    [optional, property] boolean HideInactiveSelection;

    //-------------------------------------------------------------------------

    /** specifies the maximum line count displayed in the drop down box.
     */
    [property] short LineCount;

    //-------------------------------------------------------------------------

    /** specifies the maximum character count.

        <p>There's no limitation, if set to 0.</p>
     */
    [property] short MaxTextLen;

    //-------------------------------------------------------------------------

    /** specifies that the control will be printed with the document.
     */
    [property] boolean Printable;

    //-------------------------------------------------------------------------

    /** specifies that the content of the control cannot be modified by the user.
     */
    [property] boolean ReadOnly;

    //-------------------------------------------------------------------------

    /** specifies the list of items.
     */
    [property] sequence<string> StringItemList;

    //-------------------------------------------------------------------------

    /** specifies that the control can be reached with the TAB key.
     */
    [property] boolean Tabstop;

    //-------------------------------------------------------------------------

    /** specifies the text displayed in the control.
     */
    [property] string Text;

    //-------------------------------------------------------------------------

    /** specifies the text color (RGB) of the control.
     */
    [property] com::sun::star::util::Color TextColor;

    //-------------------------------------------------------------------------

    /** specifies the text line color (RGB) of the control.
     */
    [property] com::sun::star::util::Color TextLineColor;

    /** denotes the writing mode used in the control, as specified in the
        <type scope="com::sun::star::text">WritingMode2</type> constants group.

        <p>Only <member scope="com::sun::star::text">WritingMode2::LR_TB</member> and
        <member scope="com::sun::star::text">WritingMode2::RL_TB</member> are supported at the moment.</p>

        @since OOo 3.1
    */
    [optional, property] short WritingMode;

    /** defines how the mouse wheel can be used to scroll through the control's content.

        <p>Usually, the mouse wheel scroll through the control's entry list. Using this property,
        and one of the <type>MouseWheelBehavior</type> constants, you can control under which circumstances
        this is possible.</p>
    */
    [optional, property] short MouseWheelBehavior;

    /** allows manipulating the list of items in the combo box more fine-grained than the
        <member>StringItemList</member> property.
    */
    [optional] interface XItemList;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
