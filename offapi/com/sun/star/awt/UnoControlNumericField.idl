/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_UnoControlNumericField_idl__
#define __com_sun_star_awt_UnoControlNumericField_idl__

#include <com/sun/star/awt/UnoControlEdit.idl>
#include <com/sun/star/awt/XSpinField.idl>
#include <com/sun/star/awt/XNumericField.idl>


//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** specifies a numeric field control.

    <p>A numeric field makes it possible to enter, display and edit
    formatted numeric values.</p>
 */
published service UnoControlNumericField
{
    service com::sun::star::awt::UnoControlEdit;

    /** gives access to the value of a spin field and makes it possible to
        register for spin events.

        @since OOo 1.1.2
     */
    [optional] interface com::sun::star::awt::XSpinField;

    interface com::sun::star::awt::XNumericField;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
