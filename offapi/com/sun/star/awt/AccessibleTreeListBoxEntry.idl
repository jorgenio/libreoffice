/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_AccessibleTreeListBoxEntry_idl__
#define __com_sun_star_awt_AccessibleTreeListBoxEntry_idl__

#include <com/sun/star/accessibility/AccessibleContext.idl>


module com { module sun { module star { module accessibility {

 published interface XAccessibleAction;
 published interface XAccessibleSelection;
 published interface XAccessibleText;
 published interface XAccessibleComponent;

}; }; }; };

module com { module sun { module star { module awt {


/** specifies accessibility support for a treelistbox entry.

    @see com::sun::star::accessibility::AccessibleContext

    @see com::sun::star::accessibility::XAccessibleAction
    @see com::sun::star::accessibility::XAccessibleSelection
    @see com::sun::star::accessibility::XAccessibleText

    @since OOo 1.1.2
*/
published service AccessibleTreeListBoxEntry
{
    /** This interface gives access to the structural information of a tree list box entry:

       <ul>
       <li>Role: The role of a tree list box entry is <const
           scope="com::sun::star::accessibility"
           >AccessibleRole::LABEL</const>.</li>
       <li>Name: The name of a tree list box entry is it's text.</li>
       <li>Description: The description of a tree list box entry is empty.</li>
       <li>Children: There can exist children of type <type scope="com::sun::star::awt">AccessibleTreeListBoxEntry</type>.
        </li>
       <li>Parent: The parent is either the <type scope="com::sun::star::awt">AccessibleTreeListBox</type>
            or an <type scope="com::sun::star::awt">AccessibleTreeListBoxEntry</type> that contains the tree list box entry.</li>
       <li>Relations: There are no relations.</li>
       <li>States: The states supported by this service are
           <ul>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::DEFUNC</const>
               is set if the object has already been disposed
               and subsequent calls to this object result in
               <type scope="com::sun::star::lang">DisposedException</type>
               exceptions.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::ENABLED</const> is set
               if the object is enabled.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SHOWING</const> is set
               if the object is displayed on the screen.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::VISIBLE</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::EDITABLE</const> is set when a tree list box entry can be edited.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::EXPANDABLE</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::EXPANDED</const> is set when it is expanded.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::COLLAPSED</const> is set when it is collapsed.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::CHECKED</const> is set when it is checked.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SELECTABLE</const> is always set.</li>
           <li><const scope="com::sun::star::accessibility"
               >AccessibleStateType::SELECTED</const> is set when it is selected.</li>
           </ul>
           </li>
       </ul>
    */
    service   com::sun::star::accessibility::AccessibleContext;

    interface com::sun::star::accessibility::XAccessibleComponent;

    /** This interface gives access to the actions that can be executed for
        a menu. The supported actions for a tree list box entry are:
        <ul>
        <li>toggleExpand</li>
        </ul>
     */
    interface com::sun::star::accessibility::XAccessibleAction;

    /** This interface gives access to the selectable children of a
        tree list box entry.
     */
    interface com::sun::star::accessibility::XAccessibleSelection;

    /** This interface gives read-only access to the text representation
        of a tool box item.
     */
    interface com::sun::star::accessibility::XAccessibleText;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
