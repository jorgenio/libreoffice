/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_awt_AccessibleMenuItem_idl__
#define __com_sun_star_awt_AccessibleMenuItem_idl__

#include <com/sun/star/accessibility/XAccessibleContext.idl>
#include <com/sun/star/accessibility/XAccessibleEventBroadcaster.idl>
#include <com/sun/star/accessibility/XAccessibleComponent.idl>
#include <com/sun/star/accessibility/XAccessibleExtendedComponent.idl>
#include <com/sun/star/accessibility/XAccessibleText.idl>
#include <com/sun/star/accessibility/XAccessibleAction.idl>
#include <com/sun/star/accessibility/XAccessibleValue.idl>

module com { module sun { module star { module awt {

/** specifies accessibility support for a menu item.

    @since OOo 1.1.2
 */
published service AccessibleMenuItem
{
    /** This interface gives access to the structural information of a
        menu item:

        <ul>
        <li>Role: The role of a menu item is <const
            scope="com::sun::star::accessibility"
            >AccessibleRole::MENU_ITEM</const>.</li>
        <li>Name: The name of a menu item is the localized item text.</li>
        <li>Description: The description of a menu item is its localized
            help text.</li>
        <li>Children: There are no children.</li>
        <li>Parent: The parent of a menu item is a menu bar, a pop-up menu or
            a menu, whose accessible context supports the services
            <type>AccessibleMenuBar</type>, <type>AccessiblePopupMenu</type>
            or <type>AccessibleMenu</type>.</li>
        <li>Relations: There are no relations.</li>
        <li>States: The states supported by this service are
            <ul>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::ARMED</const> is set
                if the object is highlighted.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::CHECKED</const> is set
                if the object is currently checked.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::DEFUNC</const>
                is set if the object has already been disposed
                and subsequent calls to this object result in
                <type scope="com::sun::star::lang">DisposedException</type>
                exceptions.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::ENABLED</const> is set
                if the object is enabled.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::OPAQUE</const> is always set.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::SELECTABLE</const> is always set.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::SELECTED</const> is set
                if the object is selected.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::SHOWING</const> is set
                if the object is displayed on the screen.</li>
            <li><const scope="com::sun::star::accessibility"
                >AccessibleStateType::VISIBLE</const> is always set.</li>
            </ul>
            </li>
        </ul>
     */
    interface ::com::sun::star::accessibility::XAccessibleContext;

    interface ::com::sun::star::accessibility::XAccessibleEventBroadcaster;

    interface ::com::sun::star::accessibility::XAccessibleComponent;

    interface ::com::sun::star::accessibility::XAccessibleExtendedComponent;

    /** This interface gives read-only access to the text representation
        of a menu item.
     */
    interface ::com::sun::star::accessibility::XAccessibleText;

    /** This interface gives access to the actions that can be executed for
        a menu item. The supported actions for a menu item are:
        <ul>
        <li>select</li>
        </ul>
     */
    interface ::com::sun::star::accessibility::XAccessibleAction;

    /** This interface gives access to the numerical value of a
        menu item, which is related to the menu item's
        <const scope="com::sun::star::accessibility"
        >AccessibleStateType::SELECTED</const> state.
     */
    interface ::com::sun::star::accessibility::XAccessibleValue;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
