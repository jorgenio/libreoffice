/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XSystemDependentMenuPeer_idl__
#define __com_sun_star_awt_XSystemDependentMenuPeer_idl__

#include <com/sun/star/uno/XInterface.idl>


//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** provides access to the system dependent implementation of the window.

    @see com::sun::star::lang::SystemDependent
    @see WindowAttribute
    @see WindowAttribute::SYSTEMDEPENDENT
 */
interface XSystemDependentMenuPeer: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** returns a system-specific window handle.

        @returns
        a system-specific handle to a menu
        or 0 if the menu is not in the same process.

        <p>You must check the machine ID and the process ID.<BR>
        WIN32: Returns an HMENU if possible, otherwise 0.<BR>
        WIN16: Returns an HMENU if possible, otherwise 0.<BR>

        @param ProcessId
        the process identifier. Use the sal_getGlobalProcessId
        function of the RTL library.

        @param SystemType
        one constant out of the constant group
        <type scope="com::sun::star::lang">SystemDependent</type>.
     */
    any getMenuHandle( [in] sequence< byte > ProcessId, [in] short SystemType );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
