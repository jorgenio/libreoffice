/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_UnoControlModel_idl__
#define __com_sun_star_awt_UnoControlModel_idl__

#include <com/sun/star/awt/UnoControlDialogElement.idl>
#include <com/sun/star/awt/XControlModel.idl>

#include <com/sun/star/lang/XComponent.idl>

#include <com/sun/star/beans/XPropertySet.idl>

#include <com/sun/star/beans/XMultiPropertySet.idl>

#include <com/sun/star/io/XPersistObject.idl>

#include <com/sun/star/util/XCloneable.idl>


//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** specifies the standard model of an <type>UnoControl</type>
    in the <em>Smalltalk model view controller design</em>.

    <p>The interfaces <type scope="com::sun::star::beans">XPropertySet</type>
    and <type scope="com::sun::star::beans">XMultiPropertySet</type> need
    access to the model data from the embedding environment. The control
    and the model can specify additional interfaces to exchange data or
    export more functionality from the model.</p>
 */
published service UnoControlModel
{
    /** This service is present when the control model is embedded in an
        <type>UnoControlDialogModel</type>.
     */
    [optional] service com::sun::star::awt::UnoControlDialogElement;

    interface com::sun::star::awt::XControlModel;

    interface com::sun::star::lang::XComponent;

    interface com::sun::star::beans::XPropertySet;

    interface com::sun::star::beans::XMultiPropertySet;

    interface com::sun::star::io::XPersistObject;

    interface com::sun::star::util::XCloneable;

    //-------------------------------------------------------------------------

    /** specifies the service name of the default control for this model.
     */
    [property] string DefaultControl;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
