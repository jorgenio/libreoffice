/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_XPrinterPropertySet_idl__
#define __com_sun_star_awt_XPrinterPropertySet_idl__

#include <com/sun/star/beans/XPropertySet.idl>

#include <com/sun/star/beans/PropertyVetoException.idl>

#include <com/sun/star/lang/IllegalArgumentException.idl>


//=============================================================================

 module com {  module sun {  module star {  module awt {

//=============================================================================

/** represents an extended property set for printer properties.

    <p>All properties are vetoable properties. If you change the properties
    between <member scope="com::sun::star::awt">XPrinter::startPage</member>
    and <member scope="com::sun::star::awt">XPrinter::endPage</member>, a
    <type scope="com::sun::star::beans">PropertyVetoException</type>
    is thrown.</p>

    @see XPrinter
    @see XInfoPrinter
 */
published interface XPrinterPropertySet: com::sun::star::beans::XPropertySet
{
    //-------------------------------------------------------------------------

    /** sets the orientation.
     */
    void setHorizontal( [in] boolean bHorizontal )
            raises( com::sun::star::beans::PropertyVetoException,
                    com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------

    /** returns descriptions of all available printer forms.
     */
    sequence<string> getFormDescriptions();

    //-------------------------------------------------------------------------

    /** sets the form that should be used.

        <p>Indirectly a printer is selected.</p>
     */
    void selectForm( [in] string aFormDescription )
            raises( com::sun::star::beans::PropertyVetoException,
                    com::sun::star::lang::IllegalArgumentException );

    //-------------------------------------------------------------------------

    /** returns a binary encoded version of the printer setup.
     */
    sequence<byte> getBinarySetup();

    //-------------------------------------------------------------------------

    /** sets the data specific to the printer driver.

        <p>Get this data from the info printer and set the data to the
        printer.</p>
     */
    void setBinarySetup( [in] sequence<byte> data )
            raises( com::sun::star::beans::PropertyVetoException,
                    com::sun::star::lang::IllegalArgumentException );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
