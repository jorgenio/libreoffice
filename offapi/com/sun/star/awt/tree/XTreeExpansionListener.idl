/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_awt_tree_XTreeExpansionListener_idl__
#define __com_sun_star_awt_tree_XTreeExpansionListener_idl__

#include <com/sun/star/lang/XEventListener.idl>

#include <com/sun/star/awt/tree/TreeExpansionEvent.idl>
#include <com/sun/star/awt/tree/ExpandVetoException.idl>

//=============================================================================

module com {  module sun {  module star {  module awt {  module tree {

//=============================================================================

/** An instance of this interface can get notifications from a <type>TreeControl</type>
    when nodes are expanded or collapsed.

    @see XTreeControl::addTreeExpansionListener
    @see XTreeControl::removeTreeExpansionListener
 */
interface XTreeExpansionListener: com::sun::star::lang::XEventListener
{
    /** Invoked when a node with children on demand is about to be expanded.

        <p>This event is invoked before the <method>treeExpanding()</method>
        event.</p>
    */
    void requestChildNodes( [in] TreeExpansionEvent Event );

    /** Invoked whenever a node in the tree is about to be expanded.

        @throws ExpandVetoException
            to notify the calling <type>XTreeControl</type> that expanding
            <member>TreeExpansionEvent::Node</member> should fail.
    */
    void treeExpanding( [in] TreeExpansionEvent Event )
        raises ( ExpandVetoException );

    /** Invoked whenever a node in the tree is about to be collapsed.

        @throws ExpandVetoException
            to notify the calling <type>XTreeControl</type> that collapsing
            <member>TreeExpansionEvent::Node</member> should fail.
    */
    void treeCollapsing( [in] TreeExpansionEvent Event )
        raises ( ExpandVetoException );

    /** Called whenever a node in the tree has been successfully expanded. */
    void treeExpanded( [in] TreeExpansionEvent Event );

    /** Called whenever a node in the tree has been successfully collapsed. */
    void treeCollapsed( [in] TreeExpansionEvent Event );
};

//=============================================================================

}; }; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
