/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_packages_XDataSinkEncrSupport_idl__
#define __com_sun_star_packages_XDataSinkEncrSupport_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/io/XInputStream.idl>

#include <com/sun/star/io/IOException.idl>
#include <com/sun/star/packages/WrongPasswordException.idl>
#include <com/sun/star/packages/NoEncryptionException.idl>

#include <com/sun/star/packages/EncryptionNotAllowedException.idl>
#include <com/sun/star/packages/NoRawFormatException.idl>


//=============================================================================

module com {  module sun {  module star {  module packages {

//=============================================================================

/** Allows to get access to the stream of a <type>PackageStream</type>.
 */
interface XDataSinkEncrSupport: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** Allows to get access to the data of the PackageStream.
    <p>
        In case stream is encrypted one and the key for the stream is not set,
    an exception must be thrown.
    </p>

    @returns
        the stream

    @throws ::com::sun::star::packages::WrongPasswordException
        no key or a wrong one is set

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getDataStream()
        raises( ::com::sun::star::packages::WrongPasswordException,
                ::com::sun::star::io::IOException );


    //-------------------------------------------------------------------------
    /** Allows to get access to the data of the PackageStream as to raw stream.
    In case stream is not encrypted an exception will be thrown.
    <p>
        The difference of raw stream is that it contains header for encrypted data,
    so an encrypted stream can be copied from one PackageStream to
    another one without decryption.
    </p>

    @returns
        the raw representation of stream

    @throws ::com::sun::star::packages::NoEncryptionException
        the PackageStream object is not encrypted

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getRawStream()
        raises( ::com::sun::star::packages::NoEncryptionException,
                ::com::sun::star::io::IOException );

    //-------------------------------------------------------------------------
    /** Allows to set a data stream for the PackageStream.
    <p>
        In case PackageStream is marked as encrypted the data stream will be encrypted on storing.
    </p>

    @param aStream
        new data stream

    @throws ::com::sun::star::io::IOException
        in case of io problems
     */
    void setDataStream( [in] ::com::sun::star::io::XInputStream aStream )
        raises( ::com::sun::star::io::IOException );

    //-------------------------------------------------------------------------
    /** Allows to set raw stream for the PackageStream.
    The PackageStream object can not be marked as encrypted one,
    an exception will be thrown in such case.

    @param aStream
        the new raw representation of stream

    @throws ::com::sun::star::packages::EncryptionNotAllowedException
        the PackageStream object is marked as encrypted

    @throws ::com::sun::star::packages::NoRawFormatException
        the stream is not a correct raw representation of encrypted package stream

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    void setRawStream( [in] ::com::sun::star::io::XInputStream aStream )
        raises( ::com::sun::star::packages::EncryptionNotAllowedException,
                ::com::sun::star::packages::NoRawFormatException,
                ::com::sun::star::io::IOException );

    //-------------------------------------------------------------------------
    /** Allows to get access to the raw data of the stream as it is stored in
        the package.

    @returns
        the plain raw stream as it is stored in the package

    @throws ::com::sun::star::io::IOException
        in case of io problems during retrieving
     */
    ::com::sun::star::io::XInputStream getPlainRawStream()
        raises( ::com::sun::star::io::IOException );

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
