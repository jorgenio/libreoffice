/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_packages_PackageFolder_idl__
#define __com_sun_star_packages_PackageFolder_idl__

#include <com/sun/star/container/XNamed.idl>
#include <com/sun/star/container/XChild.idl>
#include <com/sun/star/container/XNameContainer.idl>
#include <com/sun/star/beans/XPropertySet.idl>
#include <com/sun/star/container/XEnumerationAccess.idl>
//=============================================================================

module com {  module sun {  module star {   module packages {

//=============================================================================
/**
    The PackageFolder service represents a single folder or directory within
    a Package. Instances of this service can only be constructed by an
    implementation of the Package service and not via the service manager.
*/
published service PackageFolder
{
    /**
    This interface is used to get or set the name of the folder.
    */
    interface com::sun::star::container::XNamed;
    /**
    The getParent method of XChild will return the PackageFolder that
    contains this PackageFolder or nothing if this PackageFolder is
    the root PackageFolder.

    setParent will move the PackageFolder.
    */
    interface com::sun::star::container::XChild;
    /**
    This interface describes all of the PackageFolders and PackageStreams
    which are contained within this instance of the PackageFolder service.

    XElementAccess::getElementType returns service PackageStream
    XNameAccess::getByName returns either a PackageFolder or a PackageStream
        as a css::uno::Any
    XNameAccess::getElementNames returns an uno::Sequence of strings containing
        the names of all children stored in the PackageFolder
    */
    interface com::sun::star::container::XNameContainer;
    /**
    This interface will return an implementation of service
    PackageFolderEnumeration, which represents an iterator over the children
    of the PackageFolder, or the PackageStreams and PackageFolders contained
    within this instance of the PackageFolder service.

    This provides a "snapshot" of the contents of the
    PackageFolder at the time of construction. It is the responsibility of the
    caller to ensure that any given member of the enumeration refers to a valid
    PackageStream or PackageFolder.

    */
    interface com::sun::star::container::XEnumerationAccess;

    /**
    This interface provides access to the properties of the package entry.
    Currently, this only supports one entry which is a string called
    MediaType. This contains the MIME type of the stream (e.g. "text/html").
    For PackageFolders, this is always an empty string.
    */
    interface com::sun::star::beans::XPropertySet;
};
}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
