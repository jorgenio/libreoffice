/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SCUI_DEF_HXX
#define SCUI_DEF_HXX

#define SCRET_COLS 0x42
#define SCRET_ROWS 0x43

#define FDS_OPT_NONE        0  // from filldlg.hxx
#define FDS_OPT_HORZ        1   // from filldlg.hxx
#define FDS_OPT_VERT        2  // from filldlg.hxx

#define INS_CONT_NOEMPTY    0x0100 //from inscodlg.hxx
#define INS_CONT_TRANS      0x0200 //from inscodlg.hxx
#define INS_CONT_LINK       0x0400 //from inscodlg.hxx

#define SC_CELL_SHIFT_DISABLE_DOWN  0x01 //from inscodlg.hxx
#define SC_CELL_SHIFT_DISABLE_RIGHT 0x02 //from inscodlg.hxx

#define NAME_TOP        1 //from namecrea.hxx
#define NAME_LEFT       2 //from namecrea.hxx
#define NAME_BOTTOM     4  //from namecrea.hxx
#define NAME_RIGHT      8 //from namecrea.hxx

#define BTN_PASTE_NAME  100  // from namepast.hxx
#define BTN_PASTE_LIST  101  // from namepast.hxx
#define BTN_PASTE_CLOSE 102  // from namepast.hxx

#define BTN_EXTEND_RANGE       150
#define BTN_CURRENT_SELECTION  151
#define SCRET_REMOVE    0x42 //from subtdlg.hxx
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
