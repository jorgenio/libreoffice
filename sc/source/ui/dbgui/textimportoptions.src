/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "textimportoptions.hrc"

ModalDialog RID_SCDLG_TEXT_IMPORT_OPTIONS
{
    HelpID = "sc:ModalDialog:RID_SCDLG_TEXT_IMPORT_OPTIONS";
    Text [ en-US ] = "Import Options" ;
    Size = MAP_APPFONT ( 230 , 101 ) ;
    Moveable = TRUE ;
    Closeable = TRUE ;
    OutputSize = TRUE ;

    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 175, 6 ) ;
        Size = MAP_APPFONT ( 50, 14 ) ;
        DefButton = TRUE ;
    };

    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 175, 23 ) ;
        Size = MAP_APPFONT ( 50, 14 ) ;
    };

    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 175, 43 ) ;
        Size = MAP_APPFONT ( 50, 14 ) ;
    };

    FixedLine FL_CHOOSE_LANG
    {
        Pos = MAP_APPFONT( 6, 3 ) ;
        Size = MAP_APPFONT( 165, 14 ) ;

        Text [ en-US ] = "Select the language to use for import" ;
    };

    RadioButton RB_AUTOMATIC
    {
        HelpID = "sc:RadioButton:RID_SCDLG_TEXT_IMPORT_OPTIONS:RB_AUTOMATIC";
        Pos = MAP_APPFONT( 12, 20 ) ;
        Size = MAP_APPFONT( 159, 10 ) ;
        TabStop = TRUE ;

        Text [ en-US ] = "Automatic" ;
    };

    RadioButton RB_CUSTOM
    {
        HelpID = "sc:RadioButton:RID_SCDLG_TEXT_IMPORT_OPTIONS:RB_CUSTOM";
        Pos = MAP_APPFONT( 12, 34 ) ;
        Size = MAP_APPFONT( 159, 10 ) ;
        TabStop = TRUE ;

        Text [ en-US ] = "Custom" ;
    };

    ListBox LB_CUSTOM_LANG
    {
        HelpID = "sc:ListBox:RID_SCDLG_TEXT_IMPORT_OPTIONS:LB_CUSTOM_LANG";
        Pos = MAP_APPFONT( 20, 50 ) ;
        Size = MAP_APPFONT( 140, 120 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        Sort = TRUE ;
    };

    FixedLine FL_OPTION
    {
       Pos = MAP_APPFONT( 6, 70 );
       Size = MAP_APPFONT( 165, 14 );
       Text [ en-US ] = "Options" ;
    };

    CheckBox BTN_CONVERT_DATE
    {
        HelpID = "sc:CheckBox:RID_SCDLG_TEXT_IMPORT_OPTIONS:BTN_CONVERT_DATE";
        Pos = MAP_APPFONT( 12, 86 );
        Size = MAP_APPFONT( 159, 10 );
        TabStop = TRUE ;
        Text [ en-US ] = "Detect special numbers (such as dates)." ;
    };
};
