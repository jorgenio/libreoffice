/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_DBDOCUTL_HXX
#define SC_DBDOCUTL_HXX

#include "address.hxx"
#include <tools/solar.h>
#include <com/sun/star/uno/Reference.hxx>

class ScDocument;

namespace com { namespace sun { namespace star { namespace sdbc {
    class XRow;
} } } }


class ScDatabaseDocUtil
{
public:
    /**
     * Detailed information on single string value.
     */
    struct StrData
    {
        bool        mbSimpleText;
        sal_uInt32  mnStrLength;

        StrData();
    };
    static void PutData( ScDocument* pDoc, SCCOL nCol, SCROW nRow, SCTAB nTab,
                        const ::com::sun::star::uno::Reference<
                            ::com::sun::star::sdbc::XRow>& xRow, long nRowPos,
                        long nType, sal_Bool bCurrency, StrData* pStrData = NULL );
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
