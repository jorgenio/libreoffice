/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_SHELLIDS_HXX
#define SC_SHELLIDS_HXX

// Sfx Interface-IDs
#define SCID_APP                (SFX_INTERFACE_SC_START+0)
#define SCID_DOC_SHELL          (SFX_INTERFACE_SC_START+1)
#define SCID_TABVIEW_SHELL      (SFX_INTERFACE_SC_START+2)
#define SCID_TABPOP_SHELL       (SFX_INTERFACE_SC_START+3)
#define SCID_EDIT_SHELL         (SFX_INTERFACE_SC_START+4)
#define SCID_DRAW_SHELL         (SFX_INTERFACE_SC_START+5)
#define SCID_DRAW_TEXT_SHELL    (SFX_INTERFACE_SC_START+6)
#define SCID_PREVIEW_SHELL      (SFX_INTERFACE_SC_START+7)
#define SCID_PIVOT_SHELL        (SFX_INTERFACE_SC_START+8)
#define SCID_AUDITING_SHELL     (SFX_INTERFACE_SC_START+9)
#define SCID_FORM_SHELL         (SFX_INTERFACE_SC_START+10)

#define SCID_FORMAT_SHELL       (SFX_INTERFACE_SC_START+11)
#define SCID_CELL_SHELL         (SFX_INTERFACE_SC_START+12)
#define SCID_OLEOBJECT_SHELL    (SFX_INTERFACE_SC_START+13)
#define SCID_CHART_SHELL        (SFX_INTERFACE_SC_START+14)
#define SCID_GRAPHIC_SHELL      (SFX_INTERFACE_SC_START+15)
#define SCID_PAGEBREAK_SHELL    (SFX_INTERFACE_SC_START+16)
#define SCID_MEDIA_SHELL        (SFX_INTERFACE_SC_START+17)


#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
