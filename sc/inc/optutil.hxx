/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef SC_OPTUTIL_HXX
#define SC_OPTUTIL_HXX

#include <unotools/configitem.hxx>
#include <tools/link.hxx>
#include "scdllapi.h"


class ScOptionsUtil
{
public:
    // values must correspond with integer values stored in the configuration
    enum KeyBindingType { KEY_DEFAULT = 0, KEY_OOO_LEGACY = 1 };

    static sal_Bool     IsMetricSystem();
};


//  ConfigItem for classes that use items from several sub trees

class SC_DLLPUBLIC ScLinkConfigItem : public utl::ConfigItem
{
    Link    aCommitLink;

public:
            ScLinkConfigItem( const rtl::OUString& rSubTree );
            ScLinkConfigItem( const rtl::OUString& rSubTree, sal_Int16 nMode );
    void    SetCommitLink( const Link& rLink );

    virtual void    Notify( const com::sun::star::uno::Sequence<rtl::OUString>& aPropertyNames );
    virtual void    Commit();

    void    SetModified()   { ConfigItem::SetModified(); }
    com::sun::star::uno::Sequence< com::sun::star::uno::Any>
            GetProperties(const com::sun::star::uno::Sequence< rtl::OUString >& rNames)
                            { return ConfigItem::GetProperties( rNames ); }
    sal_Bool PutProperties( const com::sun::star::uno::Sequence< rtl::OUString >& rNames,
                            const com::sun::star::uno::Sequence< com::sun::star::uno::Any>& rValues)
                            { return ConfigItem::PutProperties( rNames, rValues ); }

    using ConfigItem::EnableNotification;
    using ConfigItem::GetNodeNames;

};

#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
