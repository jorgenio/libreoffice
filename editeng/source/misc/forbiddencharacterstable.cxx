/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#include <editeng/forbiddencharacterstable.hxx>

#include <unotools/localedatawrapper.hxx>
#include <editeng/unolingu.hxx>

#include <com/sun/star/lang/XMultiServiceFactory.hpp>

SvxForbiddenCharactersTable::SvxForbiddenCharactersTable( ::com::sun::star::uno::Reference< ::com::sun::star::lang::XMultiServiceFactory > xMSF)
{
    mxMSF = xMSF;
}

const com::sun::star::i18n::ForbiddenCharacters* SvxForbiddenCharactersTable::GetForbiddenCharacters( sal_uInt16 nLanguage, sal_Bool bGetDefault )
{
    com::sun::star::i18n::ForbiddenCharacters* pForbiddenCharacters = NULL;
    Map::iterator it = maMap.find( nLanguage );
    if ( it != maMap.end() )
        pForbiddenCharacters = &(it->second);
    else if ( bGetDefault && mxMSF.is() )
    {
        LocaleDataWrapper aWrapper( mxMSF, SvxCreateLocale( nLanguage ) );
        maMap[ nLanguage ] = aWrapper.getForbiddenCharacters();
        pForbiddenCharacters = &maMap[ nLanguage ];
    }
    return pForbiddenCharacters;
}

void SvxForbiddenCharactersTable::SetForbiddenCharacters( sal_uInt16 nLanguage, const com::sun::star::i18n::ForbiddenCharacters& rForbiddenChars )
{
    maMap[ nLanguage ] = rForbiddenChars;
}

void SvxForbiddenCharactersTable::ClearForbiddenCharacters( sal_uInt16 nLanguage )
{
    maMap.erase( nLanguage );
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
