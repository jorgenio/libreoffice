/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_XEngine_idl__
#define __com_sun_star_script_XEngine_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/script/XLibraryAccess.idl>

#include <com/sun/star/script/XEngineListener.idl>


//=============================================================================

module com {  module sun {  module star {  module script {

//=============================================================================
/** makes it possible to control a scripting engine.
    @deprecated
 */
published interface XEngine: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------
    /** sets an interface to an object as a scripting root.

        <p>If the root object implements the XInvocation interface,
        then the engine uses this interface to set/get properties and
        call methods.
        </p>
     */
    void setRoot( [in] com::sun::star::uno::XInterface xRoot );

    //-------------------------------------------------------------------------
    /** gets an interface to the object which is the scripting root.
     */
    com::sun::star::uno::XInterface getRoot();

    //-------------------------------------------------------------------------
    /** sets an access object to get external functions.
     */
    void setLibraryAccess( [in] com::sun::star::script::XLibraryAccess Library );

    //-------------------------------------------------------------------------
    /** compiles a script module in the scope of the root object.
     */
    boolean compile( [in] string ModuleName,
             [in] string Script,
             [in] boolean CreateDebugInfo );

    //-------------------------------------------------------------------------
    /** runs a script specified by a string.

        <p>The arguments given in <var>aArgs</var> can be ignored by
        the engine.  The Script is executed synchronously.</p>
     */
    any run( [in] string aScript,
             [in] com::sun::star::uno::XInterface xThis,
             [in] sequence<any> aArgs );

    //-------------------------------------------------------------------------
    /** runs the script specified by a string and makes callbacks.

        <p>The arguments given in <var>aArgs</var> can be ignored by
        the engine.  The script is executed asynchronously.

        </p>
     */
    void runAsync( [in] string acript,
             [in] com::sun::star::uno::XInterface xThis,
             [in] sequence<any> args,
             [in] com::sun::star::script::XEngineListener xCallback );

    //-------------------------------------------------------------------------
    /** terminates the execution of the running script.

        <p>The waiting queue is cleared too.</p>
     */
    void cancel();

    //-------------------------------------------------------------------------
    /** adds an engine listener.

       <p>It is suggested to allow multiple registration of the same listener,
        thus for each time a listener is added, it has to be removed.
     */
    void addEngineListener( [in] com::sun::star::script::XEngineListener Listener );

    //-------------------------------------------------------------------------
    /** removes an engine listener.

       <p>It is suggested to allow multiple registration of the same listener,
        thus for each time a listener is added, it has to be removed.
     */
    void removeEngineListener( [in] com::sun::star::script::XEngineListener Listener );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
