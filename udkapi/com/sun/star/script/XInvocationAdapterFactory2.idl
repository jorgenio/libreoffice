/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_XInvocationAdapterFactory2_idl__
#define __com_sun_star_script_XInvocationAdapterFactory2_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/script/XInvocation.idl>


//=============================================================================

module com {  module sun {  module star {  module script {

/** Interface to create adapter objects giving types to be supported and a
    an invocation interface incoming calls are delegated to.
*/
published interface XInvocationAdapterFactory2 : com::sun::star::uno::XInterface
{
    /** Creates an adapter interface of given types for calling the given
        <type>XInvocation</type> interface.

        @param Invocation
               invocation interface being called on incoming adapter calls
        @param aType
               supported types of adapter

        @returns
                 adapter interface; this interface can be queried for
                 XInterface and given types
    */
    com::sun::star::uno::XInterface createAdapter(
        [in] XInvocation Invocation,
        [in] sequence< type > aTypes );
};
//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
