/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_script_AllEventObject_idl__
#define __com_sun_star_script_AllEventObject_idl__

#include <com/sun/star/lang/EventObject.idl>

#include <com/sun/star/reflection/XIdlClass.idl>


//=============================================================================

 module com {  module sun {  module star {  module script {

//=============================================================================
/** This event is a wrapper for an original event in a forwarding event.

    <p> Usually the original event is the first argument in the
    array of arguments. </p>
 */
published struct AllEventObject: com::sun::star::lang::EventObject
{
    //-------------------------------------------------------------------------
    /** A helper value for the implementation that can be used arbitrarily.

        <p>This field reflects the third parameter of the method
        <member>XAllListenerAdapterService::createAllListerAdapter</member>.
        </p>
     */
    any Helper;

    //-------------------------------------------------------------------------
    /** contains the type of the original listener.
     */
    type ListenerType;

    //-------------------------------------------------------------------------
    /** The original method name on which the event was fired.
     */
    string MethodName;

    //-------------------------------------------------------------------------
    /** The arguments of the original method.
     */
    sequence<any> Arguments;
};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
