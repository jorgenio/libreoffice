/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_bridge_oleautomation_XAutomationObject_idl__
#define __com_sun_star_bridge_oleautomation_XAutomationObject_idl__

#include <com/sun/star/uno/XInterface.idl>

module com {  module sun {  module star {  module bridge { module oleautomation {


/** a tagging interface for UNO objects which represent Automation
    objects.
    <p>
    If a Automation object is bridged into the UNO environment, then
    the resulting UNO object does not distinguish itself from any
    other ordinary UNO object. However, it may be desirable to have
    that distinction regardless, if a UNO client needs to take
    particular Automation specific characteristics into account.
    By providing <type>XAutomationObject</type> an object declares
    to be representing an Automation object.

    @since OOo 1.1.2
 */
interface XAutomationObject: com::sun::star::uno::XInterface
{

};

}; }; }; }; };


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
