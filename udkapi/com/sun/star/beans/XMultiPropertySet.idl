/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_beans_XMultiPropertySet_idl__
#define __com_sun_star_beans_XMultiPropertySet_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/beans/XPropertySetInfo.idl>

#include <com/sun/star/beans/PropertyVetoException.idl>

#include <com/sun/star/lang/IllegalArgumentException.idl>

#include <com/sun/star/lang/WrappedTargetException.idl>

#include <com/sun/star/beans/XPropertiesChangeListener.idl>


//=============================================================================

module com {  module sun {  module star {  module beans {

//=============================================================================

/** provides access to multiple properties with a single call.
 */
published interface XMultiPropertySet: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** @returns
            the <type>XPropertySetInfo</type> interface, which describes
            all properties of the object to which this interface belongs.
            <const>NULL</const> is returned if the object cannot or will
            not provide information about the properties.

        @see XPropertySet::getPropertySetInfo
     */
    com::sun::star::beans::XPropertySetInfo getPropertySetInfo();

    //-------------------------------------------------------------------------

    /** sets the values to the properties with the specified names.

        <p>The values of the properties must change before the bound
        events are fired. The values of the constrained properties
        should change after the vetoable events are fired and only if no
        exception occurred. Unknown properties are ignored.  </p>

        @param aPropertyNames
            specifies the names of the properties. All names must be unique.
            This sequence must be alphabetically sorted.

        @param aValues
            contains the new values of the properties. The order is the same
            as in <var>aPropertyNames</var>.

        @throws IllegalArgumentException
            if one of the new values cannot be converted to the type
            of the underlying property by an identity or widening conversion.

        @throws com::sun::star::lang:WrappedTargetException
            if the implementation has an internal reason for the exception.
            In this case the original exception is wrapped.
     */
    void setPropertyValues( [in] sequence<string> aPropertyNames,
             [in] sequence<any> aValues )
            raises( com::sun::star::beans::PropertyVetoException,
                    com::sun::star::lang::IllegalArgumentException,
                    com::sun::star::lang::WrappedTargetException );

    //-------------------------------------------------------------------------

    /** @returns
            a sequence of all values of the properties which are specified by their names.

        <p>The order of the values in the returned sequence will be the same
        as the order of the names in the argument. </p>

        @param aPropertyNames
            specifies the names of the properties.
            This sequence must be alphabetically sorted.
     */
    sequence<any> getPropertyValues( [in] sequence<string> aPropertyNames );

    //-------------------------------------------------------------------------

    /** adds an <type>XPropertiesChangeListener</type> to the
        specified property with the specified names.

        <p>The implementation can ignore the names of the properties
        and fire the event on all properties. </p>

        <p>It is suggested to allow multiple registration of the same listener,
        thus for each time a listener is added, it has to be removed.

        @param aPropertyNames
            specifies the names of the properties.

        @param xListener
            contains the listener for the property change events.

        @see removePropertiesChangeListener
     */
    [oneway] void addPropertiesChangeListener( [in] sequence<string> aPropertyNames,
             [in] com::sun::star::beans::XPropertiesChangeListener xListener );

    //-------------------------------------------------------------------------

    /** removes an <type>XPropertiesChangeListener</type> from the listener list.

        <p>It is a "noop" if the listener is not registered.  </p>

        <p>It is suggested to allow multiple registration of the same listener,
        thus for each time a listener is added, it has to be removed.

        @param
            contains the listener to be removed.

        @see addPropertiesChangeListener
     */
    [oneway] void removePropertiesChangeListener( [in] com::sun::star::beans::XPropertiesChangeListener xListener );

    //-------------------------------------------------------------------------

    /** fires a sequence of <type>PropertyChangeEvent</type>s to the specified
        listener.

        @param aPropertynames
            specifies the sorted names of the properties.

        @param xListener
            contains the listener for the property change events.
     */
    [oneway] void firePropertiesChangeEvent( [in] sequence<string> aPropertyNames,
             [in] com::sun::star::beans::XPropertiesChangeListener xListener );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
