/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_beans_SetPropertyTolerantFailed_idl__
#define __com_sun_star_beans_SetPropertyTolerantFailed_idl__


//=============================================================================

module com {  module sun {  module star {  module beans {

//=============================================================================

/** specifies information about a single property failed to be set.

    @see com::sun::star::beans::XTolerantMultiPropertySet
 */
published struct SetPropertyTolerantFailed
{
    /** specifies the name of the property.
     */
    string  Name;

    //-------------------------------------------------------------------------

    /** specifies the success or error code for setting the
        properties value.

        <p>Since the property was not successful set the result will never be
        <member scope="com::sun::star::beans::TolerantPropertySetResultType">SUCCESS</member>.
        </p>

        @see com::sun::star::beans::TolerantPropertySetResultType
     */
    short   Result;

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
