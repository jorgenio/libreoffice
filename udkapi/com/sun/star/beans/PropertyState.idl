/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_beans_PropertyState_idl__
#define __com_sun_star_beans_PropertyState_idl__


//=============================================================================

module com {  module sun {  module star {  module beans {

//=============================================================================

/** This enumeration lists the states that a property value can have.

    <p>The state consists of two aspects: </p>
    <ol>
        <li>whether a value is available or void, </li>
        <li>whether the value is stored in the property set itself or is a
            default, or ambiguous.  </li>
    </ol>

    @see XPropertyState
    @see Property
 */
published enum PropertyState
{
    //-------------------------------------------------------------------------

    /** The value of the property is stored in the PropertySet itself.

        <p>The property value must be available and of the specified type.
        If the <member>PropertyAttribute</member> field in the struct
        <type>Property</type> contains <const>PropertyAttribute::MAYBEVOID</const>,
        then the value may be void. </p>
     */
    DIRECT_VALUE,

    //-------------------------------------------------------------------------

    /** The value of the property is available from a master (e.g., template).

        <p>The <member>PropertyAttribute</member> field in the struct
        <type>Property</type> must contain the
        <const>PropertyAttribute::MAYBEDEFAULT</const> flag. The property
        value must be available and of the specified type. If the
        <member>PropertyAttribute</member> field in the struct <type>Property</type>
        contains <const>PropertyAttribute::MAYBEVOID</const>, then the
        value may be void. </p>
     */
    DEFAULT_VALUE,

    //-------------------------------------------------------------------------

    /** The value of the property is only a recommendation because there
        are multiple values for this property (e.g., from a multi selection).

        <p>The <member>PropertyAttribute</member> field in the struct
        <type>Property</type> must contain the
        <const>PropertyAttribute::MAYBEAMBIGUOUS</const> flag. The
        property value must be available and of the specified type.
        If the Attribute field in the struct Property contains
        <const>PropertyAttribute::MAYBEVOID</const>, then the value
        may be void. </p>
     */
    AMBIGUOUS_VALUE

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
