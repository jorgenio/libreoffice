/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_container_XIndexContainer_idl__
#define __com_sun_star_container_XIndexContainer_idl__

#include <com/sun/star/container/XIndexReplace.idl>

#include <com/sun/star/lang/IllegalArgumentException.idl>

#include <com/sun/star/lang/IndexOutOfBoundsException.idl>

#include <com/sun/star/lang/WrappedTargetException.idl>

//=============================================================================

module com {  module sun {  module star {  module container {

//=============================================================================

/** This is the generic interface for supporting the insertion and removal of
    indexed elements.@see XContainer
 */
published interface XIndexContainer: com::sun::star::container::XIndexReplace
{
    //-------------------------------------------------------------------------

    /** inserts the given element at the specified index.

        <p>To append an element, use the index "last index +1".  </p>
     */
    void insertByIndex( [in] long Index,
             [in] any Element )
            raises( com::sun::star::lang::IllegalArgumentException,
                    com::sun::star::lang::IndexOutOfBoundsException,
                    com::sun::star::lang::WrappedTargetException );

    //-------------------------------------------------------------------------

    /** removes the element at the specified index.
     */
    void removeByIndex( [in] long Index )
            raises( com::sun::star::lang::IndexOutOfBoundsException,
                    com::sun::star::lang::WrappedTargetException );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
