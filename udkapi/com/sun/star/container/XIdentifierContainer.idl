/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_container_XIdentifierContainer_idl__
#define __com_sun_star_container_XIdentifierContainer_idl__

#include <com/sun/star/container/XIdentifierReplace.idl>

#include <com/sun/star/lang/IllegalArgumentException.idl>

#include <com/sun/star/container/ElementExistException.idl>

#include <com/sun/star/lang/WrappedTargetException.idl>

#include <com/sun/star/container/NoSuchElementException.idl>

//=============================================================================

module com {  module sun {  module star {  module container {

//=============================================================================

/** This is the generic interface for supporting the creation and removal of
    elements with unique identifiers.

    @see XContainer
 */
published interface XIdentifierContainer: com::sun::star::container::XIdentifierReplace
{
    //-------------------------------------------------------------------------

    /** inserts an element and creates a new unique identifier for it.

        @returns
            the newly created identifier under which the element is inserted.

        @param aElement
            The new element that will be inserted.

        @throws com::sun::star::lang::IllegalArgumentException
            if the argument is not vailid for this container.

        @throws  com::sun::star::lang::WrappedTargetException
            If the implementation has internal reasons for exceptions,
            then wrap these in a <type scope="com::sun::star::lang">WrappedTargetException</type>
            exception.

     */
    long insert( [in] any aElement )
            raises( com::sun::star::lang::IllegalArgumentException,
                    com::sun::star::lang::WrappedTargetException );

    //-------------------------------------------------------------------------

    /** removes the element with the specified identifier.

        @param Identifier
            The identifier that should be removed.

        @throws com::sun::star::lang::NoSuchElementException
            If the identifier does not exist.

        @throws  com::sun::star::lang::WrappedTargetException
            If the implementation has internal reasons for exceptions,
            then wrap these in a <type scope="com::sun::star::lang">WrappedTargetException</type>
            exception.
     */
    void removeByIdentifier( [in] long Identifier )
            raises( com::sun::star::container::NoSuchElementException,
                    com::sun::star::lang::WrappedTargetException );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
