/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_container_XHierarchicalNameAccess_idl__
#define __com_sun_star_container_XHierarchicalNameAccess_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/container/NoSuchElementException.idl>

//=============================================================================

module com {  module sun {  module star {  module container {

//=============================================================================

/** is used to have hierarchical access to elements within a container.

    <p>You address an object of a specific level in the hierarchy by giving its
    fully qualified name, e.g., "com.sun.star.uno.XInterface". </p>

    <p>To implement inaccurate name access, support the
    <type scope="com::sun::star::beans">XExactName</type> interface.  </p>

    @see com::sun::star::beans::XExactName
 */
published interface XHierarchicalNameAccess: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    /** @returns
                the object with the specified name.

        @param aName
            the name of the object.

        @throws NoSuchElementException
            if an element under Name does not exist.
     */
    any getByHierarchicalName( [in] string aName )
            raises( com::sun::star::container::NoSuchElementException );

    //-------------------------------------------------------------------------

    /** @returns
            <TRUE/> if an element with this name is in
            the container, <FALSE/> otherwise.

        <p>In many cases, the next call is <member>XNameAccess::getByName</member>.
        You should optimize this case.

        @param aName
            the name of the object.
     */
    boolean hasByHierarchicalName( [in] string aName );

};

//=============================================================================

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
