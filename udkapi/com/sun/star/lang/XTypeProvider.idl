/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_lang_XTypeProvider_idl__
#define __com_sun_star_lang_XTypeProvider_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/reflection/XIdlClass.idl>

//=============================================================================

module com {  module sun {  module star {  module lang {

//=============================================================================

/** interface to get information about the types
    (usually interface types) supported by an object.
 */
published interface XTypeProvider: com::sun::star::uno::XInterface
{

    /** returns
        a sequence of all types (usually interface types) provided by the object.
        Important: If the object aggregates other objects the sequence also has
        to contain all types supported by the aggregated objects.
     */
    sequence<type> getTypes();


    /** returns
        a sequence of bytes with length > 0 that is used as an Id to distinguish
        unambiguously between two sets of types, e.g. to realise hashing
        functionality when the object is introspected. Two objects that return
        the same UUID also have to return the same set of types in getTypes().

        If a unique implementation Id cannot be provided this method has to return
        an empty sequence.

        Important: If the object aggregates other objects the UUID has to be unique
        for the whole combination of objects.
     */
    sequence<byte> getImplementationId();
};

//=============================================================================

}; }; }; };

#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
