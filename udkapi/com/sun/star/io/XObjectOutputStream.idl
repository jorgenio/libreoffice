/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_io_XObjectOutputStream_idl__
#define __com_sun_star_io_XObjectOutputStream_idl__

#include <com/sun/star/io/IOException.idl>

#include <com/sun/star/io/XDataOutputStream.idl>

//=============================================================================

module com {  module sun {  module star {  module io {

 published interface XPersistObject;

//=============================================================================

/** stores XPersistObject implementations into the stream

    <p>An implementation of the type <type>XPersistObject</type>
    uses this interface to write its internal state into a stream.
    Have a look there for the explanation of the concept.

    @see com::sun::star::io::XPersistObject
 */
published interface XObjectOutputStream: XDataOutputStream
{
    /** writes an object to the stream.
        @param Object the object, which shall serialize itself into the stream.
     */
    void writeObject( [in] XPersistObject Object )
            raises( IOException );

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
