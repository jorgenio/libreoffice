/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_io_XOutputStream_idl__
#define __com_sun_star_io_XOutputStream_idl__

#include <com/sun/star/uno/XInterface.idl>

#include <com/sun/star/io/NotConnectedException.idl>

#include <com/sun/star/io/BufferSizeExceededException.idl>


//=============================================================================

module com {  module sun {  module star {  module io {

//=============================================================================

// DocMerge from xml: interface com::sun::star::io::XOutputStream
/** This is the basic interface to write data to a stream.
    <p>
    See the <a href="http://udk.openoffice.org/common/man/concept/streams.html">
    streaming document</a> for further information on chaining and piping streams.
 */
published interface XOutputStream: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::io::XOutputStream::writeBytes
    /** writes the whole sequence to the stream. (blocking call)
     */
    void writeBytes( [in] sequence<byte> aData )
            raises( com::sun::star::io::NotConnectedException,
                    com::sun::star::io::BufferSizeExceededException,
                    com::sun::star::io::IOException);

    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::io::XOutputStream::flush
    /** flushes out of the stream any data that may exist in buffers.

        <p>The semantics of this method are rather vague.  See
        <member scope="com::sun::star::io">
        XAsyncOutputMonitor::waitForCompletion</member> for a similar method
        with very specific semantics, that is useful in certain scenarios.</p>
     */
    void flush()
            raises( com::sun::star::io::NotConnectedException,
                    com::sun::star::io::BufferSizeExceededException,
                    com::sun::star::io::IOException);

    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::io::XOutputStream::closeOutput
    /** gets called to indicate that all data has been written.

        <p>If this method has not yet been called, no attached
        <type>XInputStream</type> receives an EOF signal. No further
        bytes may be written after this method has been called.</p>
     */
    void closeOutput()
            raises( com::sun::star::io::NotConnectedException,
                    com::sun::star::io::BufferSizeExceededException,
                    com::sun::star::io::IOException);

};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
