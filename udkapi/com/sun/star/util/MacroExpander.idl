/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_util_MacroExpander_idl__
#define __com_sun_star_util_MacroExpander_idl__

#include <com/sun/star/util/XMacroExpander.idl>
#include <com/sun/star/lang/XComponent.idl>


//=============================================================================

module com {  module sun {  module star {  module util {

//=============================================================================

/** This meta service supports the XMacroExpander interface for expanding
    arbitrary macro expressions, i.e. substitude macro names.
    The purpose of this service is to separate the use of macrofied strings,
    e.g. urls from the use of services.

    @see BootstrapMacroExpander
    @see theMacroExpander

    @since OOo 1.1.2
*/
published service MacroExpander
{
    interface XMacroExpander;
    [optional] interface com::sun::star::lang::XComponent;
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
