/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_java_XJavaThreadRegister_11_idl__
#define __com_sun_star_java_XJavaThreadRegister_11_idl__

#include <com/sun/star/uno/XInterface.idl>


//=============================================================================

module com {  module sun {  module star {  module java {

//=============================================================================

// DocMerge from xml: interface com::sun::star::java::XJavaThreadRegister_11
/** must be implemented by the user of the XJavaThreadRegister_11.

    @incomplete
    @deprecated
 */
published interface XJavaThreadRegister_11: com::sun::star::uno::XInterface
{
    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::java::XJavaThreadRegister_11::isThreadAttached
    /** returns <true/> if the current thread is already attached to the VM otherwise <false/>.
     */
    boolean isThreadAttached();

    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::java::XJavaThreadRegister_11::registerThread
    /** registers the current thread.

        <p>This method should be called every time a JNI function is called from Java. </p>
     */
    void registerThread();


    //-------------------------------------------------------------------------

    // DocMerge from xml: method com::sun::star::java::XJavaThreadRegister_11::revokeThread
    /** revokes the current thread from the list of registerd threads.

        <p>This method should be called at the end of every JNI call from Java. </p>
     */
    void revokeThread();
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
