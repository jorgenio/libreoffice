/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_reflection_XInterfaceAttributeTypeDescription2_idl__
#define __com_sun_star_reflection_XInterfaceAttributeTypeDescription2_idl__

#include "com/sun/star/reflection/XInterfaceAttributeTypeDescription.idl"

module com {  module sun {  module star {  module reflection {

interface XCompoundTypeDescription;

/**
   Reflects an interface attribute, supporting extended attributes that are
   bound or raise exceptions.

   <p>This type supersedes <type>XInterfaceAttributeTypeDescription</type>,
   which does not support extended attributes.</p>

   @since OOo 2.0
 */
interface XInterfaceAttributeTypeDescription2:
    XInterfaceAttributeTypeDescription
{
    /**
       Returns whether this object reflects a bound attribute.

       @return <TRUE/> iff this object reflects a bound attribute
     */
    boolean isBound();

    /**
       Returns the exceptions that can be raised by the attribute's getter.

       @return the reflections of all the exceptions that are listed in the
       <code>raises</code> specification of the attribute's getter (if any), in
       no particular order; all elements of the returned sequence will be
       reflections of exception types
     */
    sequence<XCompoundTypeDescription> getGetExceptions();

    /**
       Returns the exceptions that can be raised by the attribute's setter.

       @return the reflections of all the exceptions that are listed in the
       <code>raises</code> specification of the attribute's setter (if any), in
       no particular order; all elements of the returned sequence will be
       reflections of exception types
     */
    sequence<XCompoundTypeDescription> getSetExceptions();
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
