/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef __com_sun_star_reflection_XServiceConstructorDescription2_idl__
#define __com_sun_star_reflection_XServiceConstructorDescription2_idl__

#include "com/sun/star/uno/XInterface.idl"

module com {  module sun {  module star {  module reflection {

interface XCompoundTypeDescription;
interface XParameter;

/**
   Reflects a service constructor.

   @since OOo 2.0
 */
interface XServiceConstructorDescription {
    /**
       Returns whether the constructor is a default constructor.

       @return <TRUE/> if the constructor is a default constructor
     */
    boolean isDefaultConstructor();

    /**
       Returns the constructor's name.

       @return the constructor's name; for a default constructor, an empty
           <atom>string</atom> is returned
     */
    string getName();

    /**
       Returns the constructor's parameters.

       @return the reflections of all the constructor's parameters, in their
           lexical order; for a default constructor, an empty sequence is
           returned
     */
    sequence<XParameter> getParameters();

    /**
       Returns the exceptions that can be raised by the constructor.

       @return the reflections of all the exceptions that are listed in the
       constructor's <code>raises</code> specification, in no particular order;
       all elements of the returned sequence will be reflections of exception
       types; for a default constructor, an empty sequence is returned (even
       though the mapping of a default constructor in a particular language
       binding may well raise certain UNO exceptions)
     */
    sequence<XCompoundTypeDescription> getExceptions();
};

}; }; }; };

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
