/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef __com_sun_star_reflection_XInterfaceAttributeTypeDescription_idl__
#define __com_sun_star_reflection_XInterfaceAttributeTypeDescription_idl__

#include <com/sun/star/reflection/XInterfaceMemberTypeDescription.idl>

#include <com/sun/star/reflection/XTypeDescription.idl>


//=============================================================================

 module com {  module sun {  module star {  module reflection {

//=============================================================================

/** Reflects an interface attribute type.

    <p>This type is superseded by
    <type>XInterfaceAttributeTypeDescription2</type>, which supports extended
    attributes.</p>

    The type class of this type is TypeClass_INTERFACE_ATTRIBUTE.
*/
published interface XInterfaceAttributeTypeDescription: com::sun::star::reflection::XInterfaceMemberTypeDescription
{
    /** Returns true, if this attribute is read-only.

        @return
                true, if attribute is read-only
    */
    boolean isReadOnly();

    /** Returns the type of the attribute.

        @return
                type of attribute
    */
    com::sun::star::reflection::XTypeDescription getType();
};

//=============================================================================

}; }; }; };

/*=============================================================================

=============================================================================*/
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
