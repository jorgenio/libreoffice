/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_UIKS_HXX
#define _SVX_UIKS_HXX

#include <usr/uiks.hxx>

//  DBENGINE

#define UIK_XDATABASEENGINE                             UIK_DATABASE(00)
#define UIK_XDATABASEFAVORITES                          UIK_DATABASE(01)
#define UIK_XDATABASE                                   UIK_DATABASE(02)
#define UIK_XDATABASECONNECTION                         UIK_DATABASE(03)
#define UIK_XTRANSACTIONSUPPORT                         UIK_DATABASE(04)
#define UIK_XDATABASECURSOR                             UIK_DATABASE(05)
#define UIK_XDATABASETABLE                              UIK_DATABASE(06)
#define UIK_XDATABASETABLES                             UIK_DATABASE(07)
#define UIK_XDATABASEQUERY                              UIK_DATABASE(08)
#define UIK_XDATABASEQUERIES                            UIK_DATABASE(09)
#define UIK_XDATABASERELATION                           UIK_DATABASE(0a)
#define UIK_XDATABASERELATIONS                          UIK_DATABASE(0b)
#define UIK_XDATABASEFIELD                              UIK_DATABASE(0c)
#define UIK_XDATABASEFIELDS                             UIK_DATABASE(0d)
#define UIK_XDATABASEINDEX                              UIK_DATABASE(0e)
#define UIK_XDATABASEINDEXES                            UIK_DATABASE(0f)
#define UIK_XDATABASEDOCUMENT                           UIK_DATABASE(10)
#define UIK_XDATABASEDOCUMENTS                          UIK_DATABASE(11)
#define UIK_XDATABASEWORKSPACE                          UIK_DATABASE(12)
#define UIK_XDATABASEWORKSPACES                         UIK_DATABASE(13)
#define UIK_XDATABASEITERATOR                           UIK_DATABASE(14)
#define UIK_XPREPAREDDATABASECURSOR                     UIK_DATABASE(15)


//  DBENGINE

//  FORMS

#define UIK_XFORM                                       UIK_FORMS(01)
#define UIK_XFORMS                                      UIK_FORMS(02)
#define UIK_XFORMCONTROL                                UIK_FORMS(03)

#define UIK_XHTMLFORM                                   UIK_FORMS(05)
#define UIK_XHTMLFORMLISTENER                           UIK_FORMS(06)
#define UIK_XDATABASEFORM                               UIK_FORMS(07)
#define UIK_XBOUNDCONTROL                               UIK_FORMS(08)
#define UIK_XINSERTRECORDLISTENER                       UIK_FORMS(09)
#define UIK_XUPDATERECORDLISTENER                       UIK_FORMS(0a)
#define UIK_XDESTROYRECORDLISTENER                      UIK_FORMS(0b)
#define UIK_XCURRENTRECORDLISTENER                      UIK_FORMS(0c)
#define UIK_XBOUNDCONTROLLISTENER                       UIK_FORMS(0d)
#define UIK_XLOADLISTENER                               UIK_FORMS(0e)
#define UIK_XERRORLISTENER                              UIK_FORMS(0f)

#define UIK_XFORMCONTROLFACTORY                         UIK_FORMS(10)
#define UIK_XFORMCONTROLLER                             UIK_FORMS(11)
#define UIK_XFORMCONTROLLERLISTENER                     UIK_FORMS(12)

// FORMS



#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
