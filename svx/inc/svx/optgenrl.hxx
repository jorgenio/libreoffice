/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_OPTGENRL_HXX
#define _SVX_OPTGENRL_HXX

// Defines for setting the focus of a Edit via a slot from external.
#define UNKNOWN_EDIT    ((sal_uInt16)0)
#define COMPANY_EDIT    ((sal_uInt16)1)
#define FIRSTNAME_EDIT  ((sal_uInt16)2)
#define LASTNAME_EDIT   ((sal_uInt16)3)
#define STREET_EDIT     ((sal_uInt16)4)
#define COUNTRY_EDIT    ((sal_uInt16)5)
#define PLZ_EDIT        ((sal_uInt16)6)
#define CITY_EDIT       ((sal_uInt16)7)
#define STATE_EDIT      ((sal_uInt16)8)
#define TITLE_EDIT      ((sal_uInt16)9)
#define POSITION_EDIT   ((sal_uInt16)10)
#define SHORTNAME_EDIT  ((sal_uInt16)11)
#define TELPRIV_EDIT    ((sal_uInt16)12)
#define TELCOMPANY_EDIT ((sal_uInt16)13)
#define FAX_EDIT        ((sal_uInt16)14)
#define EMAIL_EDIT      ((sal_uInt16)15)

#endif // #ifndef _SVX_OPTGENRL_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
