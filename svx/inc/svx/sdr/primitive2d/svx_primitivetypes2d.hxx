/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef INCLUDED_SVX_PRIMITIVE2D_PRIMITIVETYPES2D_HXX
#define INCLUDED_SVX_PRIMITIVE2D_PRIMITIVETYPES2D_HXX

#include <drawinglayer/primitive2d/drawinglayer_primitivetypes2d.hxx>

//////////////////////////////////////////////////////////////////////////////

#define PRIMITIVE2D_ID_SDRCAPTIONPRIMITIVE2D            (PRIMITIVE2D_ID_RANGE_SVX| 0)
#define PRIMITIVE2D_ID_SDRCONNECTORPRIMITIVE2D          (PRIMITIVE2D_ID_RANGE_SVX| 1)
#define PRIMITIVE2D_ID_SDRCUSTOMSHAPEPRIMITIVE2D        (PRIMITIVE2D_ID_RANGE_SVX| 2)
#define PRIMITIVE2D_ID_SDRELLIPSEPRIMITIVE2D            (PRIMITIVE2D_ID_RANGE_SVX| 3)
#define PRIMITIVE2D_ID_SDRELLIPSESEGMENTPRIMITIVE2D     (PRIMITIVE2D_ID_RANGE_SVX| 4)
#define PRIMITIVE2D_ID_SDRGRAFPRIMITIVE2D               (PRIMITIVE2D_ID_RANGE_SVX| 5)
#define PRIMITIVE2D_ID_SDRMEASUREPRIMITIVE2D            (PRIMITIVE2D_ID_RANGE_SVX| 6)
#define PRIMITIVE2D_ID_SDROLE2PRIMITIVE2D               (PRIMITIVE2D_ID_RANGE_SVX| 7)
#define PRIMITIVE2D_ID_SDRPATHPRIMITIVE2D               (PRIMITIVE2D_ID_RANGE_SVX| 8)
#define PRIMITIVE2D_ID_SDRRECTANGLEPRIMITIVE2D          (PRIMITIVE2D_ID_RANGE_SVX| 9)
#define PRIMITIVE2D_ID_SDRCONTOURTEXTPRIMITIVE2D        (PRIMITIVE2D_ID_RANGE_SVX| 10)
#define PRIMITIVE2D_ID_SDRPATHTEXTPRIMITIVE2D           (PRIMITIVE2D_ID_RANGE_SVX| 11)
#define PRIMITIVE2D_ID_SDRBLOCKTEXTPRIMITIVE2D          (PRIMITIVE2D_ID_RANGE_SVX| 12)
#define PRIMITIVE2D_ID_SDRSTRETCHTEXTPRIMITIVE2D        (PRIMITIVE2D_ID_RANGE_SVX| 13)
#define PRIMITIVE2D_ID_SDRCELLPRIMITIVE2D               (PRIMITIVE2D_ID_RANGE_SVX| 14)
#define PRIMITIVE2D_ID_SDRBORDERLINEPRIMITIVE2D         (PRIMITIVE2D_ID_RANGE_SVX| 15)
#define PRIMITIVE2D_ID_OVERLAYBITMAPEXPRIMITIVE         (PRIMITIVE2D_ID_RANGE_SVX| 16)
#define PRIMITIVE2D_ID_OVERLAYCROSSHAIRPRIMITIVE        (PRIMITIVE2D_ID_RANGE_SVX| 17)
#define PRIMITIVE2D_ID_OVERLAYHATCHRECTANGLEPRIMITIVE   (PRIMITIVE2D_ID_RANGE_SVX| 18)
#define PRIMITIVE2D_ID_OVERLAYHELPLINESTRIPEDPRIMITIVE  (PRIMITIVE2D_ID_RANGE_SVX| 19)
#define PRIMITIVE2D_ID_OVERLAYROLLINGRECTANGLEPRIMITIVE (PRIMITIVE2D_ID_RANGE_SVX| 20)
#define PRIMITIVE2D_ID_SDRCONTROLPRIMITIVE2D            (PRIMITIVE2D_ID_RANGE_SVX| 21)
#define PRIMITIVE2D_ID_SDROLECONTENTPRIMITIVE2D         (PRIMITIVE2D_ID_RANGE_SVX| 22)
#define PRIMITIVE2D_ID_SDRAUTOFITTEXTPRIMITIVE2D        (PRIMITIVE2D_ID_RANGE_SVX| 23)

//////////////////////////////////////////////////////////////////////////////

#endif // INCLUDED_SVX_PRIMITIVE2D_PRIMITIVETYPES2D_HXX

//////////////////////////////////////////////////////////////////////////////
// eof

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
