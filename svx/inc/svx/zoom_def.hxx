/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef _SVX_ZOOM_DEF_HXX
#define _SVX_ZOOM_DEF_HXX

#define ZOOMBTN_OPTIMAL     ((sal_uInt16)0x0001)
#define ZOOMBTN_PAGEWIDTH   ((sal_uInt16)0x0002)
#define ZOOMBTN_WHOLEPAGE   ((sal_uInt16)0x0004)

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
