/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include <thread.hxx>
#include <sysdep.hxx>
#if defined(WNT)
#include <svwin.h>
#endif
#include <tlintl.hxx>
#include <tlfsys.hxx>
#include <tlbigint.hxx>

#include <sv.hxx>
#include <svtool.hxx>
#define _ANIMATION
#include <svgraph.hxx>
#include <svsystem.hxx>
#include <svcontnr.hxx>
#include <sfx.hxx>
#include <sfxitems.hxx>
#include <sfxipool.hxx>
#include <sfxiiter.hxx>
#include <sfxdoc.hxx>
#include <sfxview.hxx>
#include <sfxdlg.hxx>
#include <sfxstyle.hxx>
#include <editeng/svxenum.hxx>
#include <sbx.hxx>
#include <hmwrap.hxx>
#include <mail.hxx>
#include <urlobj.hxx>
#include <inet.hxx>
#include <inetui.hxx>
#include <svtruler.hxx>

#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <sfx.hrc>

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
