/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_STBCTRLS_H
#define _SVX_STBCTRLS_H

// defines ------------------------------------------------------------------

#define ZOOM_200            1
#define ZOOM_150            2
#define ZOOM_100            3
#define ZOOM_75             4
#define ZOOM_50             5

#define ZOOM_OPTIMAL        6
#define ZOOM_PAGE_WIDTH     7
#define ZOOM_WHOLE_PAGE     8

//  IDs wie SUBTOTAL_FUNC im Calc

#define PSZ_FUNC_AVG        1
#define PSZ_FUNC_COUNT2     3
#define PSZ_FUNC_COUNT      2
#define PSZ_FUNC_MAX        4
#define PSZ_FUNC_MIN        5
#define PSZ_FUNC_SUM        9
#define PSZ_FUNC_NONE       16

#define XMLSEC_CALL         1

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
