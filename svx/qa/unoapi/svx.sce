#i98339 -o svx.AccessibleControlShape
#i111278 -o svx.AccessibleEditableTextPara
#i111252 -o svx.AccessibleGraphicShape
#i46736 -o svx.AccessibleImageBullet
#i111252 -o svx.AccessibleOLEShape
#i111252 -o svx.AccessiblePageShape
#i111216 -o svx.AccessiblePresentationGraphicShape
#i111216 -o svx.AccessiblePresentationOLEShape
#i85539 -o svx.AccessiblePresentationShape
-o svx.AccessibleShape
#i90294 -o svx.GraphicExporter
-o svx.SvxDrawPage
#i85501 -o svx.SvxGraphCtrlAccessibleContext
#i87746 -o svx.SvxGraphicObject
#i85263 -o svx.SvxShape
#i87746 -o svx.SvxShapeCircle
-o svx.SvxShapeCollection
#i85263 -o svx.SvxShapeConnector
#i87746 -o svx.SvxShapeControl
#i87746 -o svx.SvxShapeDimensioning
#i87746 -o svx.SvxShapeGroup
#i87746 -o svx.SvxShapePolyPolygon
#i87746 -o svx.SvxShapePolyPolygonBezier
-o svx.SvxUnoNumberingRules
-o svx.SvxUnoText
-o svx.SvxUnoTextContent
-o svx.SvxUnoTextContentEnum
-o svx.SvxUnoTextCursor
-o svx.SvxUnoTextField
-o svx.SvxUnoTextRange
-o svx.SvxUnoTextRangeEnumeration
