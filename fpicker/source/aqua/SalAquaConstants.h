/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SALAQUACONSTANTS_H_
#define _SALAQUACONSTANTS_H_

#define kAppFourCharCode 'LibO'
#define kControlPropertyTracking 'Trck'
#define kControlPropertyLastPartCode 'LsPc'
#define kControlPropertySubType 'SuTy'
#define kPopupControlPropertyTitleWidth 'PoTW'

#define kAquaSpaceBetweenControls (8)
#define kAquaSpaceBetweenPopupMenus (10)

#define kAquaSpaceInsideGroupH (16)
#define kAquaSpaceInsideGroupV (11)

#define kAquaSpaceBoxFrameViewDiffTop (7)
#define kAquaSpaceBoxFrameViewDiffLeft (7)
#define kAquaSpaceBoxFrameViewDiffBottom (9)
#define kAquaSpaceBoxFrameViewDiffRight (7)

#define kAquaSpaceButtonFrameBoundsDiff (6)
#define kAquaSpaceSwitchButtonFrameBoundsDiff (2)

#define kAquaSpacePopupMenuFrameBoundsDiffTop (2)
#define kAquaSpacePopupMenuFrameBoundsDiffBottom (4)
#define kAquaSpacePopupMenuFrameBoundsDiffV (kAquaSpacePopupMenuFrameBoundsDiffTop + kAquaSpacePopupMenuFrameBoundsDiffBottom)
#define kAquaSpacePopupMenuFrameBoundsDiffLeft (3)

#define kAquaSpaceLabelFrameBoundsDiffH (3)
#define kAquaSpaceLabelPopupDiffV (6)
#define kAquaSpaceAfterPopupButtonsV (20)

#define kAquaPopupButtonDefaultHeight (26)

#endif //_SALAQUACONSTANTS_H_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
