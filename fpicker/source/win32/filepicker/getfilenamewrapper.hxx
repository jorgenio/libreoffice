/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _GETFILENAMEWRAPPER_HXX_
#define _GETFILENAMEWRAPPER_HXX_

//------------------------------------------------------------------------
// includes
//------------------------------------------------------------------------

#include <sal/types.h>

#define WIN32_LEAN_AND_MEAN
#if defined _MSC_VER
#pragma warning(push, 1)
#endif
#include <windows.h>
#include <commdlg.h>
#if defined _MSC_VER
#pragma warning(pop)
#endif

/*
    A simple wrapper around the GetOpenFileName/GetSaveFileName API.
    Because currently the Win32 API functions GetOpenFileName/GetSaveFileName
    work only properly in an Single Threaded Appartment.
*/

class CGetFileNameWrapper
{
public:
    CGetFileNameWrapper();

    bool getOpenFileName(LPOPENFILENAME lpofn);
    bool getSaveFileName(LPOPENFILENAME lpofn);
    int  commDlgExtendedError();

private:
    int m_ExtendedDialogError;
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
