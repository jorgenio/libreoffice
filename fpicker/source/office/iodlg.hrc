/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SVTOOLS_IODLGIMPL_HRC
#define _SVTOOLS_IODLGIMPL_HRC

#include "svtools/svtools.hrc"
#include "svtools/helpid.hrc"

// ModalDialog DLG_SVT_EXPLORERFILE

#define ED_EXPLORERFILE_CURRENTPATH         10
#define BTN_EXPLORERFILE_NEWFOLDER          11
#define BTN_EXPLORERFILE_UP                 12
#define BTN_EXPLORERFILE_OPEN               14
#define BTN_EXPLORERFILE_CANCEL             15
#define BTN_EXPLORERFILE_HELP               16

#define IMG_FILEDLG_BTN_UP                  10
#define IMG_FILEDLG_BTN_STD                 11
#define IMG_FILEDLG_CREATEFOLDER            14
#define IMG_FILEDLG_PLACE_LOCAL             15
#define IMG_FILEDLG_PLACE_REMOTE            16

#define CTL_EXPLORERFILE_FILELIST           20

#define FT_EXPLORERFILE_FILENAME            30
#define ED_EXPLORERFILE_FILENAME            31
#define FT_EXPLORERFILE_SHARED_LISTBOX      32
#define LB_EXPLORERFILE_SHARED_LISTBOX      33
#define FT_EXPLORERFILE_FILETYPE            34
#define LB_EXPLORERFILE_FILETYPE            35

#define CB_EXPLORERFILE_READONLY            40
#define CB_EXPLORERFILE_PASSWORD            41
#define CB_AUTO_EXTENSION                   42
#define CB_OPTIONS                          43

#define LB_EXPLORERFILE_PLACES_LISTBOX      50
#define BTN_EXPLORERFILE_CONNECT_TO_SERVER  51
#define BTN_EXPLORERFILE_ADD_PLACE          52
#define BTN_EXPLORERFILE_REMOVE_PLACE       53

// -----------------------------------------------

#define STR_EXPLORERFILE_OPEN               1
#define STR_EXPLORERFILE_SAVE               2
#define STR_EXPLORERFILE_BUTTONSAVE         3
#define STR_PATHNAME                        4
#define STR_PATHSELECT                      5
#define STR_BUTTONSELECT                    6
#define STR_ACTUALVERSION                   7
#define STR_PREVIEW             			8
#define STR_DEFAULT_DIRECTORY     			9
#define STR_PLACES_TITLE         			10

// DLG_SVT_ADDPLACE ------------------------------

#define FT_ADDPLACE_SERVERURL			    10
#define FT_ADDPLACE_SERVERNAME		  		11
#define FT_ADDPLACE_SERVERTYPE		  		12
#define FT_ADDPLACE_SERVERLOGIN			  	13
#define FT_ADDPLACE_SERVERPASSWORD	 		14
#define ED_ADDPLACE_SERVERURL		        15
#define ED_ADDPLACE_SERVERNAME		  		16
#define ED_ADDPLACE_SERVERTYPE		  		17
#define ED_ADDPLACE_SERVERLOGIN			  	18
#define ED_ADDPLACE_SERVERPASSWORD			19
#define BT_ADDPLACE_OK				      		20
#define BT_ADDPLACE_CANCEL		    			21
#define BT_ADDPLACE_DELETE			    		22

// DLG_SVT_QUERYFOLDERNAME -----------------------

#define FT_SVT_QUERYFOLDERNAME_DLG_NAME     10
#define ED_SVT_QUERYFOLDERNAME_DLG_NAME     11
#define FL_SVT_QUERYFOLDERNAME_DLG_NAME     12
#define BT_SVT_QUERYFOLDERNAME_DLG_OK       13
#define BT_SVT_QUERYFOLDERNAME_DLG_CANCEL   14
#define BT_SVT_QUERYFOLDERNAME_DLG_HELP     15

// -----------------------------------------------

#define SID_SFX_START                       5000
#define SID_OPENURL                     (SID_SFX_START + 596)

#endif

