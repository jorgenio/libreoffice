#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
#
# Copyright 2009 by Sun Microsystems, Inc.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.	If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

# this namespace list is needed both in the library and the custom makefile
WRITERFILTER_OOXMLNAMESPACES= \
	wml \
	dml-stylesheet \
	dml-styleDefaults \
	dml-shape3DLighting \
	dml-shape3DScene \
	dml-shape3DStyles \
	dml-shape3DCamera \
	dml-baseStylesheet \
	dml-textCharacter \
	dml-shapeEffects \
	dml-shapeLineProperties \
	dml-shapeProperties \
	dml-baseTypes \
	dml-documentProperties \
	dml-graphicalObject \
	dml-shapeGeometry \
	dml-wordprocessingDrawing \
	sml-customXmlMappings \
	shared-math \
	shared-relationshipReference \
	dml-chartDrawing \
	vml-main \
	vml-officeDrawing \
	vml-wordprocessingDrawing \


