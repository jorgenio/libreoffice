<?xml version="1.0" encoding="UTF-8"?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<oor:component-data oor:name="Drivers" oor:package="org.openoffice.Office.DataAccess" xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <node oor:name="Installed">
    <node oor:name="sdbc:odbc:*" oor:op="replace">
      <prop oor:name="Driver">
        <value>com.sun.star.comp.sdbc.ODBCDriver</value>
      </prop>
      <prop oor:name="DriverTypeDisplayName" oor:type="xs:string">
        <value xml:lang="en-US">ODBC</value>
      </prop>
      <node oor:name="Properties">
        <node oor:name="CharSet" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="SystemDriverSettings" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="UseCatalog" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>false</value>
          </prop>
        </node>
        <node oor:name="AutoIncrementCreation" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="AutoRetrievingStatement" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="IsAutoRetrievingEnabled" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>false</value>
          </prop>
        </node>
        <node oor:name="AddIndexAppendix" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="Features">
        <node oor:name="GeneratedValues" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseSQL92NamingConstraints" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="AppendTableAliasInSelect" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseKeywordAsBeforeAlias" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseBracketedOuterJoinSyntax" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="IgnoreDriverPrivileges" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="ParameterNameSubstitution" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="DisplayVersionColumns" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseCatalogInSelect" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseSchemaInSelect" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseIndexDirectionKeyword" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseDOSLineEnds" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="BooleanComparisonMode" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="FormsCheckRequiredFields" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="EscapeDateTime" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="PrimaryKeySupport" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="RespectDriverResultSetType" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="MetaData">
        <node oor:name="SupportsTableCreation" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="SupportsBrowsing" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="Authentication" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value>UserPassword</value>
          </prop>
        </node>
      </node>
    </node>
  </node>
</oor:component-data>
