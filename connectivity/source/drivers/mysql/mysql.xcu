<?xml version="1.0" encoding="UTF-8"?>
<!--***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************ -->
<oor:component-data oor:name="Drivers" oor:package="org.openoffice.Office.DataAccess" xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <node oor:name="Installed">
    <node oor:name="sdbc:mysql:jdbc:*" oor:op="replace">
      <prop oor:name="Driver">
        <value>org.openoffice.comp.drivers.MySQL.Driver</value>
      </prop>
      <prop oor:name="DriverTypeDisplayName" oor:type="xs:string">
        <value xml:lang="en-US">MySQL (JDBC)</value>
      </prop>
      <node oor:name="Properties">
        <node oor:name="CharSet" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="JavaDriverClass" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value>com.mysql.jdbc.Driver</value>
          </prop>
        </node>
        <node oor:name="AddIndexAppendix" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="Features">
        <node oor:name="UseKeywordAsBeforeAlias" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="IgnoreDriverPrivileges" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="DisplayVersionColumns" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseDOSLineEnds" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="BooleanComparisonMode" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="FormsCheckRequiredFields" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="MetaData">
        <node oor:name="SupportsTableCreation" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseJava" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="Authentication" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value>UserPassword</value>
          </prop>
        </node>
        <node oor:name="SupportsColumnDescription" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
    </node>
    <node oor:name="sdbc:mysql:odbc:*" oor:op="replace">
      <prop oor:name="Driver">
        <value>org.openoffice.comp.drivers.MySQL.Driver</value>
      </prop>
      <prop oor:name="DriverTypeDisplayName" oor:type="xs:string">
        <value xml:lang="en-US">MySQL (ODBC)</value>
      </prop>
      <node oor:name="Properties">
        <node oor:name="CharSet" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="AddIndexAppendix" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="Features">
        <node oor:name="UseKeywordAsBeforeAlias" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="IgnoreDriverPrivileges" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="DisplayVersionColumns" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseDOSLineEnds" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="BooleanComparisonMode" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="FormsCheckRequiredFields" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="MetaData">
        <node oor:name="SupportsTableCreation" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="SupportsBrowsing" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="Authentication" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value>UserPassword</value>
          </prop>
        </node>
      </node>
    </node>
    <node oor:name="sdbc:mysql:mysqlc:*" oor:op="replace">
      <prop oor:name="Driver">
        <value>org.openoffice.comp.drivers.MySQL.Driver</value>
      </prop>
      <prop oor:name="DriverTypeDisplayName" oor:type="xs:string">
        <value xml:lang="en-US">MySQL (Native)</value>
      </prop>
      <node oor:name="Properties">
        <node oor:name="CharSet" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="LocalSocket" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="NamedPipe" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value></value>
          </prop>
        </node>
        <node oor:name="AddIndexAppendix" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="Features">
        <node oor:name="UseKeywordAsBeforeAlias" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="IgnoreDriverPrivileges" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="DisplayVersionColumns" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="UseDOSLineEnds" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="BooleanComparisonMode" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="FormsCheckRequiredFields" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
      <node oor:name="MetaData">
        <node oor:name="SupportsTableCreation" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
        <node oor:name="Authentication" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:string">
            <value>UserPassword</value>
          </prop>
        </node>
        <node oor:name="SupportsColumnDescription" oor:op="replace">
          <prop oor:name="Value" oor:type="xs:boolean">
            <value>true</value>
          </prop>
        </node>
      </node>
    </node>
  </node>
</oor:component-data>
