/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CONNECTIVITY_ODBC_ODEFS_HXX_
#define _CONNECTIVITY_ODBC_ODEFS_HXX_

#define N3SQLAllocHandle SQLAllocHandle
#define N3SQLConnect SQLConnect
#define N3SQLDriverConnect SQLDriverConnect
#define N3SQLBrowseConnect SQLBrowseConnect
#define N3SQLDataSources SQLDataSources
#define N3SQLDrivers SQLDrivers
#define N3SQLGetInfo SQLGetInfo
#define N3SQLGetFunctions SQLGetFunctions
#define N3SQLGetTypeInfo SQLGetTypeInfo
#define N3SQLSetConnectAttr SQLSetConnectAttr
#define N3SQLGetConnectAttr SQLGetConnectAttr
#define N3SQLSetEnvAttr SQLSetEnvAttr
#define N3SQLGetEnvAttr SQLGetEnvAttr
#define N3SQLSetStmtAttr SQLSetStmtAttr
#define N3SQLGetStmtAttr SQLgetStmtAttr
#define N3SQLSetDescField SQLSetDescField
#define N3SQLGetDescField SQLGetDescField
#define N3SQLGetDescRec SQLGetDescRec
#define N3SQLSetDescRec SQLSetDescRec
#define N3SQLPrepare SQLPrepare
#define N3SQLBindParameter SQLBindParameter
#define N3SQLGetCursorName SQLGetCursorName
#define N3SQLSetCursorName SQLSetCursorName
#define N3SQLExecute SQLExecute
#define N3SQLExecDirect SQLExecDirect
#define N3SQLNativeSql SQLNativeSql
#define N3SQLDescribeParam SQLDescribeParam
#define N3SQLNumParams SQLNumParams
#define N3SQLParamData SQLParamData
#define N3SQLPutData SQLPutData
#define N3SQLRowCount SQLRowCount
#define N3SQLNumResultCols SQLNumResultCols
#define N3SQLDescribeCol SQLDescribeCol
#define N3SQLColAttribute SQLColAttribute
#define N3SQLBindCol SQLBindCol
#define N3SQLFetch SQLFetch
#define N3SQLFetchScroll SQLFetchScroll
#define N3SQLGetData SQLGetData
#define N3SQLSetPos SQLSetPos
#define N3SQLBulkOperations SQLBulkOperations
#define N3SQLMoreResults SQLMoreResults
#define N3SQLGetDiagField SQLGetDiagField
#define N3SQLGetDiagRec SQLGetDiagRec
#define N3SQLColumnPrivileges SQLColumnPrivileges
#define N3SQLColumns SQLColumns
#define N3SQLForeignKeys SQLForeignKeys
#define N3SQLPrimaryKeys SQLPrimaryKeys
#define N3SQLProcedureColumns SQLProcedureColumns
#define N3SQLProcedures SQLProcedures
#define N3SQLSpecialColumns SQLSpecialColumns
#define N3SQLStatistics SQLStatistics
#define N3SQLTablePrivileges SQLTablePrivileges
#define N3SQLTables SQLTables
#define N3SQLFreeStmt SQLFreeStmt
#define N3SQLCloseCursor SQLCloseCursor
#define N3SQLCancel SQLCancel
#define N3SQLEndTran SQLEndTran
#define N3SQLDisconnect SQLDisconnect
#define N3SQLFreeHandle SQLFreeHandle
#define N3SQLGetCursorName SQLGetCursorName

#endif // _CONNECTIVITY_ODBC_ODEFS_HXX_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
