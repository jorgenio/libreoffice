#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2000, 2010 Oracle and/or its affiliates.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

PRJ=..$/..
PRJINC=..
PRJNAME=connectivity
TARGET=dbpool

ENABLE_EXCEPTIONS=TRUE

# --- Settings ----------------------------------

.INCLUDE : settings.mk
.INCLUDE :  $(PRJ)$/version.mk


# --- Files -------------------------------------

SLOFILES=\
        $(SLO)$/ZConnectionWrapper.obj			\
        $(SLO)$/ZDriverWrapper.obj				\
        $(SLO)$/ZPooledConnection.obj			\
        $(SLO)$/ZConnectionPool.obj				\
        $(SLO)$/ZPoolCollection.obj				\
        $(SLO)$/Zregistration.obj

# --- Library -----------------------------------

SHL1TARGET=	$(DBPOOL_TARGET)$(DBPOOL_MAJOR)

SHL1VERSIONMAP=$(SOLARENV)/src/component.map

SHL1OBJS=$(SLOFILES)
SHL1STDLIBS=\
    $(COMPHELPERLIB)            \
    $(CPPULIB)					\
    $(CPPUHELPERLIB)			\
    $(DBTOOLSLIB)				\
    $(SALLIB)                                   \
    $(SALHELPERLIB)

SHL1DEPN=
SHL1IMPLIB=	i$(SHL1TARGET)

SHL1DEF=	$(MISC)$/$(SHL1TARGET).def

DEF1NAME=	$(SHL1TARGET)
DEF1EXPORTFILE=	exports.dxp


# --- Targets ----------------------------------

.INCLUDE : target.mk

ALLTAR : $(MISC)/dbpool2.component

$(MISC)/dbpool2.component .ERRREMOVE : $(SOLARENV)/bin/createcomponent.xslt \
        dbpool2.component
    $(XSLTPROC) --nonet --stringparam uri \
        '$(COMPONENTPREFIX_BASIS_NATIVE)$(SHL1TARGETN:f)' -o $@ \
        $(SOLARENV)/bin/createcomponent.xslt dbpool2.component
