#*************************************************************************
#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
# 
# Copyright 2000, 2010 Oracle and/or its affiliates.
#
# OpenOffice.org - a multi-platform office productivity suite
#
# This file is part of OpenOffice.org.
#
# OpenOffice.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# only, as published by the Free Software Foundation.
#
# OpenOffice.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License version 3 for more details
# (a copy is included in the LICENSE file that accompanied this code).
#
# You should have received a copy of the GNU Lesser General Public License
# version 3 along with OpenOffice.org.  If not, see
# <http://www.openoffice.org/license.html>
# for a copy of the LGPLv3 License.
#
#*************************************************************************

# ----------------------------ADO settings------------------------------------#
# target
ADO_TARGET=ado

# ----------------------------ODBC BASE settings-----------------------------------#
# target
ODBC2_TARGET=odbcbase

# ----------------------------ODBC settings-----------------------------------#
# target
ODBC_TARGET=odbc

# ----------------------------JDBC settings-----------------------------------#
# target
JDBC_TARGET=jdbc

# ----------------------------dbtools settings-----------------------------------#
# target
DBTOOLS_TARGET=dbtools

# -----------------------DRIVER MANAGER settings--------------------------------#
# target
SDBC_TARGET=sdbc

# the major 
SDBC_MAJOR=2
# the minor 
SDBC_MINOR=0
# the micro 
SDBC_MICRO=0


# -----------------------Connection Pool settings--------------------------------#
# target
DBPOOL_TARGET=dbpool

# the major 
DBPOOL_MAJOR=2
# the minor 
DBPOOL_MINOR=0
# the micro 
DBPOOL_MICRO=0


# ----------------------------DBASE settings-----------------------------------#
# target
DBASE_TARGET=dbase

# ----------------------------MOZAB settings-----------------------------------#
# the major 
MOZAB_MAJOR=2
# the minor 
MOZAB_MINOR=0
# the micro 
MOZAB_MICRO=0

# ----------------------------MySQL settings-----------------------------------#
# target
MYSQL_TARGET=mysql


# ----------------------------Evoab settings-----------------------------------#
# target
EVOAB_TARGET=evoab

# ----------------------------Kab settings-------------------------------------#
# target
KAB_TARGET=kab

# the major 
KAB_MAJOR=1
# the minor 
KAB_MINOR=0
# the micro 
KAB_MICRO=0


# ----------------------------Macab settings-------------------------------------#
# target
MACAB_TARGET=macab

# the major 
MACAB_MAJOR=1
# the minor 
MACAB_MINOR=0
# the micro 
MACAB_MICRO=0

# ----------------------------HSQLDB settings-----------------------------------#
# target
HSQLDB_TARGET=hsqldb

