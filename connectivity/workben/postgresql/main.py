#*************************************************************************
#
#   $RCSfile: main.py,v $
#
#   $Revision: 1.1.2.2 $
#
#   last change: $Author: jbu $ $Date: 2004/05/09 20:04:59 $
#
#   The Contents of this file are made available subject to the terms of
#   either of the following licenses
#
#          - GNU Lesser General Public License Version 2.1
#          - Sun Industry Standards Source License Version 1.1
#
#   Sun Microsystems Inc., October, 2000
#
#   GNU Lesser General Public License Version 2.1
#   =============================================
#   Copyright 2000 by Sun Microsystems, Inc.
#   901 San Antonio Road, Palo Alto, CA 94303, USA
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License version 2.1, as published by the Free Software Foundation.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston,
#   MA  02111-1307  USA
#
#
#   Sun Industry Standards Source License Version 1.1
#   =================================================
#   The contents of this file are subject to the Sun Industry Standards
#   Source License Version 1.1 (the "License"); You may not use this file
#   except in compliance with the License. You may obtain a copy of the
#   License at http://www.openoffice.org/license.html.
#
#   Software provided under this License is provided on an "AS IS" basis,
#   WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#   WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#   MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#   See the License for the specific provisions governing your rights and
#   obligations concerning the Software.
#
#   The Initial Developer of the Original Code is: Joerg Budischewski
#
#   Copyright: 2000 by Sun Microsystems, Inc.
#
#   All Rights Reserved.
#
#   Contributor(s): Joerg Budischewski
#
#
#
#*************************************************************************
import uno
import unohelper
import unittest
import statement
import preparedstatement
import metadata
import sdbcx
import sys
import os

ctx = uno.getComponentContext()

# needed for the tests
unohelper.addComponentsToContext(
    ctx,ctx,
    ("postgresql-sdbc.uno","postgresql-sdbc-impl.uno","typeconverter.uno"),
    "com.sun.star.loader.SharedLibrary")

runner = unittest.TextTestRunner(sys.stderr,1,2)
dburl = sys.argv[1] # os.environ['USER'] + "_pqtest"
print "dburl=" + dburl

suite = unittest.TestSuite()
suite.addTest(statement.suite(ctx,dburl))
suite.addTest(preparedstatement.suite(ctx,dburl))
suite.addTest(metadata.suite(ctx,dburl))
suite.addTest(sdbcx.suite(ctx,dburl))

runner.run(suite)
