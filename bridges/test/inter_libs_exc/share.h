/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <com/sun/star/lang/IllegalArgumentException.hpp>
#include <com/sun/star/uno/DeploymentException.hpp>

#define OUSTR(x) ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM(x) )

typedef void (SAL_CALL * t_throws_exc)();

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
