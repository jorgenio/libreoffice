/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef INCLUDED_FWKDLLAPI_H
#define INCLUDED_FWKDLLAPI_H

#include "sal/types.h"

#include <fwidllapi.h>

#endif /* INCLUDED_FWKDLLAPI_H */

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
