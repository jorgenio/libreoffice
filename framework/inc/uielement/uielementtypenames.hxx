/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef __FRAMEWORK_UIELEMENT_UIELEMENTTYPENAMES_HXX_
#define __FRAMEWORK_UIELEMENT_UIELEMENTTYPENAMES_HXX_

#define UIELEMENTTYPE_MENUBAR_NAME          "menubar"
#define UIELEMENTTYPE_POPUPMENU_NAME        "popupmenu"
#define UIELEMENTTYPE_TOOLBAR_NAME          "toolbar"
#define UIELEMENTTYPE_STATUSBAR_NAME        "statusbar"
#define UIELEMENTTYPE_FLOATINGWINDOW_NAME   "floater"
#define UIELEMENTTYPE_PROGRESSBAR_NAME      "progressbar"
#define UIELEMENTTYPE_TOOLPANEL_NAME        "toolpanel"

#endif // __FRAMEWORK_UIELEMENT_UIELEMENTTYPENAMES_HXX_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
