from wizards.common.HelpIds import HelpIds

TXTTITLE_TEXT_CHANGED = "txtTitleTextChanged"
TXTDATE_TEXT_CHANGED = "txtDateTextChanged"
TXTTIME_TEXT_CHANGED = "txtTimeTextChanged"
TXTLOCATION_TEXT_CHANGED = "txtLocationTextChanged"
CHKMINUTES_ITEM_CHANGED = "chkMinutesItemChanged"
CHKUSEMEETINGTYPE_ITEM_CHANGED = "chkUseMeetingTypeItemChanged"
CHKUSEREAD_ITEM_CHANGED = "chkUseReadItemChanged"
CHKUSEBRING_ITEM_CHANGED = "chkUseBringItemChanged"
CHKUSENOTES_ITEM_CHANGED = "chkUseNotesItemChanged"
CHKUSECALLEDBYNAME_ITEM_CHANGED = "chkUseCalledByItemChanged"
CHKUSEFACILITATOR_ITEM_CHANGED = "chkUseFacilitatorItemChanged"
CHKUSENOTETAKER_ITEM_CHANGED = "chkUseNoteTakerItemChanged"
CHKUSETIMEKEEPER_ITEM_CHANGED = "chkUseTimeKeeperItemChanged"
CHKUSEATTENDEES_ITEM_CHANGED = "chkUseAttendeesItemChanged"
CHKUSEOBSERVERS_ITEM_CHANGED = "chkUseObserversItemChanged"
CHKUSERESOURCEPERSONS_ITEM_CHANGED = "chkUseResourcePersonsItemChanged"
LISTPAGEDESIGN_ACTION_PERFORMED = "pageDesignChanged"
TXTTEMPLATENAME_TEXT_CHANGED = "templateTitleChanged"
BTNTEMPLATEPATH_ACTION_PERFORMED = "saveAs"
BTNINSERT_ACTION_PERFORMED = "insertRow"
BTNREMOVE_ACTION_PERFORMED = "removeRow"
BTNUP_ACTION_PERFORMED = "rowUp"
BTNDOWN_ACTION_PERFORMED = "rowDown"

INFO_IMAGE_URL = "private:resource/dbu/image/19205"

HID = 41051

LISTPAGEDESIGN_HID =      HelpIds.getHelpIdString(HID + 6)
CHKMINUTES_HID =          HelpIds.getHelpIdString(HID + 7)
TXTTIME_HID =             HelpIds.getHelpIdString(HID + 8)
TXTDATE_HID =             HelpIds.getHelpIdString(HID + 9)
TXTTITLE_HID =            HelpIds.getHelpIdString(HID + 10)
CBLOCATION_HID =          HelpIds.getHelpIdString(HID + 11)

CHKMEETINGTITLE_HID =     HelpIds.getHelpIdString(HID + 12)
CHKREAD_HID =             HelpIds.getHelpIdString(HID + 13)
CHKBRING_HID =            HelpIds.getHelpIdString(HID + 14)
CHKNOTES_HID =            HelpIds.getHelpIdString(HID + 15)

CHKCONVENEDBY_HID =       HelpIds.getHelpIdString(HID + 16)
CHKPRESIDING_HID =        HelpIds.getHelpIdString(HID + 17)
CHKNOTETAKER_HID =        HelpIds.getHelpIdString(HID + 18)
CHKTIMEKEEPER_HID =       HelpIds.getHelpIdString(HID + 19)
CHKATTENDEES_HID =        HelpIds.getHelpIdString(HID + 20)
CHKOBSERVERS_HID =        HelpIds.getHelpIdString(HID + 21)
CHKRESOURCEPERSONS_HID =  HelpIds.getHelpIdString(HID + 22)

TXTTEMPLATENAME_HID =     HelpIds.getHelpIdString(HID + 23)
TXTTEMPLATEPATH_HID =     HelpIds.getHelpIdString(HID + 24)
BTNTEMPLATEPATH_HID =     HelpIds.getHelpIdString(HID + 25)

OPTCREATEAGENDA_HID =     HelpIds.getHelpIdString(HID + 26)
OPTMAKECHANGES_HID =      HelpIds.getHelpIdString(HID + 27)

BTNINSERT_HID =           HelpIds.getHelpIdString(HID + 28)
BTNREMOVE_HID =           HelpIds.getHelpIdString(HID + 29)
BTNUP_HID =               HelpIds.getHelpIdString(HID + 30)
BTNDOWN_HID =             HelpIds.getHelpIdString(HID + 31)

LAST_HID = HID + 32
