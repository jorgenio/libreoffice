/*
 * ***********************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 * **********************************************************************
 */

package com.sun.star.wizards.reportbuilder.layout;

/**
 *
 * @author ll93751
 */
public class LayoutConstants
{
    public static final int LabelHeight = 500;
    public static final int FormattedFieldHeight = 500;
    public static final int EmptyLineHeight = 500;

    public static final int BinaryHeight = 4000;
    public static final int MemoFieldHeight = 4000;

    public static final int LineHeight = 250;

    /**
     * Groups will indent
     * This is the factor (5mm) which will multiply with the current indent
     */
    public static final int IndentFactorWidth = 500;
}
