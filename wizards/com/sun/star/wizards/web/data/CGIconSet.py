from common.ConfigGroup import ConfigGroup

class CGIconSet(ConfigGroup):
    cp_Index = -1
    cp_FNPrefix = str()
    cp_FNPostfix = str()
    cp_Name = str()
