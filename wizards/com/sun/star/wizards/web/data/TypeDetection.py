'''
a document which will open in writer.
'''
WRITER_DOC = "writer"
'''
a document which will open in calc.
'''
CALC_DOC = "calc"
'''
a document which will open in impress.
'''
IMPRESS_DOC = "impress"
'''
a document which will open in draw.
'''
DRAW_DOC = "draw"
'''
an HTML document
'''
HTML_DOC = "html"
'''
a GIF or an JPG file.
'''
WEB_GRAPHICS = "graphics"
'''
a PDF document.
'''
PDF_DOC = "pdf"
'''
a Sound file (mp3/wav ect.)
'''
SOUND_FILE = "sound"
'''
a File which can not be handled by neither SO or a Web browser
(exe, jar, zip ect.)
'''
NO_TYPE = "other"
