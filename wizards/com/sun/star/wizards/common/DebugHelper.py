class DebugHelper(object):

    @classmethod
    def exception(self, ex):
        raise NotImplementedError

    @classmethod
    def writeInfo(self, msg):
        raise NotImplementedError

