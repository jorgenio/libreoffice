/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package com.sun.star.wizards.common;

/**
 *
 * @author Ocke Janssen
 */
public class PropertyNames
{

    public static String PROPERTY_ENABLED = "Enabled";
    public static String PROPERTY_HEIGHT = "Height";
    public static String PROPERTY_HELPURL = "HelpURL";
    public static String PROPERTY_POSITION_X = "PositionX";
    public static String PROPERTY_POSITION_Y = "PositionY";
    public static String PROPERTY_LABEL = "Label";
    public static String PROPERTY_MULTILINE = "MultiLine";
    public static String PROPERTY_NAME = "Name";
    public static String PROPERTY_STEP = "Step";
    public static String PROPERTY_WIDTH = "Width";
    public static String PROPERTY_TABINDEX = "TabIndex";
    public static String PROPERTY_STATE = "State";
    public static String PROPERTY_IMAGEURL = "ImageURL";
    public static String PROPERTY_TITLE = "Title";
    public static String PROPERTY_BORDER = "Border";
    public static String PROPERTY_MOVEABLE = "Moveable";
    public static String PROPERTY_CLOSEABLE = "Closeable";
    public static String PROPERTY_ALIGN = "Align";
    public static String COMMAND = "Command";
    public static String COMMAND_TYPE = "CommandType";
    public static String SELECTED_ITEMS = "SelectedItems";
    public static String URL = "URL";
    public static String ACTIVE_CONNECTION = "ActiveConnection";
    public static String ASC = "ASC";
    public static String SEMI_COLON = ";";
    public static String EMPTY_STRING = "";
    public static String START = "start";
    public static String ORIENTATION = "Orientation";
    public static String READ_ONLY = "ReadOnly";
    public static String SPACE = " ";
    public static String STRING_ITEM_LIST = "StringItemList";
    public static String FONT_DESCRIPTOR = "FontDescriptor";
}
