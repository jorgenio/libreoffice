from wizards.common.ConfigGroup import *

class CGPaperElementLocation(ConfigGroup):

    def __init__(self):
        self.cp_Display = bool()
        self.cp_Width = float()
        self.cp_Height = float()
        self.cp_X = float()
        self.cp_Y = float()
