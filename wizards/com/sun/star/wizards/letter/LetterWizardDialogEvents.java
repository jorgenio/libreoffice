/*
 ************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
package com.sun.star.wizards.letter;

public interface LetterWizardDialogEvents
{

    public static final String OPTBUSINESSLETTER_ITEM_CHANGED = null;
    public static final String OPTPRIVOFFICIALLETTER_ITEM_CHANGED = null;
    public static final String OPTPRIVATELETTER_ITEM_CHANGED = null;
    public static final String LSTBUSINESSSTYLE_ACTION_PERFORMED = null;
    public static final String LSTBUSINESSSTYLE_ITEM_CHANGED = null;
    public static final String LSTPRIVOFFICIALSTYLE_ACTION_PERFORMED = null;
    public static final String LSTPRIVOFFICIALSTYLE_ITEM_CHANGED = null;
    public static final String CHKBUSINESSPAPER_ITEM_CHANGED = null;
    public static final String LSTPRIVATESTYLE_ACTION_PERFORMED = null;
    public static final String LSTPRIVATESTYLE_ITEM_CHANGED = null;
    public static final String CHKPAPERCOMPANYLOGO_ITEM_CHANGED = null;
    public static final String NUMLOGOHEIGHT_TEXT_CHANGED = null;
    public static final String NUMLOGOX_TEXT_CHANGED = null;
    public static final String NUMLOGOWIDTH_TEXT_CHANGED = null;
    public static final String NUMLOGOY_TEXT_CHANGED = null;
    public static final String CHKCOMPANYRECEIVER_ITEM_CHANGED = null;
    public static final String CHKPAPERFOOTER_ITEM_CHANGED = null;
    public static final String NUMFOOTERHEIGHT_TEXT_CHANGED = null;
    public static final String CHKPAPERCOMPANYADDRESS_ITEM_CHANGED = null;
    public static final String NUMADDRESSHEIGHT_TEXT_CHANGED = null;
    public static final String NUMADDRESSX_TEXT_CHANGED = null;
    public static final String NUMADDRESSWIDTH_TEXT_CHANGED = null;
    public static final String NUMADDRESSY_TEXT_CHANGED = null;
    public static final String LSTLETTERNORM_ACTION_PERFORMED = null;
    public static final String LSTLETTERNORM_ITEM_CHANGED = null;
    public static final String CHKUSELOGO_ITEM_CHANGED = null;
    public static final String CHKUSEADDRESSRECEIVER_ITEM_CHANGED = null;
    public static final String CHKUSESIGNS_ITEM_CHANGED = null;
    public static final String CHKUSESUBJECT_ITEM_CHANGED = null;
    public static final String CHKUSEBENDMARKS_ITEM_CHANGED = null;
    public static final String CHKUSEFOOTER_ITEM_CHANGED = null;
    public static final String CHKUSESALUTATION_ITEM_CHANGED = null;
    public static final String CHKUSEGREETING_ITEM_CHANGED = null;
    public static final String LSTSALUTATION_ACTION_PERFORMED = null;
    public static final String LSTSALUTATION_ITEM_CHANGED = null;
    public static final String LSTGREETING_ACTION_PERFORMED = null;
    public static final String LSTGREETING_ITEM_CHANGED = null;
    public static final String OPTSENDERFIXED_ITEM_CHANGED = null;
    public static final String OPTSENDERPLACEHOLDER_ITEM_CHANGED = null;
    public static final String OPTSENDERDEFINE_ITEM_CHANGED = null;
    public static final String OPTRECEIVERPLACEHOLDER_ITEM_CHANGED = null;
    public static final String OPTRECEIVERDATABASE_ITEM_CHANGED = null;
    public static final String TXTSENDERNAME_TEXT_CHANGED = null;
    public static final String TXTSENDERSTREET_TEXT_CHANGED = null;
    public static final String TXTSENDERCITY_TEXT_CHANGED = null;
    public static final String TXTSENDERPOSTCODE_TEXT_CHANGED = null;
    public static final String TXTFOOTER_TEXT_CHANGED = null;
    public static final String CHKFOOTERNEXTPAGES_ITEM_CHANGED = null;
    public static final String CHKFOOTERPAGENUMBERS_ITEM_CHANGED = null;
    public static final String TXTTEMPLATENAME_TEXT_CHANGED = null;
    public static final String OPTCREATELETTER_ITEM_CHANGED = null;
    public static final String OPTMAKECHANGES_ITEM_CHANGED = null;
    public static final String FILETEMPLATEPATH_TEXT_CHANGED = null;
    public static final String imageURLImageControl1 = "images/ImageControl1";
    public static final String imageURLImageControl2 = "images/ImageControl2";
    public static final String imageURLImageControl3 = "images/ImageControl3";
}
