/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef SM_SNCOMMANDS_HRC
#define SM_SMCOMMANDS_HRC

#define CMD_SID_ADJUST                              ".uno:Adjust"
#define CMD_SID_ALIGN                               ".uno:ChangeAlignment"
#define CMD_SID_DISTANCE                            ".uno:ChangeDistance"
#define CMD_SID_FONT                                ".uno:ChangeFont"
#define CMD_SID_FONTSIZE                            ".uno:ChangeFontSize"
#define CMD_SID_CMDBOXWINDOW                        ".uno:CommandWindow"
#define CMD_SID_PREFERENCES                         ".uno:Preferences"
#define CMD_SID_TEXT                                ".uno:ConfigName"
#define CMD_SID_COPYOBJECT                          ".uno:CopyObject"
#define CMD_SID_DRAW                                ".uno:Draw"
#define CMD_SID_FITINWINDOW                         ".uno:FitInWindow"
#define CMD_SID_FORMULACURSOR                       ".uno:FormelCursor"
#define CMD_SID_GAPHIC_SM                           ".uno:Graphic"
#define CMD_SID_INSERTCOMMAND                       ".uno:InsertCommand"
#define CMD_SID_INSERTTEXT                          ".uno:InsertConfigName"
#define CMD_SID_INSERT_FORMULA                      ".uno:InsertFormula"
#define CMD_SID_LOADSYMBOLS                         ".uno:LoadSymbols"
#define CMD_SID_MODIFYSTATUS                        ".uno:ModifyStatus"
#define CMD_SID_NEXTERR                             ".uno:NextError"
#define CMD_SID_NEXTMARK                            ".uno:NextMark"
#define CMD_SID_PASTEOBJECT                         ".uno:PasteObject"
#define CMD_SID_PREVERR                             ".uno:PrevError"
#define CMD_SID_PREVMARK                            ".uno:PrevMark"
#define CMD_SID_AUTO_REDRAW                         ".uno:RedrawAutomatic"
#define CMD_SID_SAVESYMBOLS                         ".uno:SaveSymbols"
#define CMD_SID_GETEDITTEXT                         ".uno:SetPaperSize"
#define CMD_SID_SYMBOLS_CATALOGUE                   ".uno:SymbolCatalogue"
#define CMD_SID_SYMBOLS                             ".uno:Symbols"
#define CMD_SID_TEXTMODE                            ".uno:Textmode"
#define CMD_SID_TEXTSTATUS                          ".uno:TextStatus"
#define CMD_SID_TOOLBOXWINDOW                       ".uno:ToolBowWindow"
#define CMD_SID_TOOLBOX                             ".uno:ToolBox"
#define CMD_SID_VIEW100                             ".uno:View100"
#define CMD_SID_VIEW200                             ".uno:View200"
#define CMD_SID_VIEW050                             ".uno:View50"
#define CMD_SID_ZOOMIN                              ".uno:ZoomIn"
#define CMD_SID_ZOOMOUT                             ".uno:ZoomOut"

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
