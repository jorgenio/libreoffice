/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/


#define SMDLL 1

#include "starmath.hrc"

#define ITEMID_FONT         1
#define ITEMID_FONTHEIGHT   2
#define ITEMID_LRSPACE      3
#define ITEMID_WEIGHT       4

//--------- ab hier die "generierten"
#include <tools/string.hxx>
#include <tools/solar.h>
#include <tools/contnr.hxx>
#include <tools/rtti.hxx>
#include <tools/ref.hxx>
#include <tools/link.hxx>
#include <svl/brdcst.hxx>
#include <svl/svarray.hxx>
#include <osl/diagnose.h>
#include <svl/hint.hxx>
#include <svl/smplhint.hxx>
#include <sot/sotref.hxx>
#include <tools/globname.hxx>
#include <sot/factory.hxx>
#include <vcl/sv.h>
#include <basic/sbxdef.hxx>
#include <tools/time.hxx>
#include <tools/gen.hxx>
#include <tools/stream.hxx>
#include <tools/errinf.hxx>
#include <tools/errcode.hxx>
#include <sot/object.hxx>
#include <sot/sotdata.hxx>
#include <sfx2/shell.hxx>
#include <sal/types.h>
#include <sal/config.h>
#include <vcl/accel.hxx>
#include <tools/resid.hxx>
#include <tools/rc.hxx>
#include <i18npool/lang.h>
#include <tools/resmgr.hxx>
#include <vcl/keycod.hxx>
#include <vcl/keycodes.hxx>
#include <vcl/vclenum.hxx>
#include <vcl/cmdevt.hxx>
#include <vcl/font.hxx>
#include <tools/color.hxx>
#include <vcl/region.hxx>
#include <vcl/mapmod.hxx>
#include <tools/fract.hxx>
#include <vcl/wall.hxx>
#include <vcl/settings.hxx>
#include <vcl/bitmap.hxx>
#include <vcl/pointr.hxx>
#include <vcl/ptrstyle.hxx>
#include <vcl/wintypes.hxx>
#include <vcl/inputctx.hxx>
#include <vcl/event.hxx>
#include <format.hxx>
#include <utility.hxx>
#include <vcl/fixed.hxx>
#include <vcl/ctrl.hxx>
#include <vcl/window.hxx>
#include <sfx2/minarray.hxx>
#include <vcl/combobox.hxx>
#include <vcl/combobox.h>
#include <vcl/menu.hxx>
#include <vcl/bitmapex.hxx>
#include <vcl/lstbox.hxx>
#include <vcl/lstbox.h>
#include <usr/guid.hxx>
#include <usr/sequ.hxx>
#include <uno/types.h>
#include <uno/macros.h>
#include <osl/mutex.h>
#include <tools/shl.hxx>
#include <sfx2/module.hxx>
#include <sfx2/imgdef.hxx>
#include <usr/uno.hxx>
#include <usr/xiface.hxx>
#include <usr/ustring.hxx>
#include <salhelper/simplereferenceobject.hxx>
#include <osl/types.h>
#include <osl/interlck.h>
#include <smdll.hxx>
#include <sfx2/sfxsids.hrc>
#include <chaos/cntids.hrc>
#include <svl/cntwids.hrc>
#include <svl/solar.hrc>
#include <svl/lstner.hxx>
#include <starmath.hrc>
#include <basic/sbx.hxx>
#include <basic/sbxform.hxx>
#include <basic/sbxobj.hxx>
#include <basic/sbxvar.hxx>
#include <basic/sbxcore.hxx>
#include <basic/sbxprop.hxx>
#include <basic/sbxmeth.hxx>
#include <svl/poolitem.hxx>
#include <svtools/args.hxx>
#include <smmod.hxx>
#include <osl/thread.hxx>
#include <osl/thread.h>
#include <vcl/apptypes.hxx>
#include <vcl/svapp.hxx>
#include <vcl/timer.hxx>
#include <sfx2/app.hxx>
#include <sfx2/sfx.hrc>
#include <svl/memberid.hrc>
#include <vcl/syswin.hxx>
#include <tools/wldcrd.hxx>
#include <parse.hxx>
#include <types.hxx>
#include <config.hxx>
#include <svtools/confitem.hxx>
#include <tools/poly.hxx>
#include <svx/xpoly.hxx>
#include <rect.hxx>
#include <vcl/outdev.hxx>
#include <smart/com/sun/star/frame/XDispatchProviderInterceptor.hxx>
#include <smart/com/sun/star/frame/XDispatch.hxx>
#include <smart/com/sun/star/frame/XDispatchProvider.hxx>
#include <smart/com/sun/star/frame/XStatusListener.hxx>
#include <smart/com/sun/star/frame/FrameSearchFlag.hxx>
#include <smart/com/sun/star/frame/XDispatchProviderInterception.hxx>
#include <smart/com/sun/star/frame/FeatureStateEvent.hxx>
#include <smart/com/sun/star/frame/DispatchDescriptor.hxx>
#include <smart/com/sun/star/frame/XFrameActionListener.hxx>
#include <smart/com/sun/star/frame/XComponentLoader.hxx>
#include <smart/com/sun/star/frame/XFrame.hxx>
#include <smart/com/sun/star/frame/FrameActionEvent.hxx>
#include <smart/com/sun/star/frame/FrameAction.hxx>
#include <smart/com/sun/star/util/XURLTransformer.hxx>
#include <smart/com/sun/star/task/XStatusIndicatorFactory.hxx>
#include <smart/com/sun/star/task/XStatusIndicatorSupplier.hxx>
#include <smart/com/sun/star/task/XStatusIndicator.hxx>
#include <smart/com/sun/star/frame/XBrowseHistoryRegistry.hxx>
#include <smart/com/sun/star/io/BufferSizeExceededException.hxx>
#include <smart/com/sun/star/io/NotConnectedException.hxx>
#include <smart/com/sun/star/io/IOException.hxx>
#include <smart/com/sun/star/io/UnexpectedEOFException.hxx>
#include <smart/com/sun/star/io/WrongFormatException.hxx>
#include <smart/com/sun/star/lang/ServiceNotRegisteredException.hxx>
#include <smart/com/sun/star/lang/NullPointerException.hxx>
#include <smart/com/sun/star/lang/ClassNotFoundException.hxx>
#include <smart/com/sun/star/lang/NoSuchMethodException.hxx>
#include <smart/com/sun/star/lang/SecurityException.hxx>
#include <smart/com/sun/star/lang/NoSuchFieldException.hxx>
#include <smart/com/sun/star/lang/DisposedException.hxx>
#include <smart/com/sun/star/lang/ArrayIndexOutOfBoundsException.hxx>
#include <smart/com/sun/star/lang/IllegalAccessException.hxx>
#include <smart/com/sun/star/lang/IndexOutOfBoundsException.hxx>
#include <smart/com/sun/star/lang/IllegalArgumentException.hxx>
#include <smart/com/sun/star/lang/NoSupportException.hxx>
#include <smart/com/sun/star/lang/WrappedTargetException.hxx>
#include <smart/com/sun/star/reflection/ParamInfo.hxx>
#include <smart/com/sun/star/reflection/XIdlArray.hxx>
#include <smart/com/sun/star/reflection/XIdlClassProvider.hxx>
#include <smart/com/sun/star/reflection/FieldAccessMode.hxx>
#include <smart/com/sun/star/reflection/XIdlClass.hxx>
#include <smart/com/sun/star/reflection/XIdlField.hxx>
#include <smart/com/sun/star/reflection/ParamMode.hxx>
#include <smart/com/sun/star/reflection/MethodMode.hxx>
#include <smart/com/sun/star/reflection/XIdlMember.hxx>
#include <smart/com/sun/star/reflection/XIdlReflection.hxx>
#include <smart/com/sun/star/reflection/XIdlMethod.hxx>
#include <smart/com/sun/star/reflection/InvocationTargetException.hxx>
#include <smart/com/sun/star/beans/PropertyValues.hxx>
#include <smart/com/sun/star/beans/XPropertySet.hxx>
#include <smart/com/sun/star/beans/PropertyValue.hxx>
#include <smart/com/sun/star/beans/PropertyState.hxx>
#include <smart/com/sun/star/beans/XPropertySetInfo.hxx>
#include <smart/com/sun/star/beans/XMultiPropertySet.hxx>
#include <smart/com/sun/star/beans/XFastPropertySet.hxx>
#include <smart/com/sun/star/beans/XVetoableChangeListener.hxx>
#include <smart/com/sun/star/beans/XPropertyState.hxx>
#include <smart/com/sun/star/beans/XPropertyStateChangeListener.hxx>
#include <smart/com/sun/star/beans/PropertyAttribute.hxx>
#include <smart/com/sun/star/beans/XPropertiesChangeListener.hxx>
#include <smart/com/sun/star/beans/XPropertyChangeListener.hxx>
#include <smart/com/sun/star/beans/XPropertyAccess.hxx>
#include <smart/com/sun/star/beans/XPropertyContainer.hxx>
#include <smart/com/sun/star/beans/PropertyStateChangeEvent.hxx>
#include <smart/com/sun/star/beans/PropertyChangeEvent.hxx>
#include <smart/com/sun/star/beans/UnknownPropertyException.hxx>
#include <smart/com/sun/star/beans/IntrospectionException.hxx>
#include <smart/com/sun/star/beans/PropertyExistException.hxx>
#include <smart/com/sun/star/beans/IllegalTypeException.hxx>
#include <smart/com/sun/star/beans/PropertyVetoException.hxx>
#include <smart/com/sun/star/container/XEnumerationAccess.hxx>
#include <smart/com/sun/star/container/XHierarchicalNameAccess.hxx>
#include <smart/com/sun/star/container/XNameAccess.hxx>
#include <smart/com/sun/star/container/XContentEnumerationAccess.hxx>
#include <smart/com/sun/star/container/XEnumeration.hxx>
#include <smart/com/sun/star/container/XElementAccess.hxx>
#include <smart/com/sun/star/container/XIndexAccess.hxx>
#include <smart/com/sun/star/lang/XEventListener.hxx>
#include <smart/com/sun/star/lang/EventObject.hxx>
#include <smart/com/sun/star/script/XAllListenerAdapterService.hxx>
#include <smart/com/sun/star/script/XAllListener.hxx>
#include <smart/com/sun/star/script/AllEventObject.hxx>
#include <smart/com/sun/star/container/XComponentEnumeration.hxx>
#include <smart/com/sun/star/lang/XComponent.hxx>
#include <smart/com/sun/star/container/XComponentEnumerationAccess.hxx>
#include <smart/com/sun/star/lang/ListenerExistException.hxx>
#include <smart/com/sun/star/container/ElementExistException.hxx>
#include <smart/com/sun/star/lang/InvalidListenerException.hxx>
#include <smart/com/sun/star/container/NoSuchElementException.hxx>
#include <smart/com/sun/star/awt/XKeyListener.hxx>
#include <smart/com/sun/star/awt/XPaintListener.hxx>
#include <smart/com/sun/star/awt/KeyEvent.hxx>
#include <smart/com/sun/star/awt/KeyModifier.hxx>
#include <smart/com/sun/star/awt/XMouseMotionListener.hxx>
#include <smart/com/sun/star/awt/FocusEvent.hxx>
#include <smart/com/sun/star/awt/XWindowListener.hxx>
#include <smart/com/sun/star/awt/XActivateListener.hxx>
#include <smart/com/sun/star/awt/MouseEvent.hxx>
#include <smart/com/sun/star/awt/XTopWindowListener.hxx>
#include <smart/com/sun/star/awt/PaintEvent.hxx>
#include <smart/com/sun/star/awt/InputEvent.hxx>
#include <smart/com/sun/star/awt/KeyGroup.hxx>
#include <smart/com/sun/star/awt/Key.hxx>
#include <smart/com/sun/star/awt/WindowEvent.hxx>
#include <smart/com/sun/star/awt/XMouseListener.hxx>
#include <smart/com/sun/star/awt/KeyFunction.hxx>
#include <smart/com/sun/star/awt/FocusChangeReason.hxx>
#include <smart/com/sun/star/awt/MouseButton.hxx>
#include <smart/com/sun/star/awt/XFocusListener.hxx>
#include <smart/com/sun/star/awt/XAdjustmentListener.hxx>
#include <smart/com/sun/star/awt/XActionListener.hxx>
#include <smart/com/sun/star/awt/XTextListener.hxx>
#include <smart/com/sun/star/awt/XSpinListener.hxx>
#include <smart/com/sun/star/awt/XItemListener.hxx>
#include <smart/com/sun/star/awt/XVclContainerListener.hxx>
#include <smart/com/sun/star/awt/XFileDialog.hxx>
#include <smart/com/sun/star/awt/XTextComponent.hxx>
#include <smart/com/sun/star/awt/XListBox.hxx>
#include <smart/com/sun/star/awt/XProgressMonitor.hxx>
#include <smart/com/sun/star/awt/TextAlign.hxx>
#include <smart/com/sun/star/awt/XScrollBar.hxx>
#include <smart/com/sun/star/awt/XVclContainerPeer.hxx>
#include <smart/com/sun/star/awt/XTabControllerModel.hxx>
#include <smart/com/sun/star/awt/XMessageBox.hxx>
#include <smart/com/sun/star/awt/XTextEditField.hxx>
#include <smart/com/sun/star/awt/Style.hxx>
#include <smart/com/sun/star/awt/XTimeField.hxx>
#include <smart/com/sun/star/awt/XVclWindowPeer.hxx>
#include <smart/com/sun/star/awt/XControlModel.hxx>
#include <smart/com/sun/star/awt/XSpinField.hxx>
#include <smart/com/sun/star/awt/XUnoControlContainer.hxx>
#include <smart/com/sun/star/awt/XTextLayoutConstrains.hxx>
#include <smart/com/sun/star/awt/XNumericField.hxx>
#include <smart/com/sun/star/awt/XButton.hxx>
#include <smart/com/sun/star/awt/XTextArea.hxx>
#include <smart/com/sun/star/awt/XImageButton.hxx>
#include <smart/com/sun/star/awt/XFixedText.hxx>
#include <smart/com/sun/star/awt/XControlContainer.hxx>
#include <smart/com/sun/star/awt/XDialog.hxx>
#include <smart/com/sun/star/awt/ScrollBarOrientation.hxx>
#include <smart/com/sun/star/awt/XRadioButton.hxx>
#include <smart/com/sun/star/awt/XCurrencyField.hxx>
#include <smart/com/sun/star/awt/XPatternField.hxx>
#include <smart/com/sun/star/awt/VclWindowPeerAttribute.hxx>
#include <smart/com/sun/star/awt/XTabController.hxx>
#include <smart/com/sun/star/awt/XVclContainer.hxx>
#include <smart/com/sun/star/awt/XDateField.hxx>
#include <smart/com/sun/star/awt/XComboBox.hxx>
#include <smart/com/sun/star/awt/XControl.hxx>
#include <smart/com/sun/star/awt/XCheckBox.hxx>
#include <smart/com/sun/star/awt/MessageBoxCommand.hxx>
#include <smart/com/sun/star/awt/XLayoutConstrains.hxx>
#include <smart/com/sun/star/awt/XProgressBar.hxx>
#include <smart/com/sun/star/awt/SimpleFontMetric.hxx>
#include <smart/com/sun/star/awt/FontWeight.hxx>
#include <smart/com/sun/star/awt/FontSlant.hxx>
#include <smart/com/sun/star/awt/CharSet.hxx>
#include <smart/com/sun/star/awt/FontDescriptor.hxx>
#include <smart/com/sun/star/awt/FontWidth.hxx>
#include <smart/com/sun/star/awt/XFont.hxx>
#include <smart/com/sun/star/awt/FontType.hxx>
#include <smart/com/sun/star/awt/FontUnderline.hxx>
#include <smart/com/sun/star/awt/FontStrikeout.hxx>
#include <smart/com/sun/star/awt/FontFamily.hxx>
#include <smart/com/sun/star/awt/FontPitch.hxx>
#include <smart/com/sun/star/awt/XTopWindow.hxx>
#include <smart/com/sun/star/awt/XWindow.hxx>
#include <smart/com/sun/star/awt/PosSize.hxx>
#include <smart/com/sun/star/awt/VclContainerEvent.hxx>
#include <smart/com/sun/star/awt/ItemEvent.hxx>
#include <smart/com/sun/star/awt/SpinEvent.hxx>
#include <smart/com/sun/star/awt/TextEvent.hxx>
#include <smart/com/sun/star/awt/AdjustmentType.hxx>
#include <smart/com/sun/star/awt/ActionEvent.hxx>
#include <smart/com/sun/star/awt/AdjustmentEvent.hxx>
#include <smart/com/sun/star/awt/Rectangle.hxx>
#include <smart/com/sun/star/awt/Selection.hxx>
#include <smart/com/sun/star/awt/Size.hxx>
#include <smart/com/sun/star/awt/WindowDescriptor.hxx>
#include <smart/com/sun/star/awt/InvalidateStyle.hxx>
#include <smart/com/sun/star/awt/XToolkit.hxx>
#include <smart/com/sun/star/awt/XWindowPeer.hxx>
#include <smart/com/sun/star/awt/WindowClass.hxx>
#include <smart/com/sun/star/awt/XSystemDependentWindowPeer.hxx>
#include <smart/com/sun/star/awt/WindowAttribute.hxx>
#include <smart/com/sun/star/awt/XPointer.hxx>
#include <smart/com/sun/star/awt/SystemPointer.hxx>
#include <smart/com/sun/star/awt/XView.hxx>
#include <usr/refl.hxx>
#include <sfx2/msg.hxx>
#include <svl/itemset.hxx>
#include <sfx2/basedlgs.hxx>
#include <sfx2/viewfrm.hxx>
#include <sfx2/frame.hxx>
#include <sfx2/objface.hxx>
#include <svl/eitem.hxx>
#include <svl/intitem.hxx>
#include <symbol.hxx>
#include <svl/itempool.hxx>
#include <vcl/image.hxx>
#include <vcl/metric.hxx>
#include <sfx2/inimgr.hxx>
#include <node.hxx>
#include <sfx2/docfac.hxx>
#include <svl/ownlist.hxx>
#include <sfx2/objsh.hxx>
#include <svl/stritem.hxx>
#include <sfx2/ipfrm.hxx>
#include <vcl/dialog.hxx>
#include <sfx2/dispatch.hxx>
#include <svl/svstdarr.hxx>
#include <sfx2/bindings.hxx>
#include <dialog.hxx>
#include <vcl/symbol.hxx>
#include <sfx2/tabdlg.hxx>
#include <vcl/button.hxx>
#include <vcl/tabdlg.hxx>
#include <vcl/tabpage.hxx>
#include <vcl/tabctrl.hxx>
#include <svx/optgenrl.hxx>
#include <vcl/edit.hxx>
#include <vcl/group.hxx>
#include <document.hxx>
#include <vcl/spinfld.hxx>
#include <vcl/menubtn.hxx>
#include <svtools/ctrlbox.hxx>
#include <vcl/virdev.hxx>
#include <vcl/field.hxx>
#include <svtools/ctrltool.hxx>
#include <sfx2/interno.hxx>
#include <sfx2/sfxdefs.hxx>
#include <sfx2/childwin.hxx>
#include <sfx2/chalign.hxx>
#include <vcl/floatwin.hxx>
#include <sot/storage.hxx>
#include <rsc/rscsfx.hxx>
#include <vcl/msgbox.hxx>
#include <vcl/btndlg.hxx>
#include <uno/uno.h>
#include <uno/string.h>
#include <uno/sequence.h>
#include <uno/any.h>
#include <uno/exceptn.h>
#include <uno/intrface.h>
#include <uno/factory.h>
#include <uno/api.h>
#include <svx/svxids.hrc>
#include <view.hxx>
#include <sfx2/dockwin.hxx>
#include <sfx2/viewsh.hxx>
#include <sfx2/clientsh.hxx>
#include <svtools/scrwin.hxx>
#include <vcl/scrbar.hxx>
#include <sfx2/ctrlitem.hxx>
#include <sfx2/viewfac.hxx>
#include <edit.hxx>
#include <editeng/editdata.hxx>
#include <toolbox.hxx>
#include <vcl/toolbox.hxx>
#include <vcl/dockwin.hxx>
#include <smslots.hxx>
#include <svl/undo.hxx>
#include <sfx2/request.hxx>
#include <svl/whiter.hxx>
#include <vcl/prntypes.hxx>
#include <vcl/jobset.hxx>
#include <vcl/gdimtf.hxx>
#include <sot/exchange.hxx>
#include <vcl/wrkwin.hxx>
#include <action.hxx>
#include <sfx2/filedlg.hxx>
#include <sfx2/iodlg.hxx>

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
