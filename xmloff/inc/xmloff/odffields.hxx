/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2008 by Sun Microsystems, Inc.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

/*
 * Copyright 2008 by Novell, Inc.
 */

#ifndef _ODFFIELDS_HXX
#define _ODFFIELDS_HXX


#define ODF_FORMTEXT "vnd.oasis.opendocument.field.FORMTEXT"

#define ODF_FORMCHECKBOX "vnd.oasis.opendocument.field.FORMCHECKBOX"
#define ODF_FORMCHECKBOX_HELPTEXT "Checkbox_HelpText"
#define ODF_FORMCHECKBOX_NAME "Checkbox_Name"
#define ODF_FORMCHECKBOX_RESULT "Checkbox_Checked"

#define ODF_FORMDROPDOWN "vnd.oasis.opendocument.field.FORMDROPDOWN"
#define ODF_FORMDROPDOWN_LISTENTRY "Dropdown_ListEntry"
#define ODF_FORMDROPDOWN_RESULT "Dropdown_Selected"

#define ODF_TOC "vnd.oasis.opendocument.field.TOC"

#define ODF_HYPERLINK "vnd.oasis.opendocument.field.HYPERLINK"

#define ODF_PAGEREF "vnd.oasis.opendocument.field.PAGEREF"

#define ODF_UNHANDLED "vnd.oasis.opendocument.field.UNHANDLED"
#define ODF_OLE_PARAM "vnd.oasis.opendocument.field.ole"
#define ODF_ID_PARAM "vnd.oasis.opendocument.field.id"
#define ODF_CODE_PARAM "vnd.oasis.opendocument.field.code"

#endif /*  _ODFFIELDS_HXX */

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
