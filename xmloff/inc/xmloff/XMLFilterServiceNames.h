/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_XMLFILTERSERVICENAMES_H
#define _XMLOFF_XMLFILTERSERVICENAMES_H


#define XML_IMPORT_FILTER_WRITER    "com.sun.star.comp.Writer.XMLOasisImporter"
#define XML_IMPORT_FILTER_CALC      "com.sun.star.comp.Calc.XMLOasisImporter"
#define XML_IMPORT_FILTER_DRAW      "com.sun.star.comp.Draw.XMLOasisImporter"
#define XML_IMPORT_FILTER_IMPRESS   "com.sun.star.comp.Impress.XMLOasisImporter"
#define XML_IMPORT_FILTER_MATH      "com.sun.star.comp.Math.XMLImporter"
#define XML_IMPORT_FILTER_CHART     "com.sun.star.comp.Chart.XMLOasisImporter"

#define XML_EXPORT_FILTER_WRITER    "com.sun.star.comp.Writer.XMLOasisExporter"
#define XML_EXPORT_FILTER_CALC      "com.sun.star.comp.Calc.XMLOasisExporter"
#define XML_EXPORT_FILTER_DRAW      "com.sun.star.comp.Draw.XMLOasisExporter"
#define XML_EXPORT_FILTER_IMPRESS   "com.sun.star.comp.Impress.XMLOasisExporter"
#define XML_EXPORT_FILTER_MATH      "com.sun.star.comp.Math.XMLExporter"
#define XML_EXPORT_FILTER_CHART     "com.sun.star.comp.Chart.XMLOasisExporter"

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
