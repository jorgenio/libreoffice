/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SCH_XMLPARAGRAPHCONTEXT_HXX_
#define _SCH_XMLPARAGRAPHCONTEXT_HXX_

#include <xmloff/xmlictxt.hxx>
#include "rtl/ustring.hxx"
#include "rtl/ustrbuf.hxx"

class SchXMLImport;

namespace com { namespace sun { namespace star { namespace xml { namespace sax {
        class XAttributeList;
}}}}}

class SchXMLParagraphContext : public SvXMLImportContext
{
private:
    ::rtl::OUString& mrText;
    ::rtl::OUString* mpId;
    ::rtl::OUStringBuffer maBuffer;

public:
    SchXMLParagraphContext( SvXMLImport& rImport,
                            const ::rtl::OUString& rLocalName,
                            ::rtl::OUString& rText,
                            ::rtl::OUString * pOutId = 0 );
    virtual ~SchXMLParagraphContext();
    virtual void StartElement( const ::com::sun::star::uno::Reference< ::com::sun::star::xml::sax::XAttributeList >& xAttrList );
    virtual void EndElement();

    virtual SvXMLImportContext* CreateChildContext(
        sal_uInt16 nPrefix,
        const ::rtl::OUString& rLocalName,
        const com::sun::star::uno::Reference< com::sun::star::xml::sax::XAttributeList >& xAttrList );

    virtual void Characters( const rtl::OUString& rChars );
};

#endif  // _SCH_XMLPARAGRAPHCONTEXT_HXX_

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
