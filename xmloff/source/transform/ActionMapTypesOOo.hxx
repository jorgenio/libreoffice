/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_ACTIONMAPTYPESOOO_HXX
#define _XMLOFF_ACTIONMAPTYPESOOO_HXX

enum ActionMapTypesOOo
{
    PROP_OOO_GRAPHIC_ATTR_ACTIONS,
    PROP_OOO_GRAPHIC_ELEM_ACTIONS,
    PROP_OOO_DRAWING_PAGE_ATTR_ACTIONS,
    PROP_OOO_PAGE_LAYOUT_ATTR_ACTIONS,
    PROP_OOO_HEADER_FOOTER_ATTR_ACTIONS,
    PROP_OOO_TEXT_ATTR_ACTIONS,
    PROP_OOO_TEXT_ELEM_ACTIONS,
    PROP_OOO_PARAGRAPH_ATTR_ACTIONS,
    PROP_OOO_PARAGRAPH_ELEM_ACTIONS,
    PROP_OOO_SECTION_ATTR_ACTIONS,
    PROP_OOO_TABLE_ATTR_ACTIONS,
    PROP_OOO_TABLE_COLUMN_ATTR_ACTIONS,
    PROP_OOO_TABLE_ROW_ATTR_ACTIONS,
    PROP_OOO_TABLE_CELL_ATTR_ACTIONS,
    PROP_OOO_TABLE_CELL_ELEM_ACTIONS,
    PROP_OOO_LIST_LEVEL_ATTR_ACTIONS,
    PROP_OOO_CHART_ATTR_ACTIONS,
    PROP_OOO_CHART_ELEM_ACTIONS,
    MAX_OOO_PROP_ACTIONS,
    OOO_STYLE_ACTIONS = MAX_OOO_PROP_ACTIONS,
    OOO_FONT_DECL_ACTIONS,
    OOO_SHAPE_ACTIONS,
    OOO_CONNECTOR_ACTIONS,
    OOO_INDEX_ENTRY_TAB_STOP_ACTIONS,
    OOO_TAB_STOP_ACTIONS,
    OOO_LINENUMBERING_ACTIONS,
    OOO_FOOTNOTE_SEP_ACTIONS,
    OOO_DROP_CAP_ACTIONS,
    OOO_COLUMNS_ACTIONS,
    OOO_TEXT_VALUE_TYPE_ACTIONS,
    OOO_TABLE_VALUE_TYPE_ACTIONS,
    OOO_PARA_ACTIONS,
    OOO_STYLE_REF_ACTIONS,
    OOO_MASTER_PAGE_ACTIONS,
    OOO_ANNOTATION_ACTIONS,
    OOO_CHANGE_INFO_ACTIONS,
    OOO_FRAME_ELEM_ACTIONS,
    OOO_FRAME_ATTR_ACTIONS,
    OOO_BACKGROUND_IMAGE_ACTIONS,
    OOO_DDE_CONNECTION_DECL_ACTIONS,
    OOO_EVENT_ACTIONS,
    OOO_FORM_CONTROL_ACTIONS,
    OOO_FORM_COLUMN_ACTIONS,
    OOO_FORM_PROP_ACTIONS,
    OOO_XLINK_ACTIONS,
    OOO_CONFIG_ITEM_SET_ACTIONS,
    OOO_FORMULA_ACTIONS,
    OOO_CHART_ACTIONS,
    OOO_ERROR_MACRO_ACTIONS,
    OOO_DDE_CONV_MODE_ACTIONS,
    OOO_ALPHABETICAL_INDEX_MARK_ACTIONS,
    OOO_DATAPILOT_MEMBER_ACTIONS,
    OOO_DATAPILOT_LEVEL_ACTIONS,
    OOO_SOURCE_SERVICE_ACTIONS,
    OOO_DRAW_AREA_POLYGON_ACTIONS,
    OOO_SCRIPT_ACTIONS,
    OOO_ANIMATION_ACTIONS,
    MAX_OOO_ACTIONS
};

#endif  //  _XMLOFF_ACTIONMAPTYPESOOO_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
