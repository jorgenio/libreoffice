/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_FAMILYTYPE_HXX
#define _XMLOFF_FAMILYTYPE_HXX

enum XMLFamilyType
{
    XML_FAMILY_TYPE_GRAPHIC,
    XML_FAMILY_TYPE_PRESENTATION,
    XML_FAMILY_TYPE_DRAWING_PAGE,
    XML_FAMILY_TYPE_MASTER_PAGE,
    XML_FAMILY_TYPE_PAGE_LAYOUT,
    XML_FAMILY_TYPE_HEADER_FOOTER,
    XML_FAMILY_TYPE_TEXT,
    XML_FAMILY_TYPE_PARAGRAPH,
    XML_FAMILY_TYPE_RUBY,
    XML_FAMILY_TYPE_SECTION,
    XML_FAMILY_TYPE_TABLE,
    XML_FAMILY_TYPE_TABLE_COLUMN,
    XML_FAMILY_TYPE_TABLE_ROW,
    XML_FAMILY_TYPE_TABLE_CELL,
    XML_FAMILY_TYPE_LIST,
    XML_FAMILY_TYPE_CHART,
    XML_FAMILY_TYPE_DATA,
    XML_FAMILY_TYPE_GRADIENT,
    XML_FAMILY_TYPE_HATCH,
    XML_FAMILY_TYPE_FILL_IMAGE,
    XML_FAMILY_TYPE_STROKE_DASH,
    XML_FAMILY_TYPE_MARKER,
    XML_FAMILY_TYPE_PRESENTATION_PAGE_LAYOUT,
    XML_FAMILY_TYPE_END
};

#endif  //  _XMLOFF_FAMILYTYPE_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
