/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_ACTIONMAPTYPESOASIS_HXX
#define _XMLOFF_ACTIONMAPTYPESOASIS_HXX

enum ActionMapTypesOOo
{
    PROP_OASIS_GRAPHIC_ATTR_ACTIONS,
    PROP_OASIS_DRAWING_PAGE_ATTR_ACTIONS,
    PROP_OASIS_PAGE_LAYOUT_ATTR_ACTIONS,
    PROP_OASIS_HEADER_FOOTER_ATTR_ACTIONS,
    PROP_OASIS_TEXT_ATTR_ACTIONS,
    PROP_OASIS_PARAGRAPH_ATTR_ACTIONS,
    PROP_OASIS_SECTION_ATTR_ACTIONS,
    PROP_OASIS_TABLE_ATTR_ACTIONS,
    PROP_OASIS_TABLE_COLUMN_ATTR_ACTIONS,
    PROP_OASIS_TABLE_ROW_ATTR_ACTIONS,
    PROP_OASIS_TABLE_CELL_ATTR_ACTIONS,
    PROP_OASIS_LIST_LEVEL_ATTR_ACTIONS,
    PROP_OASIS_CHART_ATTR_ACTIONS,
    MAX_OASIS_PROP_ACTIONS,
    OASIS_STYLE_ACTIONS = MAX_OASIS_PROP_ACTIONS,
    OASIS_FONT_FACE_ACTIONS,
    OASIS_SHAPE_ACTIONS,
    OASIS_CONNECTOR_ACTIONS,
    OASIS_INDEX_ENTRY_TAB_STOP_ACTIONS,
    OASIS_TAB_STOP_ACTIONS,
    OASIS_LINENUMBERING_ACTIONS,
    OASIS_FOOTNOTE_SEP_ACTIONS,
    OASIS_DROP_CAP_ACTIONS,
    OASIS_COLUMNS_ACTIONS,
    OASIS_TEXT_VALUE_TYPE_ACTIONS,
    OASIS_TABLE_VALUE_TYPE_ACTIONS,
    OASIS_PARA_ACTIONS,
    OASIS_LIST_STYLE_REF_ACTIONS,
    OASIS_TEXT_STYLE_REF_ACTIONS,
    OASIS_PARA_STYLE_REF_ACTIONS,
    OASIS_MASTER_PAGE_REF_ACTIONS,
    OASIS_MAP_STYLE_REF_ACTIONS,
    OASIS_MASTER_PAGE_ACTIONS,
    OASIS_NOTES_ACTIONS,
    OASIS_ANNOTATION_ACTIONS,
    OASIS_CHANGE_INFO_ACTIONS,
    OASIS_FRAME_ELEM_ACTIONS,
    OASIS_BACKGROUND_IMAGE_ACTIONS,
    OASIS_DDE_CONNECTION_DECL_ACTIONS,
    OASIS_EVENT_ACTIONS,
    OASIS_DLG_ACTIONS,
    OASIS_FORM_CONTROL_ACTIONS,
    OASIS_FORM_COLUMN_ACTIONS,
    OASIS_FORM_PROP_ACTIONS,
    OASIS_XLINK_ACTIONS,
    OASIS_CONFIG_ITEM_SET_ACTIONS,
    OASIS_FORMULA_ACTIONS,
    OASIS_CHART_ACTIONS,
    OASIS_FORM_ACTIONS,
    OASIS_ALPHABETICAL_INDEX_MARK_ACTIONS,
    OASIS_CONTENT_VALIDATION_ACTIONS,
    OASIS_DDE_CONV_MODE_ACTIONS,
    OASIS_DATAPILOT_MEMBER_ACTIONS,
    OASIS_DATAPILOT_LEVEL_ACTIONS,
    OASIS_SOURCE_SERVICE_ACTIONS,
    OASIS_DRAW_AREA_POLYGON_ACTIONS,
    OASIS_SCRIPT_ACTIONS,
    OASIS_DATETIME_ACTIONS,
    OASIS_TABLE_STYLE_REF_ACTIONS,
    OASIS_ANIMATION_ACTIONS,
    MAX_OASIS_ACTIONS
};

#endif  //  _XMLOFF_ACTIONMAPTYPESOASIS_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
