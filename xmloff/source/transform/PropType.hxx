/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _XMLOFF_PROPTYPE_HXX
#define _XMLOFF_PROPTYPE_HXX

enum  XMLPropType
{
    XML_PROP_TYPE_GRAPHIC,
    XML_PROP_TYPE_DRAWING_PAGE,
    XML_PROP_TYPE_PAGE_LAYOUT,
    XML_PROP_TYPE_HEADER_FOOTER,
    XML_PROP_TYPE_TEXT,
    XML_PROP_TYPE_PARAGRAPH,
    XML_PROP_TYPE_RUBY,
    XML_PROP_TYPE_SECTION,
    XML_PROP_TYPE_TABLE,
    XML_PROP_TYPE_TABLE_COLUMN,
    XML_PROP_TYPE_TABLE_ROW,
    XML_PROP_TYPE_TABLE_CELL,
    XML_PROP_TYPE_LIST_LEVEL,
    XML_PROP_TYPE_CHART,
    XML_PROP_TYPE_END
};

#endif  //  _XMLOFF_PROPTYPE_HXX

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
