/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _DESKTOP_HRC_
#define _DESKTOP_HRC_

#ifndef _SOLAR_HRC
#include <svl/solar.hrc>
#endif

#define RID_DESKTOP_DIALOG_START            2000
#define RID_FIRSTSTSTART_START              3000
#define RID_DESKTOP_STRING_START            10000


#define INFOBOX_CMDLINEHELP                 (RID_DESKTOP_DIALOG_START+51)
#define INFOBOX_EXPIRED                     (RID_DESKTOP_DIALOG_START+52)

#define QBX_USERDATALOCKED                  (RID_DESKTOP_DIALOG_START+100)

#define DLG_CMDLINEHELP                     (RID_DESKTOP_DIALOG_START+101)
#define TXT_DLG_CMDLINEHELP_HEADER          (RID_DESKTOP_DIALOG_START+102)
#define TXT_DLG_CMDLINEHELP_LEFT            (RID_DESKTOP_DIALOG_START+103)
#define TXT_DLG_CMDLINEHELP_RIGHT           (RID_DESKTOP_DIALOG_START+104)
#define TXT_DLG_CMDLINEHELP_BOTTOM          (RID_DESKTOP_DIALOG_START+105)
#define BTN_DLG_CMDLINEHELP_OK              (RID_DESKTOP_DIALOG_START+106)

#define QBX_CONFIG_IMPORTSETTINGS           (RID_DESKTOP_DIALOG_START+180)

#define EBX_ERR_PRINTDISABLED               (RID_DESKTOP_DIALOG_START+190)

#define STR_RECOVER_QUERY                   (RID_DESKTOP_STRING_START+0)
#define STR_RECOVER_TITLE                   (RID_DESKTOP_STRING_START+1)
#define STR_RECOVER_PREPARED                (RID_DESKTOP_STRING_START+2)

#define STR_BOOTSTRAP_ERR_CANNOT_START      (RID_DESKTOP_STRING_START+100)
#define STR_BOOTSTRAP_ERR_DIR_MISSING       (RID_DESKTOP_STRING_START+101)
#define STR_BOOTSTRAP_ERR_PATH_INVALID      (RID_DESKTOP_STRING_START+102)
#define STR_BOOTSTRAP_ERR_NO_PATH           (RID_DESKTOP_STRING_START+103)
#define STR_BOOTSTRAP_ERR_INTERNAL          (RID_DESKTOP_STRING_START+104)
#define STR_BOOTSTRAP_ERR_FILE_CORRUPT      (RID_DESKTOP_STRING_START+105)
#define STR_BOOTSTRAP_ERR_FILE_MISSING      (RID_DESKTOP_STRING_START+106)
#define STR_BOOTSTRAP_ERR_NO_SUPPORT        (RID_DESKTOP_STRING_START+107)
#define STR_BOOTSTRAP_ERR_LANGUAGE_MISSING  (RID_DESKTOP_STRING_START+108)

#define STR_BOOTSTRAP_ERR_NO_SERVICE        (RID_DESKTOP_STRING_START+120)
#define STR_BOOTSTRAP_ERR_NO_CFG_SERVICE    (RID_DESKTOP_STRING_START+121)
#define STR_BOOTSTRAP_ERR_CFG_DATAACCESS    (RID_DESKTOP_STRING_START+122)
#define STR_BOOTSTRAP_ERR_NO_PATHSET_SERVICE (RID_DESKTOP_STRING_START+123)

#define STR_ASK_START_SETUP_MANUALLY        (RID_DESKTOP_STRING_START+152)

#define STR_INTERNAL_ERRMSG                 (RID_DESKTOP_STRING_START+161)

#define STR_CONFIG_ERR_SETTINGS_INCOMPLETE  (RID_DESKTOP_STRING_START+182)
#define STR_CONFIG_ERR_CANNOT_CONNECT       (RID_DESKTOP_STRING_START+183)
#define STR_CONFIG_ERR_RIGHTS_MISSING       (RID_DESKTOP_STRING_START+184)
#define STR_CONFIG_ERR_ACCESS_GENERAL       (RID_DESKTOP_STRING_START+187)
#define STR_CONFIG_ERR_NO_WRITE_ACCESS      (RID_DESKTOP_STRING_START+188)

#define STR_BOOSTRAP_ERR_NOTENOUGHDISKSPACE (RID_DESKTOP_STRING_START+189)
#define STR_BOOSTRAP_ERR_NOACCESSRIGHTS     (RID_DESKTOP_STRING_START+190)

#define STR_TITLE_USERDATALOCKED            (RID_DESKTOP_STRING_START+206)
#define STR_TITLE_EXPIRED                   (RID_DESKTOP_STRING_START+207)

#endif // _DESKTOP_HRC_
