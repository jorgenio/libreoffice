/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int MainW();

#ifdef __cplusplus
}   /* extern "C" */
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
