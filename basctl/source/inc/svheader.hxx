/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVHEADER_HXX
#define _SVHEADER_HXX


// SV
#define _COLDLG_HXX
#define _CONFIG_HXX
#define _CURSOR_HXX
#define _FONTDLG_HXX
#define _PRVWIN_HXX
//#define _SELENG_HXX
#define _VIRTDEV_HXX

// For Brkdlg without PCH:
// #define _SPIN_HXX
// #define _FIELD_HXX

// SVTOOLS
#define _CTRLBOX_HXX
#define _EXTATTR_HXX
#define _CTRLTOOL_HXX
#define _CTRLBOX_HXX
#define _STDMENU_HXX
#define _VALUESET_HXX
#define _RULER_HXX
#define _SCRWIN_HXX

// SFX
#define _SFXAPPWIN_HXX
#define _SFX_RESMGR_HXX
#define _SFX_PRNMON_HXX
#define _INTRO_HXX
#define _SFXMSGDESCR_HXX
#define _SFXMSGPOOL_HXX
// #define _PASSWD_HXX
#define _SFXMNUMGR_HXX

// SFXDLG
// #define _SFXTABDLG_HXX
// #define _SFX_DINFDLG_HXX
#define _SFX_MGETEMPL_HXX
#define _SFX_STYLEDLG_HXX
#define _SFX_TPLPITEM_HXX
#define _NEWSTYLE_HXX
#define _SFXDOCTEMPL_HXX
#define _SFXDOCTDLG_HXX
#define _SFX_TEMPLDLG_HXX
#define _SFXNEW_HXX

// SFXDOC
// #define _SFXDOCINF_HXX
//#define _SFX_DOCFILT_HXX
#define _SFX_IPFRM_HXX
#define _SFX_INTERNO_HXX

// SVCONTNR
#define _SVICNVW_HXX

#define _SBSTDOBJ1_HXX
#define _SBXMSTRM_HXX

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
