/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "app.hrc"
#include "res_bmp.hrc"
#include "copydlg.hrc"
#include "helpids.h"

ModalDialog DLG_COPY
{
    HelpID = CMD_SID_COPYOBJECTS ;
    OutputSize = TRUE ;
    SVLook = TRUE ;
    Size = MAP_APPFONT ( 204 , 177 ) ;
    Text [ en-US ] = "Duplicate" ;
    Moveable = TRUE ;
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 148 , 6  ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 148 , 23  ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 148 , 43  ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    FixedText FT_COPIES
    {
        Pos = MAP_APPFONT ( 6 , 9 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "Number of ~copies" ;
    };
    NumericField NUM_FLD_COPIES
    {
        HelpID = "sd:NumericField:DLG_COPY:NUM_FLD_COPIES";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 7 ) ;
        Size = MAP_APPFONT ( 35 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = 1 ;
        Maximum = 999 ;
        StrictFormat = TRUE ;
        First = 1 ;
        Last = 999 ;
        SpinSize = 1 ;
    };
    ImageButton BTN_SET_VIEWDATA
    {
        HelpID = "sd:ImageButton:DLG_COPY:BTN_SET_VIEWDATA";
        Pos = MAP_APPFONT ( 122 , 6 ) ;
        Size = MAP_APPFONT ( 14 , 14 ) ;
        ButtonImage = Image
        {
            ImageBitmap = Bitmap { File = "pipette.bmp" ; };
            MaskColor = IMAGE_MASK_STDCOLOR;
        };
        TabStop = TRUE ;
        QuickHelpText [ en-US ] = "Values from Selection" ;
    };
    PushButton BTN_SET_DEFAULT
    {
        HelpID = "sd:PushButton:DLG_COPY:BTN_SET_DEFAULT";
        Pos = MAP_APPFONT ( 148 , 63  ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        Text [ en-US ] = "~Default";
    };
    MetricField MTR_FLD_ANGLE
    {
        HelpID = "sd:MetricField:DLG_COPY:MTR_FLD_ANGLE";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 69 ) ;
        Size = MAP_APPFONT ( 45 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Maximum = 359 ;
        StrictFormat = TRUE ;
        Unit = FUNIT_CUSTOM ;
        Last = 359 ;
        SpinSize = 5 ;
        CustomUnitText [ en-US ] = " degrees" ;
    };
    FixedText FT_ANGLE
    {
        Pos = MAP_APPFONT ( 12 , 71 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~Angle" ;
    };
    FixedText FT_MOVE_X
    {
        Pos = MAP_APPFONT ( 12 , 39 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~X axis" ;
    };
    MetricField MTR_FLD_MOVE_X
    {
        HelpID = "sd:MetricField:DLG_COPY:MTR_FLD_MOVE_X";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 37 ) ;
        Size = MAP_APPFONT ( 45 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = -120000 ;
        Maximum = 120000 ;
        StrictFormat = TRUE ;
        DecimalDigits = 2 ;
        Unit = FUNIT_MM ;
        First = -120000 ;
        Last = 120000 ;
        SpinSize = 100 ;
    };
    MetricField MTR_FLD_MOVE_Y
    {
        HelpID = "sd:MetricField:DLG_COPY:MTR_FLD_MOVE_Y";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 53 ) ;
        Size = MAP_APPFONT ( 45 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = -120000 ;
        Maximum = 120000 ;
        StrictFormat = TRUE ;
        DecimalDigits = 2 ;
        Unit = FUNIT_MM ;
        First = -120000 ;
        Last = 120000 ;
        SpinSize = 100 ;
    };
    MetricField MTR_FLD_WIDTH
    {
        HelpID = "sd:MetricField:DLG_COPY:MTR_FLD_WIDTH";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 98 ) ;
        Size = MAP_APPFONT ( 45 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = -120000 ;
        Maximum = 120000 ;
        StrictFormat = TRUE ;
        DecimalDigits = 2 ;
        Unit = FUNIT_MM ;
        First = -120000 ;
        Last = 120000 ;
        SpinSize = 100 ;
    };
    MetricField MTR_FLD_HEIGHT
    {
        HelpID = "sd:MetricField:DLG_COPY:MTR_FLD_HEIGHT";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 114 ) ;
        Size = MAP_APPFONT ( 45 , 12 ) ;
        TabStop = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Minimum = -120000 ;
        Maximum = 120000 ;
        StrictFormat = TRUE ;
        DecimalDigits = 2 ;
        Unit = FUNIT_MM ;
        First = -120000 ;
        Last = 120000 ;
        SpinSize = 100 ;
    };
    FixedText FT_MOVE_Y
    {
        Pos = MAP_APPFONT ( 12 , 55 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~Y axis" ;
    };
    FixedText FT_WIDTH
    {
        Pos = MAP_APPFONT ( 12 , 100 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~Width" ;
    };
    FixedText FT_HEIGHT
    {
        Pos = MAP_APPFONT ( 12 , 116 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~Height" ;
    };
    FixedLine GRP_MOVEMENT
    {
        Pos = MAP_APPFONT ( 6 , 26 ) ;
        Size = MAP_APPFONT ( 136 , 8 ) ;
        Text [ en-US ] = "Placement" ;
    };
    FixedLine GRP_ENLARGEMENT
    {
        Pos = MAP_APPFONT ( 6 , 87 ) ;
        Size = MAP_APPFONT ( 136 , 8 ) ;
        Text [ en-US ] = "Enlargement" ;
    };
    FixedLine GRP_COLOR
    {
        Pos = MAP_APPFONT ( 6 , 132 ) ;
        Size = MAP_APPFONT ( 136 , 8 ) ;
        Text [ en-US ] = "Colors" ;
    };
    ListBox LB_START_COLOR
    {
        HelpID = "sd:ListBox:DLG_COPY:LB_START_COLOR";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 143 ) ;
        Size = MAP_APPFONT ( 60 , 90 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
    };
    FixedText FT_START_COLOR
    {
        Pos = MAP_APPFONT ( 12 , 145 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~Start" ;
    };
    ListBox LB_END_COLOR
    {
        HelpID = "sd:ListBox:DLG_COPY:LB_END_COLOR";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 76 , 159 ) ;
        Size = MAP_APPFONT ( 60 , 90 ) ;
        TabStop = TRUE ;
        DropDown = TRUE ;
        DDExtraWidth = TRUE ;
    };
    FixedText FT_END_COLOR
    {
        Pos = MAP_APPFONT ( 12 , 161 ) ;
        Size = MAP_APPFONT ( 60 , 8 ) ;
        Text [ en-US ] = "~End" ;
    };
};

















































