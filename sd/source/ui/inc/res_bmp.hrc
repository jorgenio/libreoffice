/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#include "sfx2/sfx.hrc"
#include "cfgids.hxx"

// -----------
// - Defines -
// -----------

#define IMAGE_MASK_STDCOLOR Color{ Red = 0xff00; Green = 0x0000; Blue = 0xff00; }

// Bitmaps fuer Tree-ListBox im Effekte-TabDialog
#define BMP_PAGE                RID_APP_START+42
#define BMP_PAGEOBJS            RID_APP_START+43
#define BMP_OBJECTS             RID_APP_START+44
#define BMP_DOC_OPEN            RID_APP_START+45
#define BMP_DOC_CLOSED          RID_APP_START+46
#define BMP_DOC_TEXT            RID_APP_START+47
#define BMP_OLE                 RID_APP_START+48
#define BMP_PAGE_EXCLUDED       RID_APP_START+49
#define BMP_PAGEOBJS_EXCLUDED   RID_APP_START+50
#define BMP_EXPAND              RID_APP_START+51
#define BMP_COLLAPSE            RID_APP_START+52
#define BMP_GRAPHIC             RID_APP_START+53

// Bimaps fuer ValueSet im EffekteWindow
#define BMP_EFFECT_NONE                     RID_APP_START+105
#define BMP_TEXTEFFECT_DISCARD_FROM_T       RID_APP_START+106
#define BMP_TEXTEFFECT_FADE_FROM_CENTER     RID_APP_START+107
#define BMP_EFFECT_HIDE                     RID_APP_START+108
#define BMP_EFFECT_APPEAR                   RID_APP_START+109

// -----------------------------------------------------------------------------

#define BMP_FOIL_NONE                       RID_APP_START+120
#define BMP_GROUP                           RID_APP_START+121
#define BMP_WAIT_ICON                       RID_APP_START+124

// -----------------------------------------------------------------------------
// additional effects

#define BMP_FADE_EFFECT_INDICATOR           RID_SD_START+331

#define BMP_COMMENTS_INDICATOR              RID_SD_START+338

#define BMP_LAYOUT_EMPTY                    RID_SD_START+340
#define BMP_LAYOUT_HEAD01                   RID_SD_START+342
#define BMP_LAYOUT_HEAD02                   RID_SD_START+344
#define BMP_LAYOUT_HEAD02A                  RID_SD_START+346
#define BMP_LAYOUT_HEAD02B                  RID_SD_START+348
#define BMP_LAYOUT_HEAD03                   RID_SD_START+350
#define BMP_LAYOUT_HEAD03A                  RID_SD_START+352
#define BMP_LAYOUT_HEAD03B                  RID_SD_START+354
#define BMP_LAYOUT_HEAD03C                  RID_SD_START+356
#define BMP_LAYOUT_HEAD04                   RID_SD_START+358
#define BMP_LAYOUT_HEAD06                   RID_SD_START+360
#define BMP_LAYOUT_TEXTONLY                 RID_SD_START+362
#define BMP_LAYOUT_VERTICAL01               RID_SD_START+364
#define BMP_LAYOUT_VERTICAL02               RID_SD_START+366

#define BMP_FOILH_01                        RID_SD_START+388
#define BMP_FOILH_02                        RID_SD_START+390
#define BMP_FOILH_03                        RID_SD_START+392
#define BMP_FOILH_04                        RID_SD_START+394
#define BMP_FOILH_06                        RID_SD_START+396
#define BMP_FOILH_09                        RID_SD_START+398

#define BMP_FOILN_01                        RID_SD_START+400

#define BMP_PLACEHOLDER_SMALL_START         (RID_SD_START+402)      // these ids must stay in order!
#define BMP_PLACEHOLDER_TABLE_SMALL         (RID_SD_START+402)
#define BMP_PLACEHOLDER_CHART_SMALL         (RID_SD_START+403)
#define BMP_PLACEHOLDER_IMAGE_SMALL         (RID_SD_START+404)
#define BMP_PLACEHOLDER_MOVIE_SMALL         (RID_SD_START+405)
#define BMP_PLACEHOLDER_TABLE_SMALL_HOVER   (RID_SD_START+406)
#define BMP_PLACEHOLDER_CHART_SMALL_HOVER   (RID_SD_START+407)
#define BMP_PLACEHOLDER_IMAGE_SMALL_HOVER   (RID_SD_START+408)
#define BMP_PLACEHOLDER_MOVIE_SMALL_HOVER   (RID_SD_START+409)
#define BMP_PLACEHOLDER_SMALL_END           (RID_SD_START+410)
#define BMP_PLACEHOLDER_LARGE_START         (RID_SD_START+410)
#define BMP_PLACEHOLDER_TABLE_LARGE         (RID_SD_START+410)
#define BMP_PLACEHOLDER_CHART_LARGE         (RID_SD_START+411)
#define BMP_PLACEHOLDER_IMAGE_LARGE         (RID_SD_START+412)
#define BMP_PLACEHOLDER_MOVIE_LARGE         (RID_SD_START+413)
#define BMP_PLACEHOLDER_TABLE_LARGE_HOVER   (RID_SD_START+414)
#define BMP_PLACEHOLDER_CHART_LARGE_HOVER   (RID_SD_START+415)
#define BMP_PLACEHOLDER_IMAGE_LARGE_HOVER   (RID_SD_START+416)
#define BMP_PLACEHOLDER_MOVIE_LARGE_HOVER   (RID_SD_START+417)
#define BMP_PLACEHOLDER_LARGE_END           (RID_SD_START+418)      // until here!

// -----------------------------------------------------------------------------

/*
 *  ResourceIDs fuer Popup-Menues brauchen keinen Dummy-String !!!
 */

#define RID_DRAW_TEXTOBJ_POPUP              RID_APP_START+43
#define RID_DRAW_GEOMOBJ_POPUP              RID_APP_START+44
#define RID_DRAW_NOSEL_POPUP                RID_APP_START+45
#define RID_SLIDE_SORTER_IMPRESS_SEL_POPUP  RID_APP_START+46
#define RID_BEZIER_POPUP                    RID_APP_START+47
#define RID_DRAW_LINEOBJ_POPUP              RID_APP_START+48
#define RID_DRAW_MULTISELECTION_POPUP       RID_APP_START+49
#define RID_DRAW_PAGETAB_POPUP              RID_APP_START+50
#define RID_LAYERTAB_POPUP                  RID_APP_START+51
#define RID_DRAW_GRAPHIC_POPUP              RID_APP_START+52
#define RID_DRAW_OLE2_POPUP                 RID_APP_START+53
#define RID_DRAW_TEXTOBJ_INSIDE_POPUP       RID_APP_START+54
#define RID_SLIDE_SORTER_IMPRESS_NOSEL_POPUP RID_APP_START+55
#define RID_OUTLINE_POPUP                   RID_APP_START+56
#define RID_MASTERPAGE_POPUP                RID_APP_START+57
#define RID_DRAW_GROUPOBJ_POPUP             RID_APP_START+58
#define RID_DRAW_3DOBJ_POPUP                RID_APP_START+59
#define RID_DRAW_MEASUREOBJ_POPUP           RID_APP_START+60
#define RID_DRAW_EDGEOBJ_POPUP              RID_APP_START+61
#define RID_DRAW_BMP_CONVERT_POPUP          RID_APP_START+62
#define RID_DRAW_POLYLINEOBJ_POPUP          RID_APP_START+63
#define RID_DRAW_3DSCENE_POPUP              RID_APP_START+64
#define RID_DRAW_3DSCENE2_POPUP             RID_APP_START+65
#define RID_DRAW_CUSTOMSHAPE_POPUP          RID_APP_START+66
#define RID_TASKPANE_MASTERPAGESSELECTOR_POPUP  RID_APP_START+67
#define RID_TASKPANE_LAYOUTMENU_POPUP       RID_APP_START+68
#define RID_DRAW_MEDIA_POPUP                RID_APP_START+69

// Draw (Graphic)
#define RID_GRAPHIC_TEXTOBJ_POPUP           RID_APP_START+70
#define RID_GRAPHIC_GEOMOBJ_POPUP           RID_APP_START+71
#define RID_GRAPHIC_NOSEL_POPUP             RID_APP_START+72
#define RID_GRAPHIC_LINEOBJ_POPUP           RID_APP_START+73
#define RID_GRAPHIC_MULTISELECTION_POPUP    RID_APP_START+74
#define RID_GRAPHIC_PAGETAB_POPUP           RID_APP_START+75
#define RID_GRAPHIC_GRAPHIC_POPUP           RID_APP_START+76
#define RID_GRAPHIC_OLE2_POPUP              RID_APP_START+77
#define RID_GRAPHIC_GROUPOBJ_POPUP          RID_APP_START+78
#define RID_GRAPHIC_3DOBJ_POPUP             RID_APP_START+79
#define RID_GRAPHIC_MEASUREOBJ_POPUP        RID_APP_START+80
#define RID_GRAPHIC_EDGEOBJ_POPUP           RID_APP_START+81
#define RID_GRAPHIC_POLYLINEOBJ_POPUP       RID_APP_START+82
#define RID_GRAPHIC_3DSCENE_POPUP           RID_APP_START+83
#define RID_GRAPHIC_3DSCENE2_POPUP          RID_APP_START+84
#define RID_GRAPHIC_CUSTOMSHAPE_POPUP       RID_APP_START+85
#define RID_GRAPHIC_MEDIA_POPUP             RID_APP_START+86

#define RID_SLIDE_SORTER_DRAW_SEL_POPUP     RID_APP_START+87
#define RID_SLIDE_SORTER_DRAW_NOSEL_POPUP   RID_APP_START+88
#define RID_SLIDE_SORTER_MASTER_SEL_POPUP   RID_APP_START+89
#define RID_SLIDE_SORTER_MASTER_NOSEL_POPUP RID_APP_START+90

#define RID_DRAW_TABLE_POPUP                RID_APP_START+91
#define RID_GRAPHIC_TABLE_POPUP             RID_APP_START+92

#define RID_TASKPANE_CURRENT_MASTERPAGESSELECTOR_POPUP  RID_APP_START+93

#define RID_DRAW_TABLEOBJ_INSIDE_POPUP      RID_APP_START+94

#define RID_DRAW_OUTLINETEXTOBJ_POPUP       RID_APP_START+95
#define RID_GRAPHIC_OUTLINETEXTOBJ_POPUP    RID_APP_START+96


/*
 *  ResourceIDs fuer Toolboxen nicht aendern, ohne die Strings in
 *  strings.hrc auf die selben IDs zu setzen (eine Toolbox und sein
 *  Konfigurationsstring muessen dieselbe ResourceID haben!).
 */

#define RID_DRAW_SNAPOBJECT_POPUP           RID_APP_START+390
#define RID_DRAW_GLUEPOINT_POPUP            RID_APP_START+392
#define RID_DRAW_CONTROL_POPUP              RID_APP_START+393
#define RID_FORM_CONTROL_POPUP              RID_APP_START+394

#define RID_GRAPHICSTYLEFAMILY      RID_APP_START+395
#define RID_PRESENTATIONSTYLEFAMILY RID_APP_START+396
