/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SD_CFGID_HXX
#define _SD_CFGID_HXX

#include <sfx2/sfx.hrc>

// Item-Ids fuer Config-Items
#define SDCFG_IMPRESS           (SFX_ITEMTYPE_SD_BEGIN + 1)
#define SDCFG_DRAW              (SFX_ITEMTYPE_SD_BEGIN + 3)

// Acceleratoren, Menus, ...
#define RID_DRAW_STATUSBAR          (SFX_ITEMTYPE_SD_BEGIN + 7)

// Toolbars
#define RID_DRAW_TOOLBOX            (SFX_ITEMTYPE_SD_BEGIN + 11)
#define RID_SLIDE_TOOLBOX           (SFX_ITEMTYPE_SD_BEGIN + 12)
#define RID_DRAW_OBJ_TOOLBOX        (SFX_ITEMTYPE_SD_BEGIN + 13)
#define RID_SLIDE_OBJ_TOOLBOX       (SFX_ITEMTYPE_SD_BEGIN + 14)
#define RID_BEZIER_TOOLBOX          (SFX_ITEMTYPE_SD_BEGIN + 15)
#define RID_DRAW_TEXT_TOOLBOX       (SFX_ITEMTYPE_SD_BEGIN + 16)
#define RID_OUTLINE_TOOLBOX         (SFX_ITEMTYPE_SD_BEGIN + 17)
#define RID_DRAW_TABLE_TOOLBOX      (SFX_ITEMTYPE_SD_BEGIN + 18)
#define RID_GLUEPOINTS_TOOLBOX      (SFX_ITEMTYPE_SD_BEGIN + 19)
#define RID_DRAW_OPTIONS_TOOLBOX    (SFX_ITEMTYPE_SD_BEGIN + 20)
#define RID_DRAW_COMMONTASK_TOOLBOX (SFX_ITEMTYPE_SD_BEGIN + 21)
#define RID_FORMLAYER_TOOLBOX       (SFX_ITEMTYPE_SD_BEGIN + 22)
#define RID_DRAW_VIEWER_TOOLBOX     (SFX_ITEMTYPE_SD_BEGIN + 23)
#define RID_GRAPHIC_VIEWER_TOOLBOX  (SFX_ITEMTYPE_SD_BEGIN + 24)

#define RID_GRAPHIC_TOOLBOX         (SFX_ITEMTYPE_SD_BEGIN + 25)
#define RID_GRAPHIC_OPTIONS_TOOLBOX (SFX_ITEMTYPE_SD_BEGIN + 26)
#define RID_GRAPHIC_OBJ_TOOLBOX     (SFX_ITEMTYPE_SD_BEGIN + 27)
#define RID_GRAPHIC_TEXT_TOOLBOX    (SFX_ITEMTYPE_SD_BEGIN + 28)

#define RID_DRAW_GRAF_TOOLBOX       (SFX_ITEMTYPE_SD_BEGIN + 30)
#define RID_DRAW_MEDIA_TOOLBOX      (SFX_ITEMTYPE_SD_BEGIN + 31)


#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
