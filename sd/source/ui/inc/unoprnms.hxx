/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SD_UNOPRNMS_HXX
#define _SD_UNOPRNMS_HXX

#define UNO_NAME_MODEL_LANGUAGE         "CharLocale"
#define UNO_NAME_MODEL_TABSTOP          "TabStop"

#define UNO_NAME_PAGE_BACKGROUND        "Background"
#define UNO_NAME_PAGE_LEFT              "BorderLeft"
#define UNO_NAME_PAGE_RIGHT             "BorderRight"
#define UNO_NAME_PAGE_TOP               "BorderTop"
#define UNO_NAME_PAGE_BOTTOM            "BorderBottom"
#define UNO_NAME_PAGE_CHANGE            "Change"
#define UNO_NAME_PAGE_DURATION          "Duration"
#define UNO_NAME_PAGE_EFFECT            "Effect"
#define UNO_NAME_PAGE_HEIGHT            "Height"
#define UNO_NAME_PAGE_LAYOUT            "Layout"
#define UNO_NAME_PAGE_NUMBER            "Number"
#define UNO_NAME_PAGE_OBJECTS           "Objects"
#define UNO_NAME_PAGE_ORIENTATION       "Orientation"
#define UNO_NAME_PAGE_SPEED             "Speed"
#define UNO_NAME_PAGE_WIDTH             "Width"
#define UNO_NAME_PAGE_PREVIEW           "Preview"
#define UNO_NAME_PAGE_PREVIEWBITMAP     "PreviewBitmap"
#define UNO_NAME_PAGE_VISIBLE           "Visible"

#define UNO_NAME_OBJ_BOOKMARK           "Bookmark"
#define UNO_NAME_OBJ_DIMCOLOR           "DimColor"
#define UNO_NAME_OBJ_DIMHIDE            "DimHide"
#define UNO_NAME_OBJ_DIMPREV            "DimPrevious"
#define UNO_NAME_OBJ_EFFECT             "Effect"
#define UNO_NAME_OBJ_ISEMPTYPRESOBJ     "IsEmptyPresentationObject"
#define UNO_NAME_OBJ_ISPRESOBJ          "IsPresentationObject"
#define UNO_NAME_OBJ_CLICKACTION        "OnClick"
#define UNO_NAME_OBJ_PLAYFULL           "PlayFull"
#define UNO_NAME_OBJ_PRESORDER          "PresentationOrder"
#define UNO_NAME_OBJ_SOUNDFILE          "Sound"
#define UNO_NAME_OBJ_SOUNDON            "SoundOn"
#define UNO_NAME_OBJ_SPEED              "Speed"
#define UNO_NAME_OBJ_TEXTEFFECT         "TextEffect"
#define UNO_NAME_OBJ_BLUESCREEN         "TransparentColor"
#define UNO_NAME_OBJ_VERB               "Verb"
#define UNO_NAME_OBJ_STYLE              "Style"
#define UNO_NAME_OBJ_MASTERDEPENDENT    "IsPlaceholderDependent"
#define UNO_NAME_OBJ_ANIMATIONPATH      "AnimationPath"
#define UNO_NAME_OBJ_LEGACYFRAGMENT     "LegacyFragment"

#define UNO_NAME_LAYER_LOCKED           "IsLocked"
#define UNO_NAME_LAYER_PRINTABLE        "IsPrintable"
#define UNO_NAME_LAYER_VISIBLE          "IsVisible"
#define UNO_NAME_LAYER_NAME             "Name"

#define UNO_NAME_SHOW_ALLOWANIM         "AllowAnimations"
#define UNO_NAME_SHOW_CUSTOMSHOW        "CustomShow"
#define UNO_NAME_SHOW_FIRSTPAGE         "FirstPage"
#define UNO_NAME_SHOW_ONTOP             "IsAlwaysOnTop"
#define UNO_NAME_SHOW_AUTOMATIC         "IsAutomatic"
#define UNO_NAME_SHOW_ENDLESS           "IsEndless"
#define UNO_NAME_SHOW_FULLSCREEN        "IsFullScreen"
#define UNO_NAME_SHOW_MOUSEVISIBLE      "IsMouseVisible"
#define UNO_NAME_SHOW_PAGERANGE         "PageRange"
#define UNO_NAME_SHOW_PAUSE             "Pause"
#define UNO_NAME_SHOW_STARTWITHNAV      "StartWithNavigator"
#define UNO_NAME_SHOW_USEPEN            "UsePen"

#define UNO_NAME_SEARCH_BACKWARDS   "SearchBackwards"
#define UNO_NAME_SEARCH_CASE        "SearchCaseSensitive"
#define UNO_NAME_SEARCH_WORDS       "SearchWords"

#define UNO_NAME_LINKDISPLAYNAME                "LinkDisplayName"
#define UNO_NAME_LINKDISPLAYBITMAP              "LinkDisplayBitmap"

#define UNO_NAME_STYLE_FAMILY   "Family"
#endif


/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
