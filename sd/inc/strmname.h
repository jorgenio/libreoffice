/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#ifndef _SD_STRMNAME_H
#define _SD_STRMNAME_H

// Alter Name des Dokument-Streams
static const rtl::OUString pStarDrawDoc("StarDrawDocument");

// Name des Dokument-Streams
static const rtl::OUString pStarDrawDoc3( "StarDrawDocument3" );

// Sonstige
static const rtl::OUString pSfxStyleSheets( "SfxStyleSheets" );
static const rtl::OUString pVCItemPoolName( "VCPool" );
static const rtl::OUString pPreviewName( "StarDrawTemplatePreview" );

// PowerPoint-Filter
static const rtl::OUString pFilterPowerPoint97( "MS PowerPoint 97" );
static const rtl::OUString pFilterPowerPoint97Template( "MS PowerPoint 97 Vorlage" );
static const rtl::OUString pFilterPowerPoint97AutoPlay( "MS PowerPoint 97 AutoPlay" );

// XML content stream
static const rtl::OUString pStarDrawXMLContent( "content.xml" );
static const rtl::OUString pStarDrawOldXMLContent( "Content.xml" );

#endif  // _SD_STRMNAME_H

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
