Put
Microsoft_VC90_CRT_x86.msm
and
policy_9_0_Microsoft_VC90_CRT_x86.msm
into this directory for Windows builds using a VS 2008 / VC 9.0 compiler.
For builds with --enable-dbgutil also put
Microsoft_VC90_DebugCRT_x86.msm
and
policy_9_0_Microsoft_VC90_DebugCRT_x86.msm
here.
