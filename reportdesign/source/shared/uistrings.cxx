/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "uistrings.hrc"

namespace rptui
{

#include "stringconstants.cxx"
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_REPORTNAME            , "ReportName");
    IMPLEMENT_CONSTASCII_USTRING(CFG_REPORTDESIGNER             , "SunReportBuilder");
    IMPLEMENT_CONSTASCII_USTRING(DBREPORTHEADER                 , "ReportHeader");
    IMPLEMENT_CONSTASCII_USTRING(DBREPORTFOOTER                 , "ReportFooter");
    IMPLEMENT_CONSTASCII_USTRING(DBPAGEHEADER                   , "PageHeader");
    IMPLEMENT_CONSTASCII_USTRING(DBPAGEFOOTER                   , "PageFooter");
    IMPLEMENT_CONSTASCII_USTRING(DBGROUPHEADER                  , "GroupHeader");
    IMPLEMENT_CONSTASCII_USTRING(DBGROUPFOOTER                  , "GroupFooter");
    IMPLEMENT_CONSTASCII_USTRING(DBCOLUMNHEADER                 , "ColumnHeader");
    IMPLEMENT_CONSTASCII_USTRING(DBCOLUMNFOOTER                 , "ColumnFooter");
    IMPLEMENT_CONSTASCII_USTRING(DBDETAIL                       , "Detail");
    IMPLEMENT_CONSTASCII_USTRING(REPORTCONTROLFORMAT            , "ReportControlFormat");
    IMPLEMENT_CONSTASCII_USTRING(CURRENT_WINDOW                 , "CurrentWindow");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FONTCOLOR             , "FontColor");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_EMPTY_IS_NULL         , "ConvertEmptyToNull");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILTERPROPOSAL        , "UseFilterValueProposal");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_POSITION              , "Position");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FORMATKEYDATE         , "FormatKeyDate");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FORMATKEYTIME         , "FormatKeyTime");
    IMPLEMENT_CONSTASCII_USTRING(DBOVERLAPPEDCONTROL            , "OverlappedControl");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FORMULALIST           , "FormulaList");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_SCOPE                 , "Scope");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_CHARTTYPE             , "ChartType");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_PREVIEW_COUNT         , "RowLimit");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_TITLE                 , "Title");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_AREA                  , "Area");

    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLCOLOR             , "FillColor");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLGRADIENT          , "FillGradient");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLGRADIENTNAME      , "FillGradientName");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLHATCHNAME         , "FillHatchName");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLBITMAPNAME        , "FillBitmapName");
    IMPLEMENT_CONSTASCII_USTRING(PROPERTY_FILLSTYLE             , "FillStyle");

    IMPLEMENT_CONSTASCII_USTRING(DBTEXTBOXBOUNDCONTENT          , "TextBoxBoundContent");
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
