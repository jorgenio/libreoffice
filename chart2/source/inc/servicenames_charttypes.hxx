/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _CHART2_SERVICENAMES_CHARTTYPES_HXX
#define _CHART2_SERVICENAMES_CHARTTYPES_HXX

//.............................................................................
namespace chart
{
//.............................................................................

#define CHART2_SERVICE_NAME_CHARTTYPE_AREA ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.AreaChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_BAR ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.BarChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_COLUMN ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.ColumnChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_LINE ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.LineChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_SCATTER ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.ScatterChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_PIE ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.PieChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_NET ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.NetChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_FILLED_NET ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.FilledNetChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_CANDLESTICK ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.CandleStickChartType" ))
#define CHART2_SERVICE_NAME_CHARTTYPE_BUBBLE ::rtl::OUString( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.chart2.BubbleChartType" ))

//.............................................................................
} //namespace chart
//.............................................................................
#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
