/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
#ifndef _SVX_CHARDLG_H
#define _SVX_CHARDLG_H

// define ----------------------------------------------------------------

#define CHRDLG_UNDERLINE_NONE               0
#define CHRDLG_UNDERLINE_SINGLE             1
#define CHRDLG_UNDERLINE_DOUBLE             2
#define CHRDLG_UNDERLINE_DOTTED             3
#define CHRDLG_UNDERLINE_DONTKNOW           4
#define CHRDLG_UNDERLINE_DASH               5
#define CHRDLG_UNDERLINE_LONGDASH           6
#define CHRDLG_UNDERLINE_DASHDOT            7
#define CHRDLG_UNDERLINE_DASHDOTDOT         8
#define CHRDLG_UNDERLINE_SMALLWAVE          9
#define CHRDLG_UNDERLINE_WAVE               10
#define CHRDLG_UNDERLINE_DOUBLEWAVE         11
#define CHRDLG_UNDERLINE_BOLD               12
#define CHRDLG_UNDERLINE_BOLDDOTTED         13
#define CHRDLG_UNDERLINE_BOLDDASH           14
#define CHRDLG_UNDERLINE_BOLDLONGDASH       15
#define CHRDLG_UNDERLINE_BOLDDASHDOT        16
#define CHRDLG_UNDERLINE_BOLDDASHDOTDOT     17
#define CHRDLG_UNDERLINE_BOLDWAVE           18

#define CHRDLG_STRIKEOUT_NONE               0
#define CHRDLG_STRIKEOUT_SINGLE             1
#define CHRDLG_STRIKEOUT_DOUBLE             2
#define CHRDLG_STRIKEOUT_DONTKNOW           3
#define CHRDLG_STRIKEOUT_BOLD               4
#define CHRDLG_STRIKEOUT_SLASH              5
#define CHRDLG_STRIKEOUT_X                  6

#define CHRDLG_ENCLOSE_NONE                 0
#define CHRDLG_ENCLOSE_ROUND                1
#define CHRDLG_ENCLOSE_SQUARE               2
#define CHRDLG_ENCLOSE_POINTED              3
#define CHRDLG_ENCLOSE_CURVED               4
#define CHRDLG_ENCLOSE_SPECIAL_CHAR         5

#define CHRDLG_POSITION_OVER                0
#define CHRDLG_POSITION_UNDER               1

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
