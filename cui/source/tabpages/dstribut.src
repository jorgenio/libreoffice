/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// include ---------------------------------------------------------------
#include <cuires.hrc>
#include "dstribut.hrc"
#include "helpid.hrc"
#include <svx/dialogs.hrc>

// RID_SVXPAGE_DISTRIBUTE ---------------------------------------------------
#define STD_MASKCOLOR Color { Red=0xffff; Green=0x0000; Blue=0xffff; }
TabPage RID_SVXPAGE_DISTRIBUTE
{
    HelpId = HID_PAGE_DISTRIBUTE ;
    Hide = TRUE ;
    Size = MAP_APPFONT ( 260 , 81 ) ;
    Text [ en-US ] = "Distribution" ;

    FixedLine FL_HORIZONTAL
    {
        Pos = MAP_APPFONT ( 6 , 3  ) ;
        Size = MAP_APPFONT ( 248 , 8 ) ;
        Text [ en-US ] = "Horizontal" ;
    };

    RadioButton BTN_HOR_NONE
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_HOR_NONE";
        Pos = MAP_APPFONT ( 12 , 14  ) ;
        Size = MAP_APPFONT ( 48 , 10 ) ;
        Text [ en-US ] = "~None" ;
    };

    RadioButton BTN_HOR_LEFT
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_HOR_LEFT";
        Pos = MAP_APPFONT ( 56 , 26  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Left" ;
    };

    RadioButton BTN_HOR_CENTER
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_HOR_CENTER";
        Pos = MAP_APPFONT ( 105 , 26  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Center" ;
    };

    RadioButton BTN_HOR_DISTANCE
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_HOR_DISTANCE";
        Pos = MAP_APPFONT ( 154 , 26  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Spacing" ;
    };

    RadioButton BTN_HOR_RIGHT
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_HOR_RIGHT";
        Pos = MAP_APPFONT ( 203 , 26  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Right" ;
    };

    FixedImage IMG_HOR_LOW {
        Pos = MAP_APPFONT ( 65 , 10  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dsth_low.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_HOR_CENTER {
        Pos = MAP_APPFONT ( 114 , 10  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dsth_cen.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_HOR_DISTANCE {
        Pos = MAP_APPFONT ( 163 , 10  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dsth_dst.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_HOR_HIGH {
        Pos = MAP_APPFONT ( 212 , 10  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dsth_hig.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedLine FL_VERTICAL
    {
        Pos = MAP_APPFONT ( 6 , 42  ) ;
        Size = MAP_APPFONT ( 248 , 8 ) ;
        Text [ en-US ] = "Vertical" ;
    };

    RadioButton BTN_VER_NONE
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_VER_NONE";
        Pos = MAP_APPFONT ( 12 , 53  ) ;
        Size = MAP_APPFONT ( 46 , 10 ) ;
        Text [ en-US ] = "N~one" ;
    };

    RadioButton BTN_VER_TOP
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_VER_TOP";
        Pos = MAP_APPFONT ( 56 , 65  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Top" ;
    };

    RadioButton BTN_VER_CENTER
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_VER_CENTER";
        Pos = MAP_APPFONT ( 105 , 65  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "C~enter" ;
    };

    RadioButton BTN_VER_DISTANCE
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_VER_DISTANCE";
        Pos = MAP_APPFONT ( 154 , 65  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "S~pacing" ;
    };

    RadioButton BTN_VER_BOTTOM
    {
        HelpID = "cui:RadioButton:RID_SVXPAGE_DISTRIBUTE:BTN_VER_BOTTOM";
        Pos = MAP_APPFONT ( 203 , 65  ) ;
        Size = MAP_APPFONT ( 45 , 10 ) ;
        Text [ en-US ] = "~Bottom" ;
    };

    FixedImage IMG_VER_HIGH {
        Pos = MAP_APPFONT ( 65 , 49  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dstv_hig.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_VER_CENTER {
        Pos = MAP_APPFONT ( 114 , 49  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dstv_cen.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_VER_DISTANCE {
        Pos = MAP_APPFONT ( 163 , 49  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dstv_dst.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

    FixedImage IMG_VER_LOW {
        Pos = MAP_APPFONT ( 212 , 49  );
        Size = MAP_APPFONT ( 15 , 16 );
        Hide = FALSE;
        Fixed = Image {
            ImageBitmap = Bitmap { File = "dstv_low.bmp"; };
            MaskColor = STD_MASKCOLOR;
        };
    };

};

// ********************************************************************** EOF
