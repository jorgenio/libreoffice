/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "fmsearch.hrc"
#include "helpid.hrc"

ModalDialog RID_SVXDLG_SEARCHFORM
{
    OutputSize = TRUE;
    SVLook = TRUE;
    Size = MAP_APPFONT( 300, 253 );
    Text [ en-US ] = "Record Search";
    Moveable = TRUE;
    HelpId = HID_FM_DLG_SEARCH;

    FixedLine FL_SEARCHFOR
    {
        Pos = MAP_APPFONT( 6, 8 );
        Size = MAP_APPFONT( 232, 8 );
        Text [ en-US ] = "Search for";
    };
    RadioButton RB_SEARCHFORTEXT
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_SEARCHFORM:RB_SEARCHFORTEXT";
        Pos = MAP_APPFONT( 12, 23 );
        Size = MAP_APPFONT( 50, 10 );
        Group = TRUE;
        Text [ en-US ] = "~Text";
    };
    RadioButton RB_SEARCHFORNULL
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_SEARCHFORM:RB_SEARCHFORNULL";
        Pos = MAP_APPFONT( 12, 36 );
        Size = MAP_APPFONT( 207, 10 );
        Text [ en-US ] = "Field content is ~NULL";
    };
    RadioButton RB_SEARCHFORNOTNULL
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_SEARCHFORM:RB_SEARCHFORNOTNULL";
        Pos = MAP_APPFONT( 12, 49 );
        Size = MAP_APPFONT( 207, 10 );
        Text [ en-US ] = "Field content is not NU~LL";
    };
    ComboBox CMB_SEARCHTEXT
    {
        Border = TRUE;
        Pos = MAP_APPFONT( 65, 20 );
        Size = MAP_APPFONT( 154, 80 );
        DropDown = TRUE;
        TabStop = TRUE;
        HelpId = HID_SEARCH_TEXT;
    };

    FixedLine FL_WHERE
    {
        Pos = MAP_APPFONT( 6, 65 );
        Size = MAP_APPFONT( 232, 8 );
        Text [ en-US ] = "Where to search";
    };
    FixedText FT_FORM
    {
        Pos = MAP_APPFONT( 12, 77 );
        Size = MAP_APPFONT( 87, 10 );
        Text [ en-US ] = "Form";
    };
    ListBox LB_FORM
    {
        HelpID = "cui:ListBox:RID_SVXDLG_SEARCHFORM:LB_FORM";
        Border = TRUE;
        Pos = MAP_APPFONT( 110, 76 );
        Size = MAP_APPFONT( 109, 60 );
        TabStop = TRUE;
        DropDown = TRUE;
    };
    RadioButton RB_ALLFIELDS
    {
        Pos = MAP_APPFONT( 24, 91 );
        Size = MAP_APPFONT( 75, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_ALLFIELDS;
        Text [ en-US ] = "All Fields";
    };
    RadioButton RB_SINGLEFIELD
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_SEARCHFORM:RB_SINGLEFIELD";
        Pos = MAP_APPFONT( 24, 106 );
        Size = MAP_APPFONT( 75, 10 );
        Text [ en-US ] = "Single field";
    };
    ListBox LB_FIELD
    {
        Border = TRUE;
        Pos = MAP_APPFONT( 110, 105 );
        Size = MAP_APPFONT( 109, 60 );
        TabStop = TRUE;
        DropDown = TRUE;
        HelpId = HID_SEARCH_FIELDSELECTION;
    };
    FixedLine FL_OPTIONS
    {
        Pos = MAP_APPFONT( 6, 126 );
        Size = MAP_APPFONT( 288, 8 );
        Text [ en-US ] = "Settings";
    };
    FixedText FT_POSITION
    {
        Pos = MAP_APPFONT( 12, 138 );
        Size = MAP_APPFONT( 87, 8 );
        Text [ en-US ] = "Position";
    };
    ListBox LB_POSITION
    {
        Border = TRUE;
        Pos = MAP_APPFONT( 110, 136 );
        Size = MAP_APPFONT( 109, 60 );
        TabStop = TRUE;
        DropDown = TRUE;
        HelpId = HID_SEARCH_POSITION;
    };
    CheckBox CB_USEFORMATTER
    {
        Pos = MAP_APPFONT( 12, 158 );
        Size = MAP_APPFONT( 90, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_FORMATTER;
        Text [ en-US ] = "Apply field format";
    };
    CheckBox CB_CASE
    {
        Pos = MAP_APPFONT( 12, 171 );
        Size = MAP_APPFONT( 90, 20 );
        TabStop = TRUE;
        WordBreak = TRUE;
        HelpId = HID_SEARCH_CASE;
        Text [ en-US ] = "Match case";
    };
    CheckBox CB_BACKWARD
    {
        Pos = MAP_APPFONT( 105, 158 );
        Size = MAP_APPFONT( 85, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_BACKWARD;
        Text [ en-US ] = "Search backwards";
    };
    CheckBox CB_STARTOVER
    {
        Pos = MAP_APPFONT( 105, 171 );
        Size = MAP_APPFONT( 85, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_STARTOVER;
        Text [ en-US ] = "From Beginning";
    };
    CheckBox CB_WILDCARD
    {
        Pos = MAP_APPFONT( 190, 158 );
        Size = MAP_APPFONT( 100, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_WILDCARD;
        Text [ en-US ] = "Wildcard expression";
    };
    CheckBox CB_REGULAR
    {
        Pos = MAP_APPFONT( 190, 171 );
        Size = MAP_APPFONT( 100, 10 );
        TabStop = TRUE;
        HelpId = HID_SEARCH_REGULAR;
        Text [ en-US ] = "Regular expression";
    };
    CheckBox CB_APPROX
    {
        Pos = MAP_APPFONT( 190, 184 );
        Size = MAP_APPFONT( 84, 20 );
        TabStop = TRUE;
        WordBreak = TRUE;
        HelpId = HID_SEARCH_APPROX;
        Text [ en-US ] = "Similarity Search";
    };
    PushButton PB_APPROXSETTINGS
    {
        Pos = MAP_APPFONT( 275, 182 );
        Size = MAP_APPFONT( 14, 14 );
        TabStop = TRUE ;
        HelpId = HID_SEARCH_APPROXSETTINGS;
        Text [ en-US ] = "...";
    };
    CheckBox CB_HALFFULLFORMS
    {
        HelpID = "cui:CheckBox:RID_SVXDLG_SEARCHFORM:CB_HALFFULLFORMS";
        Pos = MAP_APPFONT( 105, 209 );
        Size = MAP_APPFONT( 85, 10 );
        TabStop = TRUE ;
        Text [ en-US ] = "Match character width" ;
    };
    CheckBox CB_SOUNDSLIKECJK
    {
        HelpID = "cui:CheckBox:RID_SVXDLG_SEARCHFORM:CB_SOUNDSLIKECJK";
        Pos = MAP_APPFONT( 190, 209 );
        Size = MAP_APPFONT( 84, 10 );
        TabStop = TRUE ;
        Text [ en-US ] = "Sounds like (Japanese)";
    };
    PushButton PB_SOUNDSLIKESETTINGS
    {
        HelpID = "cui:PushButton:RID_SVXDLG_SEARCHFORM:PB_SOUNDSLIKESETTINGS";
        Pos = MAP_APPFONT( 275, 207 );
        Size = MAP_APPFONT( 14, 14 );
        TabStop = TRUE ;
        Text [ en-US ] = "..." ;
    };
    FixedLine FL_STATE
    {
        Pos = MAP_APPFONT( 6, 222 );
        Size = MAP_APPFONT( 288, 8 );
        Text [ en-US ] = "State";
    };
    FixedText FT_RECORDLABEL
    {
        Pos = MAP_APPFONT( 12, 232 );
        Size = MAP_APPFONT( 45, 10 );
        Text [ en-US ] = "Record :";
    };
    FixedText FT_RECORD
    {
        Pos = MAP_APPFONT( 60, 232 );
        Size = MAP_APPFONT( 30, 10 );
        UniqueId = UID_SEARCH_RECORDSTATUS;
    };
    FixedText FT_HINT
    {
        Pos = MAP_APPFONT( 99, 232 );
        Size = MAP_APPFONT( 190, 10 );
    };
    PushButton PB_SEARCH
    {
        Pos = MAP_APPFONT( 244, 6 );
        Size = MAP_APPFONT( 50, 14 );
        TabStop = TRUE;
        DefButton = TRUE;
        HelpId = HID_SEARCH_BTN_SEARCH;
        Text [ en-US ] = "Search";
    };
    CancelButton 1
    {
        Pos = MAP_APPFONT( 244, 26 );
        Size = MAP_APPFONT( 50, 14 );
        HelpId = HID_SEARCH_BTN_CLOSE;
        TabStop = TRUE;
        Text [ en-US ] = "~Close";
    };
    HelpButton 1
    {
        Pos = MAP_APPFONT( 244, 46 );
        Size = MAP_APPFONT( 50, 14 );
        TabStop = TRUE;
        Text [ en-US ] = "~Help";
    };
};

String RID_STR_SEARCH_ANYWHERE
{
    Text [ en-US ] = "anywhere in the field";
};

String RID_STR_SEARCH_BEGINNING
{
    Text [ en-US ] = "beginning of field";
};

String RID_STR_SEARCH_END
{
    Text [ en-US ] = "end of field";
};

String RID_STR_SEARCH_WHOLE
{
    Text [ en-US ] = "entire field";
};

String RID_STR_FROM_TOP
{
    Text [ en-US ] = "From top";
};
String RID_STR_FROM_BOTTOM
{
    Text [ en-US ] = "From bottom";
};

ErrorBox RID_SVXERR_SEARCH_NORECORD
{
    Buttons = WB_OK;
    Message [ en-US ] = "No records corresponding to your data found.";
};

ErrorBox RID_SVXERR_SEARCH_GENERAL_ERROR
{
    Buttons = WB_OK;
    Message [ en-US ] = "An unknown error occurred. The search could not be finished.";
};

String RID_STR_OVERFLOW_FORWARD
{
    Text [ en-US ] = "Overflow, search continued at the beginning";
};

String RID_STR_OVERFLOW_BACKWARD
{
    Text [ en-US ] = "Overflow, search continued at the end";
};

String RID_STR_SEARCH_COUNTING
{
    Text [ en-US ] = "counting records";
};
