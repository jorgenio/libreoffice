/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/
 // include ---------------------------------------------------------------
#include <cuires.hrc>
#include "zoom.hrc"
#include <svx/dialogs.hrc> // for RID_SVXDLG_ZOOM
#include "helpid.hrc"

 // RID_SVXDLG_ZOOM -------------------------------------------------------
ModalDialog RID_SVXDLG_ZOOM
{
    HelpId = CMD_SID_ATTR_ZOOM;
    OutputSize = TRUE ;
    SvLook = TRUE ;
    Size = MAP_APPFONT ( 242 , 112 ) ;
    Moveable = TRUE ;
    Text [ en-US ] = "Zoom & View Layout";
    FixedLine FL_ZOOM
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 112 , 8 ) ;
        Text [ en-US ] = "Zoom factor";
    };
    RadioButton BTN_OPTIMAL
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_OPTIMAL";
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "~Optimal" ;
    };
    RadioButton BTN_WHOLE_PAGE
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_WHOLE_PAGE";
        Pos = MAP_APPFONT ( 12 , 27 ) ;
        Size = MAP_APPFONT ( 105 , 10 ) ;
        Text [ en-US ] = "~Fit width and height" ;
    };
    RadioButton BTN_PAGE_WIDTH
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_PAGE_WIDTH";
        Pos = MAP_APPFONT ( 12 , 40 ) ;
        Size = MAP_APPFONT ( 105, 10 ) ;
        Text [ en-US ] = "Fit ~width" ;
    };
    RadioButton BTN_100
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_100";
        Pos = MAP_APPFONT ( 12 , 53 ) ;
        Size = MAP_APPFONT ( 105, 10 ) ;
        Text = "~100%" ;
    };
    RadioButton BTN_USER
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_USER";
        Pos = MAP_APPFONT ( 12 , 67 ) ;
        Size = MAP_APPFONT ( 72 , 10 ) ;
        Text [ en-US ] = "~Variable" ;
    };
    MetricField ED_USER
    {
        HelpID = "cui:MetricField:RID_SVXDLG_ZOOM:ED_USER";
        Pos = MAP_APPFONT ( 86 , 66 ) ;
        Size = MAP_APPFONT ( 32 , 12 ) ;
        Border = TRUE ;
        Group = TRUE ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        Unit = FUNIT_CUSTOM ;
        CustomUnitText = "%" ;
        SpinSize = 1 ;
    };
    FixedLine FL_VIEWLAYOUT
    {
        Pos = MAP_APPFONT ( 124 , 3 ) ;
        Size = MAP_APPFONT ( 112 , 8 ) ;
        Text [ en-US ] = "View layout";
    };
    RadioButton BTN_AUTOMATIC
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_AUTOMATIC";
        Pos = MAP_APPFONT ( 130 , 14 ) ;
        Size = MAP_APPFONT ( 106 , 10 ) ;
        Text [ en-US ] = "~Automatic" ;
    };
    RadioButton BTN_SINGLE
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_SINGLE";
        Pos = MAP_APPFONT ( 130, 27 ) ;
        Size = MAP_APPFONT ( 106, 10 ) ;
        Text [ en-US ] = "~Single page" ;
    };
    RadioButton BTN_COLUMNS
    {
        HelpID = "cui:RadioButton:RID_SVXDLG_ZOOM:BTN_COLUMNS";
        Pos = MAP_APPFONT ( 130, 41 ) ;
        Size = MAP_APPFONT ( 75 , 10 ) ;
        Text [ en-US ] = "~Columns" ;
    };
    MetricField ED_COLUMNS
    {
        HelpID = "cui:MetricField:RID_SVXDLG_ZOOM:ED_COLUMNS";
        Pos = MAP_APPFONT ( 209 , 40 ) ;
        Size = MAP_APPFONT ( 24 , 12 ) ;
        Border = TRUE ;
        Group = TRUE ;
        Left = TRUE ;
        Repeat = TRUE ;
        Spin = TRUE ;
        SpinSize = 1 ;
        Minimum = 1;
        Maximum = 999;
    };
    CheckBox CHK_BOOK
    {
        HelpID = "cui:CheckBox:RID_SVXDLG_ZOOM:CHK_BOOK";
        Pos = MAP_APPFONT ( 136 , 55 ) ;
        Size = MAP_APPFONT ( 85 , 10 ) ;
        Text [ en-US ] = "~Book mode" ;
    };
    FixedLine FL_BOTTOM
    {
        Pos = MAP_APPFONT ( 6 , 81 ) ;
        Size = MAP_APPFONT ( 230 , 8 ) ;
    };
    OKButton BTN_ZOOM_OK
    {
        Pos = MAP_APPFONT ( 77 , 92 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        DefButton = TRUE ;
    };
    CancelButton BTN_ZOOM_CANCEL
    {
        Pos = MAP_APPFONT ( 130 , 92 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    HelpButton BTN_ZOOM_HELP
    {
        Pos = MAP_APPFONT ( 186 , 92 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
};

// ********************************************************************** EOF
