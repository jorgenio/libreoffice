/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

// include ------------------------------------------------------------------

#include <cuires.hrc>
#include "helpid.hrc"
#include "dlgname.hrc"

// RID_SVXDLG_NAME ----------------------------------------------------------

ModalDialog RID_SVXDLG_NAME
{
    HelpId = HID_DLG_NAME ;
    OutputSize = TRUE ;
    SvLook = TRUE ;
    Size = MAP_APPFONT ( 180 , 63 ) ;
    Text [ en-US ] = "Name" ;
    Moveable = TRUE ;
    FixedText FT_DESCRIPTION
    {
        Pos = MAP_APPFONT ( 6 , 6 ) ;
        Size = MAP_APPFONT ( 112 , 8 ) ;
        WordBreak = TRUE ;
    };
    Edit EDT_STRING
    {
        HelpID = "cui:Edit:RID_SVXDLG_NAME:EDT_STRING";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 17 ) ;
        Size = MAP_APPFONT ( 112 , 12 ) ;
        TabStop = TRUE ;
    };
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 124 , 6 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 124 , 23 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 124 , 43 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
};

// #i68101#
// RID_SVXDLG_OBJECT_NAME ----------------------------------------------------------

ModalDialog RID_SVXDLG_OBJECT_NAME
{
    HelpId = HID_DLG_OBJECT_NAME;
    OutputSize = TRUE ;
    SvLook = TRUE ;
    Size = MAP_APPFONT ( 180 , 54 + 2) ;
    Text [ en-US ] = "Name" ;
    Moveable = TRUE ;

    // name
    FixedText NTD_FT_NAME
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 168 , 8 ) ;
        Text [ en-US ] = "~Name";
    };
    Edit NTD_EDT_NAME
    {
        HelpID = "cui:Edit:RID_SVXDLG_OBJECT_NAME:NTD_EDT_NAME";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 14 ) ;
        Size = MAP_APPFONT ( 168 , 12 ) ;
        TabStop = TRUE ;
    };

    // divider
    FixedLine FL_SEPARATOR_A
    {
        Pos = MAP_APPFONT ( 0 , 39 - 6 - 2 ) ;
        Size = MAP_APPFONT ( 180 , 8 ) ;
    };

    // Buttons
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 6, 39 - 3 + 3) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 174 - (50 + 50 + 6), 39 - 3 + 3) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 174 - (50), 39 - 3 + 3) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
};

// #i68101#
// RID_SVXDLG_OBJECTTITLE_DESC ----------------------------------------------------------

ModalDialog RID_SVXDLG_OBJECT_TITLE_DESC
{
    HelpId = HID_DLG_OBJECT_TITLE_DESC;
    OutputSize = TRUE ;
    SvLook = TRUE ;
    Size = MAP_APPFONT ( 180 , 78 + 27 ) ;
    Text [ en-US ] = "Description" ;
    Moveable = TRUE ;

    // title
    FixedText NTD_FT_TITLE
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 168 , 8 ) ;
        WordBreak = TRUE ;
        Text [ en-US ] = "~Title";
    };
    Edit NTD_EDT_TITLE
    {
        HelpID = "cui:Edit:RID_SVXDLG_OBJECT_TITLE_DESC:NTD_EDT_TITLE";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 14 ) ;
        Size = MAP_APPFONT ( 168 , 12 ) ;
        TabStop = TRUE ;
    };

    // description
    FixedText NTD_FT_DESC
    {
        Pos = MAP_APPFONT ( 6 , 30 ) ;
        Size = MAP_APPFONT ( 168 , 8 ) ;
        WordBreak = TRUE ;
        Text [ en-US ] = "~Description";
    };
    MultiLineEdit NTD_EDT_DESC
    {
        HelpID = "cui:MultiLineEdit:RID_SVXDLG_OBJECT_TITLE_DESC:NTD_EDT_DESC";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 41 ) ;
        Size = MAP_APPFONT ( 168 , (12 * 3) - 2 ) ;
        TabStop = TRUE ;
        IgnoreTab = TRUE;
        VScroll = TRUE;
    };

    // divider
    FixedLine FL_SEPARATOR_B
    {
        Pos = MAP_APPFONT ( 0 , 41 + 39 ) ;
        Size = MAP_APPFONT ( 180 , 8 ) ;
    };

    // Buttons
    HelpButton BTN_HELP
    {
        Pos = MAP_APPFONT ( 6, 41 + 39 + 3 + 5) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    OKButton BTN_OK
    {
        Pos = MAP_APPFONT ( 174 - (50 + 50 + 6), 41 + 39 + 3 + 5) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
        DefButton = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 174 - (50), 41 + 39 + 3 + 5) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
};

// RID_SVXDLG_MESSBOX -------------------------------------------------------

ModalDialog RID_SVXDLG_MESSBOX
{
    HelpId = HID_DLG_MESSBOX ;
    OutputSize = TRUE ;
    SvLook = TRUE ;
    Size = MAP_APPFONT ( 212 , 64 ) ;
    Text = "" ;
    Moveable = TRUE ;
    PushButton BTN_1
    {
        HelpID = "cui:PushButton:RID_SVXDLG_MESSBOX:BTN_1";
        Pos = MAP_APPFONT ( 25 , 44 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    PushButton BTN_2
    {
        HelpID = "cui:PushButton:RID_SVXDLG_MESSBOX:BTN_2";
        Pos = MAP_APPFONT ( 78 , 44 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    CancelButton BTN_CANCEL
    {
        Pos = MAP_APPFONT ( 131 , 44 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        TabStop = TRUE ;
    };
    FixedText FT_DESCRIPTION
    {
        Pos = MAP_APPFONT ( 28 , 6 ) ;
        Size = MAP_APPFONT ( 200 , 35 ) ;
        WordBreak = TRUE ;
    };
};
 // ********************************************************************** EOF
