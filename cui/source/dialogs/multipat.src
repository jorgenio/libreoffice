﻿/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "multipat.hrc"
#include "helpid.hrc"
#include <cuires.hrc>

// RID_SVXDLG_MULTIPATH --------------------------------------------------

ModalDialog RID_SVXDLG_MULTIPATH
{
    HelpId = HID_MULTIPATH ;
    OutputSize = TRUE ;
    Size = MAP_APPFONT ( 260 , 120 ) ;
    Text [ en-US ] = "Select Paths" ;
    Moveable = TRUE ;
    Closeable = TRUE ;
    FixedLine FL_MULTIPATH
    {
        Pos = MAP_APPFONT ( 6 , 3 ) ;
        Size = MAP_APPFONT ( 248 , 8 ) ;
        Text [ en-US ] = "Paths" ;
    };
    ListBox LB_MULTIPATH
    {
        HelpID = "cui:ListBox:RID_SVXDLG_MULTIPATH:LB_MULTIPATH";
        Border = TRUE ;
        Pos = MAP_APPFONT ( 12 , 14 ) ;
        Size = MAP_APPFONT ( 189 , 80 ) ;
        AutoHScroll = TRUE ;
    };
    Control LB_RADIOBUTTON
    {
        HelpId = HID_OPTIONS_MULTIPATH_LIST ;
        Hide = TRUE ;
        Border = TRUE ;
        Pos = MAP_APPFONT ( 6 , 6 ) ;
        Size = MAP_APPFONT ( 195 , 77 ) ;
    };
    FixedText FT_RADIOBUTTON
    {
        Hide = TRUE ;
        NoLabel = TRUE ;
        Pos = MAP_APPFONT ( 6 , 86 );
        Size = MAP_APPFONT ( 195 , 8 );
        Text [ en-US ] = "Mark the default path for new files.";
    };
    PushButton BTN_ADD_MULTIPATH
    {
        HelpID = "cui:PushButton:RID_SVXDLG_MULTIPATH:BTN_ADD_MULTIPATH";
        Pos = MAP_APPFONT ( 204 , 14 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Add..." ;
    };
    PushButton BTN_DEL_MULTIPATH
    {
        HelpID = "cui:PushButton:RID_SVXDLG_MULTIPATH:BTN_DEL_MULTIPATH";
        Pos = MAP_APPFONT ( 204 , 31 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Delete" ;
    };
    OKButton BTN_MULTIPATH_OK
    {
        Pos = MAP_APPFONT ( 95 , 100 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        DefButton = TRUE;
    };
    CancelButton BTN_MULTIPATH_CANCEL
    {
        Pos = MAP_APPFONT ( 148 , 100 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    HelpButton BTN_MULTIPATH_HELP
    {
        Pos = MAP_APPFONT (  204 , 100 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
    };
    String STR_HEADER_PATHS
    {
        Text [ en-US ] = "Path list" ;
    };
};
String RID_MULTIPATH_DBL_ERR
{
    Text [ en-US ] = "The path %1 already exists." ;
};
String RID_SVXSTR_FILE_TITLE
{
    Text [ en-US ] = "Select files" ;
};
String RID_SVXSTR_FILE_HEADLINE
{
    Text [ en-US ] = "Files" ;
};
String RID_SVXSTR_ARCHIVE_TITLE
{
    Text [ en-US ] = "Select Archives" ;
};
String RID_SVXSTR_ARCHIVE_HEADLINE
{
    Text [ en-US ] = "Archives" ;
};
String RID_SVXSTR_MULTIFILE_DBL_ERR
{
    Text [ en-US ] = "The file %1 already exists." ;
};
