﻿/*************************************************************************
 *
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * Copyright 2000, 2010 Oracle and/or its affiliates.
 *
 * OpenOffice.org - a multi-platform office productivity suite
 *
 * This file is part of OpenOffice.org.
 *
 * OpenOffice.org is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * OpenOffice.org is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details
 * (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with OpenOffice.org.  If not, see
 * <http://www.openoffice.org/license.html>
 * for a copy of the LGPLv3 License.
 *
 ************************************************************************/

#include "optchart.hrc"
#include <cuires.hrc>
#include "helpid.hrc"

// tab page for setting the default colors used for new charts

TabPage RID_OPTPAGE_CHART_DEFCOLORS
{
    HelpID  = HID_OPTIONS_CHART_DEFCOLORS;
    Size    = MAP_APPFONT ( 260 , 185 ) ;
    Hide    = TRUE;
    Text [ en-US ] = "Default Colors";
    FixedLine FL_CHART_COLOR_LIST
    {
        Pos = MAP_APPFONT ( 6 , 3  ) ;
        Size    = MAP_APPFONT ( 80 , 8 ) ;
        Text [ en-US ] = "Chart colors";
    };
    ListBox LB_CHART_COLOR_LIST
    {
        HelpID = "cui:ListBox:RID_OPTPAGE_CHART_DEFCOLORS:LB_CHART_COLOR_LIST";
        Border      = TRUE;
        Pos     = MAP_APPFONT ( 12 , 15  );
        Size        = MAP_APPFONT ( 68 , 152 );
        DropDown    = FALSE;
        TabStop     = TRUE ;
    };
    FixedLine FL_COLOR_BOX
    {
        Pos     = MAP_APPFONT ( 92 , 3  ) ;
        Size            = MAP_APPFONT ( 106 , 8 ) ;
        Text [ en-US ]  = "Color table" ;
    };
    Control CT_COLOR_BOX
    {
        Border      = TRUE;
        Pos     = MAP_APPFONT ( 98 , 15  );
        Size        = MAP_APPFONT ( 94 , 152 );
        TabStop     = TRUE ;
    };
    PushButton PB_ADD_CHART_COLOR
    {
        Pos = MAP_APPFONT ( 204 , 15 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Add";
    };
    PushButton PB_REMOVE_CHART_COLOR
    {
        Pos = MAP_APPFONT ( 204 , 32 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Remove";
    };
    PushButton PB_RESET_TO_DEFAULT
    {
        HelpID = "cui:PushButton:RID_OPTPAGE_CHART_DEFCOLORS:PB_RESET_TO_DEFAULT";
        Pos = MAP_APPFONT ( 204 , 165 ) ;
        Size = MAP_APPFONT ( 50 , 14 ) ;
        Text [ en-US ] = "~Default";
    };
};

String RID_SVXSTR_DIAGRAM_ROW
{
    // $(ROW) can be a number or the caption of the row in quotes

    Text [ en-US ] = "Data Series $(ROW)" ;
};
QueryBox RID_OPTQB_COLOR_CHART_DELETE
{
    Buttons = WB_YES_NO ;
    DefButton = WB_DEF_NO ;
    Message [ en-US ] = "Do you really want to delete the chart color?" ;
};
String RID_OPTSTR_COLOR_CHART_DELETE
{
   Text [ en-US ] = "Chart Color Deletion" ;
};
